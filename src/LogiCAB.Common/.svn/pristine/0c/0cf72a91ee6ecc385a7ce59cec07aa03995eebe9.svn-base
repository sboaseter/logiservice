using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class Organisation
	{
		public virtual int Sequence { get; set; }
		public virtual string Type { get; set; }
		public virtual string Code { get; set; }
		public virtual string Name { get; set; }
		public virtual string NameUpper { get; set; }
		public virtual string Phone { get; set; }
		public virtual string Fax { get; set; }
		public virtual string Email { get; set; }
		public virtual string Mailbox { get; set; }
		public virtual string EAN { get; set; }
		public virtual string VAT { get; set; }
		public virtual string VBA { get; set; }
		public virtual string BVH { get; set; }
		public virtual string Referer { get; set; }
		public virtual DateTime Start { get; set; }
		public virtual DateTime End { get; set; }
		public virtual int? Picture { get; set; }

		public virtual Buyer Buyer { get; set; }
		public virtual Grower Grower { get; set; }
		public virtual OrganisationAdmin Admin { get; set; }
		public virtual OrganisationInfo Info { get; set; }

		public virtual IList<Function> Functions { get; set; }
		public virtual IList<ShopOrganisation> Shops { get; set; }

		public Organisation()
		{
			Functions = new List<Function>();
			Shops = new List<ShopOrganisation>();
		}

		public class OrganisationMap : CAB.dbo.F_CAB_ORGANISATION<Organisation>
		{
			public OrganisationMap()
			{
				ORGA_SEQUENCE(x => x.Sequence);
				ORGA_NAME(x => x.Name);
				ORGA_NAME_UPPER(x => x.NameUpper);
				ORGA_PHONE_NO(x => x.Phone);
				ORGA_FAX_NO(x => x.Fax);
				ORGA_EMAIL(x => x.Email);
				ORGA_VAT_NUMBER(x => x.VAT);
				ORGA_MAILBOX(x => x.Mailbox);
				ORGA_TYPE(x => x.Type);
				ORGA_VBA_NUMBER(x => x.VBA);
				ORGA_BVH_NUMBER(x => x.BVH);
				ORGA_EAN_CODE(x => x.EAN);
				ORGA_START_DATE(x => x.Start);
				ORGA_END_DATE(x => x.End);
				ORGA_CODE(x => x.Code);
				ORGA_FK_PICTURE_Map(x => x.Picture);
				ORGA_REFER(x => x.Referer);

				HasOne(x => x.Buyer).PropertyRef(r => r.Organisation).Cascade.All();
				HasOne(x => x.Grower).PropertyRef(r => r.Organisation).Cascade.All();
				HasOne(x => x.Admin).PropertyRef(r => r.Organisation).Cascade.All();
				HasOne(x => x.Info).PropertyRef(r => r.Organisation).Cascade.All();

				HasMany(x => x.Shops)
					.Inverse()
					.KeyColumn(CAB.dbo.F_CAB_SHOP_ORGANISATION<int>.SPON_FK_ORGA_Name)
					.Cascade.All();

				HasManyToMany(x => x.Functions)
					.Table(CAB.dbo.F_CAB_ORGANISATION_FUNCTION<int>.TableName)
					.ParentKeyColumn(CAB.dbo.F_CAB_ORGANISATION_FUNCTION<int>.ORFU_FK_ORGA_Name)
					.ChildKeyColumn(CAB.dbo.F_CAB_ORGANISATION_FUNCTION<int>.ORFU_FK_FUNC_Name)
					.Cascade.SaveUpdate();
			}
		}

	}
}
