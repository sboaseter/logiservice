﻿using System;
using Common.Logging;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using LogiCAB.Contract.AuthenticationService;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.AuthenticationService
{
	///<summary>
	/// Handler for RequestTicket Requests.
	///</summary>
	public class RequestTicketHandler : BaseHandler<RequestTicketRequest, RequestTicketResponse>
	{
		protected IServiceSeedProvider ServiceSeedProvider { get; set; }
		protected IRepository<ServiceTicket> ServiceTickets { get; set; }
		protected IRepository<Login> Logins { get; set; }
		protected IRoleProvider RoleProvider { get; set; }
		protected IShopPropertyProvider Shop { get; set; }

		/// <summary>
		/// Constructor with dependencies.
		/// </summary>
		public RequestTicketHandler(
			IServiceSeedProvider serviceSeedProvider,
			IRepository<ServiceTicket> serviceTickets,
			IRepository<Login> logins,
			IRoleProvider roleProvider,
			IShopPropertyProvider shop,
			ILog logger
		) : base(logger)
		{
			ServiceSeedProvider = serviceSeedProvider;
			ServiceTickets = serviceTickets;
			Logins = logins;
			RoleProvider = roleProvider;
			Shop = shop;
		}

		protected override RequestTicketResponse InnerHandle(RequestTicketRequest request, RequestTicketResponse response)
		{
			try
			{
				// Haal de Seed Date op.
				var seedDate = ServiceSeedProvider.GetSeedDate(request.Seed);
				if (seedDate == null)
				{
					Log.Warn("Seed not found.");
					return response.RequestInvalid();
				}

				// Seed bestaat, dus verwijderen.
				ServiceSeedProvider.RemoveSeed(request.Seed);

				// Valideer de Seed Date t.o.v. de huidige Date.
				if ((seedDate < DateTime.Now.AddMinutes(-1)) || (seedDate > DateTime.Now.AddSeconds(1)))
				{
					Log.Warn("Seed out of date.");
					return response.RequestInvalid();
				}

				// Haal de gebruiker op.
				var login = Logins.GetByName(request.Username);
				if (login == null)
				{
					Log.Warn("Login not found.");
					return response.RequestInvalid();
				}

				// Haal de rollen van deze gebruiker op.
				var roles = RoleProvider.GetRolesForOrganisation(
					login.Person.Organisation.Sequence,
					Shop.ShopId
					);
				if (Array.IndexOf(roles, Role.LogiService) < 0)
				{
					Log.WarnFormat("User [{0}] does not have the LogiService Role.", request.Username);
					return response.RequestInvalid();
				}

				// Bouw onze eigen double-hash.
				var SH1 = Helper.SHA1HashString(login.Password);
				var SH2 = Helper.SHA1HashString(SH1 + request.Seed);

				// Valideer de client hash waarde.
				if (!SH2.Equals(request.Hash))
				{
					Log.WarnFormat("User [{0}] gave invalid Password Hash.", request.Username);
					return response.RequestInvalid();
				}

				// Verwijder alle bestaande tickets voor de User.
				ServiceTickets.DeleteByLogin(login);

				// Creëer een nieuw ticket.
				var ticket = new ServiceTicket
				{
					Login = login,
					Ticket = Guid.NewGuid().ToString("N"),
					Created = DateTime.Now,
					LastRequest = DateTime.Now,
					RequestSequence = 0
				};

				// Bewaar de Ticket.
				ServiceTickets.Save(ticket);

				// Geef hem terug aan de Client.
				response.Ticket = ticket.Ticket;
				return response.Ok();
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("RequestTicket Error", ex);
				response.Ticket = null;
				return response.RequestInvalid();
			}
		}
	}
}