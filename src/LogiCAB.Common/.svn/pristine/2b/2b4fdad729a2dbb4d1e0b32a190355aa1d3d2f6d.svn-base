using System;
using Common.Logging;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Enums;
using LogiCAB.Contract;

namespace LogiCAB.Common.WCF
{
	///<summary>
	/// Handles an Authenticated Request, validating the Authentication, and
	/// Authorizing on a defined set of Roles.
	///</summary>
	public abstract class AuthenticatedHandler<TQ, TP> : BaseHandler<TQ, TP>
		where TQ : AuthenticatedRequest
		where TP : BaseResponse
	{
		protected IAuthenticatedRequestValidator AuthenticatedRequestValidator { get; private set; }

		protected abstract Role[] AllRoles { get; }
		protected abstract Role[] AnyRoles { get; }

		protected AuthenticatedHandler(
			IAuthenticatedRequestValidator authenticatedRequestValidator,
			ILog logger
		) : base(logger)
		{
			AuthenticatedRequestValidator = authenticatedRequestValidator;
		}

		protected bool HasRole(Role role)
		{
			return AuthenticatedRequestValidator.Principal.IsInRole(role);
		}

		protected override TP PreHandle(TQ request, TP response)
		{
			// Authenticate Request.
			if (!AuthenticatedRequestValidator.IsValidRequest(request, response))
				return response;

			// Authorize Request.
			if (!ValidateRoles(response))
				return response;

			// Pass through to Base.
			return base.PreHandle(request, response);
		}

		private bool ValidateRoles(TP response)
		{
			// Valideer de rollen die we allemaal moeten hebben.
			if (AllRoles != null)
				foreach (var role in AllRoles)
					if (!HasRole(role))
					{
						response.RequestUnauthorized();
						return false;
					}

			// Valideer de rollen waarvan we er minstens ��n van moeten hebben.
			if (AnyRoles != null)
			{
				foreach (var role in AnyRoles)
					if (HasRole(role))
						return true;
			}
			else
				return true;

			// Unauthorized.
			response.RequestUnauthorized();
			return false;
		}
	}
}