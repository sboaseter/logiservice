﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using LogiCAB.Contract;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	internal static class Extensions
	{
		private static readonly ILog Log = LogManager.GetCurrentClassLogger();

		internal static bool Validate(this OrganisationKey key, List<string> information)
		{
			if (key == null)
			{
				information.Add("Organisation Key is not specified.");
				return false;
			}

			var valid = false;

			if (key.Sequence == null)
				information.Add("Sequence is not specified.");
			else
				valid = true;

			if (String.IsNullOrEmpty(key.Code))
				information.Add("Code is not specified.");
			else
				valid = true;

			if (String.IsNullOrEmpty(key.Name))
				information.Add("Name is not specified.");
			else
				valid = true;

			if (String.IsNullOrEmpty(key.EAN))
				information.Add("EAN is not specified.");
			else
				valid = true;

			return valid;
		}

		internal static bool Validate(this PersonKey key, List<string> information)
		{
			if (key == null)
			{
				information.Add("Person Key is not specified.");
				return false;
			}

			var valid = false;

			if (key.Sequence == null)
				information.Add("Sequence is not specified.");
			else
				valid = true;

			return valid;
		}

		internal static bool Validate(this LoginKey key, List<string> information)
		{
			if (key == null)
			{
				information.Add("Login Key is not specified.");
				return false;
			}

			var valid = false;

			if (key.Sequence == null)
				information.Add("Sequence is not specified.");
			else
				valid = true;

			if (String.IsNullOrEmpty(key.Name))
				information.Add("Name is not specified.");
			else
				valid = true;

			return valid;
		}

		internal static bool Validate(this OrganisationKey key, BaseResponse response)
		{
			var information = new List<string>();
			if (!key.Validate(information))
			{
				response.InvalidOrganisationKey(information.ToArray());
				return false;
			}
			return true;
		}

		internal static bool Validate(this PersonKey key, BaseResponse response)
		{
			var information = new List<string>();
			if (!key.Validate(information))
			{
				response.InvalidPersonKey(information.ToArray());
				return false;
			}
			return true;
		}

		internal static bool Validate(this LoginKey key, BaseResponse response)
		{
			var information = new List<string>();
			if (!key.Validate(information))
			{
				response.InvalidLoginKey(information.ToArray());
				return false;
			}
			return true;
		}

		internal static Organisation FindIn(this OrganisationKey key, IRepository<Organisation> organisations)
		{
			try
			{
				if (key.Sequence.HasValue)
				{
					Log.DebugFormat("FindOnSequence [{0}]", key.Sequence.Value);
					return organisations.GetById(key.Sequence.Value);
				}
				if (!String.IsNullOrEmpty(key.Code))
				{
					Log.DebugFormat("FindOnCode [{0}]", key.Code);
					return organisations.GetByCode(key.Code);
				}
				if (!String.IsNullOrEmpty(key.Name))
				{
					Log.DebugFormat("FindOnName [{0}]", key.Name);
					return organisations.GetByName(key.Name);
				}
				if (!String.IsNullOrEmpty(key.EAN))
				{
					Log.DebugFormat("FindOnEAN [{0}]", key.EAN);
					return organisations.GetByEAN(key.EAN);
				}
				Log.Warn("No Key values to find Organisation with.");
				return null;
			}
			catch (Exception ex)
			{
				Log.Error("Error Finding Organisation.", ex);
				return null;
			}
		}

		internal static Person FindIn(this PersonKey key, IRepository<Person> persons)
		{
			try
			{
				if (key.Sequence.HasValue)
				{
					Log.DebugFormat("FindOnSequence [{0}]", key.Sequence.Value);
					return persons.GetById(key.Sequence.Value);
				}
				Log.Warn("No Key values to find Person with.");
				return null;
			}
			catch (Exception ex)
			{
				Log.Error("Error Finding Person.", ex);
				return null;
			}
		}

		internal static Login FindIn(this LoginKey key, IRepository<Login> logins)
		{
			try
			{
				if (key.Sequence.HasValue)
				{
					Log.DebugFormat("Find on Sequence [{0}]", key.Sequence.Value);
					return logins.GetById(key.Sequence.Value);
				}
				if (!String.IsNullOrEmpty(key.Name))
				{
					Log.DebugFormat("Find on Name [{0}]", key.Name);
					return logins.Where(l => l.Name == key.Name).SingleOrDefault();
				}
				Log.Warn("No Key values to find Login with.");
				return null;
			}
			catch (Exception ex)
			{
				Log.Error("Error Finding Person.", ex);
				return null;
			}
		}

		internal static OrganisationContract ToContract(this Organisation input)
		{
			return new OrganisationContract
			{
				Sequence = input.Sequence,
				Type = input.Type.ToOrganisationType(),
				Code = input.Code,
				Name = input.Name,
				Phone = input.Phone,
				Fax = input.Fax,
				Email = input.Email,
				Mailbox = input.Mailbox,
				EAN = input.EAN,
				VAT = input.VAT,
				VBA = input.VBA,
				BVH = input.BVH,
				Referer = input.Referer,
				ShippingDays = (input.Info != null ? input.Info.ShippingDays : String.Empty),
				Deadline = (input.Info != null ? (input.Info.Deadline.HasValue ? input.Info.Deadline.ToString() : String.Empty) : String.Empty),
			};
		}

		internal static PersonContract ToContract(this Person input)
		{
			return new PersonContract
			{
				Sequence = input.Sequence,
				Organisation = input.Organisation.ToKey(),
				FirstName = input.FirstName,
				MiddleName = input.MiddleName,
				LastName = input.LastName,
				Phone = input.Phone,
				Email = input.Email
			};
		}

		internal static LoginContract ToContract(this Login input)
		{
			return new LoginContract
			{
				Sequence = input.Sequence,
				Person = input.Person.ToKey(),
				Name = input.Name,
				Blocked = input.Blocked == 1,
				Password = null // Deze mag niet worden gevuld!
			};
		}

		internal static OrganisationKey ToKey(this Organisation input)
		{
			return new OrganisationKey
			{
				Sequence = input.Sequence,
				Code = input.Code,
				Name = input.Name,
				EAN = input.EAN
			};
		}

		internal static PersonKey ToKey(this Person input)
		{
			return new PersonKey
			{
				Sequence = input.Sequence
			};
		}

		internal static LoginKey ToKey(this Login input)
		{
			return new LoginKey
			{
				Sequence = input.Sequence,
				Name = input.Name
			};
		}

		internal static string ToDbString(this OrganisationType type)
		{
			return (type == OrganisationType.Buyer ? "BUYR" : "GROW");
		}
		internal static OrganisationType ToOrganisationType(this string input)
		{
			return (input == "BUYR" ? OrganisationType.Buyer : OrganisationType.Grower);
		}
		internal static int ToLoginGroup(this Organisation organisation)
		{
			switch (organisation.Type)
			{
				case "BUYR":
					return 2;
				case "GROW":
					return 3;
			}
			return 1;
		}
	}
}