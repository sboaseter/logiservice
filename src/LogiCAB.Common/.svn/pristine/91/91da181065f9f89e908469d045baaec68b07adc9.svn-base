using System;
using Common.Logging;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor LoginGet Requests.
	///</summary>
	public class LoginGetHandler : ShopManageHandler<LoginGetRequest, LoginGetResponse>
	{
		protected IRepository<Login> Logins { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public LoginGetHandler(
			IRepository<Login> logins,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator,
			ILog logger
		) : base(shop, authenticatedRequestValidator, logger)
		{
			Logins = logins;
		}

		protected override LoginGetResponse InnerHandle(LoginGetRequest request, LoginGetResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Key.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Login kunnen vinden.
			var found = request.Key.FindIn(Logins);
			if (found == null)
				return response.LoginNotFound();

			// Authorizeer de Shop.
			if (!IsAuthorizedFor(found, response))
				return response;

			response.Login = found.ToContract();
			return response.Ok();
		}
	}
}