using System;
using System.Data;
using Common.Logging;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implements calls to the proc_getSystemCounter procedure.
	///</summary>
	public class SystemCounterProvider : ISystemCounterProvider
	{
		protected ILog Log { get; set; }
		protected ICabConnectionProvider ConnectionProvider { get; set; }

		///<summary>
		/// Creates an instance requiring a Logger and ConnectionProvider.
		///</summary>
		public SystemCounterProvider(ILog logger, ICabConnectionProvider connectionProvider)
		{
			Log = logger;
			ConnectionProvider = connectionProvider;
		}

		public string GetOrganisationCounter(bool update)
		{
			return BaseProcedureCall("ORGA", null, null, null, String.Empty, update);
		}

		private string BaseProcedureCall(string counterType, int? organisationSequence, int? buyerSequence, int? growerSequence, string format, bool update)
		{
			Log.DebugFormat("counterType = [{0}]", counterType);
			Log.DebugFormat("update = [{0}]", update);

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateProcedure("proc_getSystemCounter"))
			{
				cmd.AddInputParameter("@COUN_TYPE", counterType);
				cmd.AddInputParameter("@ORGA_SEQ", organisationSequence);
				cmd.AddInputParameter("@BUYR_SEQ", buyerSequence);
				cmd.AddInputParameter("@GROW_SEQ", growerSequence);
				cmd.AddInputParameter("@COUN_FORMAT", format);
				cmd.AddOutputParameter("@COUN_VALUE", DbType.AnsiString, 15);
				cmd.AddInputParameter("@UPDATE_YN", update);

				try
				{
					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Error calling proc_getSystemCounter.", ex);
					Log.DebugFormat("organisationSequence = [{0}]", organisationSequence);
					Log.DebugFormat("buyerSequence = [{0}]", buyerSequence);
					Log.DebugFormat("growerSequence = [{0}]", growerSequence);
					Log.DebugFormat("format = [{0}]", format);
				}

				var result = cmd.Parameters.Get<string>("@COUN_VALUE");
				Log.DebugFormat("result = [{0}]", result);
				return result;
			}
		}
	}
}
