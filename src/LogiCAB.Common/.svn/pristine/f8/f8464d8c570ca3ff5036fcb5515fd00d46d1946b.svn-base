﻿using System;
using Common.Logging;
using LogiCAB.Common.Domain;
using LogiCAB.Contract.LogiOfferte;

namespace LogiCAB.Common.WCF.LogiOfferte
{
	///<summary>
	/// Handler voor SupplierOrder Requests.
	///</summary>
	public class OrderHeaderStatusCheckHandler : BaseHandler<OrderHeaderStatusCheckRequest, OrderHeaderStatusCheckResponse>
	{
		protected ICommonOrderProvider CommonOrderProvider { get; set; }
		protected ISignalProvider SignalProvider { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public OrderHeaderStatusCheckHandler(
			ICommonOrderProvider commonOrderProvider,
			ISignalProvider signalProvider,
			ILog logger)
			: base(logger)
		{
			CommonOrderProvider = commonOrderProvider;
			SignalProvider = signalProvider;
		}

		protected override OrderHeaderStatusCheckResponse InnerHandle(
			OrderHeaderStatusCheckRequest request,
			OrderHeaderStatusCheckResponse response)
		{
			Log.DebugFormat("request.OrderDetail.Sequence = [{0}]", request.OrderDetail.Sequence);

			var orderHeaderSequence = CommonOrderProvider.GetOrderHeaderSequenceFromDetail(request.OrderDetail.Sequence);

			// Hebben we nog openstaande regels? Dan doen we verder niets.
			var detailsRequested = CommonOrderProvider.GetOrderDetailStatusCount(orderHeaderSequence, "REQ");
			if (detailsRequested > 0)
				return response.NoChange();

			// We gaan er van uit dat alle andere regels rejected zijn.
			var detailsConfirmed = CommonOrderProvider.GetOrderDetailStatusCount(orderHeaderSequence, "CONFIRMED");

			// Minimaal één confirmed, is een confirmed Order.
			// Géén enkele confirmed, is een rejected Order.
			var newStatus = detailsConfirmed > 0 ? "CONFIRMED" : "REJECTED";

			if (CommonOrderProvider.SetOrderHeaderStatus(orderHeaderSequence, newStatus) > 0)
				return response.Ok();
			return response.NoChange();
		}
	}
}