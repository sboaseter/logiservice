﻿using System;

namespace LogiCAB.Common
{
	///<summary>
	/// Een plek om constantes op te bergen.
	///</summary>
	public static class Constants
	{
		///<summary>
		/// De globale maximum page size.
		///</summary>
		public const int MaxPageSize = 60;

		///<summary>
		/// De globale minimum page size.
		///</summary>
		public const int MinPageSize = 5;
	}
}
