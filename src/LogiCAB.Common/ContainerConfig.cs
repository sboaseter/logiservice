﻿using System.Web;
using Autofac;
using Autofac.Builder;
using AutofacContrib.NHibernate;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Services;
using LogiCAB.Common.WCF;
using LogiCAB.Common.WCF.AuthenticationService;
using LogiCAB.Common.WCF.LogiOfferte;
using LogiCAB.Common.WCF.OrganisationService;
using LogiCAB.Common.WCF.PicturesService;
using LogiCAB.Contract;
using LogiCAB.Contract.AuthenticationService;
using LogiCAB.Contract.LogiOfferte;
using LogiCAB.Contract.OrganisationService;
using LogiCAB.Contract.PicturesService;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using NHibernate;
using NHibernate.ByteCode.Castle;
using NHibernate.Type;
using System.ServiceModel.Activation;
using System;

namespace LogiCAB.Common
{
	///<summary>
	/// Contains the general registrations of LogiCAB Components.
	///</summary>
	public static class ContainerConfig
	{
        public static Uri currentUri; 
  
		///<summary>
		/// Registers any LogiCAB.Common specific Modules.
		///</summary>
		public static void RegisterLogiCABModules(this ContainerBuilder builder)
		{
		}

		///<summary>
		/// Registers any LogiCAB.Common specific Components.
		///</summary>
		public static void RegisterLogiCABComponents(this ContainerBuilder builder)
		{
			// NHibernate.
			builder.Register<SessionFactoryProvider>().SingletonScoped();
			builder.Register(c => c.Resolve<SessionFactoryProvider>().CreateSessionFactory()).As<ISessionFactory>().SingletonScoped();
			builder.Register(c => new DisposeFlushSession(
				c.Resolve<ISessionFactory>().OpenSession()
			, log4net.LogManager.GetLogger(typeof(DisposeFlushSession))
			)).As<ISession>().ContainerScoped().OwnedByContainer();
//			builder.Register(c => c.Resolve<ISessionFactory>().OpenSession()).As<ISession>().ContainerScoped().OwnedByContainer();
			builder.RegisterGeneric(typeof(LinqRepository<>)).As(typeof(IRepository<>)).ContainerScoped();

			// Simpele Providers.
			builder.Register<RoleProvider>().As<IRoleProvider>().ContainerScoped();
			builder.Register<ServiceSeedProvider>().As<IServiceSeedProvider>().ContainerScoped();
			builder.Register<ShopProvider>().As<IShopProvider>().ContainerScoped();
			builder.Register<SystemCounterProvider>().As<ISystemCounterProvider>().ContainerScoped();
			builder.Register<CommonOrderProvider>().As<ICommonOrderProvider>().ContainerScoped();
			builder.Register<SignalProvider>().As<ISignalProvider>().ContainerScoped();
			builder.Register<SimpleLoginService>().As<ISimpleLoginService>().ContainerScoped();
			builder.Register<SupplierMailInfoProvider>().As<ISupplierMailInfoProvider>().ContainerScoped();
			builder.Register<OrderUpdateNotificationMailInfoProvider>().As<IOrderUpdateNotificationMailInfoProvider>().ContainerScoped();
			builder.Register<MainGroupMatrixProvider>().As<IMainGroupMatrixProvider>().ContainerScoped();
			builder.Register<CabPropertyService>().ContainerScoped();
			builder.Register<OrderDetailViewService>().ContainerScoped();
			builder.Register<LoginService>().ContainerScoped();
			builder.Register<OrganisationService>().ContainerScoped();
			builder.Register<PrincipalService>().ContainerScoped();
			builder.Register<CabImageKiller>().As<ICabImageKiller>().ContainerScoped();
			builder.Register<EventLogProvider>().As<IEventLogProvider>().ContainerScoped();

			builder.Register<GrowerBuyerMatrixProvider>().As<IGrowerBuyerMatrixProvider>().ContainerScoped();
			builder.Register<ShortTermCache>().As<IShortTermCache>().SingletonScoped();

			#region Shop Identity Factory en Property Provider.

			// We registreren de Factory als een Singleton. Zodra hij het eerst
			// wordt gevraagd zal hij de evidence en properties ophalen uit de
			// database. Daarna zijn die gegevens volledig statisch.
			builder.Register<ShopUrlExpressionFactory>().SingletonScoped();

			// We registreren de IShopPropertyProvider interface als een call
			// naar de Factory. Hij is ExternallyOwned omdat de Factory steeds
			// dezelfde instanties terug geeft. Hij is ContainerScoped zodat
			// elk Request hem opnieuw opvraagt. Hij implementeerd zowel een
			// IShopPropertyProvider als de generieke IPropertyProvider.
			builder.Register(
				c => c.Resolve<ShopUrlExpressionFactory>().GetShopForUri(
					currentUri
				)
			).As<IShopPropertyProvider>().As<IPropertyProvider>()
			.ContainerScoped().ExternallyOwned();
			
			#endregion Shop Identity Factory en Property Provider.

			// WCF Services.
			// Ik ben niet helemaal zeker van de Container handling in de WCF
			// Integration. Volgens mij maakt hij per request een Child
			// Container aan en zou dit dus allemaal ContainerScoped kunnen
			// zijn, maar voorlopig liever safe than sorry.

			// De AuthenticatedRequestValidator is FactoryScoped, omdat hij
			// state vasthoudt over het request dat hij valideerd...
			builder.Register<AuthenticatedRequestValidator>().As<IAuthenticatedRequestValidator>().FactoryScoped();

			// AuthenticationService
			builder.Register<AuthenticationServiceContract>().FactoryScoped();
			builder.Register<RequestSeedHandler>().As<IHandler<RequestSeedRequest, RequestSeedResponse>>().FactoryScoped();
			builder.Register<RequestTicketHandler>().As<IHandler<RequestTicketRequest, RequestTicketResponse>>().FactoryScoped();
			builder.Register<ClearTicketHandler>().As<IHandler<ClearTicketRequest, ClearTicketResponse>>().FactoryScoped();

			// OrganisationService
			builder.Register<OrganisationServiceContract>().FactoryScoped();
			builder.Register<OrganisationGetHandler>().As<IHandler<OrganisationGetRequest, OrganisationGetResponse>>().FactoryScoped();
			builder.Register<OrganisationPostHandler>().As<IHandler<OrganisationPostRequest, OrganisationPostResponse>>().FactoryScoped();
			builder.Register<FunctionHandler>().As<IHandler<FunctionRequest, FunctionResponse>>().FactoryScoped();
			builder.Register<ShopLinkHandler>().As<IHandler<ShopLinkRequest, ShopLinkResponse>>().FactoryScoped();
			builder.Register<PersonGetHandler>().As<IHandler<PersonGetRequest, PersonGetResponse>>().FactoryScoped();
			builder.Register<PersonListHandler>().As<IHandler<PersonListRequest, PersonListResponse>>().FactoryScoped();
			builder.Register<PersonPostHandler>().As<IHandler<PersonPostRequest, PersonPostResponse>>().FactoryScoped();
			builder.Register<LoginGetHandler>().As<IHandler<LoginGetRequest, LoginGetResponse>>().FactoryScoped();
			builder.Register<LoginListHandler>().As<IHandler<LoginListRequest, LoginListResponse>>().FactoryScoped();
			builder.Register<LoginPostHandler>().As<IHandler<LoginPostRequest, LoginPostResponse>>().FactoryScoped();

            // PicturesService
            builder.Register<PicturesServiceContract>().FactoryScoped();
            builder.Register<PicturesGetHandler>().As<IHandler<PicturesGetRequest, PicturesGetResponse>>().FactoryScoped();
            builder.Register<PicturesPostHandler>().As<IHandler<PicturesPostRequest, PicturesPostResponse>>().FactoryScoped();

			// LogiEngine Message Handlers.
			builder.Register<SupplierMailHandler>()
				.As<IHandler<SupplierMailRequest, SupplierMailResponse>>()
				.FactoryScoped();
			builder.Register<SupplierOrderHandler>()
				.As<IHandler<SupplierOrderRequest, SupplierOrderResponse>>()
				.FactoryScoped();
			builder.Register<OrderHeaderStatusCheckHandler>()
				.As<IHandler<OrderHeaderStatusCheckRequest, OrderHeaderStatusCheckResponse>>()
				.FactoryScoped();
			builder.Register<ChildOrderDetailStatusChangeHandler>()
				.As<IHandler<ChildOrderDetailStatusChangeRequest, ChildOrderDetailStatusChangeResponse>>()
				.FactoryScoped();
			builder.Register<OrderBuyerConfirmedMailHandler>()
				.As<IHandler<OrderBuyerConfirmedMailRequest, OrderBuyerConfirmedMailResponse>>()
				.FactoryScoped();
			builder.Register<OrderUpdateNotificationMailHandler>()
				.As<IHandler<OrderUpdateNotificationMailRequest, BaseResponse>>()
				.FactoryScoped();
		}

		///<summary>
		/// Registers the Autofac Bytecode Provider with NHibernate.
		///</summary>
		public static void SetAutofacBytecodeProvider(this IContainer applicationContainer)
		{
			NHibernate.Cfg.Environment.BytecodeProvider = new AutofacBytecodeProvider(
				applicationContainer,
				new ProxyFactoryFactory(),
				new DefaultCollectionTypeFactory()
			);
		}

	}
}
