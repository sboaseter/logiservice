using System;
using System.Linq;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using LogiCAB.Common.Services;
using LogiCAB.Common.WCF;
using LogiCAB.Contract;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implements a service to validate authenticated requests.
	///</summary>
	public class AuthenticatedRequestValidator : IAuthenticatedRequestValidator
	{
		protected IRepository<ServiceTicket> ServiceTickets { get; set; }
		protected IRepository<Login> Logins { get; set; }
		protected PrincipalService PrincipalService { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public LoginPrincipal Principal { get; set; }

		///<summary>
		/// Constructor with Dependencies.
		///</summary>
		public AuthenticatedRequestValidator(
			IRepository<ServiceTicket> serviceTickets,
			IRepository<Login> logins,
			PrincipalService principalService)
		{
			ServiceTickets = serviceTickets;
			Logins = logins;
			PrincipalService = principalService;
		}

		public bool IsValidRequest(AuthenticatedRequest request, BaseResponse response)
		{
			try
			{
				// Haal de login op voor de gegeven Username.
				var login = Logins.GetByName(request.AuthenticationUsername);
				if (login == null)
				{
					Log.Warn("No login found for username.");
					response.AuthenticationFailed();
					return false;
				}

				// Haal de ticket op voor deze login.
				var ticket = ServiceTickets.GetByLogin(login);
				if (ticket == null)
				{
					Log.Warn("No ticket found for login.");
					response.AuthenticationFailed();
					return false;
				}

				// Controleer of de ticket niet out of date is.
				if ((ticket.LastRequest < DateTime.Now.AddMinutes(-60) || (ticket.LastRequest > DateTime.Now)))
				{
					Log.Warn("Ticket out of date.");
					ServiceTickets.Delete(ticket);
					response.AuthenticationFailed();
					return false;
				}

				// Bouw de server-side hash op basis van de Request Sequence.
				var sh1 = Helper.SHA1HashString(ticket.Ticket + ticket.RequestSequence);

				// Valideer de door de client meegegeven Hash.
				if (!sh1.Equals(request.AuthenticationRequestHash))
				{
					Log.Warn("Invalid AuthenticationRequestHash received.");
					ServiceTickets.Delete(ticket);
					response.AuthenticationFailed();
					return false;
				}

				// Hoog de Request Sequence op, en bewaar de ticket.
				ticket.RequestSequence++;
				ServiceTickets.Save(ticket);


				// Haal een principal op voor de login.
				var principal = PrincipalService.GetOnLogin(login);

				// Check op Validiteit.
				if (!principal.Identity.IsAuthenticated)
				{
					Log.WarnFormat("Principal Identity is not Authenticated for username [{0}]. (ORAD_STATUS)", request.AuthenticationUsername);
					ServiceTickets.Delete(ticket);
					response.AuthenticationFailed();
					return false;
				}

				// Check op LogiService Role.
				if (!principal.IsInRole(Role.LogiService))
				{
					Log.WarnFormat("Principal does not have LogiService Role for username [{0}].", request.AuthenticationUsername);
					ServiceTickets.Delete(ticket);
					response.AuthenticationFailed();
					return false;
				}


				// Zet de Principal.
				Principal = principal;

				// Alles Ok.
				return true;
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("IsValidRequest", ex);
				response.AuthenticationFailed();
				return false;
			}
		}

		public bool IsValidRequestAll(AuthenticatedRequest request, BaseResponse response, params Role[] allOfTheseRoles)
		{
			if (!IsValidRequest(request, response))
				return false;

			if (allOfTheseRoles.All(role => Principal.IsInRole(role)))
				return true;

			response.RequestUnauthorized();
			return false;
		}

		public bool IsValidRequestAny(AuthenticatedRequest request, BaseResponse response, params Role[] anyOfTheseRoles)
		{
			if (!IsValidRequest(request, response))
				return false;

			if (anyOfTheseRoles.Any(role => Principal.IsInRole(role)))
				return true;

			response.RequestUnauthorized();
			return false;
		}
	}
}