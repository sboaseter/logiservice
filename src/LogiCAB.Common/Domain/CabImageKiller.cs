﻿using System;
using System.IO;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	public class CabImageKiller : ICabImageKiller
	{
		protected IImageProvider ImageProvider { get; set; }
		protected IRepository<QueuePictureKiller> PictureKillers { get; set; }
  
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public CabImageKiller(
			IImageProvider imageProvider,
			IRepository<QueuePictureKiller> pictureKillers)
		{
			ImageProvider = imageProvider;
			PictureKillers = pictureKillers;
		}

		public void Handle()
		{
			var killers = PictureKillers.ToList();
			foreach (var killer in killers)
			{
				try
				{
					// Cleanup any pictures stored by the Image Provider.
					var path = ImageProvider.GetPathFor("C", killer.CabCodeSequence, null, null, null);
					path = Path.GetDirectoryName(path);
					if (Directory.Exists(path))
					{
						Log.DebugFormat("Cleaning up: {0}", path);
						Directory.Delete(path, true);

						do
						{
							path = Path.GetDirectoryName(path);
							if (Directory.GetFileSystemEntries(path).Length != 0)
								break;
							Log.DebugFormat("Cleaning up: {0}", path);
							Directory.Delete(path, false);
						} while (path.StartsWith(ImageProvider.CabImagesPath) && path.Length > ImageProvider.CabImagesPath.Length);
					}

					// Cleanup any pictures stored by ShowPic.
					path = Path.Combine(ImageProvider.CabImagesPath, String.Concat("C", killer.CabCodeSequence, ".jpg"));
					if (File.Exists(path))
					{
						Log.DebugFormat("Cleaning up: {0}", path);
						File.Delete(path);
					}

					Log.DebugFormat("[{0}] Killed.", killer.CabCodeSequence);
					PictureKillers.Delete(killer);
				} catch (Exception ex)
				{
					Log.ErrorFormat("Error cleaning up Cab Code Sequence [{0}]", ex, killer.CabCodeSequence);
				}
			}
		}
	}
}