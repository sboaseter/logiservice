﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implementatie van ICommonOrderProvider.
	///</summary>
	public class CommonOrderProvider : ICommonOrderProvider 
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ICabConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<CabProperty> Properties { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public CommonOrderProvider(
			ICabConnectionProvider connectionProvider,
			IRepository<CabProperty> properties)
		{
			ConnectionProvider = connectionProvider;
			Properties = properties;
		}

		public bool GetHasOriginalOffer(int sequence)
		{
			// QQQ: Dit is niet netjes, maar helaas staan deze gegevens
			// niet in de entiteiten zelf, dus is een custom query de beste
			// keuze...
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	@hasOriginalOffer = IsNull(COUNT(PPTY_SEQUENCE), 0)
From
	F_CAB_ORDER_DET
Inner Join
	F_CAB_OFFER_ORDER_MATRIX
		On	( OFOR_FK_ORDER_DET = ORDE_SEQUENCE )
Inner Join
	F_CAB_PROPERTY
		On	( PPTY_TYPE = 'F_CAB_OFFER_DETAILS' )
		And	( PPTY_KEY = OFOR_FK_OFFER_DET )
		And	( PPTY_NAME = 'OrgDetailSeq' )
Where
	ORDE_FK_ORDER_HEA = @orderHeaderSequence
;"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", sequence);
				cmd.AddOutputParameter("@hasOriginalOffer", DbType.Int32);
				cmd.ExecuteNonQuery();
				return cmd.Parameters.Get<int>("@hasOriginalOffer") > 0;
			}
		}

		public int? GetOriginalOrderDetailSequence(int orderDetailSequence)
		{
			var property = Properties.FirstOrDefault(p =>
				p.Type == "F_CAB_ORDER_DET" &&
				p.Key == orderDetailSequence &&
				p.Name == "OriginalOrderDetail"
			);
			return property == null ? null : property.Reference;
		}

		public IList<OriginalOrderInfo> GetOriginalOrderInfo(int orderHeaderSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	OFDT_SEQUENCE
,	ORDE_SEQUENCE
,	ORHE_DESC
,	ORHE_DELIVERY_DATE
,	OFHD_FK_BUYER
,	OFHD_FK_GROWER
,	sop.PPTY_REFERENCE As PERS_SEQUENCE
,	ORHE_REMARK
,	OFHD_FK_SHOP
,	ORDE_NUM_OF_UNITS
,	dbo.RealtimeStock( OFDT_SEQUENCE ) As NUM_UNITS_AVAILABLE
,	ORHE_ORDER_TYPE
From
	F_CAB_ORDER_HEA
Inner Join
	F_CAB_ORDER_DET
		On	( ORHE_SEQUENCE = ORDE_FK_ORDER_HEA )
Inner Join
	F_CAB_OFFER_ORDER_MATRIX
		On	( OFOR_FK_ORDER_DET = ORDE_SEQUENCE )
Inner Join
	F_CAB_PROPERTY ods
		On	( ods.PPTY_TYPE = 'F_CAB_OFFER_DETAILS' )
		And	( ods.PPTY_KEY = OFOR_FK_OFFER_DET )
		And	( ods.PPTY_NAME = 'OrgDetailSeq' )
Inner Join
	F_CAB_OFFER_DETAILS
		On	( OFDT_SEQUENCE = ods.PPTY_REFERENCE )
Inner Join
	F_CAB_OFFER_HEADERS
		On	( OFDT_FK_OFFER_HEADER = OFHD_SEQUENCE )
Inner Join
	F_CAB_BUYER
		On	( OFHD_FK_BUYER = BUYR_SEQUENCE )
Inner Join
	F_CAB_PROPERTY sop
		On	( sop.PPTY_TYPE = 'F_CAB_ORGANISATION' )
		And	( sop.PPTY_KEY = BUYR_FK_ORGA )
		And	( sop.PPTY_NAME = 'SupplierOrderPerson' )
Where
	ORHE_SEQUENCE = @orderHeaderSequence
Order By
	OFHD_FK_BUYER Asc
,	OFHD_FK_GROWER Asc
,	OFHD_SEQUENCE Asc
;"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				try
				{
					using (var reader = cmd.ExecuteReader())
					{
						var result = new List<OriginalOrderInfo>();
						while (reader.Read())
						{
							result.Add(new OriginalOrderInfo
							{
								OfferDetailSequence = reader.Get<int>("OFDT_SEQUENCE"),
								OrderDetailSequence = reader.Get<int>("ORDE_SEQUENCE"),
								OrderDesc = reader.Get<string>("ORHE_DESC"),
								OrderDeliveryDate = reader.Get<DateTime>("ORHE_DELIVERY_DATE"),
								BuyerSequence = reader.Get<int>("OFHD_FK_BUYER"),
								GrowerSequence = reader.Get<int>("OFHD_FK_GROWER"),
								PersonSequence = reader.Get<int>("PERS_SEQUENCE"),
								OrderRemark = reader.Get<string>("ORHE_REMARK"),
								ShopSequence = reader.Get<int>("OFHD_FK_SHOP"),
								NumOfUnits = reader.Get<decimal>("ORDE_NUM_OF_UNITS"),
								NumUnitsAvailable = reader.Get<decimal>("NUM_UNITS_AVAILABLE"),
								OfferType = (reader.Get<string>("ORHE_ORDER_TYPE") == "S" ? OfferType.Stocklist : OfferType.Offer),
							});
						}
						return result;
					}
				}
				catch (Exception ex)
				{
					Log.Error("Error executing query", ex);
					Log.DebugFormat("orderHeaderSequence = [{0}]", orderHeaderSequence);
					return null;
				}
			}
		}

		/// <summary>
		/// Get specials, die voor buyer actief zijn.
		/// </summary>
		/// <param name="buyerSequence"></param>
		/// <returns></returns>
		public IList<Special> GetSpecials(int buyerSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"

SELECT     F_CAB_CD_CAB_CODE.CABC_SEQUENCE, 
			F_CAB_CD_CAB_CODE.CABC_CAB_DESC, 
			F_CAB_OFFER_HEADERS.OFHD_SEQUENCE, 
			F_CAB_OFFER_HEADERS.OFHD_FK_GROWER, 
			F_CAB_OFFER_HEADERS.OFHD_FK_BUYER, 
			F_CAB_OFFER_DETAILS.OFDT_SEQUENCE,
			CASE WHEN F_CAB_OFFER_DETAILS.OFDT_FK_PICTURE IS NULL THEN 'C' ELSE 'P' END AS PIC_TYPE,
			CASE WHEN F_CAB_OFFER_DETAILS.OFDT_FK_PICTURE IS NULL THEN F_CAB_CD_CAB_CODE.CABC_SEQUENCE ELSE F_CAB_OFFER_DETAILS.OFDT_FK_PICTURE END AS PIC_SEQ
FROM         F_CAB_CD_CAB_CODE 
				INNER JOIN
                      F_CAB_OFFER_DETAILS ON F_CAB_CD_CAB_CODE.CABC_SEQUENCE = F_CAB_OFFER_DETAILS.OFDT_FK_CAB_CODE 
				INNER JOIN
                      F_CAB_OFFER_HEADERS ON F_CAB_OFFER_DETAILS.OFDT_FK_OFFER_HEADER = F_CAB_OFFER_HEADERS.OFHD_SEQUENCE 
				INNER JOIN
                      F_CAB_PROPERTY ON F_CAB_OFFER_DETAILS.OFDT_SEQUENCE = F_CAB_PROPERTY.PPTY_KEY
WHERE     (F_CAB_PROPERTY.PPTY_YESNO = 1) 
AND (F_CAB_PROPERTY.PPTY_NAME = N'IsSpecial') 
AND (F_CAB_OFFER_HEADERS.OFHD_VALID_FROM <= GETDATE()) 
AND (F_CAB_OFFER_HEADERS.OFHD_VALID_TO >= GETDATE())
AND (F_CAB_OFFER_HEADERS.OFHD_DELETED_YN = 0)
AND (F_CAB_OFFER_HEADERS.OFHD_FK_BUYER = @buyerSequence)

"))
			{
				cmd.AddInputParameter("@buyerSequence", buyerSequence);
				try
				{
					using (var reader = cmd.ExecuteReader())
					{
						var result = new List<Special>();
						while (reader.Read())
						{
							result.Add(new Special
							{
								GrowerSequence = reader.Get<int>("OFHD_FK_GROWER"),
								BuyerSequence = reader.Get<int>("OFHD_FK_BUYER"),
								OrderHeaderSequence = reader.Get<int>("OFHD_SEQUENCE"),
								OrdereDetailSequence = reader.Get<int>("OFDT_SEQUENCE"),
								PictureSequence = reader.Get<int>("PIC_SEQ"),
								PictureType = reader.Get<string>("PIC_TYPE"),
								CabCodeSequence = reader.Get<int>("CABC_SEQUENCE"),
								CabDescription = reader.Get<string>("CABC_CAB_DESC"),
							});
						}
						return result;
					}
				}
				catch (Exception ex)
				{
					Log.Error("Error executing query", ex);
					Log.DebugFormat("buyerSequence = [{0}]", buyerSequence);
					return null;
				}
			}
		}

     	public int CreateOrderHeader(
            string description, 
            DateTime deliveryDate, 
            int offerDetailSequence, 
            int growerSequence, 
            int buyerSequence, 
            int personSequence, 
            string remark, 
            int shopSequence, 
            Boolean deliveryDateSpecified)
		{
            int deliveryDateSpecInt = 0;

            if (deliveryDateSpecified == true)
                deliveryDateSpecInt = 1;

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateProcedure("proc_createOrderHeaderService"))
			{
				cmd.AddInputParameter("@OFDT_SEQ", offerDetailSequence);
				cmd.AddInputParameter("@ORHE_DESC", description, 250);
				cmd.AddInputParameter("@ORHE_DATE", deliveryDate);
				cmd.AddInputParameter("@BUYR_SEQ", buyerSequence);
				cmd.AddInputParameter("@GROW_SEQ", growerSequence);
				cmd.AddInputParameter("@PERS_SEQ", personSequence);
				cmd.AddInputParameter("@REMARK", remark, 250);
				cmd.AddInputParameter("@STATUS", "NEW");
				cmd.AddInputParameter("@SHOP_SEQ", shopSequence);
                cmd.AddInputParameter("@DeliverDateSpecified", deliveryDateSpecInt);
				cmd.AddOutputParameter("@ORHE_SEQ", DbType.Int32);
				try
				{
					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<int>("@ORHE_SEQ", -1);
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}

		public int CreateOrderHeaderJun2010(int offerHeaderSequence, DateTime deliveryDate, int personSequence, int shopSequence, string remark, Boolean deliveryDateSpecified, Boolean autoConfirm)
		{
            int deliveryDateSpecifiedInt = 0;

            if (deliveryDateSpecified == true)
                deliveryDateSpecifiedInt = 1;
			
                using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateProcedure("proc_createOrderHeaderServiceJun2010"))
			{
				cmd.AddInputParameter("@OFHD_SEQ", offerHeaderSequence);
				cmd.AddInputParameter("@DeliveryDate", deliveryDate);
				cmd.AddInputParameter("@PERS_SEQ", personSequence);
				cmd.AddInputParameter("@SHOP_SEQ", shopSequence);
				cmd.AddInputParameter("@REMARK", remark, 250);
                cmd.AddInputParameter("@DeliverDateSpecified", deliveryDateSpecifiedInt);
                cmd.AddInputParameter("@AutoConfirm", autoConfirm);
				cmd.AddOutputParameter("@ORHE_SEQ", DbType.Int32);
				try
				{
					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<int>("@ORHE_SEQ", -1);
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}

        public void UpdateOrderHeader(int orderHeaderSequence, bool AutoConfirm = false, bool ConfirmDetail = true) {
            using (var conn = ConnectionProvider.GetConnection())
            using (var cmd = conn.CreateProcedure("proc_updOrderHeader")) {
                cmd.AddInputParameter("@ORHE_SEQ", orderHeaderSequence);

                if (AutoConfirm) 
                    cmd.AddInputParameter("@Auto_Confirm", 1);
                else
                    cmd.AddInputParameter("@Auto_Confirm", 0);
               
                if (ConfirmDetail)
                    cmd.AddInputParameter("@Confirm_Details", 1);
                else
                    cmd.AddInputParameter("@Confirm_Details", 0);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    throw;
                }
            }
        }

		public int CreateOrderDetailService(
			int ofdtSeq,
			int orheSeq,
			decimal numOfUnits,
            string custCode,
            string orderRefID,
			string remark,
            string deliveryRemark,
            Boolean KVKOrder,
            ref int OfdtSeqBase,
            ref int OfhdSeqBase)
		{
			try
			{
				using (var conn = ConnectionProvider.GetConnection())
				using (var cmd = conn.CreateProcedure("proc_createOrderDetailService_New"))
				{
					cmd.AddInputParameter("@OFDT_SEQ", ofdtSeq);
					cmd.AddInputParameter("@ORHE_SEQ", orheSeq);
					cmd.AddInputParameter("@NUM_OF_UNITS", numOfUnits);
                    cmd.AddInputParameter("@CUSTOMER_CODE", custCode);
                    cmd.AddInputParameter("@OrderRefID", orderRefID);
                    cmd.AddInputParameter("@DeliveryRemark", deliveryRemark);
					cmd.AddInputParameter("@REMARK", remark);
                    cmd.AddInputParameter("@KvKOrder", KVKOrder);
                    cmd.AddOutputParameter("@ORDE_SEQ", DbType.Int32);
                    cmd.AddOutputParameter("@OFDT_SEQ_BASE", DbType.Int32);
                    cmd.AddOutputParameter("@OFHD_SEQ_BASE", DbType.Int32);
                    cmd.ExecuteNonQuery();
                    OfdtSeqBase = cmd.Parameters.Get<int>("@OFDT_SEQ_BASE", -1);
                    OfhdSeqBase = cmd.Parameters.Get<int>("@OFHD_SEQ_BASE", -1);
					return cmd.Parameters.Get<int>("@ORDE_SEQ", -1);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex);
				throw;
			}
		}

		public decimal GetRealtimeStock(int ofdtSeq)
		{
			try
			{
				using (var conn = ConnectionProvider.GetConnection())
				using (var cmd = conn.CreateQuery("Select dbo.RealtimeStock( @OFDT_SEQ ) As Stock;"))
				{
					cmd.AddInputParameter("@OFDT_SEQ", ofdtSeq);

					using (var reader = cmd.ExecuteReader())
						if (reader.Read())
							return reader.Get<decimal>("Stock");
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex);
				throw;
			}
			return 0;
		}

		// Even om de vaart er in te houden geen Order model/repository, maar custom queries.

		public int GetOrderHeaderSequenceFromDetail(int orderDetailSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	@orderHeaderSequence = ORDE_FK_ORDER_HEA
From
	F_CAB_ORDER_DET
Where
	ORDE_SEQUENCE = @orderDetailSequence
;"))
			{
				cmd.AddInputParameter("@orderDetailSequence", orderDetailSequence);
				cmd.AddOutputParameter("@orderHeaderSequence", DbType.Int32);
				cmd.ExecuteNonQuery();
				return cmd.Parameters.Get<int>("@orderHeaderSequence");
			}
		}

		public int GetOrderDetailStatusCount(int orderHeaderSequence, string status)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	@count = COUNT(ORDE_SEQUENCE)
From
	F_CAB_ORDER_DET
Inner Join
	F_CAB_CD_ORD_DET_STATUS
		On	( ORDE_FK_ORDER_DET_STATUS = CDDS_SEQUENCE )
		And	( CDDS_SYS_STATUS = @status )
Where
	ORDE_FK_ORDER_HEA = @orderHeaderSequence
;"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				cmd.AddInputParameter("@status", status);
				cmd.AddOutputParameter("@count", DbType.Int32);
				cmd.ExecuteNonQuery();
				return cmd.Parameters.Get<int>("@count");
			}
		}

		public int SetOrderHeaderStatus(int orderHeaderSequence, string status)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Declare @CDOS_SEQ numeric(18,0);

Select
	@CDOS_SEQ = CDOS_SEQUENCE
From
	F_CAB_CD_ORD_HEA_STATUS
Where
	CDOS_SYS_STATUS = @status
;

Update F_CAB_ORDER_HEA Set
	ORHE_FK_ORDER_STATUS = @CDOS_SEQ
Where
	ORHE_SEQUENCE = @orderHeaderSequence
And	ORHE_FK_ORDER_STATUS <> @CDOS_SEQ
;
Select @count = @@ROWCOUNT;
"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				cmd.AddInputParameter("@status", status, 250);
				cmd.AddOutputParameter("@count", DbType.Int32);
				cmd.ExecuteNonQuery();
				return cmd.Parameters.Get<int>("@count");
			}
		}

		public void SetOrderDetailStatus(int orderDetailSequence, string newStatus)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateProcedure("proc_OrderDetailChangeStatus"))
			{
				cmd.AddInputParameter("@orderDetailSequence", orderDetailSequence);
				cmd.AddInputParameter("@newStatus", newStatus, 250);
				cmd.ExecuteNonQuery();
			}
		}
        public int GetGrowerOrganisationFromHeader(int orderHeaderSequence) {
            using (var con = ConnectionProvider.GetConnection())
            using (var cmd = con.CreateQuery(@"
select @ORGA_SEQUENCE = ORGA_SEQUENCE from f_cab_order_hea 
join f_cab_grower on orhe_fk_grower = grow_Sequence
join f_cab_organisation on grow_fk_orga = orga_sequence
where orhe_sequence = @orderHeaderSequence
;")) {
                cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
                cmd.AddOutputParameter("@ORGA_SEQUENCE", DbType.Int32);
                cmd.ExecuteNonQuery();
                return cmd.Parameters.Get<int>("@ORGA_SEQUENCE");
            }
        }
	}
}