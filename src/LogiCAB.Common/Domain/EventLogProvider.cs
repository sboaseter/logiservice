using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implementation of IEventLogProvider using the F_CAB_LOG table.
	///</summary>
	public class EventLogProvider : IEventLogProvider
	{
		private ICabConnectionProvider ConnectionProvider { get; set; }

		///<summary>
		/// Creates an EventLogProvider using the given ConnectionProvider.
		///</summary>
		public EventLogProvider(ICabConnectionProvider connectionProvider)
		{
			ConnectionProvider = connectionProvider;
		}

		public void LogiServiceOrder(int logiSequence, int orheSequence)
		{
			InsertLogRecord(logiSequence, "Service Order", orheSequence.ToString());
		}

		private void InsertLogRecord(
			int? logiSequence,
			string action,
			string value
			)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Insert Into [F_CAB_LOG] (
	CALO_FK_LOGIN
,	CALO_ACTION
,	CALO_VALUE
,	CALO_CREATED_ON
) Values (
	@LoginSequence
,	@Action
,	@Value
,	GetDate()
);"))
			{
				cmd.AddInputParameter("@LoginSequence", logiSequence);
				cmd.AddInputParameter("@Action", action);
				cmd.AddInputParameter("@Value", value);
				cmd.ExecuteNonQuery();
			}
		}

	}
}
