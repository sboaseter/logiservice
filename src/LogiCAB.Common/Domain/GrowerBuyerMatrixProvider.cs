﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	public class GrowerBuyerMatrixProvider : IGrowerBuyerMatrixProvider
	{
		protected IConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<CabProperty> Properties { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public GrowerBuyerMatrixProvider(
			IConnectionProvider connectionProvider,
			IRepository<CabProperty> properties)
		{
			ConnectionProvider = connectionProvider;
			Properties = properties;
		}

		public List<GrowBuyrMatrix> GetGrowerBuyerMatrix(int growerSeq, int buyerSeq)
		{
			using (var conn = ConnectionProvider.GetConnection())
			using (var cmd = conn.CreateQuery(@"
SELECT 
	 GAMA_SEQUENCE
	,GAMA_FK_BUYER
	,GAMA_FK_GROWER
	,GAMA_LAST_ORDER_DATE
	,CAST(GAMA_BLOCKED_YN AS INTEGER) GAMA_BLOCKED_YN
FROM F_CAB_GROWER_BUYER_MATRIX
WHERE GAMA_FK_GROWER = @GrowerSeq AND GAMA_FK_BUYER = @BuyerSeq"))
			{
				cmd.AddInputParameter("@GrowerSeq", growerSeq);
				cmd.AddInputParameter("@BuyerSeq", buyerSeq);
				using (var reader = cmd.ExecuteReader())
				{
					var result = new List<GrowBuyrMatrix>();
					while(reader.Read())
					{
						result.Add(new GrowBuyrMatrix
						{
							GBMatrixSequence = reader.Get<int>("GAMA_SEQUENCE"),
							GrowerSequence = reader.Get<int>("GAMA_FK_GROWER"),
							BuyerSequence = reader.Get<int>("GAMA_FK_BUYER"),
							LastOrderDateProp = reader.Get<DateTime>("GAMA_LAST_ORDER_DATE"),
							BlockedProp = reader.Get<int>("GAMA_BLOCKED_YN"),
						});
					}
					return result;
				}
			}
		}

		public void InsertGrowerBuyerMatrix(int growerSeq, int buyerSeq, DateTime lastOrderDate, int blocked)
		{
			using(var conn = ConnectionProvider.GetConnection())
			using (var cmd = conn.CreateQuery(@"
INSERT INTO F_CAB_GROWER_BUYER_MATRIX
           (GAMA_FK_BUYER
           ,GAMA_FK_GROWER
           ,GAMA_LAST_ORDER_DATE
           ,GAMA_BLOCKED_YN)
     VALUES
           (@BuyerSeq
		   ,@GrowerSeq	
           ,@LastOrderDate
           ,@Blocked)"))
			{
				cmd.AddInputParameter("@BuyerSeq", buyerSeq);
				cmd.AddInputParameter("@GrowerSeq", growerSeq);
				cmd.AddInputParameter("@LastOrderDate", lastOrderDate);
				cmd.AddInputParameter("@Blocked", blocked);
				cmd.ExecuteNonQuery();
			}
		}
	}
}
