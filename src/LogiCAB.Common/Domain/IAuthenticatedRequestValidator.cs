using LogiCAB.Common.Enums;
using LogiCAB.Contract;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Defines a service to validate authenticated requests.
	///</summary>
	public interface IAuthenticatedRequestValidator
	{
		///<summary>
		/// Checks if the given request has a valid Username and Hash value.
		/// If not, sets the given response to the default authentication error message.
		///</summary>
		bool IsValidRequest(AuthenticatedRequest request, BaseResponse response);

		///<summary>
		/// Checks if the given request has a valid Username and Hash value.
		/// If not, sets the given response to the default authentication error message.
		/// Also checks if All of the given Roles are present. If not the request
		/// is considered Unauthorized.
		///</summary>
		bool IsValidRequestAll(AuthenticatedRequest request, BaseResponse response, params Role[] allOfTheseRoles);

		///<summary>
		/// Checks if the given request has a valid Username and Hash value.
		/// If not, sets the given response to the default authentication error message.
		/// Also checks if Any of the given Roles are present. If not the request
		/// is considered Unauthorized.
		///</summary>
		bool IsValidRequestAny(AuthenticatedRequest request, BaseResponse response, params Role[] anyOfTheseRoles);

		///<summary>
		/// If IsValidRequest returned true, this should contain a valid LoginPrincipal.
		///</summary>
		LoginPrincipal Principal { get; }
	}
}