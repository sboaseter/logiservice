﻿namespace LogiCAB.Common.Domain
{
	public interface ICabImageKiller {
		void Handle();
	}
}