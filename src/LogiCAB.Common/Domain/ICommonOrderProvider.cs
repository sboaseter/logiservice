﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Defines common functionality for orders.
	///</summary>
	public interface ICommonOrderProvider
	{

		///<summary>
		/// Haalt op of de gegeven order een Offer Detail regel heeft met een
		/// Original Offer Detail regel.
		///</summary>
		bool GetHasOriginalOffer(int sequence);

		/// <summary>
		/// Haalt op of de gegeven order detail een originele Order Detail heeft.
		/// </summary>
		int? GetOriginalOrderDetailSequence(int orderDetailSequence);

		/// <summary>
		/// Haalt een tabel met gegevens op die gebruik kan worden om nieuwe
		/// Orders te plaatsen op basis van een bestaande Order Header Sequence.
		/// </summary>
		IList<OriginalOrderInfo> GetOriginalOrderInfo(int orderHeaderSequence);

		/// <summary>
		/// Get specials, die voor buyer actief zijn.
		/// </summary>
		/// <param name="buyerSequence"></param>
		/// <returns></returns>
		IList<Special> GetSpecials(int buyerSequence);

		///<summary>
		/// Maakt een nieuwe OrderHeader record aan.
		///</summary>
		int CreateOrderHeader(
			string description,
			DateTime deliveryDate,
			int offerDetailSequence,
			int growerSequence,
			int buyerSequence,
			int personSequence,
			string remark,
			int shopSequence,
            Boolean deliveryDateSpecified);

		/// <summary>
		/// Bevestigd de opgegeven OrderHeader.
		/// </summary>
        void UpdateOrderHeader(
            int orderHeaderSequence, 
            bool AutoConfirm = false,
            bool ConfirmDetail = true);

		/// <summary>
		/// Maakt een Order Detail record aan.
		/// </summary>
		int CreateOrderDetailService(
			int ofdtSeq,
			int orheSeq,
			decimal numOfUnits,
            string custCode,
            string orderRefID,
			string remark,
            string deliveryRemark,
            bool kvkOrder,
            ref int ofdtSeqBase,
            ref int ofhdSeqBase);

		/// <summary>
		/// Haalt de realtime stock (in units) op van de Opgegeven Offer Detail regel.
		/// </summary>
		decimal GetRealtimeStock(
			int ofdtSeq);

		///<summary>
		/// Haalt de OrderHeader Sequence op bij de opgegeven OrderDetail Sequence.
		///</summary>
		int GetOrderHeaderSequenceFromDetail(int orderDetailSequence);

		///<summary>
		/// Telt het aantal OrderDetail regels met de opgegeven status bij de opgegeven OrderHeader.
		///</summary>
		int GetOrderDetailStatusCount(int orderHeaderSequence, string status);

		///<summary>
		/// Verandert de status van de OrderHeader in de opgegeven waarde.
		///</summary>
		int SetOrderHeaderStatus(int orderHeaderSequence, string status);

		///<summary>
		/// Verandert de status van de OrderDetail in de opgegeven waarde.
		///</summary>
		void SetOrderDetailStatus(int orderDetailSequence, string newStatus);

		///<summary>
		/// Maakt een nieuwe OrderHeader record aan. Deze variant gebruikt
		/// andere parameters, voor de nieuwe Service versie.
		///</summary>
		int CreateOrderHeaderJun2010(
			int offerHeaderSequence,
			DateTime deliveryDate,
			int personSequence,
			int shopSequence,
			string remark,
            Boolean deliveryDateSpecified,
            Boolean autoConfirm);
        int GetGrowerOrganisationFromHeader(int orderHeaderSequence);
	}
}