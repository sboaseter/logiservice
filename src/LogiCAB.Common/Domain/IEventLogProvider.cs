﻿using System;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Represents a general Event Log for LogiCAB.
	///</summary>
	public interface IEventLogProvider
	{

		///<summary>
		/// Logs an Order Event from the LogiService.
		///</summary>
		void LogiServiceOrder(int logiSequence, int orheSequence);

	}
}
