﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	public interface IGrowerBuyerMatrixProvider
	{
		List<GrowBuyrMatrix> GetGrowerBuyerMatrix(int growerSeq, int buyerSeq);
		void InsertGrowerBuyerMatrix(int growerSeq, int buyerSeq, DateTime lastOrderDate, int blocked);
	}
}
