namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Defines an Image Provider.
	///</summary>
	public interface IImageProvider
	{

		/// <summary>
		/// Gets the binary data of the image stored with the given Cab Code.
		/// May return null if not found.
		/// </summary>
		byte[] GetCabImage(int cabCodeSequence);
        byte[] GetCabImage2(int cabCodeSequence);

		/// <summary>
		/// Gets the binary data of the picture stored with the given sequence.
		/// May return null if not found.
		/// </summary>
		byte[] GetPictureImage(int pictureSequence);

		/// <summary>
		/// Returns a static url to an on-disk image generated from the given parameters.
		/// </summary>
		/// <param name="type">The type of image. 'C' for Cab, 'P' for Picture.</param>
		/// <param name="sequence">The sequence of the image.</param>
		/// <param name="width">The optional width of the image.</param>
		/// <param name="height">The optional height of the image.</param>
		/// <param name="maxWidth">The optional maximum width of the image.</param>
		/// <returns>A application-relative url for the generated image.</returns>
		string GetStaticUrlFor(string type, int sequence, int? width, int? height, int? maxWidth);

		/// <summary>
		/// Returns the physical path for an on-disk image with the given parameters.
		/// The image is not guaranteed to exist.
		/// </summary>
		string GetPathFor(string type, int sequence, int? width, int? height, int? maxWidth);

		/// <summary>
		/// Returns the base path of the Cab Images directory.
		/// </summary>
		string CabImagesPath { get; }
        
        /// <summary>
        /// Replace all pictures from valid offers to a given directory.
        /// </summary>
		void RefreshAllOfferPictures(string path);
	}
}