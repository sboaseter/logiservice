﻿using System.Collections.Generic;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	public interface IMainGroupMatrixProvider
	{
		List<int> SelectMainGroups(Organisation orga);
		void InsertMainGroup(Organisation orga, int groupSequence, int order);
		void CopyMainGroups(Organisation fromOrga, Organisation toOrga);
	}
}