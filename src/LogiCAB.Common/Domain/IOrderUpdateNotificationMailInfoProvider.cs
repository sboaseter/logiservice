﻿using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Definieert de interface om OrderUpdateNotificationMail gegevens op te halen.
	///</summary>
	public interface IOrderUpdateNotificationMailInfoProvider
	{
		///<summary>
		/// Haalt de benodigde informatie op om een OrderUpdateNotification
		/// Mail te sturen van de Supplier naar de Client.
		///</summary>
		OrderUpdateNotificationMailInfo SupplierToClient(int orderHeaderSequence);
	}
}