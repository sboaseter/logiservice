using LogiCAB.Common.Enums;

namespace LogiCAB.Common.Domain
{
	/// <summary>
	/// DefiniŽert een simpele dienst om de huidige verzameling Rollen voor een Organisatie te leveren.
	/// </summary>
	public interface IRoleProvider
	{
		/// <summary>
		/// Geeft alle Rollen van de opgegeven Organisatie.
		/// </summary>
		Role[] GetRolesForOrganisation(int orgaSequence);

		/// <summary>
		/// Geeft alle Rollen van de opgegeven Organisatie binnen de opgegeven Shop.
		/// </summary>
		Role[] GetRolesForOrganisation(int orgaSequence, Shop shop);

		/// <summary>
		/// Geeft alle Rollen van de opgegeven Organisatie binnen de opgegeven Shop.
		/// </summary>
		Role[] GetRolesForOrganisation(int orgaSequence, int shopId);
	}
}