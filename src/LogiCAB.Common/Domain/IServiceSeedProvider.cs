using System;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Provides access to the Service Seed in the Database.
	///</summary>
	public interface IServiceSeedProvider
	{
		///<summary>
		/// Returns the DateTime the given Seed was created, or null if it cannot be found.
		///</summary>
		DateTime? GetSeedDate(string seed);

		///<summary>
		/// Stores the given Seed in the database.
		///</summary>
		void StoreSeed(string seed);

		/// <summary>
		/// Removes the given Seed from the database.
		/// </summary>
		void RemoveSeed(string seed);
	}
}