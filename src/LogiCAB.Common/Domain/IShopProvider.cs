using System.Collections.Generic;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	public interface IShopProvider
	{

		List<ShopEvidence> GetEvidence(string service, string type);

		Dictionary<string, string> GetProperties(Shop shop, ShopSkin? skin, string service);
		Dictionary<string, string> GetProperties(ShopEvidence evidence);

	}
}