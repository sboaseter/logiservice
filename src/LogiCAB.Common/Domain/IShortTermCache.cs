﻿using System;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Defines a generic short-term cache for storing query results.
	///</summary>
	public interface IShortTermCache {

		///<summary>
		/// Gets the stored value of the given Key from the cache. If no value
		/// is present, the given function is Invoked to provide one.
		///</summary>
		T GetWithDelegate<T>(Func<T> retrieve, string key);

		///<summary>
		/// Gets the stored value of the given Key from the cache. If no value
		/// is present, the given function is Invoked to provide one.
		///</summary>
		T GetWithDelegate<T>(Func<T> retrieve, params object[] keyParts);

		///<summary>
		/// Gets the stored value of the given Key from the cache.
		///</summary>
		T Get<T>(string key);

		///<summary>
		/// Gets the stored value of the given Key from the cache.
		///</summary>
		T Get<T>(params object[] keyParts);

		///<summary>
		/// Adds the given value to the cache under the given key.
		/// Calls to this method will fail if an item with the same key parameter is already stored in the Cache. To overwrite an existing Cache item using the same key parameter, use the Set method.
		///</summary>
		void Add<T>(T value, string key);

		///<summary>
		/// Adds the given value to the cache under the given key.
		/// Calls to this method will fail if an item with the same key parameter is already stored in the Cache. To overwrite an existing Cache item using the same key parameter, use the Set method.
		///</summary>
		void Add<T>(T value, params object[] keyParts);

		///<summary>
		/// Sets the given value in the cache under the given key.
		/// This method will overwrite an existing value.
		///</summary>
		void Set<T>(T value, string key);

		///<summary>
		/// Sets the given value in the cache under the given key.
		/// This method will overwrite an existing value.
		///</summary>
		void Set<T>(T value, params object[] keyParts);

		void Clear(string key);
		void Clear(params object[] keyParts);
	}
}