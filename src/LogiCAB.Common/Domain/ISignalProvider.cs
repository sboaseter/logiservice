﻿namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Provides the definition of sending signals.
	///</summary>
	public interface ISignalProvider
	{
		///<summary>
		/// Sends an OrderConfirmed signal using the given OrderHeader Sequence.
		///</summary>
		void SignalOrderConfirmed(int orderHeaderSequence);

		/// <summary>
		/// Sends an OrderConfirmed signal using the given OrderHeader Sequence
		/// with an indication to send the Buyer Confirmed mail.
		/// </summary>
		void SignalOrderConfirmedWithMail(int orderHeaderSequence);
	}
}