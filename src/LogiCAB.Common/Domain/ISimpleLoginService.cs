﻿using System.Collections.Generic;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Defines an interface to get a list of 'Simple' Logins and create or update them.
	///</summary>
	public interface ISimpleLoginService
	{
		///<summary>
		/// Gets a list of SimpleLogin Organisatie owned by the Eigenaar.
		///</summary>
		IEnumerable<SimpleLogin> GetPaged(
			Organisation eigenaar,
			int pageIndex, int pageSize, int isBlocked);

		///<summary>
		/// Creates a new SimpleLogin Organisatie.
		///</summary>
		Login Create(SimpleLogin input);

		///<summary>
		/// Updates an existing SimpleLogin Organisatie.
		///</summary>
		void Update(SimpleLogin input);

		/// <summary>
		/// Get Buyersequence of the SimpleLogin Organisation (KvK/SubKlant)
		/// </summary>
		int? GetSimpleBuyerSeq(ILogin login);
	}
}