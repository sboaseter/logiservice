﻿using LogiCAB.Common.Model;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Definieert de interface om SupplierMail gegevens op te halen.
	///</summary>
	public interface ISupplierMailInfoProvider
	{
		///<summary>
		/// Haalt de benodigde informatie op om een Supplier Mail te sturen.
		///</summary>
		SupplierMailInfo GetSupplierMailInfo(int orderHeaderSequence);
	}
}