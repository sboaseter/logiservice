﻿namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Represents the interface to the System Counters.
	///</summary>
	public interface ISystemCounterProvider
	{

		///<summary>
		/// Returns the current/next counter for Organisations.
		///</summary>
		string GetOrganisationCounter(bool update);

	}
}
