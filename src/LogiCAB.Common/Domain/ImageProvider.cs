using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// An Image Provider for the CAB Database.
	///</summary>
	public class ImageProvider : IImageProvider
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
    	private const string NullImage = "~/Images/1pixel.gif";
		public string CabImagesPath { get; protected set; }
		protected string CabImagesUrl { get; set; }
		private ICabConnectionProvider ConnectionProvider { get; set; }

		///<summary>
		/// Constructor with parameters and dependencies.
		///</summary>
		public ImageProvider(
			string cabImagesPath,
			string cabImagesURL,
			ICabConnectionProvider connectionProvider)
		{
			CabImagesPath = cabImagesPath;
			CabImagesUrl = cabImagesURL;
			ConnectionProvider = connectionProvider;
		}

        public void RefreshAllOfferPictures(string basePath)
        {
            string type = "";

            if (basePath.Length <= 5)
                basePath = "c:\\temp\\cabPictures\\";
                //basePath = "\\\\192.168.102.10\\cabimages\\";
                
            using (var con = ConnectionProvider.GetConnection())
            using (var cmd = con.CreateQuery(@"
                SELECT 0, PICT_SEQUENCE, 0
                FROM dbo.F_CAB_PICTURE WITH (NOLOCK)

                /*SELECT 0 AS CABC_SEQUENCE, OFDT_FK_PICTURE, 0 AS GRAS_SEQUENCE
                FROM dbo.F_CAB_OFFER_DETAILS WITH (NOLOCK)
                JOIN dbo.F_CAB_CD_CAB_CODE WITH (NOLOCK) ON CABC_SEQUENCE = OFDT_FK_CAB_CODE 
                JOIN dbo.F_CAB_OFFER_HEADERS WITH (NOLOCK) ON OFDT_FK_OFFER_HEADER = OFHD_SEQUENCE 
                WHERE OFHD_VALID_FROM >= GETDATE()-1
	                AND OFDT_FK_PICTURE IS NOT NULL
                GROUP BY OFDT_FK_PICTURE
                UNION
                SELECT CABC_SEQUENCE, 0 AS OFDT_FK_PICTURE, CASE ISNULL(GRAS_FK_PICTURE, 0) WHEN 0 THEN 0 ELSE GRAS_SEQUENCE END AS GRAS_SEQUENCE
                FROM dbo.F_CAB_OFFER_DETAILS WITH (NOLOCK)
                JOIN dbo.F_CAB_CD_CAB_CODE WITH (NOLOCK) ON CABC_SEQUENCE = OFDT_FK_CAB_CODE 
                JOIN dbo.F_CAB_GROWER_ASSORTIMENT WITH (NOLOCK) ON GRAS_SEQUENCE = OFDT_FK_GROWER_ASSORTIMENT
                JOIN dbo.F_CAB_OFFER_HEADERS WITH (NOLOCK) ON OFDT_FK_OFFER_HEADER = OFHD_SEQUENCE 
                WHERE OFHD_VALID_FROM >= GETDATE()-1
	                AND OFDT_FK_PICTURE IS NULL*/"))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var cabSeq = (int)reader.GetValue(0);
                        var ofdtPicSeq = (int)reader.GetValue(1);
                        var grasPicSeq = (int)reader.GetValue(2);
                        string key = "";
                        string path = "";
                        int sequence = 0;
                        Image image = null;
                        Boolean skipRec = false;    
                        byte[] data = null;
                        
                        type = "";

                        if (ofdtPicSeq > 0)
                        {
                            type = "P";
                            sequence = ofdtPicSeq;
                        }
                        else
                            Log.Debug("geen offertefoto");
                        
                        if (sequence == 0 && grasPicSeq > 0)
                        {
                            type = "G";
                            sequence = grasPicSeq;
                        }

                        if (sequence == 0 && cabSeq > 0)
                        {
                            type = "C";
                            sequence = cabSeq;
                        }

                        if (type == "")
                            skipRec = true;


                        if (skipRec == false)
                        {
                            key = GetKey(type, sequence, 0, 0, 0);
                            path = Path.Combine(basePath, key.Replace('/', '\\'));

                            // Als deze al bestaat zijn we klaar.
                            if (File.Exists(path))
                            {
                                skipRec = true;
                            }
                        }

                        if (skipRec == false)
                        {
                            switch (type)
                            {
                                case "C":
                                    data = GetCabImage2(sequence);
                                    break;
                                case "P":
                                    data = GetPictureImage(sequence);
                                    break;
                                case "G":
                                    data = GetGrasPicture(sequence);
                                    break;
                                default:
                                    Log.WarnFormat("Invalid type specified: {0}", type);
                                    break;
                            }

                            if (data.Length ==0)
                                skipRec = true;
                        }
                         
                        if (skipRec == false)
                        {
                            // Converteren naar een Image.
                            try
                            {
                                image = Image.FromStream(new MemoryStream(data));
                            }

                            catch (Exception ex)
                            {
                                Log.Error("Error parsing Image from database.", ex);
                                skipRec = true;
                            }

                            // Let op! Vanaf nu moeten we image.Dispose aanroepen als we de methode verlaten!
                            if (skipRec == false)
                            {
                                try
                                {
                                    // Since flipping is kinda strange, use the Graphics-object
                                    using (var tn = new Bitmap(image.Width, image.Height))
                                    {
                                        using (var g = Graphics.FromImage(tn))
                                        {
                                            g.DrawImage(
                                                image
                                                , new Rectangle(0, 0, tn.Width, tn.Height)
                                                , 0
                                                , 0
                                                , image.Width
                                                , image.Height
                                                , GraphicsUnit.Pixel
                                                );
                                        }

                                        // Wegschrijven naar de cache.
                                        // Create parameters and set quality (90)
                                        using (var encPars = new EncoderParameters(1))
                                        {
                                            encPars.Param[0] = new EncoderParameter(Encoder.Quality, 90L);
                                            // put the image into the memory stream

                                            try
                                            {
                                                Directory.CreateDirectory(Path.GetDirectoryName(path));
                                                tn.Save(path, ImageHandler.GetEncoder(ImageFormat.Jpeg), encPars);
                                            }

                                            catch (Exception ex)
                                            {
                                                Log.Error("Error writing resulting image.", ex);
                                            }
                                        }
                                    }
                                }

                                catch (Exception ex)
                                {
                                    Log.Error("Error resizing Image.", ex);
                                }

                                finally
                                {
                                    image.Dispose();
                                }
                            }
                        }
                    }
                }
            }
        }

		public byte[] GetCabImage(int cabCodeSequence)
		{
            int grasExists = -1;
            using (var con = ConnectionProvider.GetConnection())
            using (var cmd = con.CreateQuery(@"
                SELECT GRAS_FK_PICTURE 
                FROM dbo.F_CAB_GROWER_ASSORTIMENT WITH (NOLOCK)
                JOIN dbo.F_CAB_CD_CAB_CODE WITH (NOLOCK) ON CABC_SEQUENCE = GRAS_FK_CAB_CODE 
                WHERE CABC_SEQUENCE = @CABC_SEQUENCE"))
            {
                cmd.AddInputParameter("@CABC_SEQUENCE", cabCodeSequence);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        try
                        {
                            grasExists = (int)reader.GetValue(0);    
                        }
                        catch (Exception ex)
                        {
                            
                        }                        
                    }
                }
            }

            if (grasExists == -1)
            {

                using (var con = ConnectionProvider.GetConnection())
                using (var cmd = con.CreateQuery(@"
                    SELECT PICT_PICTURE 
                    FROM dbo.F_CAB_PICTURE WITH (NOLOCK)
                    JOIN dbo.F_CAB_CD_CAB_CODE WITH (NOLOCK) ON CABC_FK_PICTURE = PICT_SEQUENCE 
                    WHERE CABC_SEQUENCE = @CABC_SEQUENCE"))
                {
                    cmd.AddInputParameter("@CABC_SEQUENCE", cabCodeSequence);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var result = (byte[])reader.GetValue(0);
                            return result;
                        }
                    }
                }
                return null;
            }
            else
            {
                using (var con = ConnectionProvider.GetConnection())
                using (var cmd = con.CreateQuery(@"
                    SELECT PICT_PICTURE 
                    FROM dbo.F_CAB_PICTURE WITH (NOLOCK)
                    WHERE PICT_SEQUENCE = @PICT_SEQUENCE"))
                {
                    cmd.AddInputParameter("@PICT_SEQUENCE", grasExists);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var result = (byte[])reader.GetValue(0);
                            return result;
                        }
                    }
                }
                return null;
            }
		}

        public byte[] GetCabImage2(int cabCodeSequence)
        {
            using (var con = ConnectionProvider.GetConnection())
            using (var cmd = con.CreateQuery(@"
                SELECT PICT_PICTURE 
                FROM dbo.F_CAB_CD_CAB_CODE 
                JOIN dbo.F_CAB_PICTURE WITH (NOLOCK) ON CABC_FK_PICTURE = PICT_SEQUENCE 
                WHERE CABC_SEQUENCE = @CABC_SEQUENCE"))
            {
                cmd.AddInputParameter("@CABC_SEQUENCE", cabCodeSequence);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var result = (byte[])reader.GetValue(0);
                        return result;
                    }
                }
            }
            return null;
        }


		public byte[] GetPictureImage(int pictureSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
                SELECT PICT_PICTURE
                FROM dbo.F_CAB_PICTURE WITH (NOLOCK)
                WHERE PICT_SEQUENCE = @PICT_SEQUENCE"))
			{
				cmd.AddInputParameter("@PICT_SEQUENCE", pictureSequence);

				using (var reader = cmd.ExecuteReader())
				{
					if (reader.Read())
					{
						var result = (byte[])reader.GetValue(0);
						return result;
					}
				}
			}
			return null;
		}

        public byte[] GetGrasPicture(int grasSequence)
        {
            using (var con = ConnectionProvider.GetConnection())
            using (var cmd = con.CreateQuery(@"
                SELECT PICT_PICTURE 
                FROM dbo.F_CAB_GROWER_ASSORTIMENT WITH (NOLOCK)
                JOIN dbo.F_CAB_PICTURE WITH (NOLOCK) ON GRAS_FK_PICTURE = PICT_SEQUENCE 
                WHERE GRAS_SEQUENCE = @grasSequence"))
            {
                cmd.AddInputParameter("@grasSequence", grasSequence);

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var result = (byte[])reader.GetValue(0);
                        return result;
                    }
                }
            }
            return null;
        }


		public string GetPathFor(string type, int sequence, int? width, int? height, int? maxWidth)
		{
			var key = GetKey(type, sequence, width, height, maxWidth);
			return Path.Combine(CabImagesPath, key.Replace('/', '\\'));
		}

		public string GetStaticUrlFor(string type, int sequence, int? width, int? height, int? maxWidth)
		{
            // Bepaal een on-disk sleutel voor deze parameters.
			var key = GetKey(type, sequence, width, height, maxWidth);
			var path = Path.Combine(CabImagesPath, key.Replace('/', '\\'));
			var url = Helper.UrlCombine(CabImagesUrl, key);

			// Als deze al bestaat zijn we klaar.
			if (File.Exists(path))
				return url;

			// Anders moeten we het plaatje ophalen en processen...
			Log.DebugFormat("Caching new image: [{0}]", key);

			// Ophalen van de binary data uit de database.
			byte[] data;
            
			switch (type)
			{
				case "C":
					data = GetCabImage2(sequence);
					break;
				case "P":
					data = GetPictureImage(sequence);
					break;
                case "G":
                    data = GetGrasPicture(sequence);
                    break;
                //case "E":
                //    if(sequence == 0)

                //    break;
				default:
					Log.WarnFormat("Invalid type specified: {0}", type);
					return NullImage;
			}

			if (data == null)
			{
				Log.WarnFormat("Picture not found in database.");
				return NullImage;
			}

			if (data.Length == 0)
			{
				Log.ErrorFormat("Picture in database has zero length!");
				return NullImage;
			}

			// Converteren naar een Image.
			Image image;
			
            try
			{
				image = Image.FromStream(new MemoryStream(data));
			} 

            catch (Exception ex)
			{
				Log.Error("Error parsing Image from database.", ex);
				return NullImage;
			}
			// Let op! Vanaf nu moeten we image.Dispose aanroepen als we de methode verlaten!

			// Bepalen hoe groot ze het willen...
			int outW;
			int outH;

            if (width.HasValue || height.HasValue)
                ImageHandler.CalculateSize(image.Width, image.Height, width, height, maxWidth, out outW, out outH);
            else
            {
                outW = image.Width;
                outH = image.Height;
            }

			// Resizen.
			try
			{
				// Since flipping is kinda strange, use the Graphics-object
				using (var tn = new Bitmap(outW, outH))
				{
					using (var g = Graphics.FromImage(tn))
					{
						g.DrawImage(
							image
							,	new Rectangle(0, 0, tn.Width, tn.Height)
							,	0
							,	0
							,	image.Width
							,	image.Height
							,	GraphicsUnit.Pixel
							);
					}

					// Wegschrijven naar de cache.
					// Create parameters and set quality (90)
					var encPars = new EncoderParameters(1);
					encPars.Param[0] = new EncoderParameter(Encoder.Quality, 90L);

					// put the image into the memory stream
					try
					{
						Directory.CreateDirectory(Path.GetDirectoryName(path));
						tn.Save(path, ImageHandler.GetEncoder(ImageFormat.Jpeg), encPars);
					}
					catch (Exception ex)
					{
						Log.Error("Error writing resulting image.", ex);
						return NullImage;
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error("Error resizing Image.", ex);
				return NullImage;
			}
			finally
			{
				image.Dispose();
			}

			// Klaar.
			return url;
		}

		private static string GetKey(string type, int sequence, int? width, int? height, int? maxWidth) {
			var reverseSequence = sequence.ToString().Reverse();
			if (reverseSequence.Length > 2)
				reverseSequence = reverseSequence.Substring(0, 2) + "/" + reverseSequence.Substring(2);
			if (reverseSequence.Length > 6)
				reverseSequence = reverseSequence.Substring(0, 6) + "/" + reverseSequence.Substring(6);
			return String.Concat(type, "/", reverseSequence, "/", width ?? 0, ".", height ?? 0, ".", maxWidth ?? 0, ".jpg");
		}

	}
}