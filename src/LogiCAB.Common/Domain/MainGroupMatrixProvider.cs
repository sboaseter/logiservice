﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	public class MainGroupMatrixProvider : IMainGroupMatrixProvider
	{
		protected IConnectionProvider ConnectionProvider { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public MainGroupMatrixProvider(IConnectionProvider connectionProvider)
		{
			ConnectionProvider = connectionProvider;
		}

		public List<int> SelectMainGroups(Organisation orga)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
SELECT ORMA_FK_MAIN_GROUP
FROM [F_CAB_ORGA_MAIN_GROUP_MATRIX]
WHERE ORMA_FK_ORGA = @orgaSequence
ORDER BY ORMA_FK_MAIN_GROUP
"))
			{
				cmd.AddInputParameter("@orgaSequence", orga.Sequence);
				using (var reader = cmd.ExecuteReader())
				{
					var result = new List<int>();
					while (reader.Read())
					{
						result.Add(reader.Get<int>("ORMA_FK_MAIN_GROUP"));
					}
					return result;
				}
			}
		}

		public void InsertMainGroup(Organisation orga, int groupSequence, int order)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
INSERT INTO [F_CAB_ORGA_MAIN_GROUP_MATRIX]
			(
				 ORMA_FK_ORGA
				,ORMA_FK_MAIN_GROUP
				,ORMA_ORDER	 
			)
			VALUES
			(
				 @orgaSequence
				,@groupSequence
				,@order
			);"))
			{
				cmd.AddInputParameter("@orgaSequence", orga.Sequence);
				cmd.AddInputParameter("@groupSequence", groupSequence);
				cmd.AddInputParameter("@order", order);
				cmd.ExecuteNonQuery();
			}
		}

		public void CopyMainGroups(Organisation fromOrga, Organisation toOrga)
		{
			var order = 1;
			foreach (var sequence in SelectMainGroups(fromOrga))
				InsertMainGroup(toOrga, sequence, order++);
		}
	}
}