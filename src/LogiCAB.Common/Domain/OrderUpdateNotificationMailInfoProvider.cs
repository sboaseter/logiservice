﻿using System;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implementatie van ISupplierMailInfoProvider.
	///</summary>
	public class OrderUpdateNotificationMailInfoProvider : IOrderUpdateNotificationMailInfoProvider
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		private const string dutchDateFormat = "dd-MM-yyyy";
        protected ICabConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<Buyer> Buyers { get; set; }
		protected IRepository<Grower> Growers { get; set; }
		protected IRepository<Person> Persons { get; set; }
		protected IShopProvider ShopProvider { get; set; }
		protected ICommonOrderProvider CommonOrderProvider { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public OrderUpdateNotificationMailInfoProvider(
			ICabConnectionProvider connectionProvider,
			IRepository<Buyer> buyers,
			IRepository<Grower> growers,
			IRepository<Person> persons,
			IShopProvider shopProvider,
			ICommonOrderProvider commonOrderProvider)
		{
			ConnectionProvider = connectionProvider;
			Buyers = buyers;
			Growers = growers;
			Persons = persons;
			ShopProvider = shopProvider;
			CommonOrderProvider = commonOrderProvider;
		}

		public OrderUpdateNotificationMailInfo SupplierToClient(int orderHeaderSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	ORHE_NO
,	ORHE_DATE
,	ORHE_DESC
,	CDOS_STATUS_DESC
,	ORHE_REMARK
,	ORHE_FK_BUYER
,	ORHE_FK_PERSON
,	ORGA_NAME As GrowerName
,	ORGA_EMAIL As GrowerEmail
,	ORHE_FK_SHOP As ShopId
From
	F_CAB_ORDER_HEA
Inner Join
	F_CAB_CD_ORD_HEA_STATUS
		On	( CDOS_SEQUENCE = ORHE_FK_ORDER_STATUS )
Inner Join
	F_CAB_CD_CASK
		On	( CDCA_SEQUENCE = ORHE_FK_BULK_CASK )
Inner Join
	F_CAB_GROWER
		On	( GROW_SEQUENCE = ORHE_FK_GROWER )
Inner Join
	F_CAB_ORGANISATION
		On	( ORGA_SEQUENCE = GROW_FK_ORGA )
Where
	ORHE_SEQUENCE = @orderHeaderSequence
;"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				using (var reader = cmd.ExecuteReader())
				{
					if (!reader.Read())
						throw new ApplicationException("Unable to find specified Order Header.");

					var shop = (Shop)reader.Get<int>("ShopId");
					var properties = ShopProvider.GetProperties(shop, null, "LogiService");
					if (!properties.ContainsKey("ShopBaseUrl"))
						throw new ApplicationException(String.Concat("ShopBaseUrl property not found for shop: ", shop));

					// Bepaal of de client naar de Commissionair variant moet.
					var hasOriginalOffer = CommonOrderProvider.GetHasOriginalOffer(orderHeaderSequence);

					var buyerPerson = Persons.GetById(reader.Get<int>("ORHE_FK_PERSON"));

					var info = new OrderUpdateNotificationMailInfo
					{
						From = reader.Get<string>("GrowerEmail"),
						To = buyerPerson.Email,
						OrderSequence = orderHeaderSequence,
						OrderNo = reader.Get<string>("ORHE_NO"),
						OrderDate = reader.Get<DateTime>("ORHE_DATE").ToString(dutchDateFormat),
						OrderDesc = reader.Get<string>("ORHE_DESC"),
						OrderStatus = reader.Get<string>("CDOS_STATUS_DESC"),
						OrderRemark = reader.Get<string>("ORHE_REMARK"),
						GrowerName = reader.Get<string>("GrowerName"),
						Buyer = Buyers.GetById(reader.Get<int>("ORHE_FK_BUYER")),
						Person = buyerPerson,
						Shop = shop,
						ShopBaseUrl = properties["ShopBaseUrl"],
						OrderOverviewPageName = hasOriginalOffer ? "CommOrderOverview.aspx" : "OrderOverview.aspx"
					};
					return info;
				}
			}
		}
	}
}