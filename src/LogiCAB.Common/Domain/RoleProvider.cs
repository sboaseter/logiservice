﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Enums;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	/// <summary>
	/// Een simpele RoleProvider voor het leveren van Rollen voor een Organisatie.
	/// </summary>
	public sealed class RoleProvider : IRoleProvider
	{
		private ICabConnectionProvider ConnectionProvider { get; set; }
		private IShortTermCache ShortTermCache { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public RoleProvider(
			ICabConnectionProvider connectionProvider,
			IShortTermCache shortTermCache)
		{
			ConnectionProvider = connectionProvider;
			ShortTermCache = shortTermCache;
		}

		public Role[] GetRolesForOrganisation(int orgaSequence)
		{
			return ShortTermCache.GetWithDelegate(() =>
			{
				using (var con = ConnectionProvider.GetConnection())
				using (var cmd = con.CreateQuery(@"
	-- Organisatie Rollen.
	Select
		ROLE_SEQUENCE
	From
		F_CAB_ORGANISATION_FUNCTION orfu with (nolock)
	Inner Join
		F_CAB_FUNCTION_ROLE fr with (nolock)
			On	( orfu.ORFU_FK_FUNC = fr.FURE_FK_FUNC )
	Inner Join
		F_CAB_ROLE r with (nolock)
			On	( fr.FURE_FK_ROLE = r.ROLE_SEQUENCE )
	Where
		( orfu.ORFU_FK_ORGA = @orgaSequence )
;"))
				{
					cmd.AddInputParameter("@orgaSequence", orgaSequence);
					using (var reader = cmd.ExecuteReader())
					{
						var result = new List<Role>();
						while (reader.Read())
						{
							result.Add((Role)reader.Get<int>("ROLE_SEQUENCE"));
						}
						return result.ToArray();
					}
				}
			}, "RoleProvider.GetRolesForOrganisation", orgaSequence);
		}

		public Role[] GetRolesForOrganisation(int orgaSequence, Shop shop)
		{
			return GetRolesForOrganisation(orgaSequence, (int) shop);
		}

		public Role[] GetRolesForOrganisation(int orgaSequence, int shopId)
		{
			return ShortTermCache.GetWithDelegate(() =>
			{
				using (var con = ConnectionProvider.GetConnection())
				using (var cmd = con.CreateQuery(@"
Select
	FURE_FK_ROLE As ROLE_SEQUENCE
From
	(			-- Organisatie Rollen.
		Select
			ORFU_FK_ORGA As Orga
		,	ORFU_FK_FUNC As Func
		,	Null As Shop
		From
			F_CAB_ORGANISATION_FUNCTION
	Union
		Select
			SPON_FK_ORGA As Orga
		,	SPOF_FK_FUNC As Func
		,	SPON_FK_SHOP As Shop
		From
			F_CAB_SHOP_ORGANISATION_FUNCTION
		Inner Join
			F_CAB_SHOP_ORGANISATION
				On	( SPOF_FK_SPON = SPON_SEQUENCE )
	Union
		Select
			SPON_FK_ORGA As Orga
		,	SPFN_FK_FUNC As Func
		,	SPON_FK_SHOP As Shop
		From
			F_CAB_SHOP_FUNCTION spfn with (nolock)
		Inner Join
			F_CAB_SHOP_ORGANISATION spon with (nolock)
				On	( spfn.SPFN_FK_SHOP = spon.SPON_FK_SHOP )
	) a
Inner Join
	F_CAB_FUNCTION_ROLE
		On	( a.Func = FURE_FK_FUNC )
Where
	( a.Orga = @orgaSequence )
And	( a.Shop = @shop Or a.Shop Is Null )
;"))
				{
					cmd.AddInputParameter("@orgaSequence", orgaSequence);
					cmd.AddInputParameter("@shop", shopId);
					using (var reader = cmd.ExecuteReader())
					{
						var result = new List<Role>();
						while (reader.Read())
						{
							result.Add((Role)reader.Get<int>("ROLE_SEQUENCE"));
						}
						return result.ToArray();
					}
				}
			}, "RoleProvider.GetRolesForOrganisation", orgaSequence, shopId);
		}
	}
}
