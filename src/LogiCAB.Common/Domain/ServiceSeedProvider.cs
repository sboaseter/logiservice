using System;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implements the IServiceSeedProvider interface against the CAB Database.
	///</summary>
	public class ServiceSeedProvider : IServiceSeedProvider
	{
		protected ICabConnectionProvider ConnectionProvider { get; set; }

        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public ServiceSeedProvider(ICabConnectionProvider connectionProvider)
		{
			ConnectionProvider = connectionProvider;
		}

		public DateTime? GetSeedDate(string seed)
		{
			Log.DebugFormat("GetSeedDate(seed = [{0}])", seed);

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	SESD_CREATED
From
	dbo.F_CAB_SERVICE_SEED
Where
	( SESD_SEED = @Seed )
;
"))
			{
				cmd.AddInputParameter("@Seed", seed);
				try
				{
					return (DateTime?) cmd.ExecuteScalar();
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Error Getting Seed Date: {0}", ex, seed);
					throw new ApplicationException("Error Getting Seed Date", ex);
				}
			}
		}

		public void StoreSeed(string seed)
		{
			Log.DebugFormat("StoreSeed(seed = [{0}])", seed);

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Insert Into dbo.F_CAB_SERVICE_SEED (
	SESD_SEED
,	SESD_CREATED
) Values (
	@Seed
,	@Created
);
"))
			{
				cmd.AddInputParameter("@Seed", seed);
				cmd.AddInputParameter("@Created", DateTime.Now);
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Error Storing Seed: {0}", ex, seed);
					throw new ApplicationException("Error Storing Seed", ex);
				}
			}
		}

		public void RemoveSeed(string seed)
		{
			Log.DebugFormat("RemoveSeed(seed = [{0}])", seed);

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Delete dbo.F_CAB_SERVICE_SEED Where ( SESD_SEED = @Seed );
"))
			{
				cmd.AddInputParameter("@Seed", seed);
				try
				{
					cmd.ExecuteNonQuery();
				} catch (Exception ex)
				{
					Log.ErrorFormat("Error Removing Seed: {0}", ex, seed);
					throw new ApplicationException("Error Removing Seed", ex);
				}
			}
		}
	}
}