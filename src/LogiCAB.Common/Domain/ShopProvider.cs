﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	public class ShopProvider : IShopProvider
	{
		protected ICabConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<ShopEvidence> ShopEvidences { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public ShopProvider(
			ICabConnectionProvider connectionProvider,
			IRepository<ShopEvidence> shopEvidences)
		{
			ConnectionProvider = connectionProvider;
			ShopEvidences = shopEvidences;
		}

		public List<ShopEvidence> GetEvidence(string service, string type)
		{
			return ShopEvidences
				.Where(se => se.Service == service && se.Type == type)
				.OrderByDescending(se => se.Weight)
				.ToList();
		}

		//SHW: Code verwijderd omdat in de view de skin een waarde heeft.
		public Dictionary<string, string> GetProperties(Shop shop, ShopSkin? skin, string service)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	SPPY_NAME
,	SPPY_VALUE
From
	V_CAB_SHOP_PROPERTY with (nolock)
Where
	( SPPY_FK_SHOP = @Shop )
And	( ( SPPY_FK_SPSN = @Skin ) Or ( SPPY_FK_SPSN Is Null And @Skin Is Null ) )
And	( SPPY_SERVICE = @Service )
;"))
			{
				cmd.AddInputParameter("@Shop", (int)shop);
				cmd.AddInputParameter("@Skin", (int?)skin);
				cmd.AddInputParameter("@Service", service);
				using (var reader = cmd.ExecuteReader())
				{
					var result = new Dictionary<string, string>();
					while (reader.Read())
					{
						var key = reader.Get<string>("SPPY_NAME");
						var value = reader.Get<string>("SPPY_VALUE");
						try
						{
							result.Add(key, value);
						}
						catch (Exception ex)
						{
							Log.FatalFormat("Unable to add property key [{0}] to the properties list!", ex, key);
						}
					}
					return result;
				}
			}
		}

		public Dictionary<string, string> GetProperties(ShopEvidence evidence)
		{
			return GetProperties(evidence.Shop, evidence.Skin, evidence.Service);
		}

	}
}
