﻿using System;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace LogiCAB.Common.Domain
{
	/// <summary>
	/// A simple cache meant for short term stuff.
	/// This implementation uses the System.Web.Caching.Cache as a store.
	/// It could have been fully static, but a singleton object is easier to
	/// manage as a dependency.
	/// </summary>
	public class ShortTermCache : IShortTermCache
	{
		/// <summary>
		/// The TimeSpan for the Sliding Expiration.
		/// </summary>
		protected static readonly TimeSpan Timeout = TimeSpan.FromSeconds(10);

		/// <summary>
		/// Provides access to a Cache instance through either the HttpContext
		/// or the Runtime. This should work in both Web and Non-Web Applications.
		/// </summary>
		protected static Cache Instance
		{
			get
			{
				return HttpContext.Current == null ? HttpRuntime.Cache : HttpContext.Current.Cache;
			}
		}

		/// <summary>
		/// Implements a simple strategy to turn any number of parts of any
		/// type into a key string.
		/// </summary>
		protected static string KeyFrom(object[] keyParts)
		{
			var builder = new StringBuilder();
			foreach (var part in keyParts)
			{
				if (part is string)
					builder.Append(part);
				else
					builder.Append(part.GetHashCode().ToString());
				builder.Append('|');
			}
			return builder.ToString();
		}

		///<summary>
		/// Gets the stored value of the given Key from the cache. If no value
		/// is present, the given function is Invoked to provide one.
		///</summary>
		public T GetWithDelegate<T>(Func<T> retrieve, string key)
		{
			var existing = Instance.Get(key);
			if (existing == null)
			{
				existing = retrieve.Invoke();
				Add(existing, key);
			}
			return (T)existing;
		}

		///<summary>
		/// Gets the stored value of the given Key from the cache. If no value
		/// is present, the given function is Invoked to provide one.
		///</summary>
		public T GetWithDelegate<T>(Func<T> retrieve, params object[] keyParts)
		{
			return GetWithDelegate(retrieve, KeyFrom(keyParts));
		}

		///<summary>
		/// Gets the stored value of the given Key from the cache.
		///</summary>
		public T Get<T>(string key)
		{
			return (T)Instance.Get(key);
		}

		///<summary>
		/// Gets the stored value of the given Key from the cache.
		///</summary>
		public T Get<T>(params object[] keyParts)
		{
			return Get<T>(KeyFrom(keyParts));
		}

		///<summary>
		/// Adds the given value to the cache under the given key.
		/// Calls to this method will fail if an item with the same key parameter is already stored in the Cache. To overwrite an existing Cache item using the same key parameter, use the Set method.
		///</summary>
		public void Add<T>(T value, string key)
		{
			Instance.Add(key, value, null, Cache.NoAbsoluteExpiration, Timeout, CacheItemPriority.Normal, null);
		}

		///<summary>
		/// Adds the given value to the cache under the given key.
		/// Calls to this method will fail if an item with the same key parameter is already stored in the Cache. To overwrite an existing Cache item using the same key parameter, use the Set method.
		///</summary>
		public void Add<T>(T value, params object[] keyParts)
		{
			Add(value, KeyFrom(keyParts));
		}

		///<summary>
		/// Sets the given value in the cache under the given key.
		/// This method will overwrite an existing value.
		///</summary>
		public void Set<T>(T value, string key)
		{
			Instance.Insert(key, value, null, Cache.NoAbsoluteExpiration, Timeout, CacheItemPriority.Normal, null);
		}

		///<summary>
		/// Sets the given value in the cache under the given key.
		/// This method will overwrite an existing value.
		///</summary>
		public void Set<T>(T value, params object[] keyParts)
		{
			Set(value, KeyFrom(keyParts));
		}

		public void Clear(string key)
		{
			Instance.Remove(key);
		}

		public void Clear(params object[] keyParts)
		{
			Instance.Remove(KeyFrom(keyParts));
		}
	}
}
