﻿using System;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implements the SignalProvider interface.
	///</summary>
	public class SignalProvider : ISignalProvider {

		protected ICabConnectionProvider ConnectionProvider { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public SignalProvider(ICabConnectionProvider connectionProvider)
		{
			ConnectionProvider = connectionProvider;
		}

		public void SignalOrderConfirmed(int orderHeaderSequence)
		{
			using (var conn = ConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("SignalOrderConfirmed"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}

		public void SignalOrderConfirmedWithMail(int orderHeaderSequence)
		{
			using (var conn = ConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("SignalOrderConfirmedWithMail"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}
	}
}