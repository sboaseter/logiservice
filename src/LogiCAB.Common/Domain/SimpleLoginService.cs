﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Model;
using LogiCAB.Common.Services;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implementatie van ISimpleLoginService.
	///</summary>
	public class SimpleLoginService : ISimpleLoginService
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ICabConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<Organisation> Organisations { get; set; }
		protected IRepository<Person> Persons { get; set; }
		protected IRepository<Login> Logins { get; set; }
		protected IRepository<Handshake> Handshakes { get; set; }
		protected IRepository<GrowerBuyerMatrix> GrowerBuyerMatrixes { get; set; }
		protected IRepository<Grower> Growers { get; set; }
		protected IRepository<CabProperty> Properties { get; set; }
		protected ISystemCounterProvider SystemCounterProvider { get; set; }
		protected IMainGroupMatrixProvider MainGroupMatrixProvider { get; set; }
		protected CabPropertyService CabPropertyService { get; set; }
		protected IGrowerBuyerMatrixProvider GrowerBuyerMatrixProvider { get; set; }
		protected IRepository<Function> Functions { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public SimpleLoginService(
			ICabConnectionProvider connectionProvider,
			IRepository<Organisation> organisations,
			IRepository<Person> persons,
			IRepository<Login> logins,
			IRepository<Handshake> handshakes,
			IRepository<GrowerBuyerMatrix> growerBuyerMatrixes,
			IRepository<Grower> growers,
			IRepository<CabProperty> properties,
			ISystemCounterProvider systemCounterProvider,
			IMainGroupMatrixProvider mainGroupMatrixProvider,
			CabPropertyService cabPropertyService,
			IGrowerBuyerMatrixProvider growerBuyerMatrixProvider,
			IRepository<Function> functions)
		{
			ConnectionProvider = connectionProvider;
			Organisations = organisations;
			Persons = persons;
			Logins = logins;
			Handshakes = handshakes;
			GrowerBuyerMatrixes = growerBuyerMatrixes;
			Growers = growers;
			Properties = properties;
			SystemCounterProvider = systemCounterProvider;
			MainGroupMatrixProvider = mainGroupMatrixProvider;
			CabPropertyService = cabPropertyService;
			GrowerBuyerMatrixProvider = growerBuyerMatrixProvider;
			Functions = functions;
		}

		public IEnumerable<SimpleLogin> GetPaged(
			Organisation eigenaar,
			int pageIndex, int pageSize, int isBlocked)
		{
			if (pageSize > Constants.MaxPageSize)
				throw new ArgumentOutOfRangeException("pageSize", pageSize, "pageSize is te groot.");
			if (pageSize < Constants.MinPageSize)
				throw new ArgumentOutOfRangeException("pageSize", pageSize, "pageSize is te klein.");

			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select Top (@take)
	LOGI_SEQUENCE
,	ORGA_SEQUENCE
,	ORGA_NAME
,	ORGA_PHONE_NO
,	ORGA_EMAIL
,	LOGI_USER_NAME
,	LOGI_PASS
,	LOGI_BLOCKED_YN
-- SHW ADDED LANGUAGE
,	LANGU_SEQUENCE
From (
Select
	LOGI_SEQUENCE
,	ORGA_SEQUENCE
,	ORGA_NAME
,	ORGA_PHONE_NO
,	ORGA_EMAIL
,	LOGI_USER_NAME
,	LOGI_PASS
,	LOGI_BLOCKED_YN
-- SHW ADDED LANGUAGE
,	ISNULL(SimpleLoginLanguage.PPTY_REFERENCE, 0) AS LANGU_SEQUENCE
,	ROW_NUMBER() OVER(Order By ORGA_NAME Asc) As RowNumber
From
	F_CAB_HANDSHAKE
Inner Join
	F_CAB_PROPERTY AS SimpleLogin
		On	( SimpleLogin.PPTY_TYPE = 'F_CAB_HANDSHAKE' )
		And	( SimpleLogin.PPTY_KEY = HAND_SEQUENCE )
		And	( SimpleLogin.PPTY_NAME = 'SimpleLogin' )
		And	( SimpleLogin.PPTY_YESNO = 1 )
Inner Join
	F_CAB_ORGANISATION
		On	( HAND_FK_ORGA_SEQ_BUYER = ORGA_SEQUENCE )
Inner Join
	F_CAB_PERSON
		On	( HAND_FK_ORGA_SEQ_BUYER = PERS_FK_ORGANISATION )
Inner Join
	F_CAB_LOGIN
		On	( PERS_SEQUENCE = LOGI_FK_PERSON )
left outer Join
-- SHW Get Language from F_CAB_PROPERTY
	F_CAB_PROPERTY AS SimpleLoginLanguage
		On	( SimpleLoginLanguage.PPTY_TYPE = 'F_CAB_LOGIN' )
		And	( SimpleLoginLanguage.PPTY_KEY = LOGI_SEQUENCE )
		And	( SimpleLoginLanguage.PPTY_NAME = 'SimpleLoginLanguage' )		
Where
	HAND_FK_ORGA_SEQ_GROWER = @eigenaar AND LOGI_BLOCKED_YN <= @isBlocked
) src
Where
	RowNumber > @skip
Order By
	ORGA_NAME Asc
;"))
			{
				cmd.AddInputParameter("@eigenaar", eigenaar.Sequence);
				cmd.AddInputParameter("@take", pageSize);
				cmd.AddInputParameter("@skip", (pageIndex - 1) * pageSize);
				cmd.AddInputParameter("@isBlocked", isBlocked);

				var result = new List<SimpleLogin>();
				using (var reader = cmd.ExecuteReader())
					while (reader.Read())
					{
						result.Add(new SimpleLogin
						{
							Sequence = reader.Get<int>("LOGI_SEQUENCE"),
							OrganisatieSequence = reader.Get<int>("ORGA_SEQUENCE"),
							Organisatie = reader.Get<string>("ORGA_NAME"),
							Phone = reader.Get<string>("ORGA_PHONE_NO"),
							Email = reader.Get<string>("ORGA_EMAIL"),
							Login = reader.Get<string>("LOGI_USER_NAME"),
							Password = reader.Get<string>("LOGI_PASS"),
							Blocked = reader.Get<short>("LOGI_BLOCKED_YN"),
							LanguageSeq = reader.Get<int>("LANGU_SEQUENCE") == 0 ? 1 : reader.Get<int>("LANGU_SEQUENCE"),
							Eigenaar = eigenaar
						});
					}
				return result;
			}

		}

		public Login Create(SimpleLogin input)
		{
			// Begin met het maken van een Organisatie.
			var code = String.Concat(
				DateTime.Now.ToString("MMdd"),
				SystemCounterProvider.GetOrganisationCounter(true));

			var orga = new Organisation
			{
				Type = "BUYR",
				Code = code,
				Name = input.Organisatie,
				NameUpper = input.Organisatie.ToUpperInvariant(),
				Phone = input.Phone,
				Fax = String.Empty,
				Email = input.Email,
				Mailbox = String.Empty,
				EAN = code,
				VAT = String.Empty,
				VBA = String.Empty,
				BVH = String.Empty,
				Referer = input.Eigenaar.Name,
				Start = DateTime.Now,
				End = new DateTime(3000, 1, 1),
			};

			orga.Buyer = new Buyer
			{
				Organisation = orga,
				Mobile = input.Phone
			};

			orga.Admin = new OrganisationAdmin
			{
				Organisation = orga,
				Date = DateTime.Now,
				MemberType = 0,
				Status = 0
			};

			var persEigenaar = Persons.FirstOrDefault(p => p.Organisation.Sequence == input.Eigenaar.Sequence);
			//SHW: OrganisationInfo toevoegen.
			orga.Info = new OrganisationInfo
			{
				Organisation = orga,
				CreatedBy = input.Eigenaar.Name,
				CreatedOn = DateTime.Now,
				ModifiedBy = input.Eigenaar.Name,
				ModifiedOn = DateTime.Now,
				ShippingDays = "0",
				BuyrPersonSequence = persEigenaar.Sequence,
				OrderDeliverDays = 0,
				OfferDeliverDays = 0,
				ContactName = string.Empty,
				ContactPhone = string.Empty,
				ContactFax = string.Empty,
				ContactEmail = string.Empty
			};

			// Voorlopig even alle Shops van de Eigenaar overnemen.
			// TODO: Verder over nadenken.
			foreach (var shopOrga in input.Eigenaar.Shops)
				orga.Shops.Add(new ShopOrganisation {Organisation = orga, Shop = shopOrga.Shop});

			var kvkUserFunctie = Functions.FirstOrDefault(f => f.Code == "KVK_USER");
			if (kvkUserFunctie != null)
			{
				if (!orga.Functions.Contains(kvkUserFunctie))
					orga.Functions.Add(kvkUserFunctie);
			}
			var logiOfferteFunc = Functions.FirstOrDefault(f => f.Code == "LOGIOFFERTE");
			if (logiOfferteFunc != null)
			{
				if (!orga.Functions.Contains(logiOfferteFunc))
					orga.Functions.Add(logiOfferteFunc);
			}
			var buyStockFunc = Functions.FirstOrDefault(f => f.Code == "BUY_STOCK");
			if (buyStockFunc != null)
			{
				if (input.Eigenaar.Functions.Contains(buyStockFunc))
					if (!orga.Functions.Contains(buyStockFunc))
						orga.Functions.Add(buyStockFunc);
			}
			var buyOfferFunc = Functions.FirstOrDefault(f => f.Code == "BUY_OFFER");
			if (buyOfferFunc != null)
			{
				if (input.Eigenaar.Functions.Contains(buyOfferFunc))
					if (!orga.Functions.Contains(buyOfferFunc))
						orga.Functions.Add(buyOfferFunc);
			}

			// Maak een Persoon aan.
			var person = new Person
			{
				Organisation = orga,
				Email = input.Email,
				FirstName = String.Empty,
				MiddleName = String.Empty,
				LastName = input.Organisatie,
				Phone = input.Phone
			};

			// Maak een Login aan.
			var login = new Login
			{
				Person = person,
				LoginGroup = 2,
				Name = input.Login,
				Password = input.Password,
				PasswordHash = Helper.CreateCABPasswordHash(input.Password),
				Blocked = input.Blocked
			};

			try
			{
				// Sla de hele boel op.
				Organisations.Save(orga);
				Persons.Save(person);
				Logins.Save(login);

				// Creëer de Handshake.
				var handshake = new Handshake
				{
					Orga1 = input.Eigenaar,
					Orga2 = orga,
					RequestDate = DateTime.Now,
					AcceptDate = DateTime.Now,
					ApproveDate = DateTime.Now,
					Blocked = 0,
					Mail = String.Empty,
					Remark = String.Empty
				};
				Handshakes.Save(handshake);

				// En dan nu de property om aan te geven dat dit een SimpleLogin Relatie is.
				Properties.Save(CabProperty.NewHandshakeSimpleLogin(handshake));

				// We moeten ook nog een Buyer Grower Matrix record aanmaken.
				GrowerBuyerMatrixes.Save(new GrowerBuyerMatrix
				{
					Buyer = orga.Buyer,
					Grower = input.Eigenaar.Grower,
					// Deze is wel nullable, maar hij is in de DB nergens leeg...
					LastOrderDate = DateTime.Now,
					Blocked = 0
				});

				//var matrixGrowBuyr = new GrowerBuyerMatrix
				//{
				//    Buyer = orga.Buyer,
				//    Grower = input.Eigenaar.Grower,
				//    LastOrderDate = DateTime.Now,
				//    Blocked = 0
				//};

				//GrowerBuyerMatrixes.Save(matrixGrowBuyr);
				// SHW: Get all the Maingroups that the Eigenaar of the simpleAccount has.
				MainGroupMatrixProvider.CopyMainGroups(input.Eigenaar, orga);

				// Geef de Sequence terug.
				input.Sequence = login.Sequence;
				input.OrganisatieSequence = orga.Sequence;

				Log.DebugFormat("Maakt een nieuwe SimpleLogin. LoginSequence: {0}", login.Sequence);

				//SHW Saves or updates the language for SimpleLogin User
				CabPropertyService.SimpleLoginLanguage.CreateAndSave(login, input.LanguageSeq);
			}
			catch(Exception ex)
			{
				Log.Error("Fout bij het aanmaken van nieuwe SimpleLogin. LoginSequence: {0}", ex);
			}

			return login;
		}

		public void Update(SimpleLogin input)
		{
			var isDirty = false;

			// Haal op basis van de gegeven input de Login op.
			var login = Logins.GetById(input.Sequence);

			// Haal ook de Handshake en GBM op.
			// De eigenaar is hier theoretisch overbodig.
			var handshake = Handshakes.First(h => h.Orga1 == input.Eigenaar && h.Orga2 == login.Person.Organisation);

			var buyerSeq = handshake.Orga2.Buyer == null ? login.Person.Organisation.Buyer.Sequence : handshake.Orga2.Buyer.Sequence;
			var growerSeq = handshake.Orga1.Grower == null ? input.Eigenaar.Grower.Sequence : handshake.Orga1.Grower.Sequence;

			var gbmatrix = GrowerBuyerMatrixProvider.GetGrowerBuyerMatrix(growerSeq , buyerSeq);
			if (gbmatrix.Count == 0)
			{
				GrowerBuyerMatrixProvider.InsertGrowerBuyerMatrix(growerSeq, buyerSeq, DateTime.Now, 0);
			}

			var matrix = GrowerBuyerMatrixes.First(m => m.Grower == input.Eigenaar.Grower && m.Buyer == login.Person.Organisation.Buyer);

			// Kijk of we het wachtwoord moeten wijzigen.)
			if (!string.IsNullOrEmpty(input.Password))
			{
				if (!login.Password.Equals(input.Password))
				{
					login.Password = input.Password;
					login.PasswordHash = Helper.CreateCABPasswordHash(input.Password);
					isDirty = true;
				}
			}

			// Kijk of hij soms ge(de)blokkeerd wordt.
			if (!login.Blocked.Equals(input.Blocked))
			{
				login.Blocked = input.Blocked;
				handshake.Blocked = input.Blocked;
				matrix.Blocked = input.Blocked;
				isDirty = true;
			}

			// Kijk of we iets moeten wijzigen aan de Organisatie.
			if (!login.Person.Organisation.Email.Equals(input.Email))
			{
				login.Person.Organisation.Email = input.Email;
				login.Person.Email = input.Email;
				isDirty = true;
			}
			if (!login.Person.Organisation.Phone.Equals(input.Phone))
			{
				login.Person.Organisation.Phone = input.Phone;
				login.Person.Phone = input.Phone;
				isDirty = true;
			}
			if (!login.Person.Organisation.Name.Equals(input.Organisatie))
			{
				login.Person.Organisation.Name = input.Organisatie;
				login.Person.Organisation.NameUpper = input.Organisatie.ToUpperInvariant();
				isDirty = true;
			}
			if (!login.Name.Equals(input.Login))
			{
				login.Name = input.Login;
				isDirty = true;
			}

			var languageProperty = CabPropertyService.SimpleLoginLanguage.FindOnLogin(login);
			if (languageProperty != null)
			{
				if (!languageProperty.Reference.Equals(input.LanguageSeq))
				{
					languageProperty.Reference = input.LanguageSeq;
					isDirty = true;
				}
			}
			else
			{
				languageProperty = CabPropertyService.SimpleLoginLanguage.Create(login, input.LanguageSeq);
				isDirty = true;
			}

			if (isDirty)
			{
				// Sla de hele boel op.
				Organisations.Save(login.Person.Organisation);
				Persons.Save(login.Person);
				Logins.Save(login);
				Handshakes.Save(handshake);
				GrowerBuyerMatrixes.Save(matrix);

				//SHW Saves or updates the language for SimpleLogin User
				CabPropertyService.SimpleLoginLanguage.Save(languageProperty);

				// En flush alles maar vast.
				Logins.Flush();
			}
		}

		public int? GetSimpleBuyerSeq(ILogin login)
		{
			// Dit is een buitengewoon dure manier om aan een buyersequence te
			// komen. Om onderstaande uit te voeren moeten er 4 queries worden
			// uitgevoerd op de database en worden er 4 volledige entiteiten
			// opgehaald en gevuld met data.
//			decimal result = 0;
//			try
//			{
//				var login = Logins.First(l => l.Sequence == loginSequence);
//				result = login.Person.Organisation.Buyer.Sequence;
//			}
//			catch(Exception ex)
//			{
//				Log.Error("Error GetSimpleBuyerSeq {0}", ex);
//				Log.ErrorFormat("LoginSequence: {0}", loginSequence);
//			}
//			return result;

			// Een simpele UDF is hier veruit de beste keuze.
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"Select dbo.BuyerForLogin(@LoginSequence);"))
			{
				cmd.AddInputParameter("@LoginSequence", login.Sequence);
				return cmd.Execute<int?>();
			}
		}
	}
}