﻿using System;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Domain
{
	///<summary>
	/// Implementatie van ISupplierMailInfoProvider.
	///</summary>
	public class SupplierMailInfoProvider : ISupplierMailInfoProvider
	{
		private const string dutchDateFormat = "dd-MM-yyyy";
		private const string dutchTimeFormat = "HH:mm";
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ICabConnectionProvider ConnectionProvider { get; set; }
		protected IRepository<Buyer> Buyers { get; set; }
		protected IRepository<Grower> Growers { get; set; }
		protected IRepository<Person> Persons { get; set; }
		
		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public SupplierMailInfoProvider(
			ICabConnectionProvider connectionProvider,
			IRepository<Buyer> buyers,
			IRepository<Grower> growers,
			IRepository<Person> persons)
		{
			ConnectionProvider = connectionProvider;
			Buyers = buyers;
			Growers = growers;
			Persons = persons;
	}

		public SupplierMailInfo GetSupplierMailInfo(int orderHeaderSequence)
		{
			using (var con = ConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	ORHE_NO
,	ORHE_DATE
,	ORHE_DESC
,	CDOS_STATUS_DESC
,	ORHE_NETT_HEADER_VALUE
,	ORHE_DELIVERY_DATE
,	CDCA_CASK_DESC
,	ORHE_REMARK
,	ORHE_FK_BUYER
,	ORHE_FK_PERSON
,	ISNULL( PERS_EMAIL, ORGA_EMAIL ) As ToEmail
From
	F_CAB_ORDER_HEA
Inner Join
	F_CAB_CD_ORD_HEA_STATUS
		On	( CDOS_SEQUENCE = ORHE_FK_ORDER_STATUS )
Inner Join
	F_CAB_CD_CASK
		On	( CDCA_SEQUENCE = ORHE_FK_BULK_CASK )
Inner Join
	F_CAB_GROWER
		On	( GROW_SEQUENCE = ORHE_FK_GROWER )
Inner Join
	F_CAB_ORGANISATION
		On	( ORGA_SEQUENCE = GROW_FK_ORGA )

-- Contact Persoon via Handshake Property.
Left Outer Join
	F_CAB_PROPERTY
		On	( PPTY_TYPE = 'F_CAB_HANDSHAKE' )
		And	( PPTY_KEY = dbo.GrowerBuyerHandshakeSequence(ORHE_FK_GROWER, ORHE_FK_BUYER) )
		And	( PPTY_NAME = 'ContactPersoon' )
Left Outer Join
	F_CAB_PERSON
		On	( PERS_SEQUENCE = PPTY_REFERENCE )

Where
	ORHE_SEQUENCE = @orderHeaderSequence
;"))
			{
				cmd.AddInputParameter("@orderHeaderSequence", orderHeaderSequence);
				using (var reader = cmd.ExecuteReader())
				{
					if (!reader.Read())
						throw new ApplicationException("Unable to find specified Order Header.");

					var buyerPerson = Persons.GetById(reader.Get<int>("ORHE_FK_PERSON"));
					var info = new SupplierMailInfo
					{
						From = buyerPerson.Email,
						To = reader.Get<string>("ToEmail"),
						OrderNo = reader.Get<string>("ORHE_NO"),
						OrderDate = reader.Get<DateTime>("ORHE_DATE").ToString(dutchDateFormat),
						OrderDesc = reader.Get<string>("ORHE_DESC"),
						OrderStatus = reader.Get<string>("CDOS_STATUS_DESC"),
						OrderNetto = reader.Get<double>("ORHE_NETT_HEADER_VALUE").ToString("0.00"),
						OrderLeverDatum = reader.Get<DateTime>("ORHE_DELIVERY_DATE").ToString(dutchDateFormat),
						OrderLeverTijd = reader.Get<DateTime>("ORHE_DELIVERY_DATE").ToString(dutchTimeFormat),
						OrderBulkverpakking = reader.Get<string>("CDCA_CASK_DESC"),
						OrderRemark = reader.Get<string>("ORHE_REMARK"),
						Buyer = Buyers.GetById(reader.Get<int>("ORHE_FK_BUYER")),
						Person = buyerPerson
					};

					return info;
				}
			}
		}
	}
}