﻿using System;

namespace LogiCAB.Common.Enums
{
	///<summary>
	/// Geeft een plek aan wat Extension methods voor de Enums.
	///</summary>
	public static class Extensions
	{
		///<summary>
		/// Geeft het character wat in de CAB database gebruikt wordt als
		/// marker voor het opgegeven OfferType.
		///</summary>
		///<param name="offerType">Het OfferType wat vertaald moet worden.</param>
		///<returns>'O' of 'S'</returns>
		///<exception cref="NotSupportedException">Als een andere waarde dan Offer of Stocklist wordt meegeven.</exception>
		public static char GetDbCharacter(this OfferType offerType)
		{
			switch (offerType)
			{
				case OfferType.Offer:
					return 'O';
				case OfferType.Stocklist:
					return 'S';
				default:
					throw new NotSupportedException("Unknown Offerte Type.");
			}
		}
	}
}
