﻿/* Deze file is gegenereerd. Wijzigingen worden niet behouden. Zie de bijbehorende .tt file. */
using System.ComponentModel;

namespace LogiCAB.Common.Enums
{
	/// <summary>
	/// Deze enum bevat de statische inhoud van F_CAB_HANDSHAKE_TYPE.
	/// </summary>
	public enum HandshakeType
	{
		/// <summary>
		/// Geen Handshake Type.
		/// </summary>
		[Description("Geen Handshake Type.")]
		None = -1,

		/// <summary>
		/// Klant van Klant
		/// </summary>
		/// <remarks>
		/// Deze handshake bepaald dat Orga2 klant is van Orga1, waarbij Orga1 verantwoordelijk is voor het beheren van Orga2.
		/// </remarks>
		[Description("Klant van Klant")]
		KVK = 1,

		/// <summary>
		/// Grower Buyer
		/// </summary>
		/// <remarks>
		/// Normal handshake between Grower and Buyer.
		/// </remarks>
		[Description("Grower Buyer")]
		Normal = 2,

	}
}
