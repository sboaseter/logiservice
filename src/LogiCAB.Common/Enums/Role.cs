﻿/* Deze file is gegenereerd. Wijzigingen worden niet behouden. Zie de bijbehorende .tt file. */
using System.ComponentModel;
// ReSharper disable InconsistentNaming

namespace LogiCAB.Common.Enums
{
	/// <summary>
	/// Deze enum bevat de statische inhoud van F_CAB_ROLE.
	/// </summary>
	public enum Role
	{
		/// <summary>
		/// Aanmaken suborder toestaan
		/// </summary>
		[Description("Aanmaken suborder toestaan")]
		PERM_MAKE_SUB = 2,

		/// <summary>
		/// Splitsen van order toestaan
		/// </summary>
		[Description("Splitsen van order toestaan")]
		PERM_SPLIT = 3,

		/// <summary>
		/// Wijzigen APE toestaan
		/// </summary>
		[Description("Wijzigen APE toestaan")]
		PERM_CHANGE_IPU = 4,

		/// <summary>
		/// Bestellen van voorraadlijst
		/// </summary>
		[Description("Bestellen van voorraadlijst")]
		PERM_ORDER_STOCK = 5,

		/// <summary>
		/// Bestellen van aanbiedingslijst
		/// </summary>
		[Description("Bestellen van aanbiedingslijst")]
		PERM_ORDER_OFFER = 6,

		/// <summary>
		/// Toon alleen binnengemelde partijen
		/// </summary>
		[Description("Toon alleen binnengemelde partijen")]
		PERM_ONLY_CHECKEDIN = 7,

		/// <summary>
		/// Voorraad o.b.v. Auto-Publish
		/// </summary>
		[Description("Voorraad o.b.v. Auto-Publish")]
		PERM_AUTO_PUBLISH = 8,

		/// <summary>
		/// Toon ook reserveringscellen
		/// </summary>
		/// <remarks>
		/// Alleen te gebruiken i.c.m. PERM_AUTO_PUBLISH
		/// </remarks>
		[Description("Toon ook reserveringscellen")]
		PERM_SHOW_RESERVED = 9,

		/// <summary>
		/// Beheer klantspecifieke artikelen
		/// </summary>
		/// <remarks>
		/// In tabblad 'Voorkeuren' een Eigen artikelbestand selecteren
		/// </remarks>
		[Description("Beheer klantspecifieke artikelen")]
		PERM_MANAGE_CUSTART = 10,

		/// <summary>
		/// Beheer prijscalculatie facturen
		/// </summary>
		[Description("Beheer prijscalculatie facturen")]
		PERM_MANAGE_PRICES = 11,

		/// <summary>
		/// Toon volumetotalen klantartikel
		/// </summary>
		[Description("Toon volumetotalen klantartikel")]
		PERM_SHOW_VOLUME = 12,

		/// <summary>
		/// Ingave max. prijs en notitie
		/// </summary>
		[Description("Ingave max. prijs en notitie")]
		PERM_SHOW_NOTE = 13,

		/// <summary>
		/// Toon foto bij product
		/// </summary>
		[Description("Toon foto bij product")]
		PERM_SHOW_CAMERA = 14,

		/// <summary>
		/// Overzicht openstaande orders
		/// </summary>
		/// <remarks>
		/// Alleen voor Danners
		/// </remarks>
		[Description("Overzicht openstaande orders")]
		PERM_OVERVIEW_ORDERS = 15,

		/// <summary>
		/// Automatisch orderaanmaak
		/// </summary>
		/// <remarks>
		/// Alleen voor Danners
		/// </remarks>
		[Description("Automatisch orderaanmaak")]
		PERM_AUTO_ORDER = 16,

		/// <summary>
		/// Toon externe url
		/// </summary>
		/// <remarks>
		/// In tabblad 'Communicatie' website(s) ingeven
		/// </remarks>
		[Description("Toon externe url")]
		PERM_ACC_WEBSITE = 17,

		/// <summary>
		/// Beheer verkoopprijs
		/// </summary>
		[Description("Beheer verkoopprijs")]
		PERM_RETAIL_PRICE = 18,

		/// <summary>
		/// Beheer openstaande orders
		/// </summary>
		[Description("Beheer openstaande orders")]
		PERM_MANAGE_ORDERS = 19,

		/// <summary>
		/// Verkoopprijs uit aanbieding/voorraadlijst
		/// </summary>
		/// <remarks>
		/// Alleen te gebruiken i.c.m. PERM_MANAGE_ORDERS
		/// </remarks>
		[Description("Verkoopprijs uit aanbieding/voorraadlijst")]
		PERM_PRICE_FROM_OFFER = 20,

		/// <summary>
		/// Internetshop van klant
		/// </summary>
		[Description("Internetshop van klant")]
		PERM_INTERNETSHOP = 21,

		/// <summary>
		/// Vers van de Kweker Shop
		/// </summary>
		[Description("Vers van de Kweker Shop")]
		VVDK_SHOP = 22,

		/// <summary>
		/// Vers van de Kweker Beheerder
		/// </summary>
		[Description("Vers van de Kweker Beheerder")]
		VVDK_BEHEER = 23,

		/// <summary>
		/// Mag inloggen op de LogiOfferte applicatie.
		/// </summary>
		/// <remarks>
		/// Als deze rol niet aanwezig is mag de gebruiker niet inloggen.
		/// </remarks>
		[Description("Mag inloggen op de LogiOfferte applicatie.")]
		LOGIOFFERTE = 24,

		/// <summary>
		/// LogiOfferte Start Menu
		/// </summary>
		/// <remarks>
		/// Deze rol geeft aan dat de gebruiker start met het hoofdmenu.
		/// </remarks>
		[Description("LogiOfferte Start Menu")]
		LO_START_MENU = 25,

		/// <summary>
		/// LogiOfferte Proces Create Order from Offer
		/// </summary>
		/// <remarks>
		/// Dit proces geeft de gebruiker de mogelijkheid om zelf Orders aan te maken voor zijn Offertes.
		/// </remarks>
		[Description("LogiOfferte Proces Create Order from Offer")]
		LoCreateOrderFromOffer = 27,

		/// <summary>
		/// Shop specifiek beheer van Organisaties.
		/// </summary>
		[Description("Shop specifiek beheer van Organisaties.")]
		ShopManageOrganisation = 28,

		/// <summary>
		/// Globaal beheer van Organisaties.
		/// </summary>
		[Description("Globaal beheer van Organisaties.")]
		GlobalManageOrganisation = 29,

		/// <summary>
		/// Mag gebruik maken van de LogiCAB Services.
		/// </summary>
		[Description("Mag gebruik maken van de LogiCAB Services.")]
		LogiService = 30,

		/// <summary>
		/// LogiOfferte proces om offertes uit te brengen aan eindklanten
		/// </summary>
		[Description("LogiOfferte proces om offertes uit te brengen aan eindklanten")]
		LO_COMM_CREATE_OFFERTE = 31,

		/// <summary>
		/// Identificeert dat de gebruiker een KVK is
		/// </summary>
		[Description("Identificeert dat de gebruiker een KVK is")]
		PERM_FOR_KVK_USER = 32,

		/// <summary>
		/// Basis functie voor stand alone shops.
		/// </summary>
		[Description("Basis functie voor stand alone shops.")]
		STAND_ALONE_SHOP = 33,

		/// <summary>
		/// Basis functie voor shop in combinatie met LF.
		/// </summary>
		[Description("Basis functie voor shop in combinatie met LF.")]
		SHOP_LF = 34,

		/// <summary>
		/// Mag gebruik maken van subklanten
		/// </summary>
		/// <remarks>
		/// Met deze rol mag de gebruiker kvk aanmaken, maar dan zonder opslag. Bij de order kunnen dan de subklanten worden geselecteerd. Bestellen voor subklanten.
		/// </remarks>
		[Description("Mag gebruik maken van subklanten")]
		PERM_SUBCUSTOMERS = 35,

		/// <summary>
		/// KVK Order Sub Klant Selectie
		/// </summary>
		/// <remarks>
		/// Bepaald of in het Offer scherm de SubKlant combo getoond wordt, en of de selectie wordt opgeslagen in de Order Detail. Deze is nodig voor Baardse.
		/// </remarks>
		[Description("KVK Order Sub Klant Selectie")]
		KVK_OrderSubKlantSelectie = 36,

		/// <summary>
		/// De leverancier stuurt automatisch een mail met de orderinfo naar de klant.
		/// </summary>
		/// <remarks>
		/// Als deze rol aanwezig is, stuurt de kweker automatisch een mail naar de klant (buyer). Deze mail bevat informatie over de orderregels die de klant besteld heeft.
		/// </remarks>
		[Description("De leverancier stuurt automatisch een mail met de orderinfo naar de klant.")]
		PERM_AUTO_CONFIRM_ORDER = 37,

	}
}
