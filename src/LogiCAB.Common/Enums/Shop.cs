﻿/* Deze file is gegenereerd. Wijzigingen worden niet behouden. Zie de bijbehorende .tt file. */
using System.ComponentModel;

namespace LogiCAB.Common.Enums
{
	/// <summary>
	/// Deze enum bevat de statische inhoud van F_CAB_SHOP.
	/// </summary>
	public enum Shop
	{
		/// <summary>
		/// LogiCAB
		/// </summary>
		/// <remarks>
		/// Deze shop bevat de bestaande organisaties, functionaliteiten en look en feel.
		/// </remarks>
		[Description("LogiCAB")]
		LogiCAB = 1,

		/// <summary>
		/// Vers van de Kweker
		/// </summary>
		/// <remarks>
		/// Deze shop bevat alleen de organisaties, functionaliteiten en look en feel van Vers van de Kweker.
		/// </remarks>
		[Description("Vers van de Kweker")]
		VVDK = 2,

		/// <summary>
		/// LogiWebshop
		/// </summary>
		/// <remarks>
		/// Deze shop bevat alle functionaliteiten om een demo te kunnen geven.
		/// </remarks>
		[Description("LogiWebshop")]
		LogiWebshop = 11,

		/// <summary>
		/// APH
		/// </summary>
		/// <remarks>
		/// Deze shop bevat de look-en-feel van APH.
		/// </remarks>
		[Description("APH")]
		APH = 41,

		/// <summary>
		/// DGI
		/// </summary>
		/// <remarks>
		/// Deze shop bevat de look-en-feel van DGI.
		/// </remarks>
		[Description("DGI")]
		DGI = 51,

		/// <summary>
		/// Molenaar
		/// </summary>
		/// <remarks>
		/// Deze shop bevat de look-and-feel van Molenaar.
		/// </remarks>
		[Description("Molenaar")]
		MOLENAAR = 61,

		/// <summary>
		/// USEFRESH
		/// </summary>
		/// <remarks>
		/// Deze shop bevat de look-en-feel van Usefresh.
		/// </remarks>
		[Description("USEFRESH")]
		USEFRESH = 71,

		/// <summary>
		/// MIXT
		/// </summary>
		/// <remarks>
		/// Deze shop heeft de look-and-feel van MIXT Creation.
		/// </remarks>
		[Description("MIXT")]
		MIXT = 81,

	}
}
