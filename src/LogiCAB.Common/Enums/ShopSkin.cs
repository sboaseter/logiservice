﻿/* Deze file is gegenereerd. Wijzigingen worden niet behouden. Zie de bijbehorende .tt file. */
using System.ComponentModel;

namespace LogiCAB.Common.Enums
{
	/// <summary>
	/// Deze enum bevat de statische inhoud van F_CAB_SHOP_SKIN.
	/// </summary>
	public enum ShopSkin
	{
		/// <summary>
		/// Standaard skin.
		/// </summary>
		[Description("Standaard skin.")]
		LogiOfferte = 1,

		/// <summary>
		/// Vers van de Kweker skin.
		/// </summary>
		[Description("Vers van de Kweker skin.")]
		VVDK = 2,

		/// <summary>
		/// Oudendijk
		/// </summary>
		[Description("Oudendijk")]
		Oudendijk = 3,

		/// <summary>
		/// Baardse skin
		/// </summary>
		[Description("Baardse skin")]
		BAARDSE = 4,

		/// <summary>
		/// Klant van klant skin
		/// </summary>
		[Description("Klant van klant skin")]
		KVK = 5,

		/// <summary>
		/// APH skin
		/// </summary>
		[Description("APH skin")]
		APH = 6,

		/// <summary>
		/// DGI skin
		/// </summary>
		[Description("DGI skin")]
		DGI = 7,

		/// <summary>
		/// Molenaar skin
		/// </summary>
		[Description("Molenaar skin")]
		Molenaar = 8,

	}
}
