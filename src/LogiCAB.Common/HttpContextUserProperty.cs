using System;
using System.Web;

namespace LogiCAB.Common
{
	///<summary>
	/// Maakt het mogelijk om de huidige Identity automatisch te loggen.
	///</summary>
	public class HttpContextUserProperty
	{
		public override string ToString()
		{
			if (HttpContext.Current == null)
				return String.Empty;
			if (HttpContext.Current.User == null)
				return String.Empty;
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
				return String.Empty;
			return HttpContext.Current.User.Identity.Name;
		}
	}
}