﻿using LogiCAB.Common.Model;

namespace LogiCAB.Common
{
	///<summary>
	/// Een generieke Cab Principal. Het idee hierachter is dat we identiteit
	/// in de applicatie weer gaan geven via deze specificatie. De
	/// LoginPrincipal voldoet aan deze definitie, maar ook de OrgaPrincipal
	/// die niet gebonden hoeft te zijn aan een Person of Login.
	/// 
	/// TODO: Het zou fijn zijn om een type afhankelijke Person beschikbaar te
	/// hebben.
	///</summary>
	public interface ICabPrincipal {

		///<summary>
		/// De Organisation van deze Principal.
		///</summary>
		Organisation Organisation { get; }

		///<summary>
		/// De Buyer van deze Principal. Check met IsBuyer of deze gevuld is.
		///</summary>
		Buyer Buyer { get; }

		///<summary>
		/// True als deze Principal een Buyer is.
		///</summary>
		bool IsBuyer { get; }

		///<summary>
		/// De Grower van deze Principal. Check met IsGrower of deze gevuld is.
		///</summary>
		Grower Grower { get; }

		///<summary>
		/// True als deze Principal een Grower is.
		///</summary>
		bool IsGrower { get; }
	}
}