﻿using System;

namespace LogiCAB.Common
{
	///<summary>
	/// Interface voor interop tussen Login in LogiCab.Common en User in
	/// LogiOfferte.Web.
	///</summary>
	public interface ILogin
	{
		int Sequence { get; }
		string Name { get; }
	}
}
