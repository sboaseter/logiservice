using LogiCAB.Common.Enums;
using LogiFlora.Common;

namespace LogiCAB.Common
{
	///<summary>
	/// Extends the IPropertyProvider with a ShopId and a SkinPath method.
	///</summary>
	public interface IShopPropertyProvider : IPropertyProvider
	{

		///<summary>
		/// The static Sequence of the Shop.
		///</summary>
		int ShopId { get; }

		/// <summary>
		/// The Name of the current Shop.
		/// </summary>
		string ShopName { get; }

		///<summary>
		/// The Enum of the current Shop.
		///</summary>
		Shop Shop { get; }

		///<summary>
		/// The static Sequence of the Skin.
		///</summary>
		int? SkinId { get; }

		/// <summary>
		/// The Name of the current Skin.
		/// </summary>
		string SkinName { get; }

		/// <summary>
		/// Returns an application relative path to the given relativePath for
		/// the current skin.
		/// </summary>
		string SkinPath(string relativePath);
	}
}