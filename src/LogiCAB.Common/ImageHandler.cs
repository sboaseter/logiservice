using System.Drawing.Imaging;

namespace LogiCAB.Common
{
	///<summary>
	/// Helper methods for dealing with images.
	///</summary>
	public static class ImageHandler
	{
		///<summary>
		/// Performs aspect-ratio safe resizing calculations.
		///</summary>
		public static void CalculateSize(int imgW, int imgH, int? passedWidth, int? passedHeight, int? passedMW, out int outW, out int outH)
		{
			var imgRatio = imgH / (double)imgW;

			if (passedWidth.HasValue)
			{
				var parsedW = passedWidth.Value;
				outW = parsedW < 0 ? imgW : parsedW;
				outH = (int)(outW * imgRatio);
			}
			else if (passedHeight.HasValue)
			{
				outH = passedHeight.Value;
				outW = (int)(outH / imgRatio);
				if (passedMW.HasValue)
				{
					var parsedMW = passedMW.Value;
					if (parsedMW < outW)
					{
						outW = parsedMW;
						outH = (int)(outW * imgRatio);
					}
				}
			}
			else
			{
				outW = 200;
				outH = (int)(outW * imgRatio);
			}
		}

		///<summary>
		/// Gets the ImageCodecInfo for the given ImageFormat.
		///</summary>
		public static ImageCodecInfo GetEncoder(ImageFormat format)
		{
			var codecs = ImageCodecInfo.GetImageDecoders();
			foreach (var codec in codecs)
			{
				if (codec.FormatID == format.Guid)
					return codec;
			}
			return null;
		}
	}
}