using System;
using System.Security.Principal;
using LogiCAB.Common.Model;

namespace LogiCAB.Common
{
	///<summary>
	/// Implements an IIdentity using a Model.Login.
	///</summary>
	public class LoginIdentity : IIdentity {

		///<summary>
		/// Creates a LoginIdentity with the given Login.
		/// IsAuthenticated wordt bepaald op basis van de ORAD_STATUS.
		///</summary>
		public LoginIdentity(Login login)
		{
			Name = login.Name;
			IsAuthenticated = login.Person.Organisation.Admin.LoginAllowed;
		}

		public string Name { get; private set; }

		public string AuthenticationType
		{
			get { return "LogiCAB"; }
		}

		public bool IsAuthenticated { get; private set; }
	}
}