﻿using System;
using System.Linq;
using System.Security.Principal;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;

namespace LogiCAB.Common
{
	///<summary>
	/// Implements an IPrincipal using a Model.Login.
	///</summary>
	public class LoginPrincipal : IPrincipal, ICabPrincipal
	{
		///<summary>
		/// De Login waarop deze Principal is gebaseerd.
		///</summary>
		public Login Login { get; private set; }

		///<summary>
		/// De Person van deze Login.
		///</summary>
		public Person Person { get; private set; }

		///<summary>
		/// De Organisation van deze Login.
		///</summary>
		public Organisation Organisation { get; private set; }

		///<summary>
		/// De Buyer van deze Login. Check met IsBuyer of deze gevuld is.
		///</summary>
		public Buyer Buyer { get; private set; }

		///<summary>
		/// True als deze Login een Buyer is.
		///</summary>
		public bool IsBuyer { get; private set; }

		///<summary>
		/// De Grower van deze Login. Check met IsGrower of deze gevuld is.
		///</summary>
		public Grower Grower { get; private set; }

		///<summary>
		/// True als deze Login een Grower is.
		///</summary>
		public bool IsGrower { get; private set; }

		///<summary>
		/// De Rollen die voor deze Principal gelden.
		///</summary>
		protected Role[] Roles { get; private set; }

		public IIdentity Identity { get; private set; }

		///<summary>
		/// Bouwt een LoginPrincipal met de gegeven Login en de verzameling Rollen.
		///</summary>
		public LoginPrincipal(Login login, Role[] roles)
		{
			Login = login;
			Identity = new LoginIdentity(login);
			Roles = roles;

			// We slaan deze login en gerelateerde gegevens alvast plat.
			Person = Login.Person;
			Organisation = Person.Organisation;
			Buyer = Organisation.Buyer;
			IsBuyer = Buyer != null;
			Grower = Organisation.Grower;
			IsGrower = Grower != null;
		}

		///<summary>
		/// Controleert of deze Principal de gegeven Rol heeft.
		///</summary>
		public bool IsInRole(string role)
		{
			return Identity.IsAuthenticated && Roles.Any(r => r.ToString() == role);
		}

		///<summary>
		/// Controleert of deze Principal de gegeven Rol heeft.
		///</summary>
		public bool IsInRole(Role role)
		{
			return Identity.IsAuthenticated && Roles.Contains(role);
		}

		public override string ToString()
		{
			return Identity.Name;
		}

	}
}