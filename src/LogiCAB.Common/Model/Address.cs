using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class Address
	{
        public virtual int Sequence { get; private set; }
        public virtual Organisation Organisation { get; set; }
	    public virtual string Street { get; set; }
        public virtual string AddrNo  { get; set; }
        public virtual string AddrNoSuffix  { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string City { get; set; }
        public virtual Country Country { get; set; }
        public virtual string LocationDesription { get; set; }
        public virtual string GLNLocationCode { get; set; }
        public virtual decimal GPSLatitude { get; set; }
        public virtual string GPSLongtitude { get; set; }
        public virtual int AddressPriority { get; set; }
        public virtual short AutoSyncFlorecom { get; set; }
        public virtual DateTime? EntryDateFlorecom { get; set; }
        public virtual DateTime? ChangeDateFlorecom { get; set; }

		public class AddressMap : CAB.dbo.F_CAB_ADDRESS<Address>
		{
			public AddressMap()
			{
                ADDR_SEQUENCE(x => x.Sequence);
                ADDR_FK_ORGANISATION(x => x.Organisation);
                ADDR_STREET(x => x.Street);
                ADDR_NO(x => x.AddrNo);
                ADDR_NO_SUFFIX(x => x.AddrNoSuffix);
                ADDR_ZIPCODE(x => x.ZipCode);
                ADDR_CITY(x => x.City);
                ADDR_FK_CDCO_SEQ(x => x.Country);
                ADDR_LOCATION_DESCRIPTION(x => x.LocationDesription);
                ADDR_GLN_LOCATION_CODE(x => x.GLNLocationCode);
                ADDR_GPS_LATITUDE_FLORECOM(x => x.GPSLatitude);
                ADDR_GPS_LONGTITUDE_FLORECOM(x => x.GPSLongtitude);
                ADDR_LOCATION_PRIORITY(x => x.AddressPriority);
                ADDR_AUTO_SYNC_FLORECOM(x => x.AutoSyncFlorecom);
                ADDR_ENTRY_DATE_FLORECOM(x => x.EntryDateFlorecom);
                ADDR_CHANGE_DATE_FLORECOM(x => x.ChangeDateFlorecom);
			}
		}

	}
}
