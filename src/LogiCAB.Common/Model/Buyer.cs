using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_BUYER
	///</summary>
	public class Buyer
	{
		public virtual int Sequence { get; private set; }
		public virtual Organisation Organisation { get; set; }
		public virtual string Mobile { get; set; }
		public virtual int? MPS { get; set; }
		public virtual short? ISO { get; set; }
		public virtual short? Florimark { get; set; }
		public virtual short? MPSGap { get; set; }

		public class BuyerMap : CAB.dbo.F_CAB_BUYER<Buyer>
		{
			public BuyerMap()
			{
				BUYR_SEQUENCE(x => x.Sequence);
				BUYR_FK_ORGA(x => x.Organisation);
				BUYR_MOBILE_NO(x => x.Mobile);
				BUYR_MPS(x => x.MPS);
				BUYR_ISO_YN(x => x.ISO);
				BUYR_FLORIMARK_YN(x => x.Florimark);
				BUYR_MPS_GAP(x => x.MPSGap);
			}
		}
	}
}