using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class CABCode
	{
		public virtual int Sequence { get; set; }
        public virtual Picture CABPicture { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
		
		public CABCode()
		{
		}

		public class CABCodeMap : CAB.dbo.F_CAB_CD_CAB_CODE<CABCode>
		{
			public CABCodeMap()
			{
                CABC_SEQUENCE (x => x.Sequence);
                CABC_FK_PICTURE (x => x.CABPicture);
				CABC_CAB_CODE (x => x.Code);
                CABC_CAB_DESC (x => x.Description);
        	}
		}

	}
}
