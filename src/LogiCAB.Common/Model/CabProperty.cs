﻿using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Een specifieke property waarde bij een mogelijke entiteit.
	///</summary>
	public class CabProperty
	{
		/// <summary>
		/// De unieke sleutel van deze Property.
		/// </summary>
		public virtual int Sequence { get; private set; }

		/// <summary>
		/// De identifier van de entiteit waar deze property bij hoort.
		/// Bijvoorbeeld: "F_CAB_OFFER_DETAIL".
		/// </summary>
		public virtual string Type { get; set; }

		/// <summary>
		/// De sleutel van de specifieke instantie van de entiteit waar deze
		/// property bij hoort.
		/// </summary>
		public virtual int Key { get; set; }

		/// <summary>
		/// De naam van de property.
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// De optionele index van de property waarde.
		/// </summary>
		public virtual int? Index { get; set; }

		/// <summary>
		/// Een optionele referentie naar een andere entiteit.
		/// </summary>
		public virtual int? Reference { get; set; }

		/// <summary>
		/// Een optionele YesNo waarde.
		/// </summary>
		public virtual short? YesNo { get; set; }

		/// <summary>
		/// Een optionele textuele waarde.
		/// </summary>
		public virtual string Text { get; set; }

		/// <summary>
		/// Een optionele DateTime waarde.
		/// </summary>
		public virtual DateTime? DateTime { get; set; }

		/// <summary>
		/// Een optionele numerieke waarde.
		/// </summary>
		public virtual decimal? Number { get; set; }

		/// <summary>
		/// Een optionele numerieke waarde met 3 cijfers achter de komma.
		/// </summary>
		public virtual decimal? Money { get; set; }

		// Specific Property Types and Names.

		///<summary>
		/// Returns a new CabProperty initialized as an Offer Detail IsSpecial.
		///</summary>
		public static CabProperty NewOfferDetailIsSpecial(
			int offerDetailSequence,
			bool yesNo)
		{
			return new CabProperty
			{
				Type = "F_CAB_OFFER_DETAILS",
				Key = offerDetailSequence,
				Name = "IsSpecial",
				YesNo = (short)(yesNo ? 1 : 0)
			};
		}

		///<summary>
		/// Returns a new CabProperty initialized as an Offer Detail Leverancier.
		///</summary>
		public static CabProperty NewOfferDetailLeverancier(
			int offerDetailSequence,
			string leverancier)
		{
			return new CabProperty
			{
				Type = "F_CAB_OFFER_DETAILS",
				Key = offerDetailSequence,
				Name = "Leverancier",
				Text = leverancier
			};
		}

		///<summary>
		/// Returns a new CabProperty initialized as an Offer Detail Leverancier.
		///</summary>
		public static CabProperty NewOfferDetailOrgDetailSeq(
			int offerDetailSequence,
			int? orgDetailSeq)
		{
			return new CabProperty
			{
				Type = "F_CAB_OFFER_DETAILS",
				Key = offerDetailSequence,
				Name = "OrgDetailSeq",
				Reference = orgDetailSeq
			};
		}

		///<summary>
		/// Returns a new CabProperty initialized as a Handshake SimpleLogin.
		///</summary>
		public static CabProperty NewHandshakeSimpleLogin(Handshake handshake)
		{
			return new CabProperty
			{
				Type = "F_CAB_HANDSHAKE",
				Key = handshake.Sequence,
				Name = "SimpleLogin",
				YesNo = 1
			};
		}

		///<summary>
		/// Returns a new CabProperty initialized as an Order Original Order.
		///</summary>
		public static CabProperty NewOrderOriginalOrder(
			int orderHeaderSequence,
			int? originalOrderHeaderSequence
		)
		{
			return new CabProperty
			{
				Type = "F_CAB_ORDER_HEA",
				Key = orderHeaderSequence,
				Name = "OriginalOrder",
				Reference = originalOrderHeaderSequence
			};
		}

		///<summary>
		/// Returns a new CabProperty initialized as an OrderDetail Original OrderDetail.
		///</summary>
		public static CabProperty NewOrderDetailOriginalOrderDetail(
			int orderDetailSequence,
			int? originalOrderDetailSequence
		)
		{
			return new CabProperty
			{
				Type = "F_CAB_ORDER_DET",
				Key = orderDetailSequence,
				Name = "OriginalOrderDetail",
				Reference = originalOrderDetailSequence
			};
		}
		

		///<summary>
		/// Mapping class voor CabProperty.
		///</summary>
		public class CabPropertyMap : CAB.dbo.F_CAB_PROPERTY<CabProperty>
		{
			///<summary>
			/// Default Mapping.
			///</summary>
			public CabPropertyMap()
			{
				PPTY_SEQUENCE(x => x.Sequence);

				PPTY_TYPE(x => x.Type);
				PPTY_KEY(x => x.Key);
				PPTY_NAME(x => x.Name);
				PPTY_INDEX(x => x.Index);

				PPTY_REFERENCE(x => x.Reference);
				PPTY_YESNO(x => x.YesNo);
				PPTY_TEXT(x => x.Text);
				PPTY_DATETIME(x => x.DateTime);
				PPTY_NUMBER(x => x.Number);
				PPTY_MONEY(x => x.Money);
			}
		}
	}
}
