﻿using System;

namespace LogiCAB.Common.Model
{
	public class Country
	{
		public virtual int Sequence { get; private set; }
		public virtual string Code { get; set; }
		public virtual string Description { get; set; }
		public virtual char DecimalSeperator { get; set; }
        public virtual char ThousandSeperator { get; set; }

		public class FunctionMap : CAB.dbo.F_CAB_CD_COUNTRY<Country>
		{
			public FunctionMap()
			{
				CDCO_SEQUENCE(x => x.Sequence);
                CDCO_COUNTRY_CODE(x => x.Code).Unique();
                CDCO_DECIMAL_SEPARATOR(x => x.DecimalSeperator);
                CDCO_THOUSAND_SEPARATOR(x => x.ThousandSeperator);
			}
		}

	}
}
