using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class DelMoment
	{
		public virtual int DMDT_WEEKDAY_NUMBER { get; set; }
        public virtual TimeSpan? MAX_ORDER_TIME  { get; set; }
        public virtual TimeSpan? MAX_DELIVER_TIME  { get; set; }
        public virtual string DELIVERY_TYPE { get; set; }
        public virtual string GLN_LOCATION { get; set; }
	}
}
