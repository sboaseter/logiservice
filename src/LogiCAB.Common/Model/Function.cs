﻿using System;

namespace LogiCAB.Common.Model
{
	public class Function
	{
		public virtual int Sequence { get; private set; }
		public virtual string Code { get; set; }
		public virtual string Description { get; set; }
		public virtual string Remark { get; set; }

		public class FunctionMap : CAB.dbo.F_CAB_FUNCTION<Function>
		{
			public FunctionMap()
			{
				FUNC_SEQUENCE(x => x.Sequence);
				FUNC_CODE(x => x.Code).Unique();
				FUNC_DESCRIPTION(x => x.Description);
				FUNC_REMARK(x => x.Remark);
			}
		}

	}
}
