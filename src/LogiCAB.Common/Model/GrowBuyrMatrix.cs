﻿using System;

namespace LogiCAB.Common.Model
{
	public class GrowBuyrMatrix
	{
		public int GBMatrixSequence { get; set; }
		public int BuyerSequence { get; set; }
		public int GrowerSequence { get; set; }
		public DateTime LastOrderDateProp { get; set; }
		public int BlockedProp { get; set; }
	}
}
