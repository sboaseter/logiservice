using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_GROWER
	///</summary>
	public class Grower
	{
		public virtual int Sequence { get; private set; }
		public virtual Organisation Organisation { get; set; }
		public virtual decimal NumberOfM2 { get; set; }
		public virtual decimal? BulkCask { get; set; }
		public virtual string Mobile { get; set; }
		public virtual int? MPS { get; set; }
		public virtual short ISO { get; set; }
		public virtual short Florimark { get; set; }
		public virtual short? MPSGap { get; set; }

		public class GrowerMap : CAB.dbo.F_CAB_GROWER<Grower>
		{
			public GrowerMap()
			{
				GROW_SEQUENCE(x => x.Sequence);
				GROW_FK_ORGA(x => x.Organisation);
				GROW_NUM_OF_M2(x => x.NumberOfM2);
				GROW_FK_BULK_CASK_Map(x => x.BulkCask);
				GROW_MOBILE_NO(x => x.Mobile);
				GROW_MPS(x => x.MPS);
				GROW_ISO_YN(x => x.ISO);
				GROW_FLORIMARK_YN(x => x.Florimark);
				GROW_MPS_GAP(x => x.MPSGap);
			}
		}
	}
}