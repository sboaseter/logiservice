using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class GrowerAssortment
	{
		public virtual int Sequence { get; set; }
        public virtual Picture GrasPicture { get; set; }
        public virtual CABCode CABCode { get; set; }
        public virtual string Description { get; set; }
		
		public GrowerAssortment()
		{
		}

		public class GrowerAssortmentMap : CAB.dbo.F_CAB_GROWER_ASSORTIMENT<GrowerAssortment>
		{
			public GrowerAssortmentMap()
			{
                GRAS_SEQUENCE (x => x.Sequence);
                GRAS_FK_PICTURE (x => x.GrasPicture);
                GRAS_FK_CAB_CODE(x => x.CABCode);
        	}
		}

	}
}
