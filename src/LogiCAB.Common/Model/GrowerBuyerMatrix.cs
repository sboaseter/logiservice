﻿using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_GROWER_BUYER_MATRIX
	///</summary>
	public class GrowerBuyerMatrix
	{
		public virtual int Sequence { get; private set; }
		public virtual Buyer Buyer { get; set; }
		public virtual Grower Grower { get; set; }
		public virtual DateTime? LastOrderDate { get; set; }
		public virtual short Blocked { get; set; }

		public class GrowerBuyerMatrixMap : CAB.dbo.F_CAB_GROWER_BUYER_MATRIX<GrowerBuyerMatrix>
		{
			public GrowerBuyerMatrixMap()
			{
				GAMA_SEQUENCE(x => x.Sequence);
				GAMA_FK_BUYER(x => x.Buyer);
				GAMA_FK_GROWER(x => x.Grower);
				GAMA_LAST_ORDER_DATE(x => x.LastOrderDate);
				GAMA_BLOCKED_YN(x => x.Blocked);
			}
		}
	}
}
