﻿using System;

namespace LogiCAB.Common.Model
{
	/// <summary>
	/// F_CAB_HANDSHAKE
	/// </summary>
	public class Handshake
	{
		public virtual int Sequence { get; private set; }
		public virtual Organisation Orga1 { get; set; }
		public virtual Organisation Orga2 { get; set; }
		public virtual DateTime? RequestDate { get; set; }
		public virtual DateTime? AcceptDate { get; set; }
		public virtual DateTime? ApproveDate { get; set; }

		public virtual short Blocked { get; set; }

		/// <summary>
		/// varchar(255)
		/// </summary>
		public virtual string Mail { get; set; }

		/// <summary>
		/// char(4096) ???
		/// </summary>
		public virtual string Remark { get; set; }


		public class HandshakeMap : CAB.dbo.F_CAB_HANDSHAKE<Handshake>
		{
			public HandshakeMap()
			{
				HAND_SEQUENCE(x => x.Sequence);
				HAND_FK_ORGA_SEQ_GROWER(x => x.Orga1);
				HAND_FK_ORGA_SEQ_BUYER(x => x.Orga2);
				HAND_REQUEST_DATE(x => x.RequestDate);
				HAND_ACCEPT_DATE(x => x.AcceptDate);
				HAND_APPROVE_DATE(x => x.ApproveDate);
				HAND_BLOCKED_YN(x => x.Blocked);
				HAND_REQUEST_MAIL(x => x.Mail);
				HAND_REQUEST_REMARK(x => x.Remark);
			}
		}
	}
}
