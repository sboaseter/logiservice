﻿using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model {
    /// <summary>
    /// F_CAB_HANDSHAKE_OPTIONS
    /// </summary>
    public class HandshakeOption {
        public virtual int Sequence { get; private set; }
        public virtual Handshake Handshake { get; set; }
        public virtual int? DeliveryMomentHeader { get; set; }
        public virtual short AutoEKT { get; set; }
        public virtual short AutoKWB { get; set; }
        public virtual short AutoConfirm { get; set; }
        public virtual Address DeliveryAddress { get; set; }
        public virtual string DeliveryType { get; set; }
        public virtual DateTime? LatestDeliveryTime { get; set; }
        public virtual short DelEveryDayStock { get; set; }
        public virtual short DelEveryDayOffer { get; set; }
        public virtual DateTime? CreatedOn { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? ModifiedOn { get; set; }
        public virtual string ModifiedBy { get; set; }

        public class HandshakeOptionMap : CAB.dbo.F_CAB_HANDSHAKE_OPTIONS<HandshakeOption> {
            public HandshakeOptionMap() {
                HNDO_SEQUENCE(x => x.Sequence);
                HNDO_FK_HAND_SEQ(x => x.Handshake);
                HNDO_FK_ADDR_SEQ(x => x.DeliveryAddress);
                HNDO_DELIVERY_TYPE(x => x.DeliveryType);
                HNDO_LATEST_DELIVERY_TIME(x => x.LatestDeliveryTime);
                HNDO_AUTO_EKT_YN(x => x.AutoEKT);
                HNDO_AUTO_KWB_YN(x => x.AutoKWB);
                HNDO_AUTO_CONFIRM_YN(x => x.AutoConfirm);
                HNDO_DELIVERY_OFFER_ALL_WORKDAYS_YN(x => x.DelEveryDayOffer);
                HNDO_DELIVERY_STOCK_ALL_WORKDAYS_YN(x => x.DelEveryDayStock);
                HNDO_FK_DMHD_SEQ_Map(x => x.DeliveryMomentHeader);
                HNDO_CREATED_ON(x => x.CreatedOn);
                HNDO_CREATED_BY(x => x.CreatedBy);
                HNDO_MODIFIED_ON(x => x.ModifiedOn);
                HNDO_MODIFIED_BY(x => x.ModifiedBy);
            }
        }
    }
}
