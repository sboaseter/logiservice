using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_LOGIN
	///</summary>
	public class Login : ILogin
	{
		public virtual int Sequence { get; set; }
		public virtual string Name { get; set; }
		public virtual string PasswordHash { get; set; }
		public virtual string Password { get; set; }
		public virtual Person Person { get; set; }
		public virtual int LoginGroup { get; set; }
		public virtual short Blocked { get; set; }
        public virtual int? Language { get; set; }

		public class LoginMap : CAB.dbo.F_CAB_LOGIN<Login>
		{
			public LoginMap()
			{
				LOGI_SEQUENCE(x => x.Sequence);
				LOGI_USER_NAME(x => x.Name).Unique();
				LOGI_PASSWORD(x => x.PasswordHash);
				LOGI_PASS(x => x.Password);
				LOGI_FK_PERSON(x => x.Person);
				LOGI_FK_LOGIN_GROUP_Map(x => x.LoginGroup);
				LOGI_BLOCKED_YN(x => x.Blocked);
                LOGI_FK_CDLA_SEQ_Map(x => x.Language);
			}
		}
	}
}