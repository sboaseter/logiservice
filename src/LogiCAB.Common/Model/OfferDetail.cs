using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class OfferDetail
	{
		public virtual int Sequence { get; set; }
        public virtual Picture OfferPicture { get; set; }
        public virtual CABCode CABCode { get; set; }
        public virtual GrowerAssortment GrowerAssortment { get; set; }
        
		
		public OfferDetail()
		{
		}

		public class OfferDetailMap : CAB.dbo.F_CAB_OFFER_DETAILS<OfferDetail>
		{
            public OfferDetailMap()
			{
                OFDT_SEQUENCE (x => x.Sequence);
                OFDT_FK_PICTURE (x => x.OfferPicture);
				OFDT_FK_CAB_CODE (x => x.CABCode);
                OFDT_FK_GROWER_ASSORTIMENT (x => x.GrowerAssortment);
        	}
		}
	}
}
