﻿using System;

namespace LogiCAB.Common.Model 
{

	public class    OfferDetailView 
    {
	
        public virtual int Sequence { get; set; }
		public virtual int? HeaderSequence { get; set; }
		public virtual Grower Grower { get; set; }
		public virtual Buyer Buyer { get; set; }
		public virtual int Shop { get; set; }
        public virtual int? DeliveryMomentHeader { get; set; }
		public virtual string OfferTypeString { get; set; }
		public virtual int MainGroup { get; set; }
		public virtual decimal UnitsAvailable { get; set; }
        public virtual decimal ItemsPerUnit { get; set; }
        public virtual decimal ItemPrice { get; set; }
        public virtual DateTime ValidFrom { get; set; }
        public virtual DateTime ValidTo { get; set; }
        public virtual DateTime? LatestOrderDateTime { get; set; }
        public virtual DateTime? LatestDeliveryDateTime { get; set; }
        public virtual int PartyDetailSeq { get; set; }
		
        public class OfferDetailViewMap : CAB.dbo.V_OFFER_DETAILS<OfferDetailView> 
        {
			
            public OfferDetailViewMap() 
            {
				// Deze moet even gehackt. Views hebben geen PK.
				Id(x => x.Sequence, OFDT_SEQUENCE_Name).GeneratedBy.Custom("identity")
					.Not.Nullable()
					.Length(9)
					;
				OFHD_SEQUENCE(x => x.HeaderSequence);
				References(x => x.Grower, GROW_SEQUENCE_Name);
				References(x => x.Buyer, BUYR_SEQUENCE_Name);
				OFHD_FK_SHOP(x => x.Shop);
                OFHD_FK_DMHD_SEQ_Map(x => x.DeliveryMomentHeader);
				OFHD_OFFER_TYPE(x => x.OfferTypeString);
				OFHD_FK_MAIN_GROUP(x => x.MainGroup);
                OFHD_VALID_FROM(x => x.ValidFrom);
                OFHD_VALID_TO(x => x.ValidTo);
                LATEST_DELIVERY_DATE_TIME(x => x.LatestDeliveryDateTime);
                LATEST_ORDER_DATE_TIME(x => x.LatestOrderDateTime);
                UnitsAvailable(x => x.UnitsAvailable);
                ItemsPerUnit(x => x.ItemsPerUnit);
                ItemPrice(x => x.ItemPrice);
			}
		}
	}
}