﻿namespace LogiCAB.Common.Model {
	public class OrderDetailView {
		public virtual int Sequence { get; set; }
		public virtual int? HeaderSequence { get; set; }
		public virtual string CabDescription { get; set; }
		public virtual string CustomerCode { get; set; }
		public virtual decimal ItemPrice { get; set; }
		public virtual decimal NumberOfUnits { get; set; }
		public virtual decimal NumberOfItems { get; set; }
		public virtual decimal NumberOfItemsPerUnit { get; set; }
		public virtual decimal UnitsPerLayer { get; set; }
		public virtual decimal LayersPerTrolley { get; set; }
		public virtual string Leverancier { get; set; }
		public virtual string GrowerOrgaName { get; set; }
		public virtual int OfferDetailSequence { get; set; }

		/// <summary>
		/// Berekent NumberOfItems / NumberOfItemsPerUnit / UnitsPerLayer / LayersPerTrolley.
		/// Oftewel het deel van de Trolley wat door deze detail regel gevuld wordt.
		/// </summary>
		public virtual decimal CalculateVolumePartOfTrolley() {
			var ipUnit = NumberOfItemsPerUnit != decimal.Zero ? NumberOfItemsPerUnit : 1;
			var upLayer = UnitsPerLayer != decimal.Zero ? UnitsPerLayer : 1;
			var lpTrolley = LayersPerTrolley != decimal.Zero ? LayersPerTrolley : 1;

			return NumberOfItems / ipUnit / upLayer / lpTrolley;
		}

		public class OrderDetailViewMap : CAB.dbo.V_ORDER_DETAILS<OrderDetailView> {
			public OrderDetailViewMap() {
				// Deze moet even gehackt. Views hebben geen PK.
				Id(x => x.Sequence, ORDE_SEQUENCE_Name).GeneratedBy.Custom("identity")
					.Not.Nullable()
					.Length(9)
				;
				//				ORDE_SEQUENCE(x => x.Sequence);
				ORHE_SEQUENCE(x => x.HeaderSequence); // Views hebben geen relaties.
				CABC_CAB_DESC(x => x.CabDescription);
				ORDE_CUSTOMER_CODE(x => x.CustomerCode);
				ORDE_ITEM_PRICE(x => x.ItemPrice);
				ORDE_NUM_OF_UNITS(x => x.NumberOfUnits);
				ORDE_NUM_OF_ITEMS(x => x.NumberOfItems);
				ORDE_NUM_OF_ITEMS_PER_UNIT(x => x.NumberOfItemsPerUnit);
				CACA_UNITS_PER_LAYER(x => x.UnitsPerLayer);
				CACA_LAYERS_PER_TROLLEY(x => x.LayersPerTrolley);
				Leverancier(x => x.Leverancier);
				GrowerOrgaName(x => x.GrowerOrgaName);
			}
		}
	}
}