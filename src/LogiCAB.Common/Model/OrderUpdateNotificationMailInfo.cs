﻿using System;
using LogiCAB.Common.Enums;
using LogiFlora.Common.Mail;
using StringTemplateModel;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Some information for sending the Supplier Mail.
	///</summary>
	[StringTemplate("OrderUpdateNotificationMail")]
	public class OrderUpdateNotificationMailInfo : ITemplateMailModel
	{
		// Deze properties bieden input voor de Template.
		public int OrderSequence { get; set; }
		public string OrderNo { get; set; }
		public string OrderDate { get; set; }
		public string OrderDesc { get; set; }
		public string OrderStatus { get; set; }
		public string OrderRemark { get; set; }
		public string GrowerName { get; set; }
		public string ShopBaseUrl { get; set; }
		public string OrderOverviewPageName { get; set; }
		public Shop Shop { get; set; }

		public Buyer Buyer { get; set; }
		public Person Person { get; set; }

		// Deze properties zijn voor de ITemplateMailModel.
		[StringTemplateIgnore] public string From { get; set; }
		[StringTemplateIgnore] public string To { get; set; }
		[StringTemplateIgnore] public string CC { get; set; }

		// Deze properties worden gevuld door de StringTemplateModelBuilder.
		[StringTemplate] public string Subject { get; set; }
		[StringTemplate] public string Body { get; set; }

	}
}