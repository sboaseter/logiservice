using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_ORGANISATION_ADMIN
	///</summary>
	public class OrganisationAdmin
	{

		public virtual int Sequence { get; private set; }
		public virtual Organisation Organisation { get; set; }
		public virtual int Status { get; set; }
		public virtual int MemberType { get; set; }
		public virtual DateTime? Date { get; set; }

		///<summary>
		/// Is True als deze organisatie geldig is.
		///</summary>
		public virtual bool LoginAllowed {
			get
			{
				switch (Status)
				{
					case 4: // Naam of wachtwoord niet meer geldig
						return false;
					case 5: // Naam of wachtwoord niet meer geldig
						if (!Date.HasValue)
							return false;
						if (Date < DateTime.Now)
							return false;
						break;
				}
				return true;
			}
		}

		public class OrganisationAdminMap : CAB.dbo.F_CAB_ORGANISATION_ADMIN<OrganisationAdmin>
		{
			public OrganisationAdminMap()
			{
				ORAD_SEQUENCE(x => x.Sequence);
				ORAD_FK_ORGANISATION(x => x.Organisation);
				ORAD_STATUS(x => x.Status);
				ORAD_MEMBERTYPE(x => x.MemberType);
				ORAD_DATE(x => x.Date);
			}
		}
	}
}