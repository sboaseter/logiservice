﻿using System;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Additionele Info velden voor een Organisation.
	///</summary>
	public class OrganisationInfo
	{
		public virtual int Sequence { get; set; }
		public virtual DateTime CreatedOn { get; set; }
		public virtual string CreatedBy { get; set; }
		public virtual DateTime ModifiedOn { get; set; }
		public virtual string ModifiedBy { get; set; }
		public virtual Organisation Organisation { get; set; }
		public virtual string ShippingDays { get; set; }
		public virtual TimeSpan? Deadline { get; set; }
		//SHW: Voor de contactpersoon van de Simplelogin
		public virtual int? BuyrPersonSequence { get; set; }
		public virtual int OrderDeliverDays { get; set; }
		public virtual int OfferDeliverDays { get; set; }
		public virtual string ContactName { get; set; }
        public virtual string OrderName { get;  set; }
		public virtual string ContactPhone { get; set; }
		public virtual string ContactFax { get; set; }
		public virtual string ContactEmail { get; set; }

		public class OrganisationInfoMap : CAB.dbo.F_CAB_ORGANISATION_INFO<OrganisationInfo>
		{
			public OrganisationInfoMap() {
				ORGI_SEQUENCE(x => x.Sequence);
				ORGI_CREATED_ON(x => x.CreatedOn);
				ORGI_CREATED_BY(x => x.CreatedBy);
				ORGI_MODIFIED_ON(x => x.ModifiedOn);
				ORGI_MODIFIED_BY(x => x.ModifiedBy);
				ORGI_FK_ORGA_SEQ(x => x.Organisation);
				ORGI_SHIPPING_DAYS(x => x.ShippingDays);
				Map(x => x.Deadline, "ORGI_DEADLINE").CustomType("TimeAsTimeSpan");
//				ORGI_DEADLINE(x => x.Deadline);
				//SHW: Voor de contactpersoon van de Simplelogin
				ORGI_FK_PERS_BUYER_SEQ_Map(x => x.BuyrPersonSequence);
				ORGI_ORDER_DELIVER_DAYS(x => x.OrderDeliverDays);
				ORGI_OFFER_DELIVER_DAYS(x => x.OfferDeliverDays);
				ORGI_CONTACT_NAME(x => x.ContactName);
				ORGI_CONTACT_PHONE_NO(x => x.ContactPhone);
				ORGI_CONTACT_FAX_NO(x => x.ContactFax);
				ORGI_CONTACT_EMAIL(x => x.ContactEmail);
                ORGI_ORDER_NAME(x => x.OrderName);
			}
		}
	}
}