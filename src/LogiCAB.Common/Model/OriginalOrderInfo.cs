﻿using System;
using LogiCAB.Common.Enums;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Simpele class voor de dataset van GetOriginalOrderInfo in ICommonOrderProvider.
	///</summary>
	public class OriginalOrderInfo
	{
		///<summary>
		/// De OfferDetailSequence van de Parent Offer Detail.
		///</summary>
		public int OfferDetailSequence { get; set; }

		/// <summary>
		/// De Sequence van de Child Order Detail regel.
		/// </summary>
		public int OrderDetailSequence { get; set; }

		///<summary>
		/// De Description van de Child Order.
		///</summary>
		public string OrderDesc { get; set; }

		///<summary>
		/// De Datum van de Child Order.
		///</summary>
		public DateTime OrderDeliveryDate { get; set; }

		///<summary>
		/// De Buyer Sequence voor de Parent Order. Dit is de Buyer Sequence
		/// van de Commissionair.
		///</summary>
		public int BuyerSequence { get; set; }

		///<summary>
		/// De Grower Sequence voor de Parent Order. Dit is de Grower Sequence
		/// van de Supplier.
		///</summary>
		public int GrowerSequence { get; set; }

		///<summary>
		/// De Person Sequence voor de Parent Order. Dit is voorlopig een
		/// willekeurig Person van de Commissionair.
		///</summary>
		public int PersonSequence { get; set; }

		///<summary>
		/// De Remark van de Child Order.
		///</summary>
		public string OrderRemark { get; set; }

		///<summary>
		/// De Shop Sequence van de Parent Offer.
		///</summary>
		public int ShopSequence { get; set; }

		/// <summary>
		/// Het aantal units besteld in de Child Order.
		/// </summary>
		public decimal NumOfUnits { get; set; }

		/// <summary>
		/// Het aantal units beschikbaar in de Parent Offer.
		/// </summary>
		public decimal NumUnitsAvailable { get; set; }

		///<summary>
		/// Het Type van deze Order.
		///</summary>
		public OfferType OfferType { get; set; }
	}
}