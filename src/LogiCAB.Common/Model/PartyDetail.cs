﻿using System;

namespace LogiCAB.Common.Model {
	///<summary>
	/// F_CAB_PARTY_DETAIL
	///</summary>
	public class PartyDetail {
		public virtual int Sequence { get; private set; }
		public virtual int HeaderSequence { get; set; }
		public virtual int CabCode { get; set; }
		public virtual int Caca { get; set; }
		public virtual decimal NumItems { get; set; }
		public virtual decimal NumItemsInitial { get; set; }

		public class PartyDetailMap : CAB.dbo.F_CAB_PARTY_DETAIL<PartyDetail> {
			public PartyDetailMap()
			{
				Table("F_CAB_PARTY_DETAIL");
				Id(x => x.Sequence).Column("PYDT_SEQUENCE");
				Map(x => x.HeaderSequence).Column("PYDT_FK_PYHD");
				Map(x => x.CabCode).Column("PYDT_FK_CAB_CODE");
				Map(x => x.Caca).Column("PYDT_FK_CACA_SEQ");
				Map(x => x.NumItems).Column("PYDT_NUM_OF_ITEMS");
				Map(x => x.NumItemsInitial).Column("PYDT_NUM_OF_ITEMS_INITIAL");
				//PYDT_SEQUENCE(x => x.Sequence);
				//PYDT_FK_PYHD(x => x.HeaderSequence);
				//PYDT_FK_CAB_CODE(x => x.CabCode);
				//PYDT_FK_CACA_SEQ(x => x.Caca);
				//PYDT_NUM_OF_ITEMS(x => x.NumItems);
				//PYDT_NUM_OF_ITEMS_INITIAL(x => x.NumItemsInitial);
			}
		}
	}
}