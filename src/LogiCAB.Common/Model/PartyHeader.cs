﻿using System;

namespace LogiCAB.Common.Model {
	///<summary>
	/// F_CAB_PARTY_HEADER
	///</summary>
	public class PartyHeader {
		public virtual int Sequence { get; private set; }
		public virtual int Grower { get; set; }
		public virtual DateTime ValidFrom { get; set; }
		public virtual DateTime ValidTo { get; set; }

		public class PartyHeaderMap : CAB.dbo.F_CAB_PARTY_HEADER<PartyHeader> {
			public PartyHeaderMap()
			{
				Table("F_CAB_PARTY_HEADER");
				Id(x => x.Sequence).Column("PYHD_SEQUENCE");
				Map(x => x.Grower).Column("PYHD_FK_GROWER");
				Map(x => x.ValidFrom).Column("PYHD_VALID_FROM");
				Map(x => x.ValidTo).Column("PYHD_VALID_TO");
				//PYHD_SEQUENCE(x => x.Sequence);
				//PYHD_FK_GROWER(x => x.Grower);
				//PYHD_VALID_FROM(x => x.ValidFrom);
				//PYHD_VALID_TO(x => x.ValidTo);
			}
		}
	}
}