using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
	public class Picture
	{
		public virtual int Sequence { get; set; }
		public virtual byte [] PictureBlob { get; set; }
		public virtual string Description { get; set; }

		public Picture()
		{
		}

		public class PictureMap : CAB.dbo.F_CAB_PICTURE<Picture>
		{
			public PictureMap()
			{
				PICT_SEQUENCE(x => x.Sequence);
				PICT_ARTDESC(x => x.Description);
				PICT_PICTURE(x => x.PictureBlob);
			}
		}

	}
}
