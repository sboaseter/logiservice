﻿using System;

namespace LogiCAB.Common.Model
{
	public class QueuePictureKiller
	{
		public virtual int Sequence { get; private set; }
		public virtual int CabCodeSequence { get; set; }

		public class QueuePictureKillerMap : CAB.dbo.F_CAB_QUEUE_PICTURE_KILLER<QueuePictureKiller>
		{
			public QueuePictureKillerMap()
			{
				QPKL_SEQUENCE(x => x.Sequence);
				QPKL_FK_CAB_CODE_Map(x => x.CabCodeSequence);
			}
		}
	}
}
