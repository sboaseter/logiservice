using System;
using LogiCAB.Contract.PicturesService;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// F_CAB_QUEUE_PROCESSING
	///</summary>
	public class QueueProcessing
	{
		public virtual int Sequence { get; private set; }
		public virtual Organisation Organisation { get; set; }
        public virtual int OrderSeq { get; set; }
        public virtual int OfferSeq { get; set; }
        public virtual int OrgaSeq { get; set; }
        public virtual int HandShakeSeq { get; set; }
        public virtual int PictureRefSeq { get; set; }
        public virtual string DatabasePictType { get; set; }
        public virtual DateTime InsertedOn { get; private set; }
        public virtual DateTime? ProcessedOn { get; set; }

        ///<summary>
        /// PictureType omzetten van string naar Type
        ///</summary>
        public virtual PictureType PictureRefType
        {
            get
            {
                switch (DatabasePictType)
                {
                    case "CAB":
                        return PictureType.CAB;
                    case "OFDT":
                        return PictureType.OFDT;
                    case "GRAS":
                        return PictureType.GRAS;
                    default:
                        return PictureType.UNDEF;
                }
            }
            set
            {
                switch (value)
                {
                    case PictureType.CAB:
                        DatabasePictType = "CAB";
                        break;
                    case PictureType.OFDT:
                        DatabasePictType = "OFDT";
                        break;
                    case PictureType.GRAS:
                        DatabasePictType = "GRAS";
                        break;
                    case PictureType.UNDEF:
                        DatabasePictType = "";
                        break;
                }
            }
        }

        ///<summary>
		/// Is True als de het record nog niet is verwerkt.
		///</summary>
        public virtual bool ToProcess
        {
            get
            {
                if (ProcessedOn.HasValue)
                    return false;

                return true;
            }
        }

		public class QueueProcessingMap : CAB.dbo.F_CAB_QUEUE_PROCESSING<QueueProcessing>
		{
			public QueueProcessingMap()
			{
                QPRO_SEQUENCE(x => x.Sequence);
				QPRO_FK_ORGA_SEQ(x => x.Organisation);
                QPRO_FK_ORGA_SEQ_Map(x => x.OrgaSeq);
                QPRO_FK_ORHD_SEQ_Map(x => x.OrderSeq);
                QPRO_FK_OFHD_SEQ_Map(x => x.OfferSeq);
                QPRO_FK_HAND_SEQ_Map(x => x.HandShakeSeq);
                QPRO_FK_PICT_REF_SEQ_Map(x => x.PictureRefSeq);
                QPRO_FK_PICT_REF_TYPE(x => x.DatabasePictType);
                QPRO_PROCESSED_ON(x => x.ProcessedOn);
                QPRO_INSERTED_ON(x => x.InsertedOn);
			}
		}
	}
}