﻿using System;

namespace LogiCAB.Common.Model
{
	public class ServiceTicket
	{
		public virtual int Sequence { get; private set; }
		public virtual string Ticket { get; set; }
		public virtual Login Login { get; set; }
		public virtual DateTime Created { get; set; }
		public virtual DateTime LastRequest { get; set; }
		public virtual int RequestSequence { get; set; }

		public class ServiceTicketMap : CAB.dbo.F_CAB_SERVICE_TICKET<ServiceTicket>
		{
			public ServiceTicketMap()
			{
				SETT_SEQUENCE(x => x.Sequence);
				SETT_TICKET(x => x.Ticket);
				SETT_FK_LOGI(x => x.Login);
				SETT_CREATED(x => x.Created);
				SETT_LASTREQUEST(x => x.LastRequest);
				SETT_REQUESTSEQUENCE(x => x.RequestSequence);
			}
		}
	}
}
