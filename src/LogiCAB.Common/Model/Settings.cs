﻿using System;
using System.Collections.Generic;

namespace LogiCAB.Common.Model
{
    /// <summary>
    /// F_CAB_SETTINGS
    /// </summary>
    public class Settings
    {
        public virtual int Sequence { get; private set; }
        public virtual int? ShopSeq { get; set; }
        public virtual int? OrgaSeq { get; set; }
        public virtual int? PersSeq { get; set; }
        public virtual string SettingCodeID { get; set; }
        public virtual string SettingDescription { get; set; }
        public virtual string SettingValue { get; set; }

        public class SettingsMap : CAB.dbo.F_CAB_SETTINGS<Settings>
        {
            public SettingsMap()
            {
                SETG_SEQUENCE(x => x.Sequence);
                SETG_FK_SHOP_SEQ_Map(x => x.ShopSeq);
                SETG_FK_ORGA_SEQ_Map(x => x.OrgaSeq);
                SETG_FK_PERS_SEQ_Map(x => x.PersSeq);
                SETG_CODE_ID(x => x.SettingCodeID);
                SETG_DESCRIPTION(x => x.SettingDescription);
                SETG_VALUE(x => x.SettingValue);
            }
        }
    }
}
