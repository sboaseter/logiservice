﻿using LogiCAB.Common.Enums;

namespace LogiCAB.Common.Model
{
	public class ShopEvidence
	{
		public virtual int Sequence { get; set; }
		public virtual Shop Shop { get; set; }
		public virtual int? SkinId { get; set; }
		public virtual string Service { get; set; }
		public virtual string Type { get; set; }
		public virtual string Value { get; set; }
		public virtual decimal? Weight { get; set; }

		public virtual ShopSkin? Skin
		{
			get
			{
				return SkinId.HasValue ? (ShopSkin?)SkinId : null;
			}
		}

		public class ShopEvidenceMap : CAB.dbo.F_CAB_SHOP_EVIDENCE<ShopEvidence>
		{
			public ShopEvidenceMap()
			{
				SPEE_SEQUENCE(x => x.Sequence);
				SPEE_FK_SHOP_Map(x => x.Shop); // Gebruik map, geen relatie.
				SPEE_FK_SPSN_Map(x => x.SkinId); // Gebruik map, geen relatie.
				SPEE_SERVICE(x => x.Service);
				SPEE_TYPE(x => x.Type);
				SPEE_VALUE(x => x.Value);
				SPEE_WEIGHT(x => x.Weight);
			}
		}
	}
}
