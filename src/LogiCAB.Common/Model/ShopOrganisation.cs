﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Enums;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// ShopOrganisation is in principe een relatie tabel, maar heeft zelf weer
	/// een relatie tabel met Function, dus willen we deze als entiteit
	/// beschikbaar hebben.
	///</summary>
	public class ShopOrganisation
	{
		public virtual int Sequence { get; private set; }
		public virtual Shop Shop { get; set; }
		public virtual Organisation Organisation { get; set; }

		public virtual IList<Function> Functions { get; set; }

		public ShopOrganisation()
		{
			Functions = new List<Function>();
		}

		public class ShopOrganisationMap : CAB.dbo.F_CAB_SHOP_ORGANISATION<ShopOrganisation>
		{
			public ShopOrganisationMap()
			{
				SPON_SEQUENCE(x => x.Sequence);
				SPON_FK_SHOP_Map(x => x.Shop);
				SPON_FK_ORGA(x => x.Organisation);

				HasManyToMany(x => x.Functions)
					.Table(CAB.dbo.F_CAB_SHOP_ORGANISATION_FUNCTION<int>.TableName)
					.ParentKeyColumn(CAB.dbo.F_CAB_SHOP_ORGANISATION_FUNCTION<int>.SPOF_FK_SPON_Name)
					.ChildKeyColumn(CAB.dbo.F_CAB_SHOP_ORGANISATION_FUNCTION<int>.SPOF_FK_FUNC_Name)
					.Cascade.SaveUpdate();
			}
		}
	}
}
