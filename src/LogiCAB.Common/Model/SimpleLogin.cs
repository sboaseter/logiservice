﻿using System;

namespace LogiCAB.Common.Model
{
	/// <summary>
	/// Deze classe biedt de mogelijkheid om 'simpele' KvK organisaties aan te maken.
	/// </summary>
	public class SimpleLogin : ILogin
	{
		///<summary>
		/// De identifier van de SimpleLogin. Komt overeen met de LOGI_SEQUENCE.
		///</summary>
		public virtual int Sequence { get; set; }

		///<summary>
		/// De naam van de SimpleLogin.
		///</summary>
		public string Name
		{
			get { return Login; }
		}

		// Orga velden.

		/// <summary>
		/// ORGA_SEQUENCE, De Sequence van de Organisatie van deze SimpleLogin. Wordt gevuld bij het ophalen.
		/// </summary>
		public int OrganisatieSequence { get; set; }

		///<summary>
		/// ORGA_NAME
		///</summary>
		public virtual string Organisatie { get; set; }

		///<summary>
		/// ORGA_PHONE_NO
		///</summary>
		public virtual string Phone { get; set; }

		///<summary>
		/// ORGA_EMAIL
		///</summary>
		public virtual string Email { get; set; }

		// Login velden.

		/// <summary>
		/// SHW: Sequence of the language for the user
		/// </summary>
		public virtual int LanguageSeq { get; set; }
		
		///<summary>
		/// LOGI_USER_NAME
		///</summary>
		public virtual string Login { get; set; }

		///<summary>
		/// LOGI_PASS
		///</summary>
		public virtual string Password { get; set; }

		///<summary>
		/// LOGI_BLOCKED_YN
		///</summary>
		public virtual short Blocked { get; set; }

		// Relatie velden.

		///<summary>
		/// De 'eigenaar' van deze SimpleLogin.
		///</summary>
		public virtual Organisation Eigenaar { get; set; }
	}
}