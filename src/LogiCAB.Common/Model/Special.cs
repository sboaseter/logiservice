﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Special order informatie.
	///</summary>
	public class Special
	{
		public int GrowerSequence { get; set; }

		public int BuyerSequence { get; set; }

		public int OrderHeaderSequence { get; set; }

		public int OrdereDetailSequence { get; set; }

		public int PictureSequence { get; set; }

		public string PictureType { get; set; }

		public int CabCodeSequence { get; set; }

		public string CabDescription { get; set; }

	}
}
