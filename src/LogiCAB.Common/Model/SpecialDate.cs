﻿using System;

namespace LogiCAB.Common.Model
{
    /// <summary>
    /// F_CAB_SPECIAL_DATES
    /// </summary>
    public class SpecialDate
    {
        public virtual int Sequence { get; private set; }
        public virtual DateTime SpecialDay { get; set; }
        public virtual String Description { get; set; }
        private String _SpecialType;
        public virtual String SpecialType
        {
            get { return _SpecialType.ToUpper(); }
            set { _SpecialType = value; }
        }
    


        public virtual short Blocked { get; set; }

        /// <summary>
        /// varchar(255)
        /// </summary>
        public virtual string Mail { get; set; }

        /// <summary>
        /// char(4096) ???
        /// </summary>
        public virtual string Remark { get; set; }


        public class SpecialDateMap : CAB.dbo.F_CAB_SPECIAL_DATES<SpecialDate>
        {
            public SpecialDateMap()
            {
                SPDA_SEQUENCE(x => x.Sequence);
                SPDA_DATETIME(x => x.SpecialDay);
                SPDA_DESCRIPTION(x => x.Description);
                SPDA_TYPE(x => x.SpecialType);
            }
        }
    }
}
