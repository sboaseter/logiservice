﻿using System;
using LogiFlora.Common.Mail;
using StringTemplateModel;

namespace LogiCAB.Common.Model
{
	///<summary>
	/// Some information for sending the Supplier Mail.
	///</summary>
	[StringTemplate("SupplierMail")]
	public class SupplierMailInfo : ITemplateMailModel
	{
		// Deze properties bieden input voor de Template.
		public string OrderNo { get; set; }
		public string OrderDate { get; set; }
		public string OrderDesc { get; set; }
		public string OrderStatus { get; set; }
		public string OrderNetto { get; set; }
		public string OrderLeverDatum { get; set; }
		public string OrderLeverTijd { get; set; }
		public string OrderBulkverpakking { get; set; }
		public string OrderRemark { get; set; }

		public Buyer Buyer { get; set; }
		public Person Person { get; set; }

		// Deze properties zijn voor de ITemplateMailModel.
		[StringTemplateIgnore] public string From { get; set; }
		[StringTemplateIgnore] public string To { get; set; }
		[StringTemplateIgnore] public string CC { get; set; }

		// Deze properties worden gevuld door de StringTemplateModelBuilder.
		[StringTemplate] public string Subject { get; set; }
		[StringTemplate] public string Body { get; set; }
	}
}