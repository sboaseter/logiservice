﻿using LogiCAB.Common.Model;

namespace LogiCAB.Common
{
	///<summary>
	/// OrgaPrincipal implementeert de ICabPrincipal definitie voor
	/// willekeurige organisaties.
	/// 
	/// TODO: Nadenken over een DefaultBuyerPerson, DefaultGrowerPerson?
	///</summary>
	public class OrgaPrincipal : ICabPrincipal
	{
		///<summary>
		/// De Organisation van deze Login.
		///</summary>
		public Organisation Organisation { get; private set; }

		///<summary>
		/// De Buyer van deze Login. Check met IsBuyer of deze gevuld is.
		///</summary>
		public Buyer Buyer { get; private set; }

		///<summary>
		/// True als deze Login een Buyer is.
		///</summary>
		public bool IsBuyer { get; private set; }

		///<summary>
		/// De Grower van deze Login. Check met IsGrower of deze gevuld is.
		///</summary>
		public Grower Grower { get; private set; }

		///<summary>
		/// True als deze Login een Grower is.
		///</summary>
		public bool IsGrower { get; private set; }

		///<summary>
		/// Bouwt een LoginPrincipal met de gegeven Login en de verzameling Rollen.
		///</summary>
		public OrgaPrincipal(Organisation organisation)
		{
			// We slaan deze login en gerelateerde gegevens alvast plat.
			Organisation = organisation;
			Buyer = Organisation.Buyer;
			IsBuyer = Buyer != null;
			Grower = Organisation.Grower;
			IsGrower = Grower != null;
		}

	}
}