﻿using System;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Query
{
	public static class BuyerQuery
	{
		public static QueryBase<Model.Buyer> ByOrganisation(Organisation organisation)
		{
			return new CriteriaQuery<Model.Buyer>(b => b.Organisation == organisation);
		}
	}
}
