﻿using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Query
{
	///<summary>
	/// Implements additional methods and queries on the CabProperty Repository.
	///</summary>
	public static class CabPropertyQuery
	{
		// Helper Extension

		///<summary>
		/// Gets exactly one Login with the given name.
		/// May throw an error if more than one is found.
		///</summary>
		public static bool GetOfferDetailIsSpecial(
			this IRepository<CabProperty> repository,
			int offerDetailSequence
		)
		{
			return repository.Query(OfferDetailIsSpecial(offerDetailSequence)).Count() > 0;
		}

		// Query Definitions.

		///<summary>
		/// Defines a Query for all Offer Detail IsSpecial Properties which are true.
		///</summary>
		public static IQuery<CabProperty> OfferDetailIsSpecial(
			int offerDetailSequence
		)
		{
			return new CriteriaQuery<CabProperty>(r =>
				r.Type == "F_CAB_OFFER_DETAILS" &&
				r.Name == "IsSpecial" &&
				r.Key == offerDetailSequence &&
				r.YesNo == 1
			);
		}
	}
}
