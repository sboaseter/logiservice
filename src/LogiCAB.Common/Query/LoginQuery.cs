﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Query
{
	///<summary>
	/// Implements additional methods and queries on the Login Repository.
	///</summary>
	public static class LoginQuery
	{
		// Helper Extension

		///<summary>
		/// Gets exactly one Login with the given name.
		/// May throw an error if more than one is found.
		///</summary>
		public static Login GetByName(
			this IRepository<Login> repository,
			string name
		)
		{
			return repository.Query(ByName(name)).SingleOrDefault();
		}

		// Query Definitions.

		///<summary>
		/// Defines a Query on the name field of a Login.
		///</summary>
		public static IQuery<Login> ByName(string name)
		{
			return new CriteriaQuery<Login>(r => r.Name == name);
		}

	}
}
