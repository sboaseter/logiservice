﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Query
{
	///<summary>
	/// Implements additional methods and queries on the Organisation Repository.
	///</summary>
	public static class OrganisationQuery
	{
		// Helper Extension

		///<summary>
		/// Gets exactly one Organisation with the given name.
		/// May throw an error if more than one is found.
		///</summary>
		public static Organisation GetByName(
			this IRepository<Organisation> repository,
			string name
		)
		{
			return repository.Query(ByName(name)).SingleOrDefault();
		}

		public static Organisation GetByEAN(
			this IRepository<Organisation> repository,
			string ean
		)
		{
			return repository.Query(ByEAN(ean)).SingleOrDefault();
		}

		public static Organisation GetByCode(
			this IRepository<Organisation> repository,
			string code
		)
		{
			return repository.Query(ByCode(code)).SingleOrDefault();
		}

		public static IQueryable<Organisation> ListByNameContains(
			this IRepository<Organisation> repository,
			string partialName
		)
		{
			return repository.Query(ByNameContains(partialName));
		}

		public static bool IsNameUnique(
			this IRepository<Organisation> repository,
			string name
		)
		{
			return repository.Count(o => o.Name == name) == 0;
		}

		public static bool IsNameUnique(
			this IRepository<Organisation> repository,
			string name,
			int notSequence
		)
		{
			return repository.Count(o => o.Name == name && o.Sequence != notSequence) == 0;
		}

		public static bool IsEANUnique(
			this IRepository<Organisation> repository,
			string ean
		)
		{
			return repository.Count(o => o.EAN == ean) == 0;
		}

		public static bool IsEANUnique(
			this IRepository<Organisation> repository,
			string ean,
			int notSequence
		)
		{
			return repository.Count(o => o.EAN == ean && o.Sequence != notSequence) == 0;
		}

		// Query Definitions.

		///<summary>
		/// Defines a Query on the name field of a Organisation.
		///</summary>
		public static IQuery<Organisation> ByName(string name)
		{
			return new CriteriaQuery<Organisation>(r => r.Name == name);
		}

		public static IQuery<Organisation> ByEAN(string ean)
		{
			return new CriteriaQuery<Organisation>(r => r.EAN == ean);
		}

		public static IQuery<Organisation> ByCode(string code)
		{
			return new CriteriaQuery<Organisation>(r => r.Code == code);
		}

		public static IQuery<Organisation> ByNameContains(string partialName)
		{
			return new CriteriaQuery<Organisation>(r => r.Name.Contains(partialName));
		}
	}
}
