﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Query
{
	///<summary>
	/// Implements additional methods and queries on the ServiceTicket Repository.
	///</summary>
	public static class ServiceTicketQuery
	{
		// Helper Extension

		///<summary>
		/// Deletes all ServiceTickets with the given Login.
		///</summary>
		public static int DeleteByLogin(
			this IRepository<ServiceTicket> repository,
			Login login
		)
		{
			var count = repository.Delete(ByLogin(login));

			// Omdat deze methode voornamelijk gebruikt wordt om de uniciteit
			// van Service Tickets te garanderen die gesaved worden, moeten we
			// hier altijd flushen zodat NHibernate de Delete uitgevoerd vóór de
			// Insert.
			repository.Flush();

			return count;
		}

		///<summary>
		/// Gets exactly one ServiceTicket with the given Login.
		/// May throw an error if more than one is found.
		///</summary>
		public static ServiceTicket GetByLogin(
			this IRepository<ServiceTicket> repository,
			Login login
		)
		{
			return repository.Query(ByLogin(login)).SingleOrDefault();
		}

		// Query Definitions.

		///<summary>
		/// Defines a Query on the Login field of a ServiceTicket.
		///</summary>
		public static IQuery<ServiceTicket> ByLogin(Login login)
		{
			return new CriteriaQuery<ServiceTicket>(
				r => r.Login == login
			);
		}

	}
}
