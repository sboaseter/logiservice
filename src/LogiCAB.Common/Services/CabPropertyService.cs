﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Wrapper Service voor de specifieke CabProperty Services.
	///</summary>
	public class CabPropertyService
	{
		///<summary>
		/// SimpleLoginLanguage properties.
		///</summary>
		public SimpleLoginLanguageService SimpleLoginLanguage { get; private set; }

		/// <summary>
		/// OrderOriginalOrder properties.
		/// </summary>
		public OrderOriginalOrderService OrderOriginalOrder { get; private set; }

		///<summary>
		/// Constructor met dependencies.
		/// Maakt ook alle specifieke services aan.
		///</summary>
		public CabPropertyService(IRepository<CabProperty> properties)
		{
			SimpleLoginLanguage = new SimpleLoginLanguageService(properties);
			OrderOriginalOrder = new OrderOriginalOrderService(properties);
		}

		///<summary>
		/// Een specifieke CabProperty service voor SimpleLoginLanguage properties.
		///</summary>
		public class SimpleLoginLanguageService : ISpecificCabPropertyService
		{
			protected IRepository<CabProperty> Properties { get; set; }

			internal SimpleLoginLanguageService(IRepository<CabProperty> properties)
			{
				Properties = properties;
			}

			///<summary>
			/// Maakt een nieuwe CabProperty aan met de juiste vulling voor een
			/// SimpleLoginLanguage.
			///</summary>
			public CabProperty Create(
				ILogin login,
				int languageSequence)
			{
				return new CabProperty
				{
					Type = "F_CAB_LOGIN",
					Key = login.Sequence,
					Name = "SimpleLoginLanguage",
					Reference = languageSequence
				};
			}

			///<summary>
			/// Maakt een nieuwe CabProperty aan met de juiste vulling voor een
			/// SimpleLoginLanguage en slaat deze meteen op.
			///</summary>
			public CabProperty CreateAndSave(
				ILogin login,
				int languageSequence)
			{
				var prop = Create(login, languageSequence);
				// Validatie is hier overbodig omdat wij hem zelf bouwen.
				Properties.Save(prop);
				return prop;
			}

			///<summary>
			/// Valideert of alle velden overeen komen met de vulling van een
			/// SimpleLoginLanguage.
			///</summary>
			public string[] Validate(CabProperty property)
			{
				var result = new List<string>();

				// Gevulde velden.
				if (!"F_CAB_LOGIN".Equals(property.Type))
					result.Add("Property Type must be F_CAB_LOGIN.");
				if (!"SimpleLoginLanguage".Equals(property.Name))
					result.Add("Property Name must be SimpleLoginLanguage.");
				if (0 >= property.Key)
					result.Add("Property Key must be a positive value.");
				if ((!property.Reference.HasValue) || (0 >= property.Reference))
					result.Add("Property Reference must be a positive value.");

				// Lege velden.
				if (property.DateTime.HasValue)
					result.Add("Property DateTime must be empty.");
				if (property.Index.HasValue)
					result.Add("Property Index must be empty.");
				if (property.Money.HasValue)
					result.Add("Property Money must be empty.");
				if (property.Number.HasValue)
					result.Add("Property Number must be empty.");
				if (property.YesNo.HasValue)
					result.Add("Property YesNo must be empty.");
				if (property.Text != null)
					result.Add("Property Text must be empty.");

				return result.ToArray();
			}

			///<summary>
			/// Valideert de inhoud van de property, en slaat hem op.
			///</summary>
			public void Save(CabProperty property)
			{
				// Validations.
				var validations = Validate(property);
				if (validations.Length > 0)
					throw new ArgumentException(validations[0]);

				Properties.Save(property);
			}

			///<summary>
			/// Geeft alle SimpleLoginLanguage CabProperties als IQueryable.
			///</summary>
			public IQueryable<CabProperty> SelectAll()
			{
				return Properties.Where(p =>
					p.Type == "F_CAB_LOGIN" &&
					p.Name == "SimpleLoginLanguage");
			}

			///<summary>
			/// Geeft de SimpleLoginLanguage property voor de gegeven login.
			///</summary>
			public CabProperty FindOnLogin(ILogin login)
			{
				return SelectAll().FirstOrDefault(p => p.Key == login.Sequence);
			}
		}

		///<summary>
		/// Een specifieke CabProperty service voor OrderOriginalOrder properties.
		///</summary>
		public class OrderOriginalOrderService : ISpecificCabPropertyService
		{
			protected IRepository<CabProperty> Properties { get; set; }

			internal OrderOriginalOrderService(IRepository<CabProperty> properties)
			{
				Properties = properties;
			}

			///<summary>
			/// Maakt een nieuwe CabProperty aan met de juiste vulling voor een
			/// OrderOriginalOrder.
			///</summary>
			public CabProperty Create(
				int orderHeaderSequence,
				int? originalOrderHeaderSequence)
			{
				return new CabProperty
				{
					Type = "F_CAB_ORDER_HEA",
					Key = orderHeaderSequence,
					Name = "OriginalOrder",
					Reference = originalOrderHeaderSequence
				};
			}

			///<summary>
			/// Maakt een nieuwe CabProperty aan met de juiste vulling voor een
			/// OrderOriginalOrder en slaat deze meteen op.
			///</summary>
			public CabProperty CreateAndSave(
				int orderHeaderSequence,
				int? originalOrderHeaderSequence)
			{
				var prop = Create(orderHeaderSequence, originalOrderHeaderSequence);
				// Validatie is hier overbodig omdat wij hem zelf bouwen.
				Properties.Save(prop);
				return prop;
			}

			///<summary>
			/// Valideert of alle velden overeen komen met de vulling van een
			/// OrderOriginalOrder.
			///</summary>
			public string[] Validate(CabProperty property)
			{
				var result = new List<string>();

				// Gevulde velden.
				if (!"F_CAB_ORDER_HEA".Equals(property.Type))
					result.Add("Property Type must be F_CAB_ORDER_HEA.");
				if (!"OriginalOrder".Equals(property.Name))
					result.Add("Property Name must be OriginalOrder.");
				if (0 >= property.Key)
					result.Add("Property Key must be a positive value.");
				if ((!property.Reference.HasValue) || (0 >= property.Reference))
					result.Add("Property Reference must be a positive value.");

				// Lege velden.
				if (property.DateTime.HasValue)
					result.Add("Property DateTime must be empty.");
				if (property.Index.HasValue)
					result.Add("Property Index must be empty.");
				if (property.Money.HasValue)
					result.Add("Property Money must be empty.");
				if (property.Number.HasValue)
					result.Add("Property Number must be empty.");
				if (property.YesNo.HasValue)
					result.Add("Property YesNo must be empty.");
				if (property.Text != null)
					result.Add("Property Text must be empty.");

				return result.ToArray();
			}

			///<summary>
			/// Valideert de inhoud van de property, en slaat hem op.
			///</summary>
			public void Save(CabProperty property)
			{
				// Validations.
				var validations = Validate(property);
				if (validations.Length > 0)
					throw new ArgumentException(validations[0]);

				Properties.Save(property);
			}

			///<summary>
			/// Geeft alle OrderOriginalOrder CabProperties als IQueryable.
			///</summary>
			public IQueryable<CabProperty> SelectAll()
			{
				return Properties.Where(p =>
					p.Type == "F_CAB_ORDER_HEA" &&
					p.Name == "OriginalOrder");
			}

			///<summary>
			/// Geeft de SimpleLoginLanguage property voor de gegeven login.
			///</summary>
			public CabProperty FindOnSequence(int orderHeaderSequence)
			{
				return SelectAll().FirstOrDefault(p => p.Key == orderHeaderSequence);
			}

			/// <summary>
			/// Geeft de Reference van een gevonden property terug, of null.
			/// </summary>
			public int? GetReferenceOnSequence(int orderHeaderSequence)
			{
				var property = FindOnSequence(orderHeaderSequence);
				return property == null ? null : property.Reference;
			}
		}
	}
}