﻿using System.Linq;
using LogiCAB.Common.Model;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Definiëert de standaard elementen van een specifieke CabProperty Service.
	///</summary>
	public interface ISpecificCabPropertyService
	{
		///<summary>
		/// Geeft een IQueryable met de juiste Where expression voor de specifieke properties.
		///</summary>
		IQueryable<CabProperty> SelectAll();

		///<summary>
		/// Valideert alle velden van de property op overeenkomst met de regels voor de specifieke property.
		///</summary>
		string[] Validate(CabProperty property);

		/// <summary>
		/// Valideert de property en slaat hem op.
		/// </summary>
		void Save(CabProperty property);
	}
}