﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Deze Service biedt diensten rond de F_CAB_LOGIN tabel.
	///</summary>
	public class LoginService
	{
		protected IRepository<Login> Logins { get; private set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public LoginService(
			IRepository<Login> logins)
		{
			Logins = logins;
		}

		/// <summary>
		/// Controleert of de opgegeven naam al bestaat in de Login tabel.
		/// </summary>
		public bool NameExists(string name)
		{
			return Logins.Any(l => l.Name == name);
		}

		/// <summary>
		/// Controleert of de opgegeven naam al bestaat in de Login tabel,
		/// met uitzondering van de opgegeven Login Sequence.
		/// </summary>
		public bool NameExists(string name, int? exceptForLoginSequence)
		{
			return
				exceptForLoginSequence.HasValue ?
				Logins.Any(o => o.Name == name && o.Sequence != exceptForLoginSequence.Value) :
				NameExists(name);
		}
	}
}
