﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Service voor toegang tot OrderDetailView records.
	///</summary>
	public class OrderDetailViewService
	{
		protected IRepository<OrderDetailView> OrderDetailViews { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public OrderDetailViewService(IRepository<OrderDetailView> orderDetailViews)
		{
			OrderDetailViews = orderDetailViews;
		}

		///<summary>
		/// Geeft een queryable expressie terug voor de Order Detail records
		/// van de gegeven Header.
		///</summary>
		public IQueryable<OrderDetailView> DetailsForHeader(int orderHeaderSequence)
		{
			return OrderDetailViews.Where(d => d.HeaderSequence == orderHeaderSequence);
		}
	}
}
