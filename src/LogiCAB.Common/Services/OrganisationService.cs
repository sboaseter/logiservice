﻿using System;
using System.Linq;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Deze Service biedt diensten rond de F_CAB_ORGANISATION tabel en cohorten.
	///</summary>
	public class OrganisationService
	{
		protected IRepository<Organisation> Organisations { get; private set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public OrganisationService(
			IRepository<Organisation> organisations)
		{
			Organisations = organisations;
		}

		/// <summary>
		/// Controleert of de opgegeven naam al bestaat in de Organisatie tabel.
		/// </summary>
		public bool NameExists(string name)
		{
			return Organisations.Any(o => o.Name == name);
		}

		/// <summary>
		/// Controleert of de opgegeven naam al bestaat in de Organisatie tabel,
		/// met uitzondering van de opgegeven Organisation Sequence.
		/// </summary>
		public bool NameExists(string name, int? exceptForOrganisationSequence)
		{
			return
				exceptForOrganisationSequence.HasValue ?
				Organisations.Any(o => o.Name == name && o.Sequence != exceptForOrganisationSequence.Value) :
				NameExists(name);
		}
	}
}