﻿using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.Services
{
	///<summary>
	/// Deze Service biedt de mogelijkheid om Principals aan te maken op basis
	/// van verschillende velden.
	///</summary>
	public class PrincipalService
	{
		protected IRoleProvider RoleProvider { get; set; }
		protected IShopPropertyProvider ShopPropertyProvider { get; set; }
		protected IRepository<Login> Logins { get; private set; }
		protected IRepository<Organisation> Organisations { get; private set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public PrincipalService(
			IRepository<Organisation> organisations,
			IRepository<Login> logins,
			IRoleProvider roleProvider,
			IShopPropertyProvider shopPropertyProvider)
		{
			Organisations = organisations;
			Logins = logins;
			RoleProvider = roleProvider;
			ShopPropertyProvider = shopPropertyProvider;
		}

		/// <summary>
		/// Maakt een LoginPrincipal op basis van de opgegeven username en wachtwoord.
		/// </summary>
		public LoginPrincipal GetOnUsernamePassword(string username, string password)
		{
			// Kijk of we de gegeven login kunnen vinden.
			var login = Logins.FirstOrDefault(l => l.Name == username && l.Password == password);

			// Maak de LoginPrincipal.
			return login == null ? null : GetOnLogin(login);
		}

		///<summary>
		/// Haalt de rollen op bij de gegeven Login en maakt een LoginPrincipal.
		///</summary>
		public LoginPrincipal GetOnLogin(Login login)
		{
			// Haal de rollen op.
			var roles = RoleProvider.GetRolesForOrganisation(
				login.Person.Organisation.Sequence,
				ShopPropertyProvider.Shop);

			// Maak de LoginPrincipal.
			return new LoginPrincipal(login, roles);
		}

		///<summary>
		/// Haalt een OrgaPrincipal op op basis van de EAN Code.
		///</summary>
		public ICabPrincipal GetOnEAN(string eanCode)
		{
			var orga = Organisations.FirstOrDefault(o => o.EAN == eanCode);
			return orga == null ? null : new OrgaPrincipal(orga);
		}

	}
}