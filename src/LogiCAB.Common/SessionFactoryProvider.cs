﻿using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using NHibernate;

namespace LogiCAB.Common
{
	///<summary>
	/// Provides a wired-up Session Factory.
	///</summary>
	public class SessionFactoryProvider
	{
		protected string ConnectionStringName { get; set; }

		///<summary>
		/// Creates an instance with dependencies.
		///</summary>
		public SessionFactoryProvider(string connectionStringName)
		{
			ConnectionStringName = connectionStringName;
		}

		///<summary>
		/// Provides a wired-up Session Factory.
		///</summary>
		public ISessionFactory CreateSessionFactory()
		{
            //var test = Fluently.Configure();
            //test.Database(
            //        MsSqlConfiguration.MsSql2005
            //            .ConnectionString(c => c.FromConnectionStringWithKey(ConnectionStringName))
            //            .DefaultSchema("dbo")
            //    );

            //test.Mappings(m => 
            //    m.FluentMappings.AddFromAssemblyOf<Buyer.BuyerMap>()
            //    .Conventions.AddFromAssemblyOf<EnumConvention>());

            //test.BuildSessionFactory();
			return Fluently.Configure()
				.Database(
					MsSqlConfiguration.MsSql2005
						.ConnectionString(c => c.FromConnectionStringWithKey(ConnectionStringName))
						//.Cache(c => c.UseQueryCache().UseMinimalPuts())
						.DefaultSchema("dbo")
				)
				.Mappings(m => m
					.FluentMappings.AddFromAssemblyOf<Buyer.BuyerMap>()
					.Conventions.AddFromAssemblyOf<EnumConvention>()
				)
				.BuildSessionFactory();
		}

	}
}
