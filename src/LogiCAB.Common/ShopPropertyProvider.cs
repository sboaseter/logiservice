﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LogiCAB.Common.Enums;
using LogiFlora.Common;

namespace LogiCAB.Common
{
	///<summary>
	/// Implements a basic Request Based Shop Identity and Property handler.
	///</summary>
	public class ShopPropertyProvider : IShopPropertyProvider
	{
		/// <summary>
		/// The expression on which this provider is found.
		/// </summary>
		public Regex Expression { get; internal set; }

		/// <summary>
		/// The properties for this provider.
		/// </summary>
		public Dictionary<string, string> Properties { get; internal set; }

		///<summary>
		/// The static Sequence of the Shop.
		///</summary>
		public int ShopId { get; internal set; }

		public string ShopName { get; internal set; }

		public Shop Shop { get; internal set; }

		/// <summary>
		/// The static Sequence of the Skin.
		/// </summary>
		public int? SkinId { get; internal set; }

		public string SkinName { get; internal set; }

		/// <summary>
		/// Returns the current Shop Property value for name, or missing if it
		/// doesn't exist in the property list.
		/// </summary>
		public string Property(string name)
		{
			if (Properties.ContainsKey(name))
				return Properties[name];
			return null;
		}

		/// <summary>
		/// Returns the current Shop Property value for name, or missing if it
		/// doesn't exist in the property list.
		/// </summary>
		public string Property(string name, string missing)
		{
			if (Properties.ContainsKey(name))
				return Properties[name];
			return missing;
		}

		/// <summary>
		/// Returns an application relative path to the given relativePath for
		/// the current skin.
		/// </summary>
		public string SkinPath(string relativePath)
		{
			if (String.IsNullOrEmpty(SkinName))
				throw new ApplicationException("The current Shop does not seem to have a Skin defined.");
			return Helper.UrlCombine("~/Skin", SkinName, relativePath);
		}
	}
}
