using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LogiCAB.Common.Domain;

namespace LogiCAB.Common
{
	///<summary>
	/// Provides an optimized Factory for creating ShopPropertyProviders based
	/// on UrlExpression evidence.
	///</summary>
	public class ShopUrlExpressionFactory
	{
		private List<ShopPropertyProvider> Shops { get; set; }

		///<summary>
		/// Instantiates a Factory with the given serviceName and shopProvider.
		///</summary>
		///<param name="serviceName">The name of the service who's evidence is to be used.</param>
		///<param name="shopProvider">An IShopProvider to provide us with evidence and properties.</param>
		public ShopUrlExpressionFactory(string serviceName, IShopProvider shopProvider)
		{
			Shops = new List<ShopPropertyProvider>();

			// Haal de evidence lijst op.
			foreach (var evidence in shopProvider.GetEvidence(serviceName, "UrlExpression"))
			{
				// Bouw voor elke evidence een volledige shop, inclusief regex en properties.
				var shop = new ShopPropertyProvider
				{
					Expression = new Regex(evidence.Value),
					Properties = shopProvider.GetProperties(evidence),
					ShopId = (int)evidence.Shop,
					ShopName = evidence.Shop.ToString(),
					Shop = evidence.Shop,
					SkinId = evidence.Skin == null ? null : (int?)evidence.Skin,
					SkinName = evidence.Skin == null ? null : evidence.Skin.ToString()
				};
				Shops.Add(shop);
			}
		}

		///<summary>
		/// Returns the first shop who's evidence matches the given uri.
		/// This will usually be called during BeginRequest to set the Request Shop based on the Request Uri.
		/// Will throw an ApplicationException if no Shop is found.
		///</summary>
		public IShopPropertyProvider GetShopForUri(Uri uri)
		{
			foreach (var shop in Shops)
				if (shop.Expression.IsMatch(uri.OriginalString.ToLower()))
					return shop;
			throw new ApplicationException("No Shop Found for Uri! Invalid Shop Evidence Configuration.");
		}
	}
}