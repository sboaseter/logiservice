﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using LogiCAB.Contract.AuthenticationService;

namespace LogiCAB.Common.WCF.AuthenticationService
{
	///<summary>
	/// Implementeerd het Authentication Service Contract.
	///</summary>
	[ServiceBehavior(Namespace = Meta.Namespace), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class AuthenticationServiceContract : IAuthenticationServiceContract
	{
		protected IHandler<RequestSeedRequest, RequestSeedResponse> RequestSeedHandler { get; set; }
		protected IHandler<RequestTicketRequest, RequestTicketResponse> RequestTicketHandler { get; set; }
		protected IHandler<ClearTicketRequest, ClearTicketResponse> ClearTicketHandler { get; set; }

		///<summary>
		/// Constructor with Dependencies.
		///</summary>
		public AuthenticationServiceContract(
			IHandler<RequestSeedRequest, RequestSeedResponse> requestSeedHandler,
			IHandler<RequestTicketRequest, RequestTicketResponse> requestTicketHandler,
			IHandler<ClearTicketRequest, ClearTicketResponse> clearTicketHandler)
		{
			RequestSeedHandler = requestSeedHandler;
			RequestTicketHandler = requestTicketHandler;
			ClearTicketHandler = clearTicketHandler;
		}

		public RequestSeedResponse RequestSeed(RequestSeedRequest request)
		{
			return RequestSeedHandler.Handle(request, new RequestSeedResponse());
		}

		public RequestTicketResponse RequestTicket(RequestTicketRequest request)
		{
			return RequestTicketHandler.Handle(request, new RequestTicketResponse());
		}

		public ClearTicketResponse ClearTicket(ClearTicketRequest request)
		{
			return ClearTicketHandler.Handle(request, new ClearTicketResponse());
		}
	}
}