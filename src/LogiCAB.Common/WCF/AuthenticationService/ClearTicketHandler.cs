using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiCAB.Contract.AuthenticationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.AuthenticationService
{
	///<summary>
	/// Simple Handler class for ClearTicket Requests.
	///</summary>
	public class ClearTicketHandler : AuthenticatedHandler<ClearTicketRequest, ClearTicketResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected override Role[] AllRoles
		{
			get { return null; }
		}
		protected override Role[] AnyRoles
		{
			get { return null; }
		}

		protected IRepository<ServiceTicket> ServiceTickets { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public ClearTicketHandler(
			IRepository<ServiceTicket> serviceTickets,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(authenticatedRequestValidator)
		{
			ServiceTickets = serviceTickets;
		}

		protected override ClearTicketResponse InnerHandle(ClearTicketRequest request, ClearTicketResponse response)
		{
			// Delete de Ticket van deze User.
			ServiceTickets.Delete(st => st.Login.Name == request.AuthenticationUsername);
			return response.Ok();
		}
	}
}