﻿using System;
using LogiCAB.Common.Domain;
using LogiCAB.Contract.LogiOfferte;

namespace LogiCAB.Common.WCF.LogiOfferte
{
	///<summary>
	/// Handler voor SupplierOrder Requests.
	///</summary>
	public class ChildOrderDetailStatusChangeHandler : BaseHandler<ChildOrderDetailStatusChangeRequest, ChildOrderDetailStatusChangeResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ICommonOrderProvider CommonOrderProvider { get; set; }
		protected ISignalProvider SignalProvider { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public ChildOrderDetailStatusChangeHandler(
			ICommonOrderProvider commonOrderProvider,
			ISignalProvider signalProvider)
        {
			CommonOrderProvider = commonOrderProvider;
			SignalProvider = signalProvider;
		}

		protected override ChildOrderDetailStatusChangeResponse InnerHandle(
			ChildOrderDetailStatusChangeRequest request,
			ChildOrderDetailStatusChangeResponse response)
		{
			Log.DebugFormat("request.OrderDetail.Sequence = [{0}]", request.OrderDetail.Sequence);
			Log.DebugFormat("request.ChildOrderDetail.Sequence = [{0}]", request.ChildOrderDetail.Sequence);
			Log.DebugFormat("request.NewStatus = [{0}]", request.NewStatus);

			CommonOrderProvider.SetOrderDetailStatus(request.ChildOrderDetail.Sequence, request.NewStatus);
			return response.Ok();
		}
	}
}