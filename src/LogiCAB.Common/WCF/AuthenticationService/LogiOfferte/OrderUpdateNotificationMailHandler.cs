﻿using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract;
using LogiCAB.Contract.LogiOfferte;
using LogiFlora.Common.Mail;

namespace LogiCAB.Common.WCF.LogiOfferte
{
	///<summary>
	/// Handler voor SupplierOrder Requests.
	///</summary>
	public class OrderUpdateNotificationMailHandler
		: BaseHandler<OrderUpdateNotificationMailRequest, BaseResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ITemplateMailSender TemplateMailSender { get; set; }
		protected IOrderUpdateNotificationMailInfoProvider OrderUpdateNotificationMailInfoProvider { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public OrderUpdateNotificationMailHandler(
			ITemplateMailSender templateMailSender,
			IOrderUpdateNotificationMailInfoProvider orderUpdateNotificationMailInfoProvider)
		{
			TemplateMailSender = templateMailSender;
			OrderUpdateNotificationMailInfoProvider = orderUpdateNotificationMailInfoProvider;
		}

		protected override BaseResponse InnerHandle(
			OrderUpdateNotificationMailRequest request,
			BaseResponse response)
		{
			Log.Debug("InnerHandle");
			Log.DebugFormat("request.OrderHeader.Sequence = [{0}]", request.OrderHeader.Sequence);

			// Haal de gegevens op voor het samenstellen van de mail.
			OrderUpdateNotificationMailInfo info;
			try
			{
				info = OrderUpdateNotificationMailInfoProvider.SupplierToClient(
					request.OrderHeader.Sequence);
			}
			catch (Exception ex)
			{
				Log.Error("Unable to get mail info.", ex);
				return response.Error("Unable to get mail info.");
			}

			// Property intellisense voor template.
//			var t = info.BuyerPerson.Email;

			// Verstuur hem.
			try
			{
				TemplateMailSender.Send(info);
			}
			catch (Exception ex)
			{
				Log.Error("Unable to send Mail Template.", ex);
				return response.Error(ex.Message);
			}

			// Alles Ok.
			return response.Ok();
		}
	}
}