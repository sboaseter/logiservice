﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Common.Services;
using LogiCAB.Contract.LogiOfferte;
using LogiFlora.Common.Db;
using LogiFlora.Common.Mail;

namespace LogiCAB.Common.WCF.LogiOfferte {
	///<summary>
	/// Implementeerd het versturen van de Supplier Mail.
	///</summary>
	public class PartyUpdateHandler : BaseHandler<PartyUpdateRequest, PartyUpdateResponse> 
    {
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ITemplateMailSender TemplateMailSender { get; set; }
		protected IRepository<PartyHeader> PartyHeaders { get; set; }
		protected IRepository<PartyDetail> PartyDetails { get; set; }
		protected IRepository<OfferDetailView> OfferDetailViewR { get; set; }
		protected OrderDetailViewService OrderDetailViewService { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public PartyUpdateHandler(
			ITemplateMailSender templateMailSender,
			IRepository<PartyHeader> partyHeaders,
			IRepository<PartyDetail> partyDetails,
			OrderDetailViewService orderDetailViewService,
			IRepository<OfferDetailView> offerDetailViewR)
        {
			TemplateMailSender = templateMailSender;
			PartyHeaders = partyHeaders;
			PartyDetails = partyDetails;
			OrderDetailViewService = orderDetailViewService;
			OfferDetailViewR = offerDetailViewR;
		}

		protected override PartyUpdateResponse InnerHandle(
			PartyUpdateRequest request,
			PartyUpdateResponse response) {
			Log.Debug("InnerHandle");
			Log.DebugFormat("request.OrderHeader.Sequence = [{0}]", request.OrderHeader.Sequence);

			// Getting the Order Header Sequence.
			// Using the order header sequence to get a collection of order details
			// For each of the order details, get the related party details(via the offer detail)
			// Create a booking with number of items from the order detail
			// Recalculate the number of items in the party detail (stored procedure)

			var myOrderDetails = OrderDetailViewService.DetailsForHeader(request.OrderHeader.Sequence);
			var count = PartyDetails.Count();
			Log.DebugFormat("Count partydetails: {0}", PartyDetails.Count());

			foreach(var orDetails in myOrderDetails) {
				var ofDetail = OfferDetailViewR.First(x => x.Sequence == orDetails.OfferDetailSequence);
				var pDetail1 = PartyDetails.First(p => p.Sequence == ofDetail.PartyDetailSeq);
				//				var pHeader = PartyHeaders.First(p => p.Sequence == pDetail1.HeaderSequence);
				pDetail1.NumItems -= orDetails.NumberOfUnits;
				PartyDetails.Save(pDetail1);
			}
			var test = 1;
			//				Log.DebugFormat("Num of items: {0}", 



			//            myOrderDetails.First().

			//            myOrderDetails.

			//List<OrderDetailView> 
			//for (int i = 0; i < myOrderDetails.Count();i++ ) 

			//var myPartyDetail = orderDetail.PartyDetail; // ? Or a lookup based on PartyDetailSequence

			//myPartyDetail.Bookings.Add(new Booking { PartyDetail = myPartyDetail, NumOfItems = orderDetail.NumberOfItems });

			//// Invoke Procedure to update Party Detail.
			//PartyDetailProcedureProvider.UpdatePartyDetail(myPartyDetail.Sequence);

			// Alles Ok.));));));)))
			return response.Ok();
		}
	}
}
