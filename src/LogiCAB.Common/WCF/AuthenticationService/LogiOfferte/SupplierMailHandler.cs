﻿using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.LogiOfferte;
using LogiFlora.Common.Mail;

namespace LogiCAB.Common.WCF.LogiOfferte
{
	///<summary>
	/// Implementeerd het versturen van de Supplier Mail.
	///</summary>
	public class SupplierMailHandler : BaseHandler<SupplierMailRequest, SupplierMailResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ITemplateMailSender TemplateMailSender { get; set; }
		protected ISupplierMailInfoProvider SupplierMailInfoProvider { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public SupplierMailHandler(
			ITemplateMailSender templateMailSender,
			ISupplierMailInfoProvider supplierMailInfoProvider)
		{
			TemplateMailSender = templateMailSender;
			SupplierMailInfoProvider = supplierMailInfoProvider;
		}

		protected override SupplierMailResponse InnerHandle(
			SupplierMailRequest request,
			SupplierMailResponse response)
		{
			Log.Debug("InnerHandle");
			Log.DebugFormat("request.OrderHeader.Sequence = [{0}]", request.OrderHeader.Sequence);

			// Haal de gegevens op voor het samenstellen van de mail.
			SupplierMailInfo info;
			try
			{
				info = SupplierMailInfoProvider.GetSupplierMailInfo(
					request.OrderHeader.Sequence);
			}
			catch (Exception ex)
			{
				Log.Error("Unable to get Supplier Mail Info.", ex);
				return response.Error("Unable to get Supplier Mail Info.");
			}

			// Property intellisense voor template.
//			var t = info.BuyerPerson.Email;

			// Verstuur hem.
			try
			{
				TemplateMailSender.Send(info);
			} catch (Exception ex)
			{
				Log.Error("Unable to send Mail Template.", ex);
				return response.Error(ex.Message);
			}

			// Alles Ok.
			return response.Ok();
		}
	}
}