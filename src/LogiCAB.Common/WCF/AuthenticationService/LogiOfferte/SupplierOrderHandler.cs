﻿using System;
using System.Collections.Generic;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.LogiOfferte;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.LogiOfferte
{
	///<summary>
	/// Handler voor SupplierOrder Requests.
	///</summary>
	public class SupplierOrderHandler : BaseHandler<SupplierOrderRequest, SupplierOrderResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected ICommonOrderProvider CommonOrderProvider { get; set; }
		protected ISignalProvider SignalProvider { get; set; }
		protected IRepository<CabProperty> Properties { get; set; }

		///<summary>
		/// Constructor met dependencies.
		///</summary>
		public SupplierOrderHandler(
			ICommonOrderProvider commonOrderProvider,
			ISignalProvider signalProvider,
			IRepository<CabProperty> properties)
		{
			CommonOrderProvider = commonOrderProvider;
			SignalProvider = signalProvider;
			Properties = properties;
		}

		protected override SupplierOrderResponse InnerHandle(
			SupplierOrderRequest request,
			SupplierOrderResponse response)
		{
			Log.DebugFormat("request.OrderHeader.Sequence = [{0}]", request.OrderHeader.Sequence);

			// Haal de gegevens op om nieuwe orders mee aan te maken.
			var info = CommonOrderProvider.GetOriginalOrderInfo(
				request.OrderHeader.Sequence);

			// Controleren of er wel iets te doen valt.
			if (info == null || info.Count == 0)
			{
				Log.WarnFormat("Geen Original Order Info gevonden voor OrderHeader Sequence: {0}", request.OrderHeader.Sequence);
				return response.NoChange();
			}

			int lastBuyer = -1;
			int lastGrower = -1;
			int supplierOrderHeaderSequence = -1;
			var orderHeaders = new List<OrderHeaderInfo>();
			foreach (var row in info)
			{
				// Controleer of er wel voldoende voorraad is om een order te plaatsen.
				if (row.NumOfUnits > row.NumUnitsAvailable)
				{
					Log.Warn("Not enough stock available to place parent order.");
					Log.WarnFormat("OfferDetailSequence = [{0}]", row.OfferDetailSequence);
					Log.WarnFormat("NumOfUnits = [{0}]", row.NumOfUnits);
					Log.WarnFormat("NumUnitsAvailable = [{0}]", row.NumUnitsAvailable);
					continue;
				}

				// Als de buyer/grower combo gewijzigd is hebben we een nieuwe
				// order header nodig.
				if ((lastBuyer != row.BuyerSequence) || (lastGrower != row.GrowerSequence))
				{
					// Als we al bezig waren met een supplier order moeten we
					// die bevestigen.
					BevestigOpenOrder(supplierOrderHeaderSequence);

					// Creëer een nieuwe Order Header voor de supplier.
					supplierOrderHeaderSequence = CommonOrderProvider.CreateOrderHeader(
						row.OrderDesc,
						row.OrderDeliveryDate,
						row.OfferDetailSequence,
						row.GrowerSequence,
						row.BuyerSequence,
						row.PersonSequence,
						row.OrderRemark,
						row.ShopSequence,
                        true
					);
					Log.DebugFormat("Supplier Order Header Created: [{0}]", supplierOrderHeaderSequence);
					orderHeaders.Add(new OrderHeaderInfo{ Sequence = supplierOrderHeaderSequence });

					// Voeg een verwijzing terug naar de originele Order toe aan deze order.
					Properties.Save(CabProperty.NewOrderOriginalOrder(
						supplierOrderHeaderSequence,
						request.OrderHeader.Sequence
					));

					lastBuyer = row.BuyerSequence;
					lastGrower = row.GrowerSequence;
				}

				// Creëer de Order Detail regel voor de supplier.
                int ofhdSeq = -1;
                int ofdtSeq = -1;

				var supplierOrderDetailSequence = CommonOrderProvider.CreateOrderDetailService(
					row.OfferDetailSequence,
					supplierOrderHeaderSequence,
					row.NumOfUnits,
                    string.Empty,
                    string.Empty,
					String.Empty,
                    string.Empty,
                    false,
                    ref ofdtSeq,
                    ref ofhdSeq);
				Log.DebugFormat("Supplier Order Detail Created: [{0}]", supplierOrderDetailSequence);

				// En voeg een verwijzing terug naar de child order regel toe.
				Properties.Save(CabProperty.NewOrderDetailOriginalOrderDetail(
					supplierOrderDetailSequence,
					row.OrderDetailSequence
				));
			}

			// Bevestigen als hij open stond.
			BevestigOpenOrder(supplierOrderHeaderSequence);

			// Geef de Order Headers terug in de response.
			response.OrderHeaders = orderHeaders.ToArray();

			return response.Ok();
		}

		private void BevestigOpenOrder(int supplierOrderHeaderSequence)
		{
			if (supplierOrderHeaderSequence < 0)
				return;

			CommonOrderProvider.UpdateOrderHeader(supplierOrderHeaderSequence);
			SignalProvider.SignalOrderConfirmedWithMail(supplierOrderHeaderSequence);
		}
	}
}