﻿using System;
using LogiCAB.Common.Domain;
using LogiCAB.Contract.AuthenticationService;

namespace LogiCAB.Common.WCF.AuthenticationService
{
	///<summary>
	/// Handler for RequestSeed Requests.
	///</summary>
	public class RequestSeedHandler : BaseHandler<RequestSeedRequest, RequestSeedResponse>
	{
		protected IServiceSeedProvider ServiceSeedProvider { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		/// <summary>
		/// Constructor with dependencies.
		/// </summary>
		public RequestSeedHandler(
			IServiceSeedProvider serviceSeedProvider) 
		{
			ServiceSeedProvider = serviceSeedProvider;
		}

		protected override RequestSeedResponse InnerHandle(RequestSeedRequest request, RequestSeedResponse response)
		{
			// Creëer een nieuwe Seed, en sla deze op in de database.
			var seed = Guid.NewGuid().ToString("N");

			try
			{
				ServiceSeedProvider.StoreSeed(seed);
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("There was an error generating a new Seed value.", ex);
				return response.Error();
			}

			response.Seed = seed;
			return response.Ok();
		}
	}
}