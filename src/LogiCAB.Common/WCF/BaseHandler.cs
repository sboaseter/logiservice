using System;
using LogiCAB.Contract;

namespace LogiCAB.Common.WCF
{
	///<summary>
	/// BaseRequest handler, offers basic functionality, including the call to
	/// InnerHandle, which should be implemented by specific Handlers.
	///</summary>
	public abstract class BaseHandler<TQ, TP> : IHandler<TQ, TP>
		where TQ : BaseRequest
		where TP : BaseResponse
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  		
		protected BaseHandler()
		{
		}

		public TP Handle(TQ request, TP response)
		{
			Log.DebugFormat("RequestTag = [{0}]", request.RequestTag ?? "<null>");
			response.RequestTag = request.RequestTag;
			response.ResultCode = 0;

			// Voer eerst de PreHandle uit voor authenticatie en authorisatie.
			try
			{
				response = PreHandle(request, response);

				// Warn/Debug Log the response to save on a lot of basic logging in the handlers.
				if (response.ResultCode < 0)
					Log.WarnFormat("PreHandle Response [{0}] {1}", response.ResultCode, response.ResultDescription);
				else
					Log.DebugFormat("PreHandle Response [{0}] {1}", response.ResultCode, response.ResultDescription);
				if (response.ResultInformation != null)
					foreach (var info in response.ResultInformation)
						Log.DebugFormat("Info: {0}", info);
			}
			catch (Exception ex)
			{
				Log.Error("Unhandled error in PreHandle", ex);
				return response.Error();
			}

			// Als we een negatieve resultcode uit PreHandle kregen, stoppen we
			// hier.
			if (response.ResultCode < 0)
				return response;

			// Voer dan de InnerHandle uit voor het echte werk.
			try
			{
				response = InnerHandle(request, response);

				// Warn/Debug Log the response to save on a lot of basic logging in the handlers.
				if (response.ResultCode < 0)
					Log.WarnFormat("InnerHandle Response [{0}] {1}", response.ResultCode, response.ResultDescription);
				else
					Log.DebugFormat("InnerHandle Response [{0}] {1}", response.ResultCode, response.ResultDescription);
				if (response.ResultInformation != null)
					foreach (var info in response.ResultInformation)
						Log.DebugFormat("Info: {0}", info);
			}
			catch (Exception ex)
			{
				Log.Error("Unhandled error in InnerHandle", ex);
				return response.Error();
			}

			// Klaar.
			return response;
		}

		/// <summary>
		/// PreHandle wordt aangeroepen v��r InnerHandle en kan gebruikt worden
		/// voor zaken als authenticatie en authorisatie. Als PreHandle een
		/// negatieve ResultCode zet wordt InnerHandle niet meer aangeroepen.
		/// </summary>
		protected virtual TP PreHandle(TQ request, TP response)
		{
			return response;
		}

		/// <summary>
		/// InnerHandle wordt aangeroepen en wordt gezien als de plek waar het
		/// echte werk wordt afgehandeld.
		/// </summary>
		protected abstract TP InnerHandle(TQ request, TP response);
	}
}