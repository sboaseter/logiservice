using System;
using LogiCAB.Contract;

namespace LogiCAB.Common.WCF
{
	///<summary>
	/// Extensions for common wcf classes. Mostly BaseResponse.
	///</summary>
	public static class Extensions
	{
		public static T Result<T>(this T response, int code, string description, params string[] information) where T : BaseResponse
		{
			response.ResultCode = code;
			response.ResultDescription = description;
			response.ResultInformation = information;
			return response;
		}

		public static T Ok<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(0, "Ok", information);
		}
		public static T NoChange<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(1, "No Change", information);
		}
		public static T Error<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-1, "Error", information);
		}
		public static T NotImplemented<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2, "Not Implemented", information);
		}

		public static T AuthenticationFailed<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-1001, "Authentication Failed", information);
		}
		public static T RequestUnauthorized<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-1002, "Request Unauthorized", information);
		}
		public static T RequestInvalid<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-1003, "Request Invalid", information);
		}
		

		public static T InvalidOrganisationKey<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2001, "Invalid Organisation Key", information);
		}
		public static T OrganisationNotFound<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2002, "Organisation Not Found", information);
		}
		public static T FunctionNotSpecified<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2003, "Function Not Specified", information);
		}
		public static T FunctionInvalid<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2004, "Function Invalid", information);
		}
		public static T OrganisationNotInShop<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2005, "Organisation Not In Shop", information);
		}
		public static T ReadonlyFieldChanged<T>(this T response, string fieldName, params string[] information) where T : BaseResponse
		{
			return response.Result(-2006, String.Format("Readonly Field {0} Changed", fieldName), information);
		}
		public static T UniqueFieldNotUnique<T>(this T response, string fieldName, params string[] information) where T : BaseResponse
		{
			return response.Result(-2007, String.Format("Unique Field {0} Not Unique", fieldName), information);
		}
		public static T InvalidPersonKey<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2008, "Invalid Person Key", information);
		}
		public static T PersonNotFound<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2009, "Person Not Found", information);
		}
		public static T InvalidLoginKey<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2010, "Invalid Login Key", information);
		}
		public static T LoginNotFound<T>(this T response, params string[] information) where T : BaseResponse
		{
			return response.Result(-2011, "Login Not Found", information);
		}
        public static T InvalidPictureKey<T>(this T response, params string[] information) where T : BaseResponse
        {
            return response.Result(-2012, "Invalid Picture Key", information);
        }
        public static T PictureNotFound<T>(this T response, params string[] information) where T : BaseResponse
        {
            return response.Result(-2013, "Picture Not Found", information);
        }

	}
}