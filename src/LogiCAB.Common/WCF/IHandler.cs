﻿using System;
using LogiCAB.Contract;

namespace LogiCAB.Common.WCF
{
	///<summary>
	/// Defines a Request Handler for a specific Service Request/Response.
	///</summary>
	public interface IHandler<TQ, TP>
		where TQ : BaseRequest
		where TP : BaseResponse
	{
		///<summary>
		/// Handles the given Request and returns a valid Response.
		///</summary>
		TP Handle(TQ request, TP response);
	}
}