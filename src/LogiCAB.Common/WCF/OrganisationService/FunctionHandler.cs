using System;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Simple Handler class for Function Requests.
	///</summary>
	public class FunctionHandler : ShopManageHandler<FunctionRequest, FunctionResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }
		protected IRepository<Function> Functions { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public FunctionHandler(
			IRepository<Organisation> organisations,
			IRepository<Function> functions,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
			Functions = functions;
		}

		protected override FunctionResponse InnerHandle(FunctionRequest request, FunctionResponse response)
		{
			// Valideer de Organisation Key.
			if (!request.Key.Validate(response))
				return response;

			// Controleer of er een Function is opgegeven.
			if (String.IsNullOrEmpty(request.Function))
			{
				Log.WarnFormat("Invalid Request, no Function specified.");
				return response.FunctionNotSpecified();
			}

			// Haal de Function op.
			var function = Functions.SingleOrDefault(f => f.Code == request.Function);
			if (function == null)
			{
				Log.WarnFormat("Function [{0}] not found.", request.Function);
				return response.FunctionInvalid();
			}

			// Controleer of de Function wel mag...
			// TODO: Mechanisme verzinnen hiervoor. Bit veld in Function?
			if (request.Function != "Dummy")
			{
				Log.WarnFormat("Function [{0}] not allowed.", request.Function);
				return response.FunctionInvalid();
			}

			// Dan gaan we eens kijken of we een Organisatie kunnen vinden.
			var found = request.Key.FindIn(Organisations);
			if (found == null)
			{
				Log.WarnFormat("Unable to find Organisation.");
				return response.OrganisationNotFound();
			}

			// Check if we're authorized.
			if (!IsAuthorizedFor(found, response))
				return response;

			// Controleer of de Organisatie wel in deze Shop zit.
			var shopOrga = found.Shops.SingleOrDefault(s => s.Shop == Shop.Shop);
			if (shopOrga == null)
			{
				Log.WarnFormat("Organisation not in Shop.");
				return response.OrganisationNotInShop();
			}

			// Controleer of hij de Function al heeft of niet.
			var hasFunction = shopOrga.Functions.Contains(function);
			if ((hasFunction && !request.Remove) || (!hasFunction && request.Remove))
				return response.NoChange();

			// Voeg de Function toe, of verwijder deze.
			if (request.Remove)
				shopOrga.Functions.Remove(function);
			else
				shopOrga.Functions.Add(function);
			Organisations.Save(found);

			return response.Ok();
		}
	}
}