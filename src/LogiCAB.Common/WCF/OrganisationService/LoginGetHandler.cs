using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor LoginGet Requests.
	///</summary>
	public class LoginGetHandler : ShopManageHandler<LoginGetRequest, LoginGetResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Login> Logins { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public LoginGetHandler(
			IRepository<Login> logins,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Logins = logins;
		}

		protected override LoginGetResponse InnerHandle(LoginGetRequest request, LoginGetResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Key.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Login kunnen vinden.
			var found = request.Key.FindIn(Logins);
			
            if (found == null)
				return response.LoginNotFound();

			// Authorizeer de Shop.
			if (!IsAuthorizedFor(found, response))
				return response;

			response.Login = found.ToContract();
			return response.Ok();
		}
	}
}