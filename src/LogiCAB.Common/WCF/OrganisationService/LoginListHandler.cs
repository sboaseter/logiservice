using System;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor LoginList Requests.
	///</summary>
	public class LoginListHandler : ShopManageHandler<LoginListRequest, LoginListResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Person> Persons { get; set; }
		protected IRepository<Login> Logins { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public LoginListHandler(
			IRepository<Person> persons,
			IRepository<Login> logins,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Persons = persons;
			Logins = logins;
		}

		protected override LoginListResponse InnerHandle(LoginListRequest request, LoginListResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Person.Validate(response))
				return response;

			// Kijken of we de Persoon kunnen vinden.
			var person = request.Person.FindIn(Persons);
			if (person == null)
				return response.PersonNotFound();

			// Controleer authorisatie.
			if (!IsAuthorizedFor(person, response))
				return response;

			// Geef de lijst terug als response.
			// Extra ToArray, omdat NHibernate.Linq de Select expressie niet slikt.
			response.Logins = Logins
				.Where(l => l.Person == person).ToArray()
				.Select(l => l.ToContract()).ToArray();
			return response.Ok();
		}
	}
}