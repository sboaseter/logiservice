using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor LoginPost Requests.
	///</summary>
	public class LoginPostHandler : ShopManageHandler<LoginPostRequest, LoginPostResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Person> Persons { get; set; }
		protected IRepository<Login> Logins { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public LoginPostHandler(
			IRepository<Person> persons,
			IRepository<Login> logins,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Persons = persons;
			Logins = logins;
		}

		protected override LoginPostResponse InnerHandle(LoginPostRequest request, LoginPostResponse response)
		{
			// Beginnen met validatie van het request.
			if (request.Login == null)
				return response.RequestInvalid("Login is null.");

			// Valideer de Persoon.
			if (!request.Login.Person.Validate(response))
				return response;

			var person = request.Login.Person.FindIn(Persons);
            //var person = Persons.SingleOrDefault(x => x.ExtRef == request.Login.Person.Sequence);
			
            if (person == null)
				return response.PersonNotFound();

			// Valideer dat de Persoon in de huidige shop zit.
			if (!IsAuthorizedFor(person, response))
				return response;

			// Valideer de verplichte velden.
			var information = new List<string>();
			var valid = true .ValidateNotNullOrEmpty(request.Login.Name, "Name", information);

			if (!request.Login.Sequence.HasValue)
				valid = valid.ValidateNotNullOrEmpty(request.Login.Password, "Password", information);
			
            if (!valid)
				return response.RequestInvalid(information.ToArray());

			// Cleanup de niet verplichte velden.
			request.Login.Password = String.IsNullOrEmpty(request.Login.Password) ? null : request.Login.Password;

			// Valideer de uniciteit van de naam.
            var existingLoginOnName = Logins.Where(l => l.Name == request.Login.Name && l.Password == request.Login.Password).SingleOrDefault();
			
            if (existingLoginOnName != null)
			{
                if (existingLoginOnName.Person.Sequence != request.Login.Person.Sequence)
                    return response.UniqueFieldNotUnique("Name and password");
                //if (!existingLoginOnName.Sequence.Equals(request.Login.Sequence))
                //    return response.UniqueFieldNotUnique("Name");
                else
                {
                    request.Login.Sequence = existingLoginOnName.Sequence;
                    return PerformUpdate(request, response, person);
                }
			}

            var existingLoginOnPers = Logins.Where(l => l.Person.Sequence == request.Login.Person.Sequence && l.Blocked == 0).FirstOrDefault();

            if (existingLoginOnPers != null)
            {
                request.Login.Sequence = existingLoginOnPers.Sequence;
                return PerformUpdate(request, response, person);
            }

			// Update of Insert?
            //if (request.Login.Person.HasValue)
            //    return PerformUpdate(request, response, person);
			return PerformInsert(request, response, person);
		}

		private LoginPostResponse PerformUpdate(LoginPostRequest request, LoginPostResponse response, Person person)
		{
			// Haal de bestaande persoon op.
			var login = Logins.GetById(request.Login.Sequence);
			if (login == null)
				return response.LoginNotFound();

			// Valideer dat de Persoon niet gewijzigd wordt.
			if (!login.Person.Equals(person))
				return response.ReadonlyFieldChanged("Person");

			// Zet de velden die gewijzigd zijn. Hij is live, dus het updaten
			// gaat automatisch.
			var changed = false;

			if (!login.Name.Equals(request.Login.Name))
			{
				changed = true;
				login.Name = request.Login.Name;
			}

			if (!login.Blocked.Equals(request.Login.Blocked))
			{
				changed = true;
				login.Blocked = (short)(request.Login.Blocked ? 1 : 0);
			}

			if (!String.IsNullOrEmpty(request.Login.Password))
			{
				changed = true;
				login.Password = request.Login.Password;
				login.PasswordHash = Helper.CreateCABPasswordHash(request.Login.Password);
			}

            if (request.Login.language.HasValue)
            {
                login.Language = request.Login.language;
            }

			response.Login = login.ToContract();
            
            if (changed)
                Logins.Save(login);

			return !changed ? response.NoChange() : response.Ok();
		}

		private LoginPostResponse PerformInsert(LoginPostRequest request, LoginPostResponse response, Person person)
		{
			// Cre�er een Model versie van de Persoon.
			var login = new Login
			{
				Person = person,
				Name = request.Login.Name,
				Password = request.Login.Password,
				PasswordHash = Helper.CreateCABPasswordHash(request.Login.Password),
				Blocked = (short)(request.Login.Blocked ? 1 : 0),
				LoginGroup = person.Organisation.ToLoginGroup(),
                Language = request.Login.language
			};

			try
			{
				Logins.Save(login);
				Log.InfoFormat("Login [{0}] Inserted.", login.Sequence);
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("Insert Failed. Error Saving Login.", ex);
				return response.Error();
			}

			// Geef de resulterende Persoon terug.
			response.Login = login.ToContract();
			return response.Ok();
		}


	}
}