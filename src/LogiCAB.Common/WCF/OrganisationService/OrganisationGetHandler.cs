using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor OrganisationGet Requests.
	///</summary>
	public class OrganisationGetHandler : ShopManageHandler<OrganisationGetRequest, OrganisationGetResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public OrganisationGetHandler(
			IRepository<Organisation> organisations,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
		}

		protected override OrganisationGetResponse InnerHandle(OrganisationGetRequest request, OrganisationGetResponse response)
		{
			// Valideer de Organisation Key.
			if (!request.Key.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Organisatie kunnen vinden.
			var found = request.Key.FindIn(Organisations);

			if (found == null)
			{
				Log.WarnFormat("Unable to find Organisation.");
				return response.OrganisationNotFound();
			}

			// Check Authorisatie.
			if (!IsAuthorizedFor(found, response))
				return response;

			response.Organisation = found.ToContract();
			return response.Ok();
		}
	}
}