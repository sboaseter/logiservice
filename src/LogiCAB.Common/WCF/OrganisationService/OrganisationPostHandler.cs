using System;
using System.Collections.Generic;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor OrganisationPost Requests.
	///</summary>
	public class OrganisationPostHandler : ShopManageHandler<OrganisationPostRequest, OrganisationPostResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }
		protected ISystemCounterProvider SystemCounterProvider { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public OrganisationPostHandler(
			IRepository<Organisation> organisations,
			ISystemCounterProvider systemCounterProvider,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
			SystemCounterProvider = systemCounterProvider;
		}

		protected override OrganisationPostResponse InnerHandle(OrganisationPostRequest request, OrganisationPostResponse response)
		{
			// Beginnen met validatie van het Request.

			if (request.Organisation == null)
			{
				Log.WarnFormat("Organisation is null.");
				return response.RequestInvalid("Organisation is null.");
			}

			var requestValid = true;
			var information = new List<string>();

            OrganisationContract tmpOrganisation = new OrganisationContract();
            Organisation tmpOrga = new Organisation();

            if (!String.IsNullOrEmpty(request.Organisation.EAN))
            {
                tmpOrga = Organisations.GetByEAN(request.Organisation.EAN);

                if (tmpOrga != null)
                    request.Organisation.Sequence = tmpOrga.Sequence;
            }

			if (String.IsNullOrEmpty(request.Organisation.Name))
			{
				requestValid = false;
				information.Add("Name is null or empty.");
				Log.WarnFormat("Name is null or empty.");
			}

			if (String.IsNullOrEmpty(request.Organisation.Email))
			{
				requestValid = false;
				information.Add("Email is null or empty.");
				Log.WarnFormat("Email is null or empty.");
			}

			if (String.IsNullOrEmpty(request.Organisation.EAN))
			{
				requestValid = false;
				information.Add("EAN is null or empty.");
				Log.WarnFormat("EAN is null or empty.");
			}

			if (!requestValid)
			{
				Log.WarnFormat("Request Invalid.");
				return response.RequestInvalid(information.ToArray());
			}

			// Update of Insert?
            if (tmpOrga != null)
				return OrganisationPostUpdate(request, response);

			return OrganisationPostInsert(request, response);
		}


		private OrganisationPostResponse OrganisationPostUpdate(OrganisationPostRequest request, OrganisationPostResponse response)
		{
			// Nu zouden we een bestaande moeten kunnen vinden.
// ReSharper disable PossibleInvalidOperationException
// Die check staat vlak voor de aanroep van deze function.
			var existing = Organisations.GetById(request.Organisation.Sequence.Value);
// ReSharper restore PossibleInvalidOperationException
			
            if (existing == null)
			{
				Log.WarnFormat("Update Failed. Invalid Sequence. [{0}]", request.Organisation.Sequence.Value);
				return response.OrganisationNotFound();
			}

			// Bepaal of het een non-local Organisation is.
			if (!IsAuthorizedFor(existing, response))
				return response;

			// Controleren of het Type niet gewijzigd wordt...
			//if (!existing.Type.Equals(request.Organisation.Type.ToDbString()))
			//{
				//Log.WarnFormat("Update Failed. Organisation Type changed.");
				//return response.ReadonlyFieldChanged("Type");
			//}

			// Validate Unicity of values.
			if (!Organisations.IsNameUnique(request.Organisation.Name, existing.Sequence))
			{
				Log.WarnFormat("Name not unique.");
				return response.UniqueFieldNotUnique("Name");
			}
			
            if (!Organisations.IsEANUnique(request.Organisation.EAN, existing.Sequence))
			{
				Log.WarnFormat("EAN not unique.");
				return response.UniqueFieldNotUnique("EAN");
			}

			// Map de te updaten waarden van het Contract.
			var changed = false;
			
            if (!existing.Name.Equals(request.Organisation.Name))
			{
				existing.Name = request.Organisation.Name;
				existing.NameUpper = existing.Name.ToUpperInvariant();
				changed = true;
			}
			
            if (!existing.Phone.Equals(request.Organisation.Phone ?? String.Empty))
			{
				existing.Phone = request.Organisation.Phone ?? String.Empty;
				changed = true;
			}
			
            if (!existing.Fax.Equals(request.Organisation.Fax ?? String.Empty))
			{
				existing.Fax = request.Organisation.Fax ?? String.Empty;
				changed = true;
			}
			
            if (!existing.Email.Equals(request.Organisation.Email ?? String.Empty))
			{
				existing.Email = request.Organisation.Email ?? String.Empty;
				changed = true;
			}
			
            if (!existing.Mailbox.Equals(request.Organisation.Mailbox ?? String.Empty))
			{
				existing.Mailbox = request.Organisation.Mailbox ?? String.Empty;
				changed = true;
			}
			
            if (!existing.EAN.Equals(request.Organisation.EAN ?? String.Empty))
			{
				existing.EAN = request.Organisation.EAN ?? String.Empty;
				changed = true;
			}
			
            if (!existing.VAT.Equals(request.Organisation.VAT ?? String.Empty))
			{
				existing.VAT = request.Organisation.VAT ?? String.Empty;
				changed = true;
			}
			
            if (!existing.VBA.Equals(request.Organisation.VBA ?? String.Empty))
			{
				existing.VBA = request.Organisation.VBA ?? String.Empty;
				changed = true;
			}

			if (!existing.BVH.Equals(request.Organisation.BVH ?? String.Empty))
			{
				existing.BVH = request.Organisation.BVH ?? String.Empty;
				changed = true;
			}

			if (existing.Info == null)
			{
				// Maak een Info record aan.
				existing.Info = new OrganisationInfo
				{
					CreatedBy = request.AuthenticationUsername,
					CreatedOn = DateTime.Now,
					ModifiedBy = request.AuthenticationUsername,
					ModifiedOn = DateTime.Now,
					Organisation = existing,
					ShippingDays = request.Organisation.ShippingDays ?? String.Empty,
					Deadline = request.Organisation.Deadline.ToTimeSpan()
				};
				changed = true;
			} 
            else
			{
				if (!existing.Info.ShippingDays.Equals(request.Organisation.ShippingDays ?? String.Empty))
				{
					existing.Info.ShippingDays = request.Organisation.ShippingDays ?? String.Empty;
					existing.Info.ModifiedBy = request.AuthenticationUsername;
					existing.Info.ModifiedOn = DateTime.Now;
					changed = true;
				}

				if (!existing.Info.Deadline.Equals(request.Organisation.Deadline.ToTimeSpan()))
				{
					existing.Info.Deadline = request.Organisation.Deadline.ToTimeSpan();
					existing.Info.ModifiedBy = request.AuthenticationUsername;
					existing.Info.ModifiedOn = DateTime.Now;
					changed = true;
				}
			}

			// Geen update als er geen wijzigingen waren.
			if (!changed)
			{
				Log.InfoFormat("Organisation Posted without Change.");
				response.Organisation = existing.ToContract();
				return response.NoChange();
			}

			try
			{
				Organisations.Save(existing);
				Log.InfoFormat("Organisation [{0}] Updated.", existing.Sequence);
				response.Organisation = existing.ToContract();
				return response.Ok();
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("Update Failed. Error Saving Organisation.", ex);
				return response.Error();
			}
		}


		private OrganisationPostResponse OrganisationPostInsert(OrganisationPostRequest request, OrganisationPostResponse response)
		{
			// Validate Unicity of values.
			if (!Organisations.IsNameUnique(request.Organisation.Name))
			{
				Log.WarnFormat("Name not unique.");
				return response.UniqueFieldNotUnique("Name");
			}

			if (!Organisations.IsEANUnique(request.Organisation.EAN))
			{
				Log.WarnFormat("EAN not unique.");
				return response.UniqueFieldNotUnique("EAN");
			}

			// Map de Contract gegevens op een Model entiteit.
			var input = new Organisation
			{
				Type = request.Organisation.Type.ToDbString(),
				Code = request.Organisation.Code,
				Name = request.Organisation.Name,
				Phone = request.Organisation.Phone ?? String.Empty,
				Fax = request.Organisation.Fax ?? String.Empty,
				Email = request.Organisation.Email ?? String.Empty,
				Mailbox = request.Organisation.Mailbox ?? String.Empty,
				EAN = request.Organisation.EAN,
				VAT = request.Organisation.VAT ?? String.Empty,
				VBA = request.Organisation.VBA ?? String.Empty,
				BVH = request.Organisation.BVH ?? String.Empty,
				Referer = request.Organisation.Referer ?? String.Empty
			};

			// Bereken de server-side dingen.
			input.Code = String.Concat(
				DateTime.Now.ToString("MMdd"),
				SystemCounterProvider.GetOrganisationCounter(true)
				);
			input.NameUpper = input.Name.ToUpperInvariant();
			input.Start = DateTime.Today;
			input.End = new DateTime(3000, 1, 1);

			// Maak een Buyer of Grower record aan.
			if (request.Organisation.Type == OrganisationType.Buyer)
				input.Buyer = new Buyer
				{
					Organisation = input,
					Mobile = request.Organisation.Phone ?? String.Empty
				};
			else
				input.Grower = new Grower
				{
					Organisation = input,
					Mobile = request.Organisation.Phone ?? String.Empty,
					NumberOfM2 = 0,
					ISO = 0,
					Florimark = 0
				};

			// Maak een OrganisationAdmin record aan.
			input.Admin = new OrganisationAdmin
			{
				Organisation = input,
				Date = DateTime.Now,
				MemberType = 0,
				Status = 0
			};

			// Maak een ShopOrganisation record aan voor de Shop van de gebruiker.
			input.Shops.Add(new ShopOrganisation
			{
				Organisation = input,
				Shop = Shop.Shop
			});

			// Maak een Info record aan.
			input.Info = new OrganisationInfo
			{
				CreatedBy = request.AuthenticationUsername,
				CreatedOn = DateTime.Now,
				ModifiedBy = request.AuthenticationUsername,
				ContactEmail = String.Empty,
				ContactFax = String.Empty,
				ContactName = String.Empty,
                OrderName = string.Empty,
				ContactPhone = String.Empty,
				ModifiedOn = DateTime.Now,
				Organisation = input,
				ShippingDays = request.Organisation.ShippingDays ?? String.Empty,
				Deadline = request.Organisation.Deadline.ToTimeSpan()
			};

			// Insert de nieuwe Organisatie.
			try
			{
				Organisations.Save(input);

				// Log
				Log.InfoFormat("Organisation [{0}] Inserted.", input.Sequence);

				if (input.Buyer != null)
					Log.InfoFormat("Buyer [{0}] Inserted", input.Buyer.Sequence);
				
                if (input.Grower != null)
					Log.InfoFormat("Grower [{0}] Inserted", input.Grower.Sequence);
				
                Log.InfoFormat("OrganisationAdmin [{0}] Inserted", input.Admin.Sequence);
				
                foreach (var shopOrga in input.Shops)
					Log.InfoFormat("ShopOrganisation [{0}] Inserted", shopOrga.Sequence);
				
                Log.InfoFormat("OrganisationInfo [{0}] Inserted", input.Info.Sequence);
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("Insert Failed. Error Saving Organisation.", ex);
				return response.Error();
			}

			// Geef de resulterende Organisatie terug.
			response.Organisation = input.ToContract();
			return response.Ok();
		}
	}
}