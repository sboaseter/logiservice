﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using LogiCAB.Contract.OrganisationService;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Implementeerd het Organisation Service Contract.
	///</summary>
	[ServiceBehavior(Namespace = Meta.Namespace), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class OrganisationServiceContract : IOrganisationServiceContract
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
		protected IHandler<OrganisationGetRequest, OrganisationGetResponse> OrganisationGetHandler { get; set; }
		protected IHandler<OrganisationPostRequest, OrganisationPostResponse> OrganisationPostHandler { get; set; }
		protected IHandler<FunctionRequest, FunctionResponse> FunctionHandler { get; set; }
		protected IHandler<ShopLinkRequest, ShopLinkResponse> ShopLinkHandler { get; set; }
		protected IHandler<PersonGetRequest, PersonGetResponse> PersonGetHandler { get; set; }
		protected IHandler<PersonListRequest, PersonListResponse> PersonListHandler { get; set; }
		protected IHandler<PersonPostRequest, PersonPostResponse> PersonPostHandler { get; set; }
		protected IHandler<LoginGetRequest, LoginGetResponse> LoginGetHandler { get; set; }
		protected IHandler<LoginListRequest, LoginListResponse> LoginListHandler { get; set; }
		protected IHandler<LoginPostRequest, LoginPostResponse> LoginPostHandler { get; set; }

		///<summary>
		/// Constructor with Dependencies.
		///</summary>
		public OrganisationServiceContract(
			IHandler<OrganisationGetRequest, OrganisationGetResponse> organisationGetHandler,
			IHandler<OrganisationPostRequest, OrganisationPostResponse> organisationPostHandler,
			IHandler<FunctionRequest, FunctionResponse> functionHandler,
			IHandler<ShopLinkRequest, ShopLinkResponse> shopLinkHandler,
			IHandler<PersonGetRequest, PersonGetResponse> personGetHandler,
			IHandler<PersonListRequest, PersonListResponse> personListHandler,
			IHandler<PersonPostRequest, PersonPostResponse> personPostHandler,
			IHandler<LoginGetRequest, LoginGetResponse> loginGetHandler,
			IHandler<LoginListRequest, LoginListResponse> loginListHandler,
			IHandler<LoginPostRequest, LoginPostResponse> loginPostHandler) 
        {
			OrganisationGetHandler = organisationGetHandler;
			OrganisationPostHandler = organisationPostHandler;
			FunctionHandler = functionHandler;
			ShopLinkHandler = shopLinkHandler;
			PersonGetHandler = personGetHandler;
			PersonListHandler = personListHandler;
			PersonPostHandler = personPostHandler;
			LoginGetHandler = loginGetHandler;
			LoginListHandler = loginListHandler;
			LoginPostHandler = loginPostHandler;
		}

		public OrganisationGetResponse OrganisationGet(OrganisationGetRequest request)
		{
			return OrganisationGetHandler.Handle(request, new OrganisationGetResponse());
		}

		public OrganisationListResponse OrganisationList(OrganisationListRequest request)
		{
			Log.DebugFormat("RequestTag = [{0}]", request.RequestTag ?? "<null>");
			var response = new OrganisationListResponse
			{
				RequestTag = request.RequestTag
			};

			return response.NotImplemented();
		}

		public OrganisationPostResponse OrganisationPost(OrganisationPostRequest request)
		{
			return OrganisationPostHandler.Handle(request, new OrganisationPostResponse());
		}

		public ShopLinkResponse ShopLink(ShopLinkRequest request)
		{
			return ShopLinkHandler.Handle(request, new ShopLinkResponse());
		}

		public FunctionResponse Function(FunctionRequest request)
		{
			return FunctionHandler.Handle(request, new FunctionResponse());
		}

		public PersonGetResponse PersonGet(PersonGetRequest request)
		{
			return PersonGetHandler.Handle(request, new PersonGetResponse());
		}

		public PersonListResponse PersonList(PersonListRequest request)
		{
			return PersonListHandler.Handle(request, new PersonListResponse());
		}

		public PersonPostResponse PersonPost(PersonPostRequest request)
		{
			return PersonPostHandler.Handle(request, new PersonPostResponse());
		}

		public LoginGetResponse LoginGet(LoginGetRequest request)
		{
			return LoginGetHandler.Handle(request, new LoginGetResponse());
		}

		public LoginListResponse LoginList(LoginListRequest request)
		{
			return LoginListHandler.Handle(request, new LoginListResponse());
		}

		public LoginPostResponse LoginPost(LoginPostRequest request)
		{
			return LoginPostHandler.Handle(request, new LoginPostResponse());
		}
	}
}
