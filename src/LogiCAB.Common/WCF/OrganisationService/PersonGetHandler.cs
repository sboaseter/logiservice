using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor PersonGet Requests.
	///</summary>
	public class PersonGetHandler : ShopManageHandler<PersonGetRequest, PersonGetResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Person> Persons { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public PersonGetHandler(
			IRepository<Person> persons,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Persons = persons;
		}

		protected override PersonGetResponse InnerHandle(PersonGetRequest request, PersonGetResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Key.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Person kunnen vinden.
			var found = request.Key.FindIn(Persons);
			if (found == null)
				return response.PersonNotFound();

			// Check Authorisatie.
			if (!IsAuthorizedFor(found, response))
				return response;

			response.Person = found.ToContract();
			return response.Ok();
		}
	}
}