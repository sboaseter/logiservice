using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor PersonList Requests.
	///</summary>
	public class PersonListHandler : ShopManageHandler<PersonListRequest, PersonListResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }
		protected IRepository<Person> Persons { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public PersonListHandler(
			IRepository<Organisation> organisations,
			IRepository<Person> persons,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
			Persons = persons;
		}

		protected override PersonListResponse InnerHandle(PersonListRequest request, PersonListResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Organisation.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Person kunnen vinden.
			var orga = request.Organisation.FindIn(Organisations);
			if (orga == null)
				return response.OrganisationNotFound();

			// Check Authorisatie.
			if (!IsAuthorizedFor(orga, response))
				return response;

			// Haal een lijst op van de Personen van deze Organisatie.
			var contracts = new List<PersonContract>();
			foreach (var person in Persons.Where(p => p.Organisation == orga))
				contracts.Add(person.ToContract());

			// Geef de lijst terug als response.
			response.Persons = contracts.ToArray();
			return response.Ok();
		}
	}
}