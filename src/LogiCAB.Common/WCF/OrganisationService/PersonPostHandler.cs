using System;
using System.Collections.Generic;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using System.Linq;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor PersonPost Requests.
	///</summary>
	public class PersonPostHandler : ShopManageHandler<PersonPostRequest, PersonPostResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }
		protected IRepository<Person> Persons { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public PersonPostHandler(
			IRepository<Organisation> organisations,
			IRepository<Person> persons,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
			Persons = persons;
		}

		protected override PersonPostResponse InnerHandle(PersonPostRequest request, PersonPostResponse response)
		{
			// Beginnen met validatie van het request.
			if (request.Person == null)
				return response.RequestInvalid("Person is null.");

			// Valideer de Organisatie.
			if (!request.Person.Organisation.Validate(response))
				return response;

			var orga = request.Person.Organisation.FindIn(Organisations);

			if (orga == null)
				return response.OrganisationNotFound();

			// Valideer dat de Organisatie in de huidige shop zit.
			if (!IsAuthorizedFor(orga, response))
				return response;

			//var existingPerson = Persons.Select(x => 
			//    x.FirstName == request.Person.FirstName && 
			//    x.LastName == request.Person.LastName &&
			//    x.Email == request.Person.Email &&
			//    x.Organisation.Sequence == request.Person.Organisation.Sequence &&
			//    x.Sequence == request.Person.Sequence);

			PersonContract tmpPerson = new PersonContract();
            Person tmpPers = new Person();

            if (request.Person.Sequence.HasValue)
            {
                tmpPers = Persons.SingleOrDefault(x => x.ExtRef == request.Person.Sequence);

                if(tmpPers != null)
                    tmpPerson = tmpPers.ToContract();
            }


			if(tmpPerson.Sequence.HasValue)
				Log.InfoFormat("Person found, sequence: {0}, extref: {1}", tmpPerson.Sequence, tmpPerson.ExtRef);

			// Valideer de verplichte velden.
			var information = new List<string>();
			if (!(true
				.ValidateNotNullOrEmpty(request.Person.FirstName, "FirstName", information)
				.ValidateNotNullOrEmpty(request.Person.LastName, "LastName", information)
				.ValidateNotNullOrEmpty(request.Person.Email, "Email", information)))
				return response.RequestInvalid(information.ToArray());

			// Update de niet verplichte velden zodat ze niet null zijn.
			request.Person.MiddleName = request.Person.MiddleName ?? String.Empty;
			request.Person.Phone = request.Person.Phone ?? String.Empty;

			// Update of Insert?
			//if (request.Person.Sequence.HasValue)
			if(tmpPerson.Sequence.HasValue)
				return PerformUpdate(request, response, orga);

			return PerformInsert(request, response, orga);
		}

		private PersonPostResponse PerformUpdate(PersonPostRequest request, PersonPostResponse response, Organisation organisation)
		{
			// Haal de bestaande persoon op.
			//var person = Persons.GetById(request.Person.Sequence);
            var person = Persons.SingleOrDefault(x => x.ExtRef == request.Person.Sequence);
			if (person == null)
				return response.PersonNotFound();

			// Valideer dat de Organisatie niet gewijzigd wordt.
			if (!person.Organisation.Equals(organisation))
				return response.ReadonlyFieldChanged("Organisation");

			// Zet de velden die gewijzigd zijn. Hij is live, dus het updaten
			// gaat automatisch.
			var changed = false;
			if (!person.FirstName.Equals(request.Person.FirstName))
			{
				changed = true;
				person.FirstName = request.Person.FirstName;
			}
			if (!person.MiddleName.Equals(request.Person.MiddleName))
			{
				changed = true;
				person.MiddleName = request.Person.MiddleName;
			}
			if (!person.LastName.Equals(request.Person.LastName))
			{
				changed = true;
				person.LastName = request.Person.LastName;
			}
			if (!person.Email.Equals(request.Person.Email))
			{
				changed = true;
				person.Email = request.Person.Email;
			}
			if (!person.Phone.Equals(request.Person.Phone))
			{
				changed = true;
				person.Phone = request.Person.Phone;
			}
			if(changed)
				Persons.Save(person);
			response.Person = person.ToContract();
			return !changed ? response.NoChange() : response.Ok();
		}

		private PersonPostResponse PerformInsert(PersonPostRequest request, PersonPostResponse response, Organisation organisation)
		{
			// Cre�er een Model versie van de Persoon.
			var person = new Person
			{
				Organisation = organisation,
				FirstName = request.Person.FirstName,
				MiddleName = request.Person.MiddleName ?? String.Empty,
				LastName = request.Person.LastName,
				Email = request.Person.Email,
				Phone = request.Person.Phone ?? String.Empty,
                ExtRef = (decimal) request.Person.Sequence
			};

			try
			{
				Persons.Save(person);
				Log.InfoFormat("Person [{0}] Inserted.", person.Sequence);
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("Insert Failed. Error Saving Person.", ex);
				return response.Error();
			}

			// Geef de resulterende Persoon terug.
			response.Person = person.ToContract();
			return response.Ok();
		}

	}
}