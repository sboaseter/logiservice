using System;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.OrganisationService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.OrganisationService
{
	///<summary>
	/// Handler voor ShopLink Requests.
	///</summary>
	public class ShopLinkHandler : ShopManageHandler<ShopLinkRequest, ShopLinkResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Organisation> Organisations { get; set; }
		protected IRepository<Handshake> Handshakes { get; set; }
		protected IRepository<GrowerBuyerMatrix> GrowerBuyerMatrixes { get; set; }
		protected IMainGroupMatrixProvider MainGroupMatrixProvider { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public ShopLinkHandler(
			IRepository<Organisation> organisations,
			IRepository<Handshake> handshakes,
			IRepository<GrowerBuyerMatrix> growerBuyerMatrixes,
			IMainGroupMatrixProvider mainGroupMatrixProvider,
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
			Organisations = organisations;
			Handshakes = handshakes;
			GrowerBuyerMatrixes = growerBuyerMatrixes;
			MainGroupMatrixProvider = mainGroupMatrixProvider;
		}

		protected override ShopLinkResponse InnerHandle(ShopLinkRequest request, ShopLinkResponse response)
		{
			// Beginnen met validatie van de Key.
			if (!request.Key.Validate(response))
				return response;

			// Dan gaan we eens kijken of we een Organisatie kunnen vinden.
			var found = request.Key.FindIn(Organisations);
			if (found == null)
			{
				Log.WarnFormat("Unable to find Organisation.");
				return response.OrganisationNotFound();
			}

			var changed = false;

			// Controleer of we de shop niet al hebben.
			if (found.Shops.Count(s => s.Shop == Shop.Shop) == 0)
			{
				// Voeg de ShopOrganisation toe.
				found.Shops.Add(new ShopOrganisation
				{
					Organisation = found,
					Shop = Shop.Shop
				});
				Organisations.Save(found);
				changed = true;
			}


			// Controleer of we een Handshake hebben.
			var myOrga = AuthenticatedRequestValidator.Principal.Login.Person.Organisation;

			var count12 = Handshakes.Where(h => h.Orga1 == myOrga && h.Orga2 == found).Count();
			var count21 = Handshakes.Where(h => h.Orga2 == myOrga && h.Orga1 == found).Count();
			if ((count12 == 0) && (count21 == 0))
			{
				// Cre�er de Handshake.
				var handshake = new Handshake
				{
					Orga1 = myOrga,
					Orga2 = found,
					RequestDate = DateTime.Now,
					AcceptDate = DateTime.Now,
					ApproveDate = DateTime.Now,
					Blocked = 0,
					Mail = String.Empty,
					Remark = String.Empty
				};
				Handshakes.Save(handshake);
				changed = true;
			}

			// Volgorde logica.
			var gbm = new GrowerBuyerMatrix
			{
				LastOrderDate = DateTime.Now,
				Blocked = 0
			};
			if (myOrga.Grower != null && found.Buyer != null)
			{
				gbm.Grower = myOrga.Grower;
				gbm.Buyer = found.Buyer;
				Log.Info("User is grower, subject is buyer.");
				if (GrowerBuyerMatrixes.Where(g => g.Grower == gbm.Grower && g.Buyer == gbm.Buyer).Count() == 0)
				{
					GrowerBuyerMatrixes.Save(gbm);
					MainGroupMatrixProvider.CopyMainGroups(myOrga, found);
					changed = true;
				}
			}
			else if (myOrga.Buyer != null && found.Grower != null)
			{
				gbm.Grower = found.Grower;
				gbm.Buyer = myOrga.Buyer;
				Log.Info("User is buyer, subject is grower.");
				if (GrowerBuyerMatrixes.Where(g => g.Grower == gbm.Grower && g.Buyer == gbm.Buyer).Count() == 0)
				{
					GrowerBuyerMatrixes.Save(gbm);
					MainGroupMatrixProvider.CopyMainGroups(found, myOrga);
					changed = true;
				}
			}
			else
			{
				Log.Warn("No sensible Grower-Buyer relation can be made between User and Subject!");
			}

			if (changed)
				return response.Ok();
			return response.NoChange();
		}
	}
}