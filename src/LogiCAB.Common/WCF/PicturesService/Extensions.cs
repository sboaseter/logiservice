﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using LogiCAB.Contract;
using LogiCAB.Contract.PicturesService;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.PicturesService
{
	internal static class Extensions
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  	
		internal static bool Validate(this PictureKey key, List<string> information)
		{
			if (key == null)
			{
				information.Add("Picture Key is not specified.");
				return false;
			}

			var valid = false;

			if (key.Sequence == null)
				information.Add("Sequence is not specified.");
			else
				valid = true;

			if (String.IsNullOrEmpty(key.Description))
				information.Add("Description is not specified.");
			else
				valid = true;

            if (key.PictType == null)
                information.Add("Picture type to get is not specified.");
            else
                valid = true;

			return valid;
		}

        internal static bool Validate(this PictureKey key, BaseResponse response)
        {
            var information = new List<string>();

            if (!key.Validate(information))
            {
                response.InvalidOrganisationKey(information.ToArray());
                return false;
            }

            return true;
        }

		internal static Picture FindIn(this PictureKey key, IRepository<Picture> pictures)
		{
			try
			{
				if (key.Sequence.HasValue)
				{
					Log.DebugFormat("FindOnSequence {0}", key.Sequence.Value);
                    return pictures.Where(p => p.Sequence == key.Sequence).SingleOrDefault();
				}

                if (!String.IsNullOrEmpty(key.Description))
				{
					Log.DebugFormat("FindOnDescription {0}", key.Description);
                    return pictures.Where(p => p.Description == key.Description).SingleOrDefault();
				}
				
				Log.Warn("No Key values to find picture with.");
				return null;
			}
			catch (Exception ex)
			{
				Log.Error("Error Finding picture.", ex);
				return null;
			}
		}

		internal static PicturesContract [] ToContract(this Picture input)
		{
            return new[] 
            {
                new PicturesContract
			    {
				    Sequence = input.Sequence,
				    //Type = input.typE //todo type implementeren...
				    Description = input.Description,
			    }
            };
		}
        
        internal static PicturesContract ToContract(this QueueProcessing input)
        {
            return new PicturesContract
			{
				Sequence = input.Sequence,
				//Type = input.typE //todo type implementeren...
                Type = input.PictureRefType,
                InsertedOn = input.InsertedOn,
                TypeSequence = input.PictureRefSeq,
			};
        }

		internal static PictureKey ToKey(this Picture input)
		{
			return new PictureKey
			{
				Sequence = input.Sequence,
                Description = input.Description,
			};
		}

		internal static string ToDbString(this PictureType type)
		{
            switch (type)
            {
                case PictureType.CAB:
                    return "CAB";
                case PictureType.GRAS:
                    return "GRAS";
                case PictureType.OFDT:
                    return "OFDT";
                default:
                    return "";
            }
		}

		internal static PictureType ToPictureType(this string input)
		{

            switch (input)
            {
                case "CAB":
                    return PictureType.CAB;
                case "GRAS":
                    return PictureType.GRAS;
                case "OFDT":
                    return PictureType.OFDT;
                default:
                    return PictureType.CAB;
            }
		}

	}
}