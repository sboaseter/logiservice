using System;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Contract.PicturesService;
using System.Collections.Generic;
using LogiFlora.Common.Db;
using System.Linq;

namespace LogiCAB.Common.WCF.PicturesService
{
	///<summary>
	/// Handler voor PicturesGet Requests.
	///</summary>
	public class PicturesGetHandler : ShopManageHandler<PicturesGetRequest, PicturesGetResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
    	protected IRepository<QueueProcessing> QueuePictures { get; set; }
        protected IAuthenticatedRequestValidator AuthenticatedValidator { get; set; }
        protected IRepository<CABCode> CABCodes { get; set; }
        protected IRepository<GrowerAssortment> GrowerAssortments { get; set; }
        protected IRepository<OfferDetail> OfferDetails { get; set; }
     
        
		///<summary>
		/// Constructor with dependencies.
		///</summary>
		public PicturesGetHandler(
			IRepository<QueueProcessing> queuePictures,
            IRepository<CABCode> cabCodes,
            IRepository<GrowerAssortment> growerAssortments,
            IRepository<OfferDetail> offerDetails,
			IShopPropertyProvider shop,
            IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
            QueuePictures = queuePictures;
            CABCodes = cabCodes;
            GrowerAssortments = growerAssortments;
            OfferDetails = offerDetails;
            AuthenticatedValidator = authenticatedRequestValidator;
		}

		protected override PicturesGetResponse InnerHandle(PicturesGetRequest request, PicturesGetResponse response)
		{
			// Valideer de Picture Key.
			if (!request.Key.Validate(response))
				return response;

            var orgaSeq = AuthenticatedValidator.Principal.Organisation.Sequence;
            var pictures = new List<PicturesContract>();
            // Dan gaan we eens kijken of we een foto kunnen vinden.
            string refType = "UNDEF";

            switch (request.Key.PictType)
            {
                case PictureType.CAB:
                    refType = "CAB";
                    break;
                case PictureType.OFDT:
                    refType ="OFDT";
                    break;
                case PictureType.GRAS:
                    refType = "GRAS";
                    break;
            }
            
            IQueryable<QueueProcessing> queuePicList2;
            var queuePicList = QueuePictures.Where(p => (p.PictureRefSeq != 0 && p.OrgaSeq == orgaSeq && p.ProcessedOn == null));

            if (refType != "UNDEF")
                queuePicList2 = queuePicList.Where(p => p.DatabasePictType == refType);
            else
                queuePicList2 = queuePicList;

            foreach (var queuePic in queuePicList2)
            {
                var pictContr = new PicturesContract();
                pictContr.Description = "";

                switch (queuePic.PictureRefType)
                {
                    case PictureType.CAB:
                        var cabCode = CABCodes.GetById(queuePic.PictureRefSeq);

                        if (cabCode == null)
                            Log.WarnFormat("Unable to find cabCode on seq {0}.", queuePic.PictureRefSeq);
                        else
                            pictContr.Description = cabCode.Description;
                        break;
                    case PictureType.GRAS:
                        var grasRec = GrowerAssortments.GetById(queuePic.PictureRefSeq);
                        if (grasRec == null)
                            Log.WarnFormat("Unable to find grasrecord on seq {0}.", queuePic.PictureRefSeq);
                        else
                            pictContr.Description = grasRec.CABCode.Description;
                        break;
                    case PictureType.OFDT:
                        var ofdtDetail = OfferDetails.GetById(queuePic.PictureRefSeq);

                        if (ofdtDetail == null)
                            Log.WarnFormat("Unable to find cabCode on seq {0}.", queuePic.PictureRefSeq);
                        else
                            pictContr.Description = ofdtDetail.CABCode.Description;
                        break;
                }

                pictContr.Type = queuePic.PictureRefType; 
                pictContr.TypeSequence = queuePic.PictureRefSeq;
                pictContr.InsertedOn = queuePic.InsertedOn;
                pictures.Add(pictContr);
            }

			if (pictures.Count == 0)
			{
				Log.WarnFormat("Unable to find Pictures.");
				return response.PictureNotFound();
			}

			response.Pictures = pictures.ToArray();
			return response.Ok();
		}
	}
}