using System;
using System.Collections.Generic;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Common.Query;
using System.Linq;
using LogiCAB.Contract.PicturesService;
using LogiFlora.Common;
using LogiFlora.Common.Db;

namespace LogiCAB.Common.WCF.PicturesService
{
	///<summary>
	/// Handler voor PicturesPost Requests.
	///</summary>
	public class PicturesPostHandler : ShopManageHandler<PicturesPostRequest, PicturesPostResponse>
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IRepository<Picture> Pictures { get; set; }
        protected IRepository<CABCode> CABCodes { get; set; }
        protected IRepository<GrowerAssortment> GrowerAssortments { get; set; }
        protected IRepository<OfferDetail> OfferDetails { get; set; }
        protected IRepository<QueueProcessing> QueueProcesRecs { get; set; }
		protected ISystemCounterProvider SystemCounterProvider { get; set; }

		///<summary>
		/// Constructor with dependencies.
		///</summary>
        public PicturesPostHandler(
			IRepository<Picture> pictures,
            IRepository<CABCode> cabCodes,
            IRepository<GrowerAssortment> growerAssortments,
            IRepository<OfferDetail> offerDetails,
            IRepository<QueueProcessing> queueProcesRecs,
            IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(shop, authenticatedRequestValidator)
		{
            Pictures = pictures;
            CABCodes = cabCodes;
            GrowerAssortments = growerAssortments;
            OfferDetails = offerDetails;
            QueueProcesRecs = queueProcesRecs;
		}

        protected override PicturesPostResponse InnerHandle(PicturesPostRequest request, PicturesPostResponse response)
		{
			// Beginnen met validatie van het Request.

			if (request.Picture == null)
			{
				Log.WarnFormat("Picture is null.");
				return response.RequestInvalid("Picture is null.");
			}

			var requestValid = true;
			var information = new List<string>();

            PicturesContract tmpPicture = new PicturesContract();
            Picture tmpPict = new Picture();

			if (String.IsNullOrEmpty(request.Picture.Description))
			{
				requestValid = false;
				information.Add("Description is null or empty.");
				Log.WarnFormat("Description is null or empty.");
			}

			if (!requestValid)
			{
				Log.WarnFormat("Request Invalid.");
				return response.RequestInvalid(information.ToArray());
			}

			// Update of Insert?
            if (tmpPict.Sequence != 0)
				return PicturesPostUpdate(request, response);

			return PicturesPostInsert(request, response);
		}


        private PicturesPostResponse PicturesPostUpdate(PicturesPostRequest request, PicturesPostResponse response)
		{
			// Nu zouden we een bestaande moeten kunnen vinden.
            // Todo zoeken naar de picture op basis van type..
            var existing = Pictures.GetById(request.Picture.Sequence.Value);
			
            if (existing == null)
			{
				Log.WarnFormat("Update Failed. Invalid Sequence. {0}", request.Picture.Sequence.Value);
				return response.PictureNotFound();
			}

			// Map de te updaten waarden van het Contract.
			var changed = false;
			
            if (!existing.Description.Equals(request.Picture.Description))
			{
				existing.Description = request.Picture.Description;
				changed = true;
			}

			// Geen update als er geen wijzigingen waren.
			if (!changed)
			{
				Log.InfoFormat("Picture Posted without Change.");
				//response.Pictures = existing.PictureBlob;
				return response.NoChange();
			}

            try
			{
                Pictures.Save(existing);
				Log.InfoFormat("Picture {0} updated.", existing.Sequence);
				response.Pictures = existing.ToContract();
				return response.Ok();
			}
			catch (Exception ex)
			{
				Log.ErrorFormat("Update Failed. Error Saving Organisation.", ex);
				return response.Error();
			}
            
		}
        
		private PicturesPostResponse PicturesPostInsert(PicturesPostRequest request, PicturesPostResponse response)
		{
		    // Validate Unicity of values.
			//if (Pictures.Where(p => p.Description == request.Picture.Description).SingleOrDefault() != null)
			//{
				//Log.WarnFormat("Name not unique.");
				//return response.UniqueFieldNotUnique("Name");
			//}
			
            // Map de Contract gegevens op een Model entiteit.
			var inputPict = new Picture
			{
				PictureBlob = request.Picture.Picture,
				Description = request.Picture.Description,
			};

            try
            {
                Pictures.Save(inputPict);
                Log.InfoFormat("Picture inserted {0}.", inputPict.Sequence);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Insert Failed. Error Saving Picture.", ex);
                return response.Error();
            }

            if (request.Picture.Type == PictureType.CAB)
            {
                var CABCodeType = CABCodes.GetById(request.Picture.TypeSequence);

                if (CABCodeType == null)
                {
                    Log.ErrorFormat("CABCode not found on refSeq {0}.", request.Picture.TypeSequence);
                    return response.Error();
                }

                CABCodeType.CABPicture = inputPict;

                try
                {
                    CABCodes.Save(CABCodeType);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Insert Failed. Error Saving Picture in CAB table.", ex);
                    return response.Error();
                }
            }

            if (request.Picture.Type == PictureType.GRAS)
            {
                var GrasAssort = GrowerAssortments.GetById(request.Picture.TypeSequence);

                if (GrasAssort == null)
                {
                    Log.ErrorFormat("Gras record not found on refSeq {0}.", request.Picture.TypeSequence);
                    return response.Error();
                }

                GrasAssort.GrasPicture = inputPict;

                try
                {
                    GrowerAssortments.Save(GrasAssort);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Insert Failed. Error Saving Picture in GRAS table.", ex);
                    return response.Error();
                }
            }
            
            if (request.Picture.Type == PictureType.OFDT)
            {
                var OfferDet = OfferDetails.GetById(request.Picture.TypeSequence);

                if (OfferDet == null)
                {
                    Log.ErrorFormat("Offerte record not found on refSeq {0}.", request.Picture.TypeSequence);
                    return response.Error();
                }

                OfferDet.OfferPicture = inputPict;

                try
                {
                    OfferDetails.Save(OfferDet);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Insert Failed. Error Saving Picture in offerdetail.", ex);
                    return response.Error();
                }
            }

            //Alles nog goed dan nu de queue afhandelen..
            if (request.Picture.QueueSeq.HasValue == true)
            {
                var QueueReq = QueueProcesRecs.GetById(request.Picture.QueueSeq.Value);

                if(QueueReq == null)
                {
                    Log.ErrorFormat("Queue record not found on Seq {0}.", request.Picture.QueueSeq.Value);
                    return response.Error();
                }

                QueueReq.ProcessedOn = System.DateTime.Now;

                try
                {
                    QueueProcesRecs.Save(QueueReq);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Insert Failed. Error Saving processedOn in queueRec.", ex);
                    return response.Error();
                }
            }

            response.Pictures = inputPict.ToContract();
			return response.Ok();
		}
	}
}