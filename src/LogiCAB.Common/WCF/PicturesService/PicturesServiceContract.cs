﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using LogiCAB.Contract.PicturesService;

namespace LogiCAB.Common.WCF.PicturesService
{
	///<summary>
	/// Implementeert Pictures Service Contract.
	///</summary>
	[ServiceBehavior(Namespace = Meta.Namespace), AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class PicturesServiceContract : IPicturesServiceContract
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected IHandler<PicturesGetRequest, PicturesGetResponse> PicturesGetHandler { get; set; }
		protected IHandler<PicturesPostRequest, PicturesPostResponse> PicturesPostHandler { get; set; }
		
		///<summary>
		/// Constructor with Dependencies.
		///</summary>
		public PicturesServiceContract(
			IHandler<PicturesGetRequest, PicturesGetResponse> picturesGetHandler,
			IHandler<PicturesPostRequest, PicturesPostResponse> picturesPostHandler) 
        {
			PicturesGetHandler = picturesGetHandler;
			PicturesPostHandler = picturesPostHandler;
		}

		public PicturesGetResponse PicturesGet(PicturesGetRequest request)
		{
            return PicturesGetHandler.Handle(request, new PicturesGetResponse());
		}

        public PicturesPostResponse PicturesPost(PicturesPostRequest request)
		{
            return PicturesPostHandler.Handle(request, new PicturesPostResponse());
		}

	}
}
