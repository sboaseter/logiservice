﻿using System;
using System.Linq;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Model;
using LogiCAB.Contract;

namespace LogiCAB.Common.WCF
{
	///<summary>
	/// Implements general Shop Management related stuff for handlers.
	///</summary>
	public abstract class ShopManageHandler<TQ, TP> : AuthenticatedHandler<TQ, TP>
		where TQ : AuthenticatedRequest
		where TP : BaseResponse
	{
		protected override Role[] AllRoles
		{
			get { return null; }
		}
		protected override Role[] AnyRoles
		{
			get { return new[] { Role.ShopManageOrganisation, Role.GlobalManageOrganisation }; }
		}
	
		protected IShopPropertyProvider Shop { get; private set; }

		protected ShopManageHandler(
			IShopPropertyProvider shop,
			IAuthenticatedRequestValidator authenticatedRequestValidator) : base(authenticatedRequestValidator)
		{
			Shop = shop;
		}

		protected virtual bool IsAuthorizedFor(Organisation organisation, BaseResponse response)
		{
			if (HasRole(Role.GlobalManageOrganisation))
				return true;

			if (organisation.Shops.Count(so => so.Shop == Shop.Shop) == 0)
			{
				response.RequestUnauthorized("Organisation is not in your shop.");
				return false;
			}

			if (organisation.Shops.Count > 1)
			{
				response.RequestUnauthorized("Organisation is not local to your shop.");
				return false;
			}
			return true;
		}

		protected virtual bool IsAuthorizedFor(Person person, BaseResponse response)
		{
			return IsAuthorizedFor(person.Organisation, response);
		}

		protected virtual bool IsAuthorizedFor(Login login, BaseResponse response)
		{
			return IsAuthorizedFor(login.Person, response);
		}
	}
}