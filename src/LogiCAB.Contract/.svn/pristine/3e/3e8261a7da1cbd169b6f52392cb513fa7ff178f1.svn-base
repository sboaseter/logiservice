using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Defines the standard information about a Person for Requests and Responses.
	///</summary>
	[DataContract(Name = "Person")]
	public class PersonContract
	{
		///<summary>
		/// The Primary Key of the Person. If given in a Post operation, an attempt will be made to update the relevant Person.
		/// System Generated, Read Only.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

		///<summary>
		/// The Organisation of the Person.
		/// Required.
		///</summary>
		[DataMember(IsRequired = true)]
		public OrganisationKey Organisation { get; set; }

		///<summary>
		/// The First Name of the Person.
		/// Required.
		///</summary>
		[DataMember(IsRequired = true)]
		public string FirstName { get; set; }

		///<summary>
		/// The Middle Name of the Person.
		///</summary>
		[DataMember]
		public string MiddleName { get; set; }

		///<summary>
		/// The Last Name of the Person.
		/// Required.
		///</summary>
		[DataMember(IsRequired = true)]
		public string LastName { get; set; }

		/// <summary>
		/// The Phone of the Person.
		/// </summary>
		[DataMember]
		public string Phone { get; set; }

		/// <summary>
		/// The Email of the Person.
		/// Required.
		/// </summary>
		[DataMember(IsRequired = true)]
		public string Email { get; set; }

		/// <summary>
		/// The External Reference for the person.
		/// Required.
		/// </summary>
		[DataMember(IsRequired = true)]
		public string ExtRef { get; set; }
	}
}