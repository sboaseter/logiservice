using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to an Organisation List Request.
	///</summary>
	[DataContract]
	public class OrganisationListResponse : BaseResponse
	{
		///<summary>
		/// If the List request was successfull, this will contain zero or more matching Organisations.
		/// If the List request was not successfull, this will contain null.
		/// Note that zero matching Organisations is considered a successfull list.
		///</summary>
		[DataMember]
		public OrganisationContract[] Organisations { get; set; }
	}
}