using System.Runtime.Serialization;

namespace LogiCAB.Contract
{
	///<summary>
	/// The base contract plus the necessary fields for authentication.
	///</summary>
	[DataContract]
	public class AuthenticatedRequest : BaseRequest
	{
		///<summary>
		/// The name of the authenticated User.
		///</summary>
		[DataMember(IsRequired = true)]
		public string AuthenticationUsername { get; set; }

		///<summary>
		/// The Base64 encoded hash of the Authentication Ticket, and the Request Sequence.
		///</summary>
		[DataMember(IsRequired = true)]
		public string AuthenticationRequestHash { get; set; }
	}
}