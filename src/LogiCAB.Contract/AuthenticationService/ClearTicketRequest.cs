using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// A Request to Clear the Ticket.
	///</summary>
	[DataContract]
	public class ClearTicketRequest : AuthenticatedRequest { }
}