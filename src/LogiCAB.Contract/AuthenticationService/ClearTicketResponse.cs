using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// A Response to a Clear Ticket Request.
	///</summary>
	[DataContract]
	public class ClearTicketResponse : BaseResponse { }
}