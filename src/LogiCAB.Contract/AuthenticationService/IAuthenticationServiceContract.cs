using System.ServiceModel;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// Defines the Service Contract for the Organisation Service.
	///</summary>
	[ServiceContract(Namespace = Meta.Namespace), XmlSerializerFormat(Use = OperationFormatUse.Literal)]
	public interface IAuthenticationServiceContract
	{

		///<summary>
		/// Requests a new Seed value.
		///</summary>
		[OperationContract]
		RequestSeedResponse RequestSeed(RequestSeedRequest request);

		///<summary>
		/// Requests a new Ticket.
		///</summary>
		[OperationContract]
		RequestTicketResponse RequestTicket(RequestTicketRequest request);

		///<summary>
		/// Clears a valid Ticket.
		///</summary>
		[OperationContract]
		ClearTicketResponse ClearTicket(ClearTicketRequest request);
	}
}