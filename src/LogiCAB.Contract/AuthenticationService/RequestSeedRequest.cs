using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// A Request for an Authentication Seed.
	///</summary>
	[DataContract]
	public class RequestSeedRequest : BaseRequest
	{
	}
}