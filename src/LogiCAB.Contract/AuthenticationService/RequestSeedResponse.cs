using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// A Response to a Seed Request.
	///</summary>
	[DataContract]
	public class RequestSeedResponse : BaseResponse
	{
		///<summary>
		/// A Seed value to use for a Ticket Request.
		/// This Seed value is valid for one minute from the time it was created.
		///</summary>
		[DataMember]
		public string Seed { get; set; }
	}
}