using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// A Request for an Authentication Ticket.
	///</summary>
	[DataContract]
	public class RequestTicketRequest : BaseRequest
	{

		///<summary>
		/// The name of the User to log in.
		///</summary>
		[DataMember(IsRequired = true)]
		public string Username { get; set; }

		///<summary>
		/// The Seed value used.
		///</summary>
		[DataMember(IsRequired = true)]
		public string Seed { get; set; }

		///<summary>
		/// The Base64 encoded double-hash of the User's Password and the Seed value.
		///</summary>
		[DataMember(IsRequired = true)]
		public string Hash { get; set; }
	}
}