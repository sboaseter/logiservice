using System.Runtime.Serialization;

namespace LogiCAB.Contract.AuthenticationService
{
	///<summary>
	/// The Response to a Ticket Request.
	///</summary>
	[DataContract]
	public class RequestTicketResponse : BaseResponse
	{
		///<summary>
		/// The Authentication Ticket if the Request was valid.
		///</summary>
		[DataMember]
		public string Ticket { get; set; }
	}
}