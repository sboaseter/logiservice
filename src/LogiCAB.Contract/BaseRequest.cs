using System.Runtime.Serialization;

namespace LogiCAB.Contract
{
	///<summary>
	/// The base contract for all Requests.
	///</summary>
	[DataContract]
	public class BaseRequest
	{
		///<summary>
		/// An optional client specific tag that will be returned in the Response and may be used for Logging purposes.
		/// May be null or empty.
		///</summary>
		[DataMember]
		public string RequestTag { get; set; }
	}
}