using System.Runtime.Serialization;

namespace LogiCAB.Contract
{
	///<summary>
	/// The base contract for all Responses.
	///</summary>
	[DataContract]
	public class BaseResponse
	{
		///<summary>
		/// An optional client specific tag that was given in the matching Request.
		/// May be null or empty.
		///</summary>
		[DataMember]
		public string RequestTag { get; set; }

		///<summary>
		/// An operation specific result code.
		/// Zero or a positive number generally indicates a (partial) success.
		/// A negative number generally indicates a failure.
		///</summary>
		[DataMember(IsRequired = true)]
		public int ResultCode { get; set; }

		///<summary>
		/// An optional textual message describing the result code.
		/// May be null or empty.
		///</summary>
		[DataMember]
		public string ResultDescription { get; set; }

		///<summary>
		/// Optional additional response information such as errors encountered processing the request.
		/// May be null or empty.
		///</summary>
		[DataMember]
		public string[] ResultInformation { get; set; }
	}
}