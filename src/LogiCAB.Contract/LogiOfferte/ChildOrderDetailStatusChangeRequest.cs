﻿using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// Child Order Status Change Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class ChildOrderDetailStatusChangeRequest : BaseRequest
	{
		///<summary>
		/// De OrderDetail die hem initiëert.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderDetail")]
		public OrderDetailInfo OrderDetail { get; set; }

		/// <summary>
		/// De Child Order Detail.
		/// </summary>
		[DataMember(IsRequired = true, Name = "ChildOrderDetail")]
		public OrderDetailInfo ChildOrderDetail { get; set; }

		/// <summary>
		/// De nieuwe status van de initiërende Order Detail.
		/// </summary>
		[DataMember(IsRequired = true, Name = "NewStatus")]
		public string NewStatus { get; set; }
	}
}