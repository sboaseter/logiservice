﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Response voor een Child Order Status Change Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class ChildOrderDetailStatusChangeResponse : BaseResponse { }
}