﻿namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Meta data for the LogiOfferte Services.
	///</summary>
	public class Meta
	{
		/// <summary>
		/// The proper Namespace for the OrganisationService. Using this same
		/// value for all namespace definitions allows for nice flat WSDL's.
		/// 
		/// On the Service Implementation:
		/// [ServiceBehavior(Namespace = Meta.Namespace)]
		/// 
		/// On the Service Contract interface:
		/// [ServiceContract(Namespace = Meta.Namespace)]
		/// 
		/// On the endpoint in the web.config:
		/// bindingNamespace="http://logicab.nl/LogiOfferte"
		/// </summary>
		public const string Namespace = "http://logicab.nl/LogiOfferte";
	}
}