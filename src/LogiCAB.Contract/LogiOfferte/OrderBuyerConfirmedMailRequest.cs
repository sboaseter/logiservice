﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// OrderBuyerConfirmedMail Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class OrderBuyerConfirmedMailRequest : BaseRequest
	{
		///<summary>
		/// De OrderHeader waar het over gaat.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}