﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Response voor een SupplierMail Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class OrderBuyerConfirmedMailResponse : BaseResponse
	{
		///<summary>
		/// De OrderHeader waar het over gaat.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}