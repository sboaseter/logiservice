﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Standaard informatie voor OrderDetails.
	///</summary>
	[DataContract(Namespace = Meta.Namespace, Name = "OrderDetail")]
	public class OrderDetailInfo
	{
		///<summary>
		/// De Order Detail Sequence.
		///</summary>
		[DataMember(IsRequired = true, Name = "Sequence")]
		public int Sequence { get; set; }
	}
}