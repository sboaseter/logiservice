﻿using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Standaard informatie voor OrderHeaders.
	///</summary>
	[DataContract(Namespace = Meta.Namespace, Name = "OrderHeader")]
	public class OrderHeaderInfo
	{
		///<summary>
		/// De Order Header Sequence.
		///</summary>
		[DataMember(IsRequired = true, Name = "Sequence")]
		public int Sequence { get; set; }
	}
}
