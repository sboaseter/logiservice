﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// OrderHeader Status Check Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class OrderHeaderStatusCheckRequest : BaseRequest
	{
		///<summary>
		/// De OrderDetail die hem initiëert.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderDetail")]
		public OrderDetailInfo OrderDetail { get; set; }
	}
}