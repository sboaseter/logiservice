﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Response voor een OrderHeader Status Check Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class OrderHeaderStatusCheckResponse : BaseResponse { }
}