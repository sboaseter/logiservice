﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// OrderUpdateNotificationMail Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class OrderUpdateNotificationMailRequest : BaseRequest
	{
		///<summary>
		/// De OrderHeader op basis waarvan een OrderUpdateNotificationMail gemaakt moet worden.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}