﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte {
	/// <summary>
	/// PartyUpdate Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class PartyUpdateRequest : BaseRequest {
		///<summary>
		/// De OrderHeader op basis waarvan een PartyUpdate gemaakt moet worden.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}