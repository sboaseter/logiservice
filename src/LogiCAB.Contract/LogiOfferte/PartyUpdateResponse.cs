﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte {
	///<summary>
	/// Response voor een PartyUpdate Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class PartyUpdateResponse : BaseResponse {

	}
}