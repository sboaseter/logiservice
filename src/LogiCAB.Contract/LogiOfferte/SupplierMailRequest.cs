﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// SupplierMail Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class SupplierMailRequest : BaseRequest
	{
		///<summary>
		/// De OrderHeader waar het over gaat.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}