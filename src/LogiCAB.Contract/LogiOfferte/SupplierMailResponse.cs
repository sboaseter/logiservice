﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Response voor een SupplierMail Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class SupplierMailResponse : BaseResponse
	{

	}
}