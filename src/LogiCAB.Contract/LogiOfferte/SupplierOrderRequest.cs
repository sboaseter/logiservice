﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	/// <summary>
	/// SupplierOrder Request message.
	/// </summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class SupplierOrderRequest : BaseRequest
	{
		///<summary>
		/// De OrderHeader op basis waarvan een SupplierOrder gemaakt moet worden.
		///</summary>
		[DataMember(IsRequired = true, Name = "OrderHeader")]
		public OrderHeaderInfo OrderHeader { get; set; }
	}
}