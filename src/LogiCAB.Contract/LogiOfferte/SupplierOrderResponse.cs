﻿using System.Runtime.Serialization;

namespace LogiCAB.Contract.LogiOfferte
{
	///<summary>
	/// Response voor een SupplierOrder Request.
	///</summary>
	[DataContract(Namespace = Meta.Namespace)]
	public class SupplierOrderResponse : BaseResponse
	{
		///<summary>
		/// De resulterende verzameling Supplier OrderHeaders.
		///</summary>
		[DataMember(Name = "OrderHeaders")]
		public OrderHeaderInfo[] OrderHeaders { get; set; }
	}
}