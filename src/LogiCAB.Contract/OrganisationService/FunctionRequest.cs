using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Allows for the Adding or Removing of Shop Functions to/from an Organisation.
	///</summary>
	[DataContract]
	public class FunctionRequest : AuthenticatedRequest
	{
		///<summary>
		/// Key information on which to find the Organisation.
		///</summary>
		[DataMember(IsRequired = true)]
		public OrganisationKey Key { get; set; }

		/// <summary>
		/// The Function to Add or Remove.
		/// </summary>
		[DataMember(IsRequired = true)]
		public string Function { get; set; }

		/// <summary>
		/// Whether to remove the Function instead of adding it.
		/// </summary>
		[DataMember(IsRequired = true)]
		public bool Remove { get; set; }
	}
}