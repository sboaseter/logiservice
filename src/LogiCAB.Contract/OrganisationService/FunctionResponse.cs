using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to an Organisation Function Request.
	///</summary>
	[DataContract]
	public class FunctionResponse : BaseResponse { }
}