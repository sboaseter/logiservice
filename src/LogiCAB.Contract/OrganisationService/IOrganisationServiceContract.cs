using System.ServiceModel;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Defines the Service Contract for the Organisation Service.
	///</summary>
	[ServiceContract(Namespace = Meta.Namespace), XmlSerializerFormat(Use = OperationFormatUse.Literal)]
	public interface IOrganisationServiceContract
	{
		///<summary>
		/// Gets a single Organisation based on the request values.
		///</summary>
		[OperationContract]
		OrganisationGetResponse OrganisationGet(OrganisationGetRequest request);

		///<summary>
		/// Lists a number of Organisations based on the request values.
		///</summary>
		[OperationContract]
		OrganisationListResponse OrganisationList(OrganisationListRequest request);

		///<summary>
		/// Posts the given Organisation, updating the information store.
		///</summary>
		[OperationContract]
		OrganisationPostResponse OrganisationPost(OrganisationPostRequest request);

		///<summary>
		/// Checks if the given Organisation has a link to the current shop,
		/// and creates it if not.
		///</summary>
		[OperationContract]
		ShopLinkResponse ShopLink(ShopLinkRequest request);

		///<summary>
		/// Allows for the Adding or Removing of Shop Functions to/from an Organisation.
		///</summary>
		[OperationContract]
		FunctionResponse Function(FunctionRequest request);

		///<summary>
		/// Gets a single Person based on the Key values.
		///</summary>
		[OperationContract]
		PersonGetResponse PersonGet(PersonGetRequest request);

		///<summary>
		/// Lists all Persons based on the Request values.
		///</summary>
		[OperationContract]
		PersonListResponse PersonList(PersonListRequest request);

		///<summary>
		/// Posts the given Person.
		///</summary>
		[OperationContract]
		PersonPostResponse PersonPost(PersonPostRequest request);

		///<summary>
		/// Gets a single Login based on the Key values.
		///</summary>
		[OperationContract]
		LoginGetResponse LoginGet(LoginGetRequest request);

		///<summary>
		/// Lists all Logins based on the Request values.
		///</summary>
		[OperationContract]
		LoginListResponse LoginList(LoginListRequest request);

		/// <summary>
		/// Posts the given Login.
		/// </summary>
		[OperationContract]
		LoginPostResponse LoginPost(LoginPostRequest request);
	}
}