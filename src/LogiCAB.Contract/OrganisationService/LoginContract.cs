using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Defines the standard information about a Login for Requests and Responses.
	///</summary>
	[DataContract(Name = "Login")]
	public class LoginContract
	{
		///<summary>
		/// The Primary Key of the Person. If given in a Post operation, an attempt will be made to update the relevant Person.
		/// System Generated, Read Only.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

		///<summary>
		/// The Person of the Login.
		/// Required.
		///</summary>
		[DataMember(IsRequired = true)]
		public PersonKey Person { get; set; }

		///<summary>
		/// The Name of the Login.
		/// Required and must be Unique.
		///</summary>
		[DataMember(IsRequired = true)]
		public string Name { get; set; }

		/// <summary>
		/// The Password of the Login.
		/// Only used to set or change the password in Posts.
		/// This field will not be set in responses.
		/// </summary>
		[DataMember]
		public string Password { get; set; }

		///<summary>
		/// If true, this Login is blocked.
		///</summary>
		[DataMember(IsRequired = true)]
		public bool Blocked { get; set; }

        ///<summary>
        /// Languagesequence
        ///</summary>
        [DataMember]
        public int? language { get; set; }
	}
}