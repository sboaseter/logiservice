using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Requests a single Login.
	///</summary>
	[DataContract]
	public class LoginGetRequest : AuthenticatedRequest
	{
		///<summary>
		/// The Key of the Login to get.
		///</summary>
		[DataMember(IsRequired = true)]
		public LoginKey Key { get; set; }
	}
}