using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to a Login Get Request.
	///</summary>
	[DataContract]
	public class LoginGetResponse : BaseResponse
	{
		///<summary>
		/// If the Get request was successfull, this will contain the matching Login.
		/// If the Get request was not successfull, this will contain null.
		/// Note that zero matching Logins is not considered a successfull get.
		///</summary>
		[DataMember]
		public LoginContract Login { get; set; }
	}
}