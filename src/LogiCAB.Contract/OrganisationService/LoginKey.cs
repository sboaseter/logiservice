using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Key Fields on which a single Login may be found.
	///</summary>
	[DataContract]
	public class LoginKey
	{
		///<summary>
		/// The Sequence of the Login. If not null, this will be used to find the Login.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

		///<summary>
		/// The Name of the Login. If not null, this will be used to find the Login.
		///</summary>
		[DataMember]
		public string Name { get; set; }
	}
}