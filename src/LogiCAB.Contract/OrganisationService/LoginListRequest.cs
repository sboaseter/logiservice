using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Requests a list of Logins for a specific Person.
	///</summary>
	[DataContract]
	public class LoginListRequest : AuthenticatedRequest
	{
		///<summary>
		/// The Key of the Person for which to list the Logins.
		///</summary>
		[DataMember(IsRequired = true)]
		public PersonKey Person { get; set; }
	}
}