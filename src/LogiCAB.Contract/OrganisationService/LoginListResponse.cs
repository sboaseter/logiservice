using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to a Login List Request.
	///</summary>
	[DataContract]
	public class LoginListResponse : BaseResponse
	{
		///<summary>
		/// If the List request was successfull, this will contain zero or more matching Logins.
		/// If the List request was not successfull, this will contain null.
		/// Note that zero matching Logins is considered a successfull list.
		///</summary>
		[DataMember]
		public LoginContract[] Logins { get; set; }
	}
}