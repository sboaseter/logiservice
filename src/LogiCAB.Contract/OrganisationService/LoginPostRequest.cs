using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Posts the given Login.
	///</summary>
	[DataContract]
	public class LoginPostRequest : AuthenticatedRequest
	{
		///<summary>
		/// The Login to create or modify.
		///</summary>
		[DataMember]
		public LoginContract Login { get; set; }
	}
}