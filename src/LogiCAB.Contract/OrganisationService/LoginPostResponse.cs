using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to a Login Post Request.
	///</summary>
	[DataContract]
	public class LoginPostResponse : BaseResponse
	{
		///<summary>
		/// If the Post was a success, this will contain updated Login information, including the Sequence and other service generated values.
		/// If the Post failed, this will be null.
		///</summary>
		[DataMember]
		public LoginContract Login { get; set; }
	}
}