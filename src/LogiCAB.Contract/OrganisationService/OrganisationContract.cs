using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Defines the standard information about an Organisation for Requests and Responses.
	///</summary>
	[DataContract(Name = "Organisation")]
	public class OrganisationContract
	{
		///<summary>
		/// The Primary Key of the Organisation. If given in a Post operation, an attempt will be made to update the relevant Organisation.
		/// System Generated, Read Only.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

		///<summary>
		/// The Type of the Organisation.
		/// May not be changed once the Organisation exists.
		/// Required, Read Only.
		///</summary>
		[DataMember]
		public OrganisationType Type { get; set; }

		///<summary>
		/// The Code of the Organisation.
		/// System Generated, Read Only, MaxLength 10.
		///</summary>
		[DataMember]
		public string Code { get; set; }

		///<summary>
		/// The Name of the Organisation.
		/// Required, Unique, MaxLength 250.
		///</summary>
		[DataMember]
		public string Name { get; set; }

		///<summary>
		/// The Phone Number of the Organisation.
		/// MaxLength 250.
		///</summary>
		[DataMember]
		public string Phone { get; set; }

		///<summary>
		/// The Fax Number of the Organisation.
		/// MaxLength 250.
		///</summary>
		[DataMember]
		public string Fax { get; set; }

		///<summary>
		/// The Email of the Organisation.
		/// Required, MaxLength 250.
		///</summary>
		[DataMember]
		public string Email { get; set; }

		///<summary>
		/// The Mailbox of the Organisation.
		/// MaxLength 250.
		///</summary>
		[DataMember]
		public string Mailbox { get; set; }

		///<summary>
		/// The EAN Code of the Organisation.
		/// Required, Unique, MaxLength 50.
		///</summary>
		[DataMember]
		public string EAN { get; set; }

		///<summary>
		/// The VAT Number of the Organisation.
		/// MaxLength 50.
		///</summary>
		[DataMember]
		public string VAT { get; set; }

		///<summary>
		/// The VBA Number of the Organisation.
		/// MaxLength 50.
		///</summary>
		[DataMember]
		public string VBA { get; set; }

		///<summary>
		/// The BVH Number of the Organisation.
		/// MaxLength 50.
		///</summary>
		[DataMember]
		public string BVH { get; set; }

		///<summary>
		/// The Referer of the Organisation.
		/// May not be changed once the Organisation exists.
		/// Read Only, MaxLength 250.
		///</summary>
		[DataMember]
		public string Referer { get; set; }

		///<summary>
		/// The Shipping Days of the Organisation.
		/// MaxLength 25.
		///</summary>
		///
		[DataMember]
		public string ShippingDays { get; set; }

		///<summary>
		/// The Deadline of the Organisation.
		/// Must be specified as a Time component in 24-hour format, ie: "17:00:00".
		///</summary>
		[DataMember]
		public string Deadline { get; set; }

		///<summary>
		/// The DeadlineOffer of the Organisation.
		/// Must be specified as a Time component in 24-hour format, ie: "17:00:00".
		///</summary>
		[DataMember]
		public string DeadlineOffer { get; set; }
	}
}