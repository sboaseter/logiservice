using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Requests a single Organisation.
	///</summary>
	[DataContract]
	public class OrganisationGetRequest : AuthenticatedRequest
	{
		///<summary>
		/// Key information on which to find the Organisation.
		///</summary>
		[DataMember(IsRequired = true)]
		public OrganisationKey Key { get; set; }
	}
}