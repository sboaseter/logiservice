using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to an Organisation Get Request.
	///</summary>
	[DataContract]
	public class OrganisationGetResponse : BaseResponse
	{
		///<summary>
		/// If the Get request was successfull, this will contain the matching Organisation.
		/// If the Get request was not successfull, this will contain null.
		/// Note that zero matching Organisations is not considered a successfull get.
		///</summary>
		[DataMember]
		public OrganisationContract Organisation { get; set; }
	}
}