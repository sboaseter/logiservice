using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Key Fields on which a single Organisation may be found.
	///</summary>
	[DataContract]
	public class OrganisationKey
	{
		///<summary>
		/// The Sequence of the Organisation. If not null, this will be used to find the Organisation.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

		///<summary>
		/// The Code of the Organisation. If not null or empty, this will be used to find the Organisation.
		///</summary>
		[DataMember]
		public string Code { get; set; }

		///<summary>
		/// The Name of the Organisation. If not null or empty, this will be used to find the Organisation.
		///</summary>
		[DataMember]
		public string Name { get; set; }

		///<summary>
		/// The EAN of the Organisation. If not null or empty, this will be used to find the Organisation.
		///</summary>
		[DataMember]
		public string EAN { get; set; }
	}
}