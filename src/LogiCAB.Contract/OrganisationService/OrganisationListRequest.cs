using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Requests a List of Organisations.
	///</summary>
	[DataContract]
	public class OrganisationListRequest : AuthenticatedRequest
	{
	}
}