using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Request to Post an Organisation.
	///</summary>
	[DataContract]
	public class OrganisationPostRequest : AuthenticatedRequest
	{
		///<summary>
		/// The Organisation information to Post.
		///</summary>
		[DataMember]
		public OrganisationContract Organisation { get; set; }
	}
}