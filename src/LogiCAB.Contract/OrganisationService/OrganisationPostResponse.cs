using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to an Organisation Post Request.
	///</summary>
	[DataContract]
	public class OrganisationPostResponse : BaseResponse
	{
		///<summary>
		/// If the Post was a success, this will contain updated Organisation information, including the Sequence and other service generated values.
		/// If the Post failed, this will be null.
		///</summary>
		[DataMember]
		public OrganisationContract Organisation { get; set; }
	}
}