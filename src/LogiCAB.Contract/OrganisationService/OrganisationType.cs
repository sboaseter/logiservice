using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Type of an Organisation.
	///</summary>
	[DataContract(Name = "OrganisationType")]
	public enum OrganisationType
	{
		///<summary>
		/// An Organisation with Buyer privileges.
		///</summary>
		[EnumMember]
		Buyer,

		///<summary>
		/// An Organisation with Grower privileges.
		///</summary>
		[EnumMember]
		Grower,
	}
}