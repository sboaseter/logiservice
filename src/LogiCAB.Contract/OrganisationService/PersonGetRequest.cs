using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Requests a single Person.
	///</summary>
	[DataContract]
	public class PersonGetRequest : AuthenticatedRequest
	{
		///<summary>
		/// Key information on which to find the Person.
		///</summary>
		[DataMember(IsRequired = true)]
		public PersonKey Key { get; set; }
	}
}