using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// Key Fields on which a single Person may be found.
	///</summary>
	[DataContract]
	public class PersonKey
	{
		///<summary>
		/// The Sequence of the Person. If not null, this will be used to find the Person.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }
	}
}