using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to a Person List Request.
	///</summary>
	[DataContract]
	public class PersonListResponse : BaseResponse
	{
		///<summary>
		/// If the List request was successfull, this will contain zero or more matching Persons.
		/// If the List request was not successfull, this will contain null.
		/// Note that zero matching Persons is considered a successfull list.
		///</summary>
		[DataMember]
		public PersonContract[] Persons { get; set; }
	}
}