using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to a Person Post Request.
	///</summary>
	[DataContract]
	public class PersonPostResponse : BaseResponse
	{
		///<summary>
		/// If the Post was a success, this will contain updated Person information, including the Sequence and other service generated values.
		/// If the Post failed, this will be null.
		///</summary>
		[DataMember]
		public PersonContract Person { get; set; }
	}
}