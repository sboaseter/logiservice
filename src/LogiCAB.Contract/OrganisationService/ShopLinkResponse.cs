using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.OrganisationService
{
	///<summary>
	/// The Response to an Organisation Shop Link Request.
	///</summary>
	[DataContract]
	public class ShopLinkResponse : BaseResponse { }
}