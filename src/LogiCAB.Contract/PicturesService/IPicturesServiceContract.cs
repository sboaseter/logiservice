using System.ServiceModel;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// Defines the Service Contract for the Pictures Service.
	///</summary>
	[ServiceContract(Namespace = Meta.Namespace), XmlSerializerFormat(Use = OperationFormatUse.Literal)]
	public interface IPicturesServiceContract
	{
		///<summary>
		/// Posts the given picture, updating the information store.
		///</summary>
		[OperationContract]
		PicturesPostResponse PicturesPost(PicturesPostRequest request);

        ///<summary>
        /// Gets the given picture based on type of sequence.
        ///</summary>
        [OperationContract]
        PicturesGetResponse PicturesGet(PicturesGetRequest request);

	}
}