﻿namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// Meta data for the PicturesService.
	///</summary>
	public class Meta
	{
		/// <summary>
		/// The proper Namespace for the PicturesService. Using this same
		/// value for all namespace definitions allows for nice flat WSDL's.
		/// 
		/// On the Service Implementation:
		/// [ServiceBehavior(Namespace = Meta.Namespace)]
		/// 
		/// On the Service Contract interface:
		/// [ServiceContract(Namespace = Meta.Namespace)]
		/// 
		/// On the endpoint in the web.config:
		/// bindingNamespace="http://www.logicab.nl/PicturesService"
		/// </summary>
		public const string Namespace = "http://www.logicab.nl/PicturesService";
	}
}