using System;
using System.Runtime.Serialization;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// Key Fields on which pictures may be found.
    /// May be we can add more fields later..
    /// For now I the key or description will be enough..
	///</summary>
	[DataContract]
	public class PictureKey
	{
		///<summary>
		/// The Sequence of the picture. If not null, this will be used to find the Picture.
		///</summary>
		[DataMember]
		public int? Sequence { get; set; }

        ///<summary>
        /// The description of the picture(s).
        ///</summary>
        [DataMember]
        public string Description { get; set; }

        ///<summary>
        /// The type of the picture(s).
        ///</summary>
        [DataMember]
        public PictureType PictType { get; set; }

        ///<summary>
        /// The sequence of the picture in combination with type.
        ///</summary>
        [DataMember]
        public int? PictTypeSeq { get; set; }

	}
}