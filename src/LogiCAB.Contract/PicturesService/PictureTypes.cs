﻿using System;
using System.ComponentModel;

namespace LogiCAB.Contract.PicturesService
{
	/// <summary>
	/// Mogelijke foto types, van CAB, OFDT of Gras.
	/// </summary>
	public enum PictureType
	{
		///<summary>
		/// Picture on CAB level..
		///</summary>
        [Description("Picture on CAB")]
		CAB = 1,

        ///<summary>
        /// Picture on Grower assortment level..
        ///</summary>
        [Description("Picture on GrowerAssortment")]
        GRAS = 2,

        ///<summary>
        /// Picture on offerte level..
        ///</summary>
        [Description("Picture on OfferDetail")]
        OFDT = 3,

        ///<summary>
        /// Level niet kunnen bepalen!
        ///</summary>
        [Description("Undefined!")]
        UNDEF = 4,

	}
}
