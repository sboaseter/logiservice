using System.Runtime.Serialization;

namespace LogiCAB.Contract.PicturesService
{
    ///<summary>
    /// Defines the standard information about a Picture for Requests and Responses.
    ///</summary>
    [DataContract(Name = "Pictures")]
    public class PicturesContract
    {
        ///<summary>
        /// The Primary Key of the picture. If given in a Post operation, an attempt will be made to replace the relevant picture.
        /// System Generated, Read Only.
        ///</summary>
        [DataMember]
        public int? Sequence { get; set; }

        ///<summary>
        /// The sequence of the Queurecord.
        ///</summary>
        [DataMember]
        public int? QueueSeq { get; set; }

        ///<summary>
        /// The Type of the picture.
        /// May not be changed once the picture exists.
        ///</summary>
        [DataMember]
        public PictureType Type { get; set; }

        ///<summary>
        /// The Primary Key of the pictureType.
        /// Required, nessecary to find the right base record.
        /// Could be CAB, GRAS or OFDT seq.
        ///</summary>
        [DataMember]
        public int TypeSequence { get; set; }

        ///<summary>
        /// The description of the Picture.
        /// Required, MaxLength 200.
        ///</summary>
        [DataMember]
        public string Description { get; set; }

        ///<summary>
        /// The Picture itself.
        /// Required, byte array base64 encoded.
        ///</summary>
        [DataMember]
        public byte[] Picture { get; set; }

        ///<summary>
        /// Datetime inserted in the queue.
        ///</summary>
        [DataMember]
        public System.DateTime InsertedOn { get; set; }
    }
}