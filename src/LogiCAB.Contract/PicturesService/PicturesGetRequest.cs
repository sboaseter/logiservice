using System.Runtime.Serialization;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// Requests pictures.
	///</summary>
	[DataContract]
	public class PicturesGetRequest : AuthenticatedRequest
	{
		///<summary>
		/// Key information on which to find the Picture.
		///</summary>
		[DataMember(IsRequired = true)]
		public PictureKey Key { get; set; }

    }
}