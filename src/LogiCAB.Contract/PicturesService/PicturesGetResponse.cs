using System.Runtime.Serialization;
using System.Collections;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// The Response to an pictures Get Request.
	///</summary>
	[DataContract]
	public class PicturesGetResponse : BaseResponse
	{
		///<summary>
		/// If the Get request was successfull, this will contain the matching picture(s).
		/// If the Get request was not successfull, this will contain null.
		/// Note that zero matching Pictures is not considered a successfull get.
		///</summary>
		[DataMember]
		public PicturesContract [] Pictures { get; set; }
	}
}