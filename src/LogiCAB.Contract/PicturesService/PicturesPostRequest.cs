using System.Runtime.Serialization;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// The Request to Post Pictures.
	///</summary>
	[DataContract]
	public class PicturesPostRequest : AuthenticatedRequest
	{
		///<summary>
		/// The Pictures information to Post.
		///</summary>
		[DataMember]
		public PicturesContract Picture { get; set; }
	}
}