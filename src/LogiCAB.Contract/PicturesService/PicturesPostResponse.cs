using System.Runtime.Serialization;

namespace LogiCAB.Contract.PicturesService
{
	///<summary>
	/// The Response to a Picture Post Request.
	///</summary>
	[DataContract]
	public class PicturesPostResponse : BaseResponse
	{
		///<summary>
		/// If the Post was a success, this will contain updated picture information, including the Sequence and other service generated values.
		/// If the Post failed, this will be null.
		///</summary>
		[DataMember]
		public PicturesContract [] Pictures { get; set; }
	}
}