﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace LogiCAB.Contract
{
	/// <summary>
	/// A simple envelope for requests.
	/// </summary>
	[DataContract]
	[KnownType("GetKnownTypes")]
	public class RequestEnvelope
	{
		///<summary>
		/// The contained Request.
		///</summary>
		[DataMember(IsRequired = true)]
		public BaseRequest Request { get; set; }

// ReSharper disable UnusedMember.Local
		static Type[] GetKnownTypes()
// ReSharper restore UnusedMember.Local
		{
			var baseType = typeof(BaseRequest);
			return baseType.Assembly.GetTypes()
				.Where(baseType.IsAssignableFrom)
				.ToArray();
		}
	}
}