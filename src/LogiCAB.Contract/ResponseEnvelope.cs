﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace LogiCAB.Contract
{
	/// <summary>
	/// A simple envelope for responses.
	/// </summary>
	[DataContract]
	[KnownType("GetKnownTypes")]
	public class ResponseEnvelope
	{
		///<summary>
		/// The contained Response.
		///</summary>
		[DataMember(IsRequired = true)]
		public BaseResponse Response { get; set; }

// ReSharper disable UnusedMember.Local
		static Type[] GetKnownTypes()
// ReSharper restore UnusedMember.Local
		{
			var baseType = typeof(BaseResponse);
			return baseType.Assembly.GetTypes()
				.Where(baseType.IsAssignableFrom)
				.ToArray();
		}
	}
}