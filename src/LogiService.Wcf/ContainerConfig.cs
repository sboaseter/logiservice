﻿using System;
using System.Web;
using Autofac.Builder;
using Autofac.Integration.Wcf;
using Autofac.Integration.Web;
using LogiCAB.Common.Domain;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using LogiCAB.Common;
using LogiService.Wcf.Florecom.Jul2010;
using LogiService.Wcf.Florecom.Jul2011;
using LogiService.Wcf.Florecom.Jul2012;
using LogiService.Wcf.Florecom.Client_V05;
using LogiService.Wcf.OudeCode;
using LogiService.Wcf.OudeCode.Util;
using System.ServiceModel.Activation;

namespace LogiService.Wcf
{
	/// <summary>
	/// Containers all the registration code for components.
	/// </summary>
	public static class ContainerConfig
	{
		public static ContainerProvider CreateContainerProvider()
		{
			var builder = new ContainerBuilder();
			RegisterModules(builder);
			RegisterComponents(builder);
			var containerProvider = new ContainerProvider(builder.Build());

			// Hook up WCF Integration.
			AutofacHostFactory.Container = containerProvider.ApplicationContainer;
			// Hook up NHibernate Integration.
			containerProvider.ApplicationContainer.SetAutofacBytecodeProvider();
			return containerProvider;
		}

		public static void RegisterModules(ContainerBuilder builder)
		{
			// LogiCAB registraties.
			builder.RegisterLogiCABModules();
			builder.RegisterModule(new LoggingModule());
			builder.RegisterModule(new ValueTypeParameterModule());
		}

		public static void RegisterComponents(ContainerBuilder builder)
		{
			// Laad de configuratie files.
			builder.Register(
				new ConfigurationReader(
					HttpContext.Current.Server.MapPath("~/properties.config")
				,	HttpContext.Current.Server.MapPath("~/properties.local.config")
				).ReadParameters()
			);

			// De basis ConnectionProvider.
			builder.Register<ConnectionProvider>()
				.As<ICabConnectionProvider>()
				.As<IConnectionProvider>()
				.ContainerScoped();

			// LogiCAB registraties.
            builder.RegisterLogiCABComponents();
			// Algemene registraties.
			//builder.Register(new Random()).SingletonScoped().ExternallyOwned();
			// LogiService Providers.
			builder.Register<PathTool>().ContainerScoped();
			builder.Register(c => new ImageProvider(
				Config.CabImagesPath,
				Config.CabImagesUrl,
				c.Resolve<ICabConnectionProvider>())
			).As<IImageProvider>().ContainerScoped();

			// Jul2010
            builder.Register<LogiService.Wcf.Florecom.Jul2010.ServiceAuthenticator>().As<LogiService.Wcf.Florecom.Jul2010.IServiceAuthenticator>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2010.GetSupplyProcessor>().As<LogiService.Wcf.Florecom.Jul2010.IGetSupplyProcessor>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2010.PutOrderProcessor>().As<LogiService.Wcf.Florecom.Jul2010.IPutOrderProcessor>().ContainerScoped();
        
            // Jul2011
            builder.Register<LogiService.Wcf.Florecom.Jul2011.ServiceAuthenticator>().As<LogiService.Wcf.Florecom.Jul2011.IServiceAuthenticator>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2011.GetSupplyProcessor>().As<LogiService.Wcf.Florecom.Jul2011.IGetSupplyProcessor>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2011.PutOrderProcessor>().As<LogiService.Wcf.Florecom.Jul2011.IPutOrderProcessor>().ContainerScoped();
            
            // Jul2012
            builder.Register<LogiService.Wcf.Florecom.Jul2012.ServiceAuthenticator>().As<LogiService.Wcf.Florecom.Jul2012.IServiceAuthenticator>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2012.GetSupplyProcessor>().As<LogiService.Wcf.Florecom.Jul2012.IGetSupplyProcessor>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Jul2012.PutOrderProcessor>().As<LogiService.Wcf.Florecom.Jul2012.IPutOrderProcessor>().ContainerScoped();

            //Client_v05
            builder.Register<LogiService.Wcf.Florecom.Client_V05.ServiceAuthenticator>().As<LogiService.Wcf.Florecom.Client_V05.IServiceAuthenticator>().ContainerScoped();
            builder.Register<LogiService.Wcf.Florecom.Client_V05.GovernmentExporter>().As<LogiService.Wcf.Florecom.Client_V05.IGovernmentExporter>().ContainerScoped();
        }

	}
}