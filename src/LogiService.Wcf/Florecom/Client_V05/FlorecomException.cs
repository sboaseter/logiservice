﻿using System.Web;
using System.Web.Services.Protocols;
using System.Xml;

namespace LogiService.Wcf.Florecom.Client_V05
{
	public class FlorecomException : SoapException
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public FlorecomException(string message) : this(message, "Unknown") {}

		public FlorecomException(string message, string location) :
			base(message, ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, GetDetailNode(message, location))
		{
			Log.ErrorFormat("{0} [{1}]", message, location);
		}

		private static XmlNode GetDetailNode(string message, string location) {
			var doc = new XmlDocument();
			var ns = new XmlNamespaceManager(doc.NameTable);
			ns.AddNamespace("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			ns.AddNamespace("ferab", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var detailNode = doc.CreateNode(XmlNodeType.Element, DetailElementName.Name, DetailElementName.Namespace);
			var florecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var florecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var florecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			florecomErrorNode.AppendChild(florecomErrorLevelNode);
			florecomErrorNode.AppendChild(florecomErrorSeverity);
			florecomErrorNode.AppendChild(florecomErrorErrorCode);
			florecomErrorNode.AppendChild(florecomErrorLocation);
			florecomErrorNode.AppendChild(florecomErrorDescriptionText);
			florecomErrorListNode.AppendChild(florecomErrorNode);
			//Add the custom values to the ErrorList strucure
			florecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
			florecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
			florecomErrorErrorCode.AppendChild(doc.CreateTextNode("10000"));
			florecomErrorLocation.AppendChild(doc.CreateTextNode(location));
			florecomErrorDescriptionText.AppendChild(doc.CreateTextNode(message));
			detailNode.AppendChild(florecomErrorListNode);
			return detailNode;
		}
	}
}