﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Xml;
using System.Transactions;

namespace LogiService.Wcf.Florecom.Client_V05
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    /// 
    [System.Web.Services.WebServiceAttribute(Namespace = "http://webservice.client.florecom.org/GovernmentExporter", Description = "Web Service for the 'GovernmentExporter' versie 0p5.", Name = "Web Service Government. versie 0p5.")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "GovernmentExporter", Namespace = "http://webservice.client.florecom.org/GovernmentExporter")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class GovernmentExporter : IGovernmentExporter
    {
        #region Get Details
        #endregion

        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
        public PutCertificateResponseMessage PutCertificate(PutCertificateRequestMessage DetailsRequest)
        {
            try
            {
                Log.DebugFormat("GetDetails certificate started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(DetailsRequest, "/DetailsRequest/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("GetSupply Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/SupplyRequest/Header");
                }

                //Log.InfoFormat("GetSupply Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new PutCertificateResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }

            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("GetSupply Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }

        }

        public PutCertificateCancelResponseMessage PutCertificateCancel(PutCertificateCancelRequestMessage DetailsCancel)
        {
            try
            {
                Log.DebugFormat("Cancel request certificate started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(DetailsCancel, "/DetailsCancel/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("GetCancel Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/DetailsRequest/Header");
                }

                //Log.InfoFormat("Cacnel Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new PutCertificateCancelResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }

            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("GetSupply Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }

        }

        public GetCertificateStatusResponseMessage GetCertificateStatus(GetCertificateStatusRequestMessage certificateStatus)
        {
            try
            {
                Log.DebugFormat("Certificate status request certificate started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(certificateStatus, "/DetailsCancel/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("GetCancel Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/DetailsRequest/Header");
                }

                //Log.InfoFormat("Cacnel Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new GetCertificateStatusResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }

            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("GetSupply Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }

        }

        public GetCertificateStatusResponseMessage GetAsyncCertificateStatus(GetCertificateStatusRequestMessage certificateStatus)
        {
            try
            {
                Log.DebugFormat("Certificate status request certificate started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(certificateStatus, "/DetailsCancel/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("GetCancel Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/DetailsRequest/Header");
                }

                //Log.InfoFormat("Cacnel Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new GetCertificateStatusResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }
            
            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("GetSupply Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }
        }

        public GetInspectionResponseMessage GetInspection(GetInspectionMessage inspectionMessage)
        {
            try
            {
                Log.DebugFormat("Inspection message started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(inspectionMessage, "/DetailsCancel/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/DetailsRequest/Header");
                }

                //Log.InfoFormat("Cacnel Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new GetInspectionResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }

            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }

        }

        public GetCertificateDocumentResponseMessage GetCertificateDocument(GetCertificateDocumentMessage certificateDocument)
        {
            try
            {
                Log.DebugFormat("Certificate document request certificate started from [{0}].", HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container onze algemene Service Authenticator op.
                var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
                // De verschillende Headers hebben allemaal een UserName en
                // Password, dus kunnen die voldoen aan de IRequestHeader
                // interface. Die moet wel handmatig worden toegevoegd aan de
                // betreffende class definitie.
                var principal = authenticator.Authenticate(certificateDocument, "/DetailsCancel/Header");

                // Simpele check of het een valide gebruiker was.
                if (principal == null)
                {
                    Log.DebugFormat("GetCancel Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
                    throw new FlorecomException("Invalid Login.", "/DetailsRequest/Header");
                }

                //Log.InfoFormat("Cacnel Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);
                // Haal via de Container de specifieke Processor voor dit soort
                // Request op.
                var processor = Global.Container.Resolve<IGovernmentExporter>();
                // Voer de processing uit.
                var result = new GetCertificateDocumentResponseMessage();
                return result; //processor.Process(DetailsRequest, principal);
            }

            catch (SoapException)
            {
                throw; // Simply pass on SoapExceptions.
            }

            catch (Exception err)
            {
                Log.Error("GetSupply Unhandled Exception.", err);
                throw new FlorecomException(err.Message, "Unknown");
            }

        }

        
        #region Error Handling
        #endregion
        /// <summary>
        /// Errors should be returned using a SOAP:Fault (SOAP version 1.1). The Detail-element of the Soap:fault should contain the errors according to
        /// The ErrorList scheme is described in the document "Webservice Beschrijvingen Virtuele Marktplaats" that is publised on sdk.florecom.org.
        /// the Florecom Errorlist-scheme e.g.:
        /// <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xml="http://www.w3.org/XML/1998/namespace">
        //  <env:Body>
        //    <env:Fault>
        //      <env:faultcode>env:Sender</env:faultcode>
        //      <env:faultstring>Error detected by the webservice. Check the Detail-element in this SOAP:Fault response message for more error details.</env:faultstring>
        //      <env:detail>
        //        <ErrorList xmlns="urn:fec:messages:data:final:FlorecomStandardMessage:0_5">
        //          <Error>
        //            <ErrorLevel>message</ErrorLevel>
        //            <Severity>error</Severity>
        //            <ErrorCode>102</ErrorCode>
        //            <ErrorLocation>SupplyResponse/Body/SupplyResponseDetails/AgentParty</fsm:ErrorLocation>
        //            <DescriptionText>Unknown agent party.</DescriptionText>
        //          </Error>
        //        </ErrorList>
        //      </env:detail>
        //    </env:Fault>
        //  </env:Body>
        //</env:Envelope>
        /// </summary>
        /// <param name="ErrorMessage"></param>
        public void ThrowFlorecomError(string ErrorMessage, int ErrorCode, string ErrorLocation)
        {
            //Construct the Florecom ErrorList structure
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Xml.XmlNamespaceManager ns = new System.Xml.XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            ns.AddNamespace("ferab", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
            System.Xml.XmlNode FlorecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            FlorecomErrorNode.AppendChild(FlorecomErrorLevelNode);
            FlorecomErrorNode.AppendChild(FlorecomErrorSeverity);
            FlorecomErrorNode.AppendChild(FlorecomErrorErrorCode);
            FlorecomErrorNode.AppendChild(FlorecomErrorLocation);
            FlorecomErrorNode.AppendChild(FlorecomErrorDescriptionText);
            FlorecomErrorListNode.AppendChild(FlorecomErrorNode);
            //Add the custom values to the ErrorList strucure
            FlorecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
            FlorecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
            FlorecomErrorErrorCode.AppendChild(doc.CreateTextNode(ErrorCode.ToString()));
            FlorecomErrorLocation.AppendChild(doc.CreateTextNode(ErrorLocation));
            FlorecomErrorDescriptionText.AppendChild(doc.CreateTextNode(ErrorMessage));
            detailNode.AppendChild(FlorecomErrorListNode);
            //Return a Soap:Fault that incorporates the Florecom ErrorList scheme
            throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, detailNode);
        }
    }
}
