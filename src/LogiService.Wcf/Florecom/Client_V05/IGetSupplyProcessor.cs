﻿using System;
using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Client_V05
{
	public interface IGetSupplyProcessor
	{
		PutCertificateResponseMessage Process(PutCertificateRequestMessage SupplyRequest, LoginPrincipal principal);
	}
}