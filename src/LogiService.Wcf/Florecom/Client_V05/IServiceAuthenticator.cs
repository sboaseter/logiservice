﻿using System;
using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Client_V05
{
	public interface IServiceAuthenticator
	{
        LoginPrincipal Authenticate(PutCertificateRequestMessage requestHeader, string requestHeaderPrefix);
        LoginPrincipal Authenticate(PutCertificateCancelRequestMessage requestHeader, string requestHeaderPrefix);
        LoginPrincipal Authenticate(GetCertificateStatusRequestMessage requestHeader, string requestHeaderPrefix);
        LoginPrincipal Authenticate(GetCertificateDocumentMessage requestHeader, string requestHeaderPrefix);
        LoginPrincipal Authenticate(GetInspectionMessage requestHeader, string requestHeaderPrefix);
	}
}