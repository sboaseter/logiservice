﻿namespace LogiService.Wcf.Florecom.Client_V05
{
	/// <summary>
	/// Interface om de verschillende soorten Request Headers gelijk te
	/// kunnen behandelen.
	/// </summary>
	public interface IRequestHeader
	{
		TextType UserName { get; }
		TextType Password { get; }
	}
}