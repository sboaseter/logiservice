using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <summary>
	/// Florecom Webservice Implementation
	/// ==================================
	/// System:     Florecom Commercial Scenario's
	/// Actor:      Buyer
	/// Version:    Juli 2010 (fixed)
	/// 
	/// This example contains both stubs and demos for all commercial use cases that are relevant for buyers of floricultural products. 
	/// 
	/// Notes:
	/// -SOAP 1.2 is disabled using the web.config, because Florecom conversations rely on SOAP 1.1.
	/// -Please, do not alter the serialization attributes. This will guarantee the interoperability (WSDL compatibility) for buyers among different market places.
	/// -Please, do check the functional and technical documentation as publisehed on sdk.florecom.org.
	/// 
	/// Author:
	/// -For more information, see http://sdk.florecom.org or contact Cris Ilbrink (cris.ilbrink@florecom.nl)
	/// </summary>
	[WebServiceAttribute(Namespace = "http://webservice.florecom.org/CommercialBuyer", Description = "Florecom Web Service for the 'Commercial Use Cases' as used by the actor 'Buyer'. Release Jul 2010.", Name = "Florecom Commerical Web Service for the Buyer. Release Jul 2010.")]
	[WebServiceBindingAttribute(Name = "CommercialBuyer", Namespace = "http://webservice.florecom.org/CommercialBuyer")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]

	public class CommercialCustomer : ICommercialCustomer
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		/// <summary>
		/// Return supply to a buyer. This demo returns a single trade line item representing an article that is available (2881 pieces).
		/// 
		/// Notes:
		///     -This demo neglects the supply filter i.e. the request could contain a filter to specify what items to return.
		/// </summary>
		/// <param name="SupplyRequest"></param>
		/// <returns></returns>
		public SupplyResponseMessage GetSupply(SupplyRequestMessage SupplyRequest)
		{
			try
			{
				Log.DebugFormat("GetSupply Request started from [{0}].", HttpContext.Current.Request.UserHostAddress);

				// Haal via de Container onze algemene Service Authenticator op.
				var authenticator = Global.Container.Resolve<IServiceAuthenticator>();

				// De verschillende Headers hebben allemaal een UserName en
				// Password, dus kunnen die voldoen aan de IRequestHeader
				// interface. Die moet wel handmatig worden toegevoegd aan de
				// betreffende class definitie.
				var principal = authenticator.Authenticate(SupplyRequest.Header, "/SupplyRequest/Header");

				// Simpele check of het een valide gebruiker was.
				if (principal == null)
				{
					Log.DebugFormat("GetSupply Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
					throw new FlorecomException("Invalid Login.", "/SupplyRequest/Header");
				}
				Log.InfoFormat("GetSupply Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);

				// Haal via de Container de specifieke Processor voor dit soort
				// Request op.
				var processor = Global.Container.Resolve<IGetSupplyProcessor>();

				// Voer de processing uit.
				return processor.Process(SupplyRequest, principal);
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				Log.Error("GetSupply Unhandled Exception.", err);
				throw new FlorecomException(err.Message, "Unknown");
			}
		}

		/// <summary>
		/// This is a stub for a web method to place an order.
		/// </summary>
		/// <param name="PutOrderRequest"></param>
		/// <returns></returns>
		public OrderResponseMessage PutOrder(OrderRequestMessage PutOrderRequest)
		{
			try
			{
				Log.DebugFormat("PutOrder Request started from [{0}].", HttpContext.Current.Request.UserHostAddress);
				var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
				var principal = authenticator.Authenticate(PutOrderRequest.Header, "/PutOrderRequest/Header");
				if (principal == null)
				{
					Log.DebugFormat("PutOrder Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
					throw new FlorecomException("Invalid Login.", "/PutOrderRequest/Header");
				}
				Log.InfoFormat("PutOrder Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);

				var processor = Global.Container.Resolve<IPutOrderProcessor>();

				// Voer de processing uit.
				return processor.Process(PutOrderRequest, principal);
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				throw new FlorecomException(err.Message, "Unknown");
			}
		}

		/// <summary>
		/// Get status of (specific) supply line items. 
		/// 
		/// This demo retuns the physical location of a specific item at the market place (used for example to estimate the time of auction). 
		/// Antoher could example would be to retreive the availability (quantity) of a specific supply line item.
		/// </summary>
		/// <param name="StatusRequest"></param>
		/// <returns></returns>
		public StatusResponseMessage GetStatus(StatusRequestMessage StatusRequest)
		{
			try
			{
				Log.DebugFormat("GetStatus Request started from [{0}].", HttpContext.Current.Request.UserHostAddress);
				var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
				var principal = authenticator.Authenticate(StatusRequest.Header, "/StatusRequest/Header");
				if (principal == null)
				{
					Log.DebugFormat("GetStatus Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
					throw new FlorecomException("Invalid Login.", "/StatusRequest/Header");
				}
				Log.InfoFormat("GetStatus Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);

				throw new FlorecomException("Not Yet Implemented.", "/");
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				throw new FlorecomException(err.Message, "Unknown");
			}
		}

		/// <summary>
		/// Get the full binary image of a supply line item. The image is encoded in base64.
		/// 
		/// This is a stub web service i.e. not a full working demo.
		/// </summary>
		/// <param name="ImagesRequest"></param>
		/// <returns></returns>
		public ImagesResponseMessage GetImage(ImagesRequestMessage ImagesRequest)
		{
			try
			{
				Log.DebugFormat("GetImage Request started from [{0}].", HttpContext.Current.Request.UserHostAddress);
				var authenticator = Global.Container.Resolve<IServiceAuthenticator>();
				var principal = authenticator.Authenticate(ImagesRequest.Header, "/ImagesRequest/Header");
				if (principal == null)
				{
					Log.DebugFormat("GetImage Invalid Login from [{0}].", HttpContext.Current.Request.UserHostAddress);
					throw new FlorecomException("Invalid Login.", "/ImagesRequest/Header");
				}
				Log.InfoFormat("GetImage Valid Login for [{0}] from [{1}].", principal, HttpContext.Current.Request.UserHostAddress);

				throw new FlorecomException("Not Yet Implemented.", "/");
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				throw new FlorecomException(err.Message, "Unknown");
			}
		}

	}
}