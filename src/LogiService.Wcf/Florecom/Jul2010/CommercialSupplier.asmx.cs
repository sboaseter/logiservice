using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <summary>
	/// Florecom Webservice Implementation
	/// ==================================
	/// System:     Florecom Commercial Scenario's
	/// Actor:      Supplier
	/// Version:    Juli 2010 (fixed)
	/// 
	/// This example contains both stubs and demos for all commercial use cases that are relevant for suppliers of floricultural products. 
	/// 
	/// Notes:
	/// -SOAP 1.2 is disabled using the web.config, because Florecom conversations rely on SOAP 1.1.
	/// -Please, do not alter the serialization attributes. This will guarantee the interoperability (WSDL compatibility) for buyers among different market places.
	/// -Please, do check the functional and technical documentation as publisehed on sdk.florecom.org.
	/// 
	/// Author:
	/// -For more information, see http://sdk.florecom.org or contact Cris Ilbrink (cris.ilbrink@florecom.nl)
	/// </summary>
	[WebServiceAttribute(Namespace = "http://webservice.florecom.org/CommercialSupplier", Description = "Florecom Web Service for the 'Commercial Use Cases' as used by the actor 'Supplier'. Release Jul 2010.", Name = "Florecom Commerical Web Service for the Supplier. Release Jul 2013.")]
	[WebServiceBindingAttribute(Name = "CommercialSupplier", Namespace = "http://webservice.florecom.org/CommercialSupplier")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class CommercialSupplier : ICommercialSupplier
	{

		/// <summary>
		/// Stub for providing supply of a supplier to this market place
		/// </summary>
		/// <param name="putSupplyRequest">Supply request message</param>
		/// <returns>Suplpy response message</returns>
		PutSupplyResponseMessage ICommercialSupplier.PutSupply(PutSupplyMessage putSupplyRequest)
		{
			try
			{
				return new PutSupplyResponseMessage();
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				throw new FlorecomException(err.Message, "Unknown");
			}
		}

		/// <summary>
		/// Stub for providing an image of a supplier to this market place
		/// </summary>
		/// <param name="putImagesRequest">Images request message</param>
		/// <returns>Images response message</returns>
		PutImagesResponseMessage ICommercialSupplier.PutImage(PutImagesMessage putImagesRequest)
		{
			try
			{
				return new PutImagesResponseMessage();
			}
			catch (SoapException)
			{
				throw; // Simply pass on SoapExceptions.
			}
			catch (Exception err)
			{
				throw new FlorecomException(err.Message, "Unknown");
			}
		}
	}
}