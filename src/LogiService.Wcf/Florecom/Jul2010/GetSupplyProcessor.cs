﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LogiCAB.Common;
using LogiCAB.Common.Model;
using LogiCAB.Common.Domain;
using LogiFlora.Common;
using LogiService.Wcf.OudeCode.DAL;
using LogiFlora.Common.Db;

namespace LogiService.Wcf.Florecom.Jul2010
{
	public class GetSupplyProcessor : IGetSupplyProcessor
	{
		protected IImageProvider ImageProvider { get; set; }
		protected IShopPropertyProvider ShopPropertyProvider { get; set; }
        protected IRepository<Handshake> Handshakes { get; set; }
        protected IRepository<SpecialDate> SpecDate { get; set; }
        protected IRepository<HandshakeOption> HandShakeOptions { get; set; }
        protected IRepository<Address> Addresses { get; set; }
        
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public GetSupplyProcessor(
			IImageProvider imageProvider,
			IShopPropertyProvider shopPropertyProvider,
            IRepository<Handshake> handshakes,
            IRepository<HandshakeOption> handshakeOptions,
            IRepository<Address> addresses,
            IRepository<SpecialDate> specDate)
		{
			ImageProvider = imageProvider;
			ShopPropertyProvider = shopPropertyProvider;
            Handshakes = handshakes;
            HandShakeOptions = handshakeOptions;
            Addresses = addresses;
            SpecDate = specDate;
		}

		public SupplyResponseMessage Process(SupplyRequestMessage SupplyRequest, LoginPrincipal principal)
		{
			// Controleer of de gebruiker wel een Buyer is. Anders heeft dit weinig zin.
			if (!principal.IsBuyer)
				throw new FlorecomException("Only Buyers can access GetSupply.", "/SupplyRequest/Header");

            List<System.DateTime> listHoliDays;
            DateTime? latestedDelTime = null;

            listHoliDays = (from spd in SpecDate
                            where spd.SpecialType.Equals("NO_DELIVERY", StringComparison.OrdinalIgnoreCase)
                            select spd.SpecialDay).ToList();

			// Bepaal of er een MutationDateTime gegeven wordt.
			var mutationDateTime = new DateTime(2000, 1, 1);
            Boolean mutationDateTimeSpec = false;
            var settingSupplierOrg = new SettingData().GetSettingByID("SUPPLIERGLNVMP", null, principal.Organisation.Sequence, null);

            if ((SupplyRequest.Body != null) && (SupplyRequest.Body.SupplyRequestDetails != null) && (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine != null)
                 && (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine.Length > 0))
            {
                if (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine[0].MutationDateTimeSpecified)
                {
                    mutationDateTime = SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine[0].MutationDateTime;
                    mutationDateTimeSpec = true;
                }
            }

			Log.Debug ("Header verwerkt");
            Log.DebugFormat("Meegegeven mutatiedatum (2000-1-1 is geen datum): {0} mutatiedatetime specified: {1}", mutationDateTime, mutationDateTimeSpec);

			try
			{
                Boolean delOptionsAdded = false;
				var supplyLines = new List<SupplyTradeLineItemType>();
                Int16 recCount = 0;
				
				var offerDetailData = new OfferDetailData();
                Log.DebugFormat("BuyerSequence: {0}, ShopID: {1}, mutationDate: {2}", principal.Buyer.Sequence, ShopPropertyProvider.ShopId, mutationDateTime);
                //ImageProvider.RefreshAllOfferPictures("");

				foreach (var offerDetail in offerDetailData.GetOfferDetailsDS(
					principal.Buyer.Sequence,
					ShopPropertyProvider.ShopId,
					mutationDateTime))
				{
                    int imageSequence = 0;
                    string imageUrl;
                    string imageType = "P";
                    string supplierOrgGLN = "";
                   
                    //Log.Debug("Er zijn details kijken naar foto");
                    recCount++;

                    if (!string.IsNullOrEmpty(offerDetail.PictureRef))
                        imageUrl = offerDetail.PictureRef;
                    else
                        imageUrl = "";

                    if (offerDetail.CABPicture.HasValue)
                    {
                        //Gevuld met de pict_sequence
                        imageType = "P";
                        imageSequence = offerDetail.CABPicture.Value;
                    }

                    if (offerDetail.OfferPicture.HasValue)
                    {
                        //Gevuld met de pict_sequence
                        imageType = "P";
                        imageSequence = offerDetail.OfferPicture.Value;
                    }
                    else
                    {
                        if (offerDetail.GrowAssortmentPicture.HasValue)
                        {
                            //Gevuld met de pict_sequence
                            imageType = "P";
                            imageSequence = offerDetail.GrowAssortmentPicture.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(imageUrl))
                        imageUrl = HttpContext.Current.Request.ResolveFullUrl(ImageProvider.GetStaticUrlFor(imageType, imageSequence, null, null, null));

					// Bouw een lijst met Characteristics.
					//Log.Debug("Lijst met kenmerken opbouwen");

					var characteristicTypes = offerDetailData
						.PropertiesDS(offerDetail.OfdtFkCABCode)
						.Rows.Cast<DataRow>()
						.Select(drP => CharacteristicTypeVBN(drP))
						.Where(chara => chara != null).ToList();
					// Voeg de imageUrl toe.
				    //Log.Debug("Kenmerken zijn opgebouwd nu foto toevoegen.");

                    if (offerDetail.PictureRef.Length > 1)
                    {
                        characteristicTypes.Add(GetCharacteristic("refPict", offerDetail.PictureRef, "1", "LogiFlora", "1", "LogiFlora"));
                        characteristicTypes.Add(GetCharacteristic("refpic", offerDetail.PictureRef, "1", "LogiFlora", "1", "LogiFlora"));
                    }
                    else
                    {
                        characteristicTypes.Add(GetCharacteristic("refPict", imageUrl, "1", "LogiFlora", "1", "LogiFlora"));
                        characteristicTypes.Add(GetCharacteristic("refpic", imageUrl, "1", "LogiFlora", "1", "LogiFlora"));
                    }
					
                    if (offerDetail.CABDesc.Length > 0)
                        characteristicTypes.Add(GetCharacteristic("CabDesc", offerDetail.CABDesc, "1", "LogiFlora", "1", "CAB omschrijving"));

					var priceOptions = new List<PriceType>
					{
						// Standaardprijs-regel
						GetPriceType(offerDetail.OfdtItemPrice)
					};
					//Log.Debug("Prijslijst opgebouwd");

					if (offerDetail.HasStaffelBool)
						priceOptions.AddRange(
							offerDetailData.StaffelDS(offerDetail.OfdtSequence)
								.Rows.Cast<DataRow>()
								.Select(row => GetPriceTypeStaffel(row)));
					
                    //Log.Debug("Staffels toegevoegd");
					// Haal de GLN en Naam op van de huidige marktplaats.
					var marketPlaceGLN = ShopPropertyProvider.Property("MarketPlaceGLN", "8713783248188");
					var marketPlaceName = ShopPropertyProvider.Property("MarketPlaceName", "LogiCAB");
					//Log.Debug("Marktplaats en naam");

					var details = GetBaseLine(
						offerDetail.OfdtSequence,
						String.Concat(offerDetail.OfhdRemark, " ", offerDetail.OfdtRemark),
						offerDetail.MutationDate ?? DateTime.Now,
						marketPlaceGLN,
						marketPlaceName);

					//Log.Debug("Baseline gevuld");
					details.TradingTerms.TradePeriod = GetSupplyPeriod(offerDetail.OfhdValidFrom, offerDetail.OfhdValidTo);
					//Log.Debug("TradingTerms");
					details.Product = GetSuppliedProductIdentity(offerDetail.CabcCABCode, offerDetail.VBNDesc, offerDetail.VbncVbnCode, characteristicTypes.ToArray());
					//Log.Debug("Product gevuld");
                    details.SellerParty = GetSellerPartyType(offerDetail.OrgaEANCode);
					//Log.Debug("Sellerparty");

                    if (offerDetail.GrowerGLN != null)
                    {
                        if (offerDetail.GrowerGLN.Length > 1)
                            details.Product.ManufacturerParty = GetManufacturerPartyType(offerDetail.GrowerGLN);
                        else
                            details.Product.ManufacturerParty = GetManufacturerPartyType(offerDetail.OrgaEANCode);
                    }
                    //Log.Debug("Manufacturerparty");
                    supplierOrgGLN = offerDetail.OrgaEANCode;

                    //De Supplierparty kan via een setting worden gestuurd..
                    if (settingSupplierOrg != null)
                        if (settingSupplierOrg.SettingValue.Length > 1)
                            supplierOrgGLN = settingSupplierOrg.SettingValue;

                    details.SupplierParty = GetSupplierPartyType(supplierOrgGLN);
                    //Log.Debug("Supplierparty");
					details.Quantity = QuantityType(offerDetail.OfdtNumOfItems, FEC_MeasurementUnitCommonCodeContentType.Item3);
					//Log.Debug("Quantity");
					details.Price = priceOptions.ToArray();
					//Log.Debug("Price");
					details.Packing = GetPackingOptions(offerDetail.CdcaCaskCode, offerDetail.BulkCdcaCaskCode, offerDetail.CacaNumOfUnits, offerDetail.CacaUnitsPerLayer, offerDetail.CacaLayersPerTrolley);
					//Log.Debug("Verpakking");
                    //Voor de delivery options hebben we eerst de handshake nodig om op basis van de handshakeoptions achter de afspraak te komen.
                    //Orga1 is kweker, orga2 is koper..
                    var handshake = Handshakes.First(h => h.Orga1.Sequence == offerDetail.GrowOrgSeq && h.Orga2.Sequence == principal.Buyer.Organisation.Sequence);
                    var delAddress = new Address();
                    var deltermsCode = new DeliveryTermsCodeContentType();
                    Boolean useAllWorkDaysDel = false;
                    deltermsCode = DeliveryTermsCodeContentType.EXW;

                    try
                    {
                        if (handshake.Sequence != null)
                        {
                            var handshakeOption = HandShakeOptions.First(ho => ho.Handshake.Sequence == handshake.Sequence);

                            if (handshakeOption.Sequence != null && handshakeOption.DeliveryAddress != null)
                            {
                                if (offerDetail.OfferType.ToUpper() == "S")
                                    useAllWorkDaysDel = handshakeOption.DelEveryDayStock == 1;

                                if (offerDetail.OfferType.ToUpper() == "O")
                                    useAllWorkDaysDel = handshakeOption.DelEveryDayOffer == 1;

                                delAddress = Addresses.First(addr => addr.Sequence == handshakeOption.DeliveryAddress.Sequence);
                                latestedDelTime = handshakeOption.LatestDeliveryTime;

                                if (handshakeOption.DeliveryType.ToUpper() == "EXW")
                                    deltermsCode = DeliveryTermsCodeContentType.EXW;
                                else
                                    deltermsCode = DeliveryTermsCodeContentType.DDP;
                     
                                //Log.Info("Delivery options gevonden en toegevoegd");
                                details.Delivery = GetDeliveryOptions(offerDetail.OfhdValidFrom, offerDetail.OfhdValidTo, offerDetail.OfhdValidTo, latestedDelTime, delAddress.GLNLocationCode, deltermsCode, useAllWorkDaysDel, listHoliDays);
                                delOptionsAdded = true;
                            }
                     
                            if (handshakeOption.Sequence != null && handshakeOption.LatestDeliveryTime != null)
                            {
                                latestedDelTime = handshakeOption.LatestDeliveryTime;
                            }

                            if (handshakeOption.Sequence != null)
                            {
                                if (offerDetail.OfferType.ToUpper() == "S")
                                    useAllWorkDaysDel = handshakeOption.DelEveryDayStock == 1;

                                if (offerDetail.OfferType.ToUpper() == "O")
                                    useAllWorkDaysDel = handshakeOption.DelEveryDayOffer == 1;
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        delOptionsAdded = false;
                    }

                    if (delOptionsAdded == false)
                    {
                        //Log.Info("Delivery options niet gevonden gebruik default..");
                        details.Delivery = GetDeliveryOptions(offerDetail.OfhdValidFrom, offerDetail.OfhdValidTo, offerDetail.OfhdValidTo, latestedDelTime, "", deltermsCode, useAllWorkDaysDel, listHoliDays);
                    }

                    details.Status = GetStatus();
					supplyLines.Add(details);
				}

				var response = new SupplyResponseMessage
				{
					Body = new SupplyResponseMessageBody
					{
						SupplyResponseDetails = new SupplyType
						{
							SupplyTradeLineItem = supplyLines.ToArray()
						}
					}
				};

                Log.DebugFormat("Totaal {0} verwerkt", recCount.ToString());
				return response;
			}
			catch (Exception ex)
			{
				Log.Error("Error in FillMessage", ex);
				throw new FlorecomException("Error creating Response.");
			}
		}

		private static SupplyTradeLineItemType GetBaseLine(
			decimal sequence,
			string remark,
			DateTime lineDateTime,
			string marketPlaceGLN,
			string marketPlaceName)
		{
			return new SupplyTradeLineItemType
			{
				ID = new IDType
				{
					//GLN bedrijfscode van de marktplaats
					schemeDataURI = marketPlaceGLN,
					schemeName = "AAG",
					//Partij identificatie
					Value = sequence.ToString()
				},

				TradingTerms = new TradingTermsType
				{
					MarketPlace = MarketPlaceTypeCAB(marketPlaceGLN, marketPlaceName),
				},

				LineDateTime = lineDateTime,

				// Ik denk dat dit de nieuwe MarketForm is, op basis van de
				// tekst in CI0010_pro_commercial_EN_jul_2010.pdf en het
				// commentaar bij de oude MarketForm.
				// (002 = Standard supply, price made by seller )
				DocumentType = new DocumentCodeType1
				{
					Value = DocumentNameCodeContentType1.Item2
				},

				// General or promotion information.
				AdditionalInformationTradeNote = new[]
				{
					new TradeNoteType
					{
						ContentCode = CodeType("Remark"),
						Content = TextType(remark.Trim())
					},
				},
			};
		}

		private static CodeType CodeType(string value)
		{
			return new CodeType { Value = value };
		}

		private static MarketPlaceType MarketPlaceTypeCAB(string gln, string name)
		{
			// TODO: Deze moeten waarschijnlijk Shop specifiek worden...
			// TODO: De SupplyResponseDetails heeft ook een Agent veld wat mischien interessant is om te vullen.
			return new MarketPlaceType
			{
				//GLN bedrijfscode van de marktplaats
				ID = IDType(gln, "AAG", name),
				NameText = TextType(name)
			};
		}

		private static PeriodType GetSupplyPeriod(DateTime validFrom, DateTime validTo)
		{
			return new PeriodType
			{
				StartDateTime = validFrom,
				StartDateTimeSpecified = true,
				EndDateTime = validTo,
				EndDateTimeSpecified = true
			};
		}

		private static ProductType GetSuppliedProductIdentity(
			string cabCode,
			string cabDesc,
			string vbnCode,
			CharacteristicType[] characteristicTypes)
		{
			return new ProductType
			{
				IndustryAssignedID = new  IDType
                {
                    Value = vbnCode,
                    schemeAgencyName = "VBN",
                    schemeID = "1",
                },      
				ManufacturerAssignedID = IDType("CAB", cabCode),
                SupplierAssignedID = IDType("CAB", cabCode),
				DescriptionText = TextType(cabDesc),
				ApplicableGoodsCharacteristics = characteristicTypes
			};
		}

		private static IDType IDType(string scheme, string value)
		{
			return new IDType
			{
				schemeName = scheme,
				Value = value
			};
		}

		private static IDType IDType(string schemeDataUri, string scheme, string value)
		{
			return new IDType
			{
				schemeDataURI = schemeDataUri,
				schemeName = scheme,
				Value = value
			};
		}

		private static TextType TextType(string value)
		{
			return new TextType { Value = value };
		}

		private static CharacteristicType CharacteristicTypeVBN(DataRow drP)
		{
			return GetCharacteristic(
				(string)drP["VBNPropCode"],
				(string)drP["VBNPropValue"],
				"8", "VBN",
				"9", "VBN");
		}

		private static CharacteristicType GetCharacteristic(
			string classCode,
			string valueCode,
			string classCodeListId,
			string classCodeListAgencyName,
			string valueCodeListId,
			string valueCodeListAgencyName)
		{
			if (String.IsNullOrEmpty(valueCode))
				return null;

			// TypeCode was ClassCode en single.
			// ValueCode was array.
			var result = new CharacteristicType
			{
				TypeCode = new[] {
					new CodeType
					{
						listID = classCodeListId,
						listAgencyName = classCodeListAgencyName,
						Value = classCode
					}
				},
				ValueCode = new CodeType
				{
					listID = valueCodeListId,
					listAgencyName = valueCodeListAgencyName,
					Value = valueCode
				}
			};
			return result;
		}

		private static SupplierPartyType GetSupplierPartyType(string supplierEAN)
		{
			return new SupplierPartyType
			{
				PrimaryID = new IDType
				{
					schemeID = "1",
					schemeAgencyName = "FEC",
					Value = String.IsNullOrEmpty(supplierEAN) ? "8700000000000" : supplierEAN
				}
			};
		}

        private static ManufacturerPartyType GetManufacturerPartyType(string manufacturerEAN)
        {
            return new ManufacturerPartyType
            {
                PrimaryID = new IDType
                {
                    schemeID = "1",
                    schemeAgencyName = "FEC",
                    Value = String.IsNullOrEmpty(manufacturerEAN) ? "8700000000000" : manufacturerEAN
                }
            };
        }

		private static SellerPartyType GetSellerPartyType(string sellerEAN)
		{
			return new SellerPartyType
			{
				PrimaryID = new IDType
				{
					schemeID = "1",
					schemeAgencyName = "FEC",
					Value = String.IsNullOrEmpty(sellerEAN) ? "8700000000000" : sellerEAN
				}
			};
		}

		private static PriceType GetPriceType(decimal price)
		{
			return new PriceType
			{
				TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.PC },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
				BasisQuantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item1)
			};
		}

		private static PriceType GetPriceTypeStaffel(DataRow drS)
		{
			var ofdsItemPrice = (decimal)drS["OFDS_ITEM_PRICE"];
			var ofdsFrom = (decimal)drS["OFDS_FROM"];
			var ofdsTo = (decimal)drS["OFDS_TO"];
			var ofdsParentType = (short)drS["OFDS_PARENT_TYPE"];
			return GetPriceTypeStaffel(ofdsItemPrice, ofdsFrom, ofdsTo, ofdsParentType);
		}

		private static PriceType GetPriceTypeStaffel(
			decimal price,
			decimal minQuantity,
			decimal maxQuantity,
			int itemType)
		{
			var m = GetM(itemType);
			return new PriceType
			{
				TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.PC },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
				MinimumQuantity = QuantityType(minQuantity, m),
				MaximumQuantity = QuantityType(maxQuantity, m)
			};
		}

		private static QuantityType QuantityType(
			decimal value,
			FEC_MeasurementUnitCommonCodeContentType unit)
		{
			return new QuantityType
			{
				unitCode = unit,
				unitCodeSpecified = true,
				Value = value
			};
		}

		private static AmountType EuroAmount(decimal price)
		{
			return new AmountType
			{
				currencyCode =  ISO3AlphaCurrencyCodeContentType.EUR,
				currencyCodeSpecified = true,
				Value = price
			};
		}

		private static FEC_MeasurementUnitCommonCodeContentType GetM(int itemType)
		{
			switch (itemType)
			{
				case 2:
					return FEC_MeasurementUnitCommonCodeContentType.Item4;
				case 3:
					return FEC_MeasurementUnitCommonCodeContentType.Item5;
			}
			return FEC_MeasurementUnitCommonCodeContentType.Item3;
		}

		private static PackingType[] GetPackingOptions(string caskCode, string bulkCaskCode, decimal perUnit, decimal perLayer, decimal perTrolley)
		{
			return new[]
			{
				new PackingType
				{
					Package = new PackageType
					{
						TypeCode = new CodeType
						{
							listAgencyName = "VBN",
							listID = "901",
							Value = bulkCaskCode
						},
					},
					InnerPackageQuantity = QuantityType(perTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4),
					InnerPacking = new[]
					{
						new PackingType
						{
							Package = new PackageType
							{
								TypeCode = new CodeType
								{
									listAgencyName = "FEC",
									listID = "DE",
									Value = "18"
								},
								Quantity = QuantityType(perTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4),
							},
							InnerPackageQuantity = QuantityType(perLayer, FEC_MeasurementUnitCommonCodeContentType.Item3),
							InnerPacking = new[]
							{
								new PackingType
								{
									Package = new PackageType
									{
										TypeCode = new CodeType
										{
											listAgencyName = "VBN",
											listID = "901",
											Value = caskCode
										},
										Quantity = QuantityType(perLayer, FEC_MeasurementUnitCommonCodeContentType.Item3),
									},
									InnerPackageQuantity = QuantityType(perUnit, FEC_MeasurementUnitCommonCodeContentType.Item1)
								}
							}
						}
					}
				}
			};
		}

        private static DeliveryType[] GetDeliveryOptions(DateTime valid_from, DateTime valid_to, DateTime latestOrder, DateTime? latestDelivery, String locationGLN, DeliveryTermsCodeContentType typeOfDel, Boolean useEveryWorkDayDel, List<DateTime> listHolidays)
        {
            DateTime valid_from_cor = valid_from;
            DateTime newDelDate = (latestDelivery ?? valid_to);

            //Mocht valid_from in het verleden liggen dan corrigeren en beginnen vanaf vandaag..
            if (valid_from_cor.Date < DateTime.Now.Date)
                valid_from_cor = DateTime.Now;

            List<DeliveryType> deltype = new List<DeliveryType>();

            if (useEveryWorkDayDel == true)
            {
                for (DateTime date = valid_from_cor; date.Date <= valid_to.Date; date = date.AddDays(1))
                {
                    //In de handshake optie is het mogelijk voor de kweker aan te geven dat ze iedere dag leveren.
                    //Als een offerte (voorraad of aanbieding) langer dan 1 dag geldig is moeten we de levermomenten weten.
                    //Dus als in de handshake staat dat ze iedere dag leveren dan check of we vandaag of morgen moeten leveren.
                    if (DateTime.Now.TimeOfDay > new TimeSpan(6, 0, 0) && date.Date == DateTime.Now.Date)
                        date = OudeCode.Util.Helper.NextWorkday(date, listHolidays);

                    if (latestDelivery.HasValue)
                        latestDelivery = date.Date + new TimeSpan(latestDelivery.Value.Hour, latestDelivery.Value.Minute, 0);
                    else
                        latestDelivery = date.Date + new TimeSpan(12, 29, 0); //Tijd onbekend, dus maar een realistische tijd meegeven (die herkenbaar is..)

                    //Misschien date we de laatste order tijd ook een keer vastleggen..
                    //Tot die tijd houden we 6:00 uur aan..
                    if (valid_to.Date <= date.Date)
                        latestOrder = valid_to;
                    else
                        latestOrder = date + new TimeSpan(6, 0, 0);

                    // De offerte is geldig tot na de levertijd, dan wordt het lijkt me leveren op de volgende dag. 
                    if (latestOrder > latestDelivery.Value)
                    {
                        newDelDate = OudeCode.Util.Helper.NextWorkday(latestOrder, listHolidays);
                        latestDelivery = newDelDate.Date + new TimeSpan(latestDelivery.Value.Hour, latestDelivery.Value.Minute, 0);
                    }

                    deltype.Add(GetDelType(latestOrder, locationGLN, typeOfDel, latestDelivery));
                }
            }
                
            else
            {
                if (latestDelivery.HasValue)
                {
                    latestDelivery = valid_to.Date + new TimeSpan(latestDelivery.Value.Hour, latestDelivery.Value.Minute, 0);

                    // De offerte is geldig tot na de levertijd, dan wordt het lijkt me leveren op de volgende dag. 
                    if (valid_to > latestDelivery.Value)
                    {
                        newDelDate = OudeCode.Util.Helper.NextWorkday(valid_to, listHolidays);
                        latestDelivery = newDelDate.Date + new TimeSpan(latestDelivery.Value.Hour, latestDelivery.Value.Minute, 0);
                    }

                    deltype.Add(GetDelType(latestOrder, locationGLN, typeOfDel, latestDelivery));
                }
            }

            return deltype.ToArray();
        }
        
        private static DeliveryType GetDelType(DateTime latestOrder, String locationGLN, DeliveryTermsCodeContentType typeOfDel, DateTime? latestDelivery)
        {
           return new DeliveryType
			{
                DeliveryTerms = new TradeDeliveryTermsType
				{
                    //EXW – EX WORKS, is klant haalt het op bij de leverancier, locatie gln is de locatie van de kweker.
                    //DDP – DELIVERED DUTY PAID, is afleveren op locatie die de klant wil, de leverancier is verantwoordelijk voor de
                    //correcte levering en betaalt ook de kosten die hier bijkomen!.
                       
                    DeliveryTypeCode =  new DeliveryTermsCodeType { Value = typeOfDel },
					RelevantTradeLocation = new TradeLocationType
					{
						ID = new IDType
						{
							schemeID = "4",
							schemeAgencyName = "FEC",
							Value = (String.IsNullOrEmpty(locationGLN) ? "8714231208754" : locationGLN)
						}
					}
				},
				LatestOrderDateTime = latestOrder, // todo florecom test
				LatestOrderDateTimeSpecified = true,
				LatestDeliveryDateTime = (latestDelivery.HasValue ? latestDelivery.Value: latestOrder), // todo florecom test
				LatestDeliveryDateTimeSpecified = (latestDelivery.HasValue ? true: false),// todo florecom test
                //LatestDeliveryDateTimeSpecified = false, //todo deze gebruiken bij florecom test.
			};
        }

        private static StatusType[] GetStatus()
        {
            return new[]
            {
                new StatusType
                {
                    ConditionCode = new [] 
                    {
                        new StatusConditionCodeType { Value = StatusConditionCodeContentType.Item71 },
                    },
                
                   // Mag niet in minimum offer..
                   // DescriptionText = new []
                   // {
                   //     new TextType { Value = "available for ordering", languageCode = "EN" }
                   // }
                },
            };
        }

	}
}