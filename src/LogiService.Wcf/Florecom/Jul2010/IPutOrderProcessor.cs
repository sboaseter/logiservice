﻿using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Jul2010
{
	public interface IPutOrderProcessor
	{
		OrderResponseMessage Process(OrderRequestMessage PutOrderRequest, LoginPrincipal principal);
	}
}