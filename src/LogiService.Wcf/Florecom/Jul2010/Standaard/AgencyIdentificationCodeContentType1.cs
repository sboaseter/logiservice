﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(TypeName = "AgencyIdentificationCodeContentType", Namespace = "urn:un:unece:uncefact:codelist:standard:6:3055:D09A")]
	public enum AgencyIdentificationCodeContentType1
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		[XmlEnum("9")]
		Item9,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("12")]
		Item12,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("16")]
		Item16,

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("18")]
		Item18,

		/// <remarks/>
		[XmlEnum("19")]
		Item19,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("26")]
		Item26,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("39")]
		Item39,

		/// <remarks/>
		[XmlEnum("40")]
		Item40,

		/// <remarks/>
		[XmlEnum("41")]
		Item41,

		/// <remarks/>
		[XmlEnum("42")]
		Item42,

		/// <remarks/>
		[XmlEnum("43")]
		Item43,

		/// <remarks/>
		[XmlEnum("44")]
		Item44,

		/// <remarks/>
		[XmlEnum("45")]
		Item45,

		/// <remarks/>
		[XmlEnum("46")]
		Item46,

		/// <remarks/>
		[XmlEnum("47")]
		Item47,

		/// <remarks/>
		[XmlEnum("48")]
		Item48,

		/// <remarks/>
		[XmlEnum("49")]
		Item49,

		/// <remarks/>
		[XmlEnum("50")]
		Item50,

		/// <remarks/>
		[XmlEnum("51")]
		Item51,

		/// <remarks/>
		[XmlEnum("52")]
		Item52,

		/// <remarks/>
		[XmlEnum("53")]
		Item53,

		/// <remarks/>
		[XmlEnum("54")]
		Item54,

		/// <remarks/>
		[XmlEnum("55")]
		Item55,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("58")]
		Item58,

		/// <remarks/>
		[XmlEnum("59")]
		Item59,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("61")]
		Item61,

		/// <remarks/>
		[XmlEnum("62")]
		Item62,

		/// <remarks/>
		[XmlEnum("63")]
		Item63,

		/// <remarks/>
		[XmlEnum("64")]
		Item64,

		/// <remarks/>
		[XmlEnum("65")]
		Item65,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,

		/// <remarks/>
		[XmlEnum("67")]
		Item67,

		/// <remarks/>
		[XmlEnum("68")]
		Item68,

		/// <remarks/>
		[XmlEnum("69")]
		Item69,

		/// <remarks/>
		[XmlEnum("70")]
		Item70,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("72")]
		Item72,

		/// <remarks/>
		[XmlEnum("73")]
		Item73,

		/// <remarks/>
		[XmlEnum("74")]
		Item74,

		/// <remarks/>
		[XmlEnum("75")]
		Item75,

		/// <remarks/>
		[XmlEnum("76")]
		Item76,

		/// <remarks/>
		[XmlEnum("77")]
		Item77,

		/// <remarks/>
		[XmlEnum("78")]
		Item78,

		/// <remarks/>
		[XmlEnum("79")]
		Item79,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("82")]
		Item82,

		/// <remarks/>
		[XmlEnum("83")]
		Item83,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("86")]
		Item86,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		[XmlEnum("88")]
		Item88,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("90")]
		Item90,

		/// <remarks/>
		[XmlEnum("91")]
		Item91,

		/// <remarks/>
		[XmlEnum("92")]
		Item92,

		/// <remarks/>
		[XmlEnum("93")]
		Item93,

		/// <remarks/>
		[XmlEnum("94")]
		Item94,

		/// <remarks/>
		[XmlEnum("95")]
		Item95,

		/// <remarks/>
		[XmlEnum("96")]
		Item96,

		/// <remarks/>
		[XmlEnum("97")]
		Item97,

		/// <remarks/>
		[XmlEnum("98")]
		Item98,

		/// <remarks/>
		[XmlEnum("99")]
		Item99,

		/// <remarks/>
		[XmlEnum("100")]
		Item100,

		/// <remarks/>
		[XmlEnum("101")]
		Item101,

		/// <remarks/>
		[XmlEnum("102")]
		Item102,

		/// <remarks/>
		[XmlEnum("103")]
		Item103,

		/// <remarks/>
		[XmlEnum("104")]
		Item104,

		/// <remarks/>
		[XmlEnum("105")]
		Item105,

		/// <remarks/>
		[XmlEnum("106")]
		Item106,

		/// <remarks/>
		[XmlEnum("107")]
		Item107,

		/// <remarks/>
		[XmlEnum("108")]
		Item108,

		/// <remarks/>
		[XmlEnum("109")]
		Item109,

		/// <remarks/>
		[XmlEnum("110")]
		Item110,

		/// <remarks/>
		[XmlEnum("111")]
		Item111,

		/// <remarks/>
		[XmlEnum("112")]
		Item112,

		/// <remarks/>
		[XmlEnum("113")]
		Item113,

		/// <remarks/>
		[XmlEnum("114")]
		Item114,

		/// <remarks/>
		[XmlEnum("116")]
		Item116,

		/// <remarks/>
		[XmlEnum("117")]
		Item117,

		/// <remarks/>
		[XmlEnum("118")]
		Item118,

		/// <remarks/>
		[XmlEnum("119")]
		Item119,

		/// <remarks/>
		[XmlEnum("120")]
		Item120,

		/// <remarks/>
		[XmlEnum("121")]
		Item121,

		/// <remarks/>
		[XmlEnum("122")]
		Item122,

		/// <remarks/>
		[XmlEnum("123")]
		Item123,

		/// <remarks/>
		[XmlEnum("124")]
		Item124,

		/// <remarks/>
		[XmlEnum("125")]
		Item125,

		/// <remarks/>
		[XmlEnum("126")]
		Item126,

		/// <remarks/>
		[XmlEnum("127")]
		Item127,

		/// <remarks/>
		[XmlEnum("128")]
		Item128,

		/// <remarks/>
		[XmlEnum("129")]
		Item129,

		/// <remarks/>
		[XmlEnum("130")]
		Item130,

		/// <remarks/>
		[XmlEnum("131")]
		Item131,

		/// <remarks/>
		[XmlEnum("132")]
		Item132,

		/// <remarks/>
		[XmlEnum("133")]
		Item133,

		/// <remarks/>
		[XmlEnum("134")]
		Item134,

		/// <remarks/>
		[XmlEnum("135")]
		Item135,

		/// <remarks/>
		[XmlEnum("136")]
		Item136,

		/// <remarks/>
		[XmlEnum("137")]
		Item137,

		/// <remarks/>
		[XmlEnum("138")]
		Item138,

		/// <remarks/>
		[XmlEnum("139")]
		Item139,

		/// <remarks/>
		[XmlEnum("140")]
		Item140,

		/// <remarks/>
		[XmlEnum("141")]
		Item141,

		/// <remarks/>
		[XmlEnum("142")]
		Item142,

		/// <remarks/>
		[XmlEnum("143")]
		Item143,

		/// <remarks/>
		[XmlEnum("144")]
		Item144,

		/// <remarks/>
		[XmlEnum("145")]
		Item145,

		/// <remarks/>
		[XmlEnum("146")]
		Item146,

		/// <remarks/>
		[XmlEnum("147")]
		Item147,

		/// <remarks/>
		[XmlEnum("148")]
		Item148,

		/// <remarks/>
		[XmlEnum("149")]
		Item149,

		/// <remarks/>
		[XmlEnum("150")]
		Item150,

		/// <remarks/>
		[XmlEnum("151")]
		Item151,

		/// <remarks/>
		[XmlEnum("152")]
		Item152,

		/// <remarks/>
		[XmlEnum("153")]
		Item153,

		/// <remarks/>
		[XmlEnum("154")]
		Item154,

		/// <remarks/>
		[XmlEnum("155")]
		Item155,

		/// <remarks/>
		[XmlEnum("156")]
		Item156,

		/// <remarks/>
		[XmlEnum("157")]
		Item157,

		/// <remarks/>
		[XmlEnum("158")]
		Item158,

		/// <remarks/>
		[XmlEnum("159")]
		Item159,

		/// <remarks/>
		[XmlEnum("160")]
		Item160,

		/// <remarks/>
		[XmlEnum("161")]
		Item161,

		/// <remarks/>
		[XmlEnum("162")]
		Item162,

		/// <remarks/>
		[XmlEnum("163")]
		Item163,

		/// <remarks/>
		[XmlEnum("164")]
		Item164,

		/// <remarks/>
		[XmlEnum("165")]
		Item165,

		/// <remarks/>
		[XmlEnum("166")]
		Item166,

		/// <remarks/>
		[XmlEnum("167")]
		Item167,

		/// <remarks/>
		[XmlEnum("168")]
		Item168,

		/// <remarks/>
		[XmlEnum("169")]
		Item169,

		/// <remarks/>
		[XmlEnum("170")]
		Item170,

		/// <remarks/>
		[XmlEnum("171")]
		Item171,

		/// <remarks/>
		[XmlEnum("172")]
		Item172,

		/// <remarks/>
		[XmlEnum("173")]
		Item173,

		/// <remarks/>
		[XmlEnum("174")]
		Item174,

		/// <remarks/>
		[XmlEnum("175")]
		Item175,

		/// <remarks/>
		[XmlEnum("176")]
		Item176,

		/// <remarks/>
		[XmlEnum("177")]
		Item177,

		/// <remarks/>
		[XmlEnum("178")]
		Item178,

		/// <remarks/>
		[XmlEnum("179")]
		Item179,

		/// <remarks/>
		[XmlEnum("180")]
		Item180,

		/// <remarks/>
		[XmlEnum("181")]
		Item181,

		/// <remarks/>
		[XmlEnum("182")]
		Item182,

		/// <remarks/>
		[XmlEnum("183")]
		Item183,

		/// <remarks/>
		[XmlEnum("184")]
		Item184,

		/// <remarks/>
		[XmlEnum("185")]
		Item185,

		/// <remarks/>
		[XmlEnum("186")]
		Item186,

		/// <remarks/>
		[XmlEnum("187")]
		Item187,

		/// <remarks/>
		[XmlEnum("188")]
		Item188,

		/// <remarks/>
		[XmlEnum("189")]
		Item189,

		/// <remarks/>
		[XmlEnum("190")]
		Item190,

		/// <remarks/>
		[XmlEnum("191")]
		Item191,

		/// <remarks/>
		[XmlEnum("192")]
		Item192,

		/// <remarks/>
		[XmlEnum("193")]
		Item193,

		/// <remarks/>
		[XmlEnum("194")]
		Item194,

		/// <remarks/>
		[XmlEnum("195")]
		Item195,

		/// <remarks/>
		[XmlEnum("196")]
		Item196,

		/// <remarks/>
		[XmlEnum("197")]
		Item197,

		/// <remarks/>
		[XmlEnum("198")]
		Item198,

		/// <remarks/>
		[XmlEnum("199")]
		Item199,

		/// <remarks/>
		[XmlEnum("200")]
		Item200,

		/// <remarks/>
		[XmlEnum("201")]
		Item201,

		/// <remarks/>
		[XmlEnum("202")]
		Item202,

		/// <remarks/>
		[XmlEnum("203")]
		Item203,

		/// <remarks/>
		[XmlEnum("204")]
		Item204,

		/// <remarks/>
		[XmlEnum("205")]
		Item205,

		/// <remarks/>
		[XmlEnum("206")]
		Item206,

		/// <remarks/>
		[XmlEnum("207")]
		Item207,

		/// <remarks/>
		[XmlEnum("208")]
		Item208,

		/// <remarks/>
		[XmlEnum("209")]
		Item209,

		/// <remarks/>
		[XmlEnum("210")]
		Item210,

		/// <remarks/>
		[XmlEnum("211")]
		Item211,

		/// <remarks/>
		[XmlEnum("212")]
		Item212,

		/// <remarks/>
		[XmlEnum("213")]
		Item213,

		/// <remarks/>
		[XmlEnum("214")]
		Item214,

		/// <remarks/>
		[XmlEnum("215")]
		Item215,

		/// <remarks/>
		[XmlEnum("216")]
		Item216,

		/// <remarks/>
		[XmlEnum("217")]
		Item217,

		/// <remarks/>
		[XmlEnum("218")]
		Item218,

		/// <remarks/>
		[XmlEnum("219")]
		Item219,

		/// <remarks/>
		[XmlEnum("220")]
		Item220,

		/// <remarks/>
		[XmlEnum("221")]
		Item221,

		/// <remarks/>
		[XmlEnum("222")]
		Item222,

		/// <remarks/>
		[XmlEnum("223")]
		Item223,

		/// <remarks/>
		[XmlEnum("224")]
		Item224,

		/// <remarks/>
		[XmlEnum("225")]
		Item225,

		/// <remarks/>
		[XmlEnum("226")]
		Item226,

		/// <remarks/>
		[XmlEnum("227")]
		Item227,

		/// <remarks/>
		[XmlEnum("228")]
		Item228,

		/// <remarks/>
		[XmlEnum("229")]
		Item229,

		/// <remarks/>
		[XmlEnum("230")]
		Item230,

		/// <remarks/>
		[XmlEnum("231")]
		Item231,

		/// <remarks/>
		[XmlEnum("232")]
		Item232,

		/// <remarks/>
		[XmlEnum("233")]
		Item233,

		/// <remarks/>
		[XmlEnum("234")]
		Item234,

		/// <remarks/>
		[XmlEnum("235")]
		Item235,

		/// <remarks/>
		[XmlEnum("236")]
		Item236,

		/// <remarks/>
		[XmlEnum("237")]
		Item237,

		/// <remarks/>
		[XmlEnum("238")]
		Item238,

		/// <remarks/>
		[XmlEnum("239")]
		Item239,

		/// <remarks/>
		[XmlEnum("240")]
		Item240,

		/// <remarks/>
		[XmlEnum("241")]
		Item241,

		/// <remarks/>
		[XmlEnum("244")]
		Item244,

		/// <remarks/>
		[XmlEnum("245")]
		Item245,

		/// <remarks/>
		[XmlEnum("246")]
		Item246,

		/// <remarks/>
		[XmlEnum("247")]
		Item247,

		/// <remarks/>
		[XmlEnum("248")]
		Item248,

		/// <remarks/>
		[XmlEnum("249")]
		Item249,

		/// <remarks/>
		[XmlEnum("250")]
		Item250,

		/// <remarks/>
		[XmlEnum("251")]
		Item251,

		/// <remarks/>
		[XmlEnum("252")]
		Item252,

		/// <remarks/>
		[XmlEnum("253")]
		Item253,

		/// <remarks/>
		[XmlEnum("254")]
		Item254,

		/// <remarks/>
		[XmlEnum("255")]
		Item255,

		/// <remarks/>
		[XmlEnum("256")]
		Item256,

		/// <remarks/>
		[XmlEnum("257")]
		Item257,

		/// <remarks/>
		[XmlEnum("258")]
		Item258,

		/// <remarks/>
		[XmlEnum("259")]
		Item259,

		/// <remarks/>
		[XmlEnum("260")]
		Item260,

		/// <remarks/>
		[XmlEnum("261")]
		Item261,

		/// <remarks/>
		[XmlEnum("262")]
		Item262,

		/// <remarks/>
		[XmlEnum("263")]
		Item263,

		/// <remarks/>
		[XmlEnum("264")]
		Item264,

		/// <remarks/>
		[XmlEnum("265")]
		Item265,

		/// <remarks/>
		[XmlEnum("266")]
		Item266,

		/// <remarks/>
		[XmlEnum("267")]
		Item267,

		/// <remarks/>
		[XmlEnum("268")]
		Item268,

		/// <remarks/>
		[XmlEnum("269")]
		Item269,

		/// <remarks/>
		[XmlEnum("270")]
		Item270,

		/// <remarks/>
		[XmlEnum("271")]
		Item271,

		/// <remarks/>
		[XmlEnum("272")]
		Item272,

		/// <remarks/>
		[XmlEnum("273")]
		Item273,

		/// <remarks/>
		[XmlEnum("274")]
		Item274,

		/// <remarks/>
		[XmlEnum("275")]
		Item275,

		/// <remarks/>
		[XmlEnum("276")]
		Item276,

		/// <remarks/>
		[XmlEnum("277")]
		Item277,

		/// <remarks/>
		[XmlEnum("278")]
		Item278,

		/// <remarks/>
		[XmlEnum("279")]
		Item279,

		/// <remarks/>
		[XmlEnum("280")]
		Item280,

		/// <remarks/>
		[XmlEnum("281")]
		Item281,

		/// <remarks/>
		[XmlEnum("282")]
		Item282,

		/// <remarks/>
		[XmlEnum("283")]
		Item283,

		/// <remarks/>
		[XmlEnum("284")]
		Item284,

		/// <remarks/>
		[XmlEnum("285")]
		Item285,

		/// <remarks/>
		[XmlEnum("286")]
		Item286,

		/// <remarks/>
		[XmlEnum("287")]
		Item287,

		/// <remarks/>
		[XmlEnum("288")]
		Item288,

		/// <remarks/>
		[XmlEnum("289")]
		Item289,

		/// <remarks/>
		[XmlEnum("290")]
		Item290,

		/// <remarks/>
		[XmlEnum("291")]
		Item291,

		/// <remarks/>
		[XmlEnum("292")]
		Item292,

		/// <remarks/>
		[XmlEnum("293")]
		Item293,

		/// <remarks/>
		[XmlEnum("294")]
		Item294,

		/// <remarks/>
		[XmlEnum("295")]
		Item295,

		/// <remarks/>
		[XmlEnum("296")]
		Item296,

		/// <remarks/>
		[XmlEnum("297")]
		Item297,

		/// <remarks/>
		[XmlEnum("298")]
		Item298,

		/// <remarks/>
		[XmlEnum("299")]
		Item299,

		/// <remarks/>
		[XmlEnum("300")]
		Item300,

		/// <remarks/>
		[XmlEnum("301")]
		Item301,

		/// <remarks/>
		[XmlEnum("302")]
		Item302,

		/// <remarks/>
		[XmlEnum("303")]
		Item303,

		/// <remarks/>
		[XmlEnum("304")]
		Item304,

		/// <remarks/>
		[XmlEnum("305")]
		Item305,

		/// <remarks/>
		[XmlEnum("306")]
		Item306,

		/// <remarks/>
		[XmlEnum("307")]
		Item307,

		/// <remarks/>
		[XmlEnum("309")]
		Item309,

		/// <remarks/>
		[XmlEnum("310")]
		Item310,

		/// <remarks/>
		[XmlEnum("311")]
		Item311,

		/// <remarks/>
		[XmlEnum("312")]
		Item312,

		/// <remarks/>
		[XmlEnum("313")]
		Item313,

		/// <remarks/>
		[XmlEnum("314")]
		Item314,

		/// <remarks/>
		[XmlEnum("315")]
		Item315,

		/// <remarks/>
		[XmlEnum("316")]
		Item316,

		/// <remarks/>
		[XmlEnum("317")]
		Item317,

		/// <remarks/>
		[XmlEnum("318")]
		Item318,

		/// <remarks/>
		[XmlEnum("319")]
		Item319,

		/// <remarks/>
		[XmlEnum("320")]
		Item320,

		/// <remarks/>
		[XmlEnum("321")]
		Item321,

		/// <remarks/>
		[XmlEnum("322")]
		Item322,

		/// <remarks/>
		[XmlEnum("323")]
		Item323,

		/// <remarks/>
		[XmlEnum("324")]
		Item324,

		/// <remarks/>
		[XmlEnum("325")]
		Item325,

		/// <remarks/>
		[XmlEnum("326")]
		Item326,

		/// <remarks/>
		[XmlEnum("327")]
		Item327,

		/// <remarks/>
		[XmlEnum("328")]
		Item328,

		/// <remarks/>
		[XmlEnum("329")]
		Item329,

		/// <remarks/>
		[XmlEnum("330")]
		Item330,

		/// <remarks/>
		[XmlEnum("331")]
		Item331,

		/// <remarks/>
		[XmlEnum("332")]
		Item332,

		/// <remarks/>
		[XmlEnum("333")]
		Item333,

		/// <remarks/>
		[XmlEnum("334")]
		Item334,

		/// <remarks/>
		[XmlEnum("335")]
		Item335,

		/// <remarks/>
		[XmlEnum("336")]
		Item336,

		/// <remarks/>
		[XmlEnum("337")]
		Item337,

		/// <remarks/>
		[XmlEnum("338")]
		Item338,

		/// <remarks/>
		[XmlEnum("339")]
		Item339,

		/// <remarks/>
		[XmlEnum("340")]
		Item340,

		/// <remarks/>
		[XmlEnum("341")]
		Item341,

		/// <remarks/>
		[XmlEnum("342")]
		Item342,

		/// <remarks/>
		[XmlEnum("343")]
		Item343,

		/// <remarks/>
		[XmlEnum("344")]
		Item344,

		/// <remarks/>
		[XmlEnum("345")]
		Item345,

		/// <remarks/>
		[XmlEnum("346")]
		Item346,

		/// <remarks/>
		[XmlEnum("347")]
		Item347,

		/// <remarks/>
		[XmlEnum("348")]
		Item348,

		/// <remarks/>
		[XmlEnum("349")]
		Item349,

		/// <remarks/>
		[XmlEnum("350")]
		Item350,

		/// <remarks/>
		[XmlEnum("351")]
		Item351,

		/// <remarks/>
		[XmlEnum("352")]
		Item352,

		/// <remarks/>
		[XmlEnum("353")]
		Item353,

		/// <remarks/>
		[XmlEnum("354")]
		Item354,

		/// <remarks/>
		[XmlEnum("355")]
		Item355,

		/// <remarks/>
		[XmlEnum("356")]
		Item356,

		/// <remarks/>
		[XmlEnum("357")]
		Item357,

		/// <remarks/>
		[XmlEnum("358")]
		Item358,

		/// <remarks/>
		[XmlEnum("359")]
		Item359,

		/// <remarks/>
		[XmlEnum("360")]
		Item360,

		/// <remarks/>
		[XmlEnum("361")]
		Item361,

		/// <remarks/>
		[XmlEnum("362")]
		Item362,

		/// <remarks/>
		[XmlEnum("363")]
		Item363,

		/// <remarks/>
		[XmlEnum("364")]
		Item364,

		/// <remarks/>
		[XmlEnum("365")]
		Item365,

		/// <remarks/>
		[XmlEnum("366")]
		Item366,

		/// <remarks/>
		[XmlEnum("367")]
		Item367,

		/// <remarks/>
		[XmlEnum("368")]
		Item368,

		/// <remarks/>
		[XmlEnum("369")]
		Item369,

		/// <remarks/>
		[XmlEnum("370")]
		Item370,

		/// <remarks/>
		[XmlEnum("371")]
		Item371,

		/// <remarks/>
		[XmlEnum("372")]
		Item372,

		/// <remarks/>
		[XmlEnum("373")]
		Item373,

		/// <remarks/>
		[XmlEnum("374")]
		Item374,

		/// <remarks/>
		[XmlEnum("375")]
		Item375,

		/// <remarks/>
// ReSharper disable InconsistentNaming
		ZZZ,
// ReSharper restore InconsistentNaming
	}
}