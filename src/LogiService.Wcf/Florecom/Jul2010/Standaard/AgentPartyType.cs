﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class AgentPartyType
	{
		/// <remarks/>
		public IDType PrimaryID { get; set; }

		/// <remarks/>
		[XmlElement("AdditionalID")]
		public IDType[] AdditionalID { get; set; }

		/// <remarks/>
		[XmlElement("TaxID")]
		public IDType[] TaxID { get; set; }

		/// <remarks/>
		public TextType Name { get; set; }

		/// <remarks/>
		public StructuredAddressType PostalStructuredAddress { get; set; }

		/// <remarks/>
		public UnstructuredAddressType PostalUnstructuredAddress { get; set; }

		/// <remarks/>
		public PostBoxAddressType PostalPostBoxAddress { get; set; }

		/// <remarks/>
		[XmlElement("DefinedTradeContact")]
		public TradeContactType[] DefinedTradeContact { get; set; }
	}
}