﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:IANA:CharacterSetCode:2007-05-14")]
	public enum CharacterSetCodeContentType
	{

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		[XmlEnum("9")]
		Item9,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("12")]
		Item12,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("16")]
		Item16,

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("18")]
		Item18,

		/// <remarks/>
		[XmlEnum("19")]
		Item19,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("26")]
		Item26,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("39")]
		Item39,

		/// <remarks/>
		[XmlEnum("40")]
		Item40,

		/// <remarks/>
		[XmlEnum("41")]
		Item41,

		/// <remarks/>
		[XmlEnum("42")]
		Item42,

		/// <remarks/>
		[XmlEnum("43")]
		Item43,

		/// <remarks/>
		[XmlEnum("44")]
		Item44,

		/// <remarks/>
		[XmlEnum("45")]
		Item45,

		/// <remarks/>
		[XmlEnum("46")]
		Item46,

		/// <remarks/>
		[XmlEnum("47")]
		Item47,

		/// <remarks/>
		[XmlEnum("48")]
		Item48,

		/// <remarks/>
		[XmlEnum("49")]
		Item49,

		/// <remarks/>
		[XmlEnum("50")]
		Item50,

		/// <remarks/>
		[XmlEnum("51")]
		Item51,

		/// <remarks/>
		[XmlEnum("52")]
		Item52,

		/// <remarks/>
		[XmlEnum("53")]
		Item53,

		/// <remarks/>
		[XmlEnum("54")]
		Item54,

		/// <remarks/>
		[XmlEnum("55")]
		Item55,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("58")]
		Item58,

		/// <remarks/>
		[XmlEnum("59")]
		Item59,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("61")]
		Item61,

		/// <remarks/>
		[XmlEnum("62")]
		Item62,

		/// <remarks/>
		[XmlEnum("63")]
		Item63,

		/// <remarks/>
		[XmlEnum("64")]
		Item64,

		/// <remarks/>
		[XmlEnum("65")]
		Item65,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,

		/// <remarks/>
		[XmlEnum("67")]
		Item67,

		/// <remarks/>
		[XmlEnum("68")]
		Item68,

		/// <remarks/>
		[XmlEnum("69")]
		Item69,

		/// <remarks/>
		[XmlEnum("70")]
		Item70,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("72")]
		Item72,

		/// <remarks/>
		[XmlEnum("73")]
		Item73,

		/// <remarks/>
		[XmlEnum("74")]
		Item74,

		/// <remarks/>
		[XmlEnum("75")]
		Item75,

		/// <remarks/>
		[XmlEnum("76")]
		Item76,

		/// <remarks/>
		[XmlEnum("77")]
		Item77,

		/// <remarks/>
		[XmlEnum("78")]
		Item78,

		/// <remarks/>
		[XmlEnum("79")]
		Item79,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("82")]
		Item82,

		/// <remarks/>
		[XmlEnum("83")]
		Item83,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("86")]
		Item86,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		[XmlEnum("88")]
		Item88,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("90")]
		Item90,

		/// <remarks/>
		[XmlEnum("91")]
		Item91,

		/// <remarks/>
		[XmlEnum("92")]
		Item92,

		/// <remarks/>
		[XmlEnum("93")]
		Item93,

		/// <remarks/>
		[XmlEnum("94")]
		Item94,

		/// <remarks/>
		[XmlEnum("95")]
		Item95,

		/// <remarks/>
		[XmlEnum("96")]
		Item96,

		/// <remarks/>
		[XmlEnum("97")]
		Item97,

		/// <remarks/>
		[XmlEnum("98")]
		Item98,

		/// <remarks/>
		[XmlEnum("99")]
		Item99,

		/// <remarks/>
		[XmlEnum("100")]
		Item100,

		/// <remarks/>
		[XmlEnum("101")]
		Item101,

		/// <remarks/>
		[XmlEnum("102")]
		Item102,

		/// <remarks/>
		[XmlEnum("103")]
		Item103,

		/// <remarks/>
		[XmlEnum("104")]
		Item104,

		/// <remarks/>
		[XmlEnum("105")]
		Item105,

		/// <remarks/>
		[XmlEnum("106")]
		Item106,

		/// <remarks/>
		[XmlEnum("109")]
		Item109,

		/// <remarks/>
		[XmlEnum("110")]
		Item110,

		/// <remarks/>
		[XmlEnum("111")]
		Item111,

		/// <remarks/>
		[XmlEnum("112")]
		Item112,

		/// <remarks/>
		[XmlEnum("113")]
		Item113,

		/// <remarks/>
		[XmlEnum("114")]
		Item114,

		/// <remarks/>
		[XmlEnum("115")]
		Item115,

		/// <remarks/>
		[XmlEnum("116")]
		Item116,

		/// <remarks/>
		[XmlEnum("117")]
		Item117,

		/// <remarks/>
		[XmlEnum("118")]
		Item118,

		/// <remarks/>
		[XmlEnum("119")]
		Item119,

		/// <remarks/>
		[XmlEnum("1000")]
		Item1000,

		/// <remarks/>
		[XmlEnum("1001")]
		Item1001,

		/// <remarks/>
		[XmlEnum("1002")]
		Item1002,

		/// <remarks/>
		[XmlEnum("1003")]
		Item1003,

		/// <remarks/>
		[XmlEnum("1004")]
		Item1004,

		/// <remarks/>
		[XmlEnum("1005")]
		Item1005,

		/// <remarks/>
		[XmlEnum("1006")]
		Item1006,

		/// <remarks/>
		[XmlEnum("1007")]
		Item1007,

		/// <remarks/>
		[XmlEnum("1008")]
		Item1008,

		/// <remarks/>
		[XmlEnum("1009")]
		Item1009,

		/// <remarks/>
		[XmlEnum("1010")]
		Item1010,

		/// <remarks/>
		[XmlEnum("1011")]
		Item1011,

		/// <remarks/>
		[XmlEnum("1012")]
		Item1012,

		/// <remarks/>
		[XmlEnum("1013")]
		Item1013,

		/// <remarks/>
		[XmlEnum("1014")]
		Item1014,

		/// <remarks/>
		[XmlEnum("1015")]
		Item1015,

		/// <remarks/>
		[XmlEnum("1016")]
		Item1016,

		/// <remarks/>
		[XmlEnum("1017")]
		Item1017,

		/// <remarks/>
		[XmlEnum("1018")]
		Item1018,

		/// <remarks/>
		[XmlEnum("1019")]
		Item1019,

		/// <remarks/>
		[XmlEnum("1020")]
		Item1020,

		/// <remarks/>
		[XmlEnum("2000")]
		Item2000,

		/// <remarks/>
		[XmlEnum("2001")]
		Item2001,

		/// <remarks/>
		[XmlEnum("2002")]
		Item2002,

		/// <remarks/>
		[XmlEnum("2003")]
		Item2003,

		/// <remarks/>
		[XmlEnum("2004")]
		Item2004,

		/// <remarks/>
		[XmlEnum("2005")]
		Item2005,

		/// <remarks/>
		[XmlEnum("2006")]
		Item2006,

		/// <remarks/>
		[XmlEnum("2007")]
		Item2007,

		/// <remarks/>
		[XmlEnum("2008")]
		Item2008,

		/// <remarks/>
		[XmlEnum("2009")]
		Item2009,

		/// <remarks/>
		[XmlEnum("2012")]
		Item2012,

		/// <remarks/>
		[XmlEnum("2013")]
		Item2013,

		/// <remarks/>
		[XmlEnum("2014")]
		Item2014,

		/// <remarks/>
		[XmlEnum("2015")]
		Item2015,

		/// <remarks/>
		[XmlEnum("2016")]
		Item2016,

		/// <remarks/>
		[XmlEnum("2017")]
		Item2017,

		/// <remarks/>
		[XmlEnum("2018")]
		Item2018,

		/// <remarks/>
		[XmlEnum("2019")]
		Item2019,

		/// <remarks/>
		[XmlEnum("2020")]
		Item2020,

		/// <remarks/>
		[XmlEnum("2021")]
		Item2021,

		/// <remarks/>
		[XmlEnum("2022")]
		Item2022,

		/// <remarks/>
		[XmlEnum("2023")]
		Item2023,

		/// <remarks/>
		[XmlEnum("2024")]
		Item2024,

		/// <remarks/>
		[XmlEnum("2025")]
		Item2025,

		/// <remarks/>
		[XmlEnum("2026")]
		Item2026,

		/// <remarks/>
		[XmlEnum("2027")]
		Item2027,

		/// <remarks/>
		[XmlEnum("2028")]
		Item2028,

		/// <remarks/>
		[XmlEnum("2029")]
		Item2029,

		/// <remarks/>
		[XmlEnum("2030")]
		Item2030,

		/// <remarks/>
		[XmlEnum("2031")]
		Item2031,

		/// <remarks/>
		[XmlEnum("2032")]
		Item2032,

		/// <remarks/>
		[XmlEnum("2033")]
		Item2033,

		/// <remarks/>
		[XmlEnum("2034")]
		Item2034,

		/// <remarks/>
		[XmlEnum("2035")]
		Item2035,

		/// <remarks/>
		[XmlEnum("2036")]
		Item2036,

		/// <remarks/>
		[XmlEnum("2037")]
		Item2037,

		/// <remarks/>
		[XmlEnum("2038")]
		Item2038,

		/// <remarks/>
		[XmlEnum("2039")]
		Item2039,

		/// <remarks/>
		[XmlEnum("2040")]
		Item2040,

		/// <remarks/>
		[XmlEnum("2041")]
		Item2041,

		/// <remarks/>
		[XmlEnum("2042")]
		Item2042,

		/// <remarks/>
		[XmlEnum("2043")]
		Item2043,

		/// <remarks/>
		[XmlEnum("2011")]
		Item2011,

		/// <remarks/>
		[XmlEnum("2044")]
		Item2044,

		/// <remarks/>
		[XmlEnum("2045")]
		Item2045,

		/// <remarks/>
		[XmlEnum("2010")]
		Item2010,

		/// <remarks/>
		[XmlEnum("2046")]
		Item2046,

		/// <remarks/>
		[XmlEnum("2047")]
		Item2047,

		/// <remarks/>
		[XmlEnum("2048")]
		Item2048,

		/// <remarks/>
		[XmlEnum("2049")]
		Item2049,

		/// <remarks/>
		[XmlEnum("2050")]
		Item2050,

		/// <remarks/>
		[XmlEnum("2051")]
		Item2051,

		/// <remarks/>
		[XmlEnum("2052")]
		Item2052,

		/// <remarks/>
		[XmlEnum("2053")]
		Item2053,

		/// <remarks/>
		[XmlEnum("2054")]
		Item2054,

		/// <remarks/>
		[XmlEnum("2055")]
		Item2055,

		/// <remarks/>
		[XmlEnum("2056")]
		Item2056,

		/// <remarks/>
		[XmlEnum("2057")]
		Item2057,

		/// <remarks/>
		[XmlEnum("2058")]
		Item2058,

		/// <remarks/>
		[XmlEnum("2059")]
		Item2059,

		/// <remarks/>
		[XmlEnum("2060")]
		Item2060,

		/// <remarks/>
		[XmlEnum("2061")]
		Item2061,

		/// <remarks/>
		[XmlEnum("2062")]
		Item2062,

		/// <remarks/>
		[XmlEnum("2063")]
		Item2063,

		/// <remarks/>
		[XmlEnum("2064")]
		Item2064,

		/// <remarks/>
		[XmlEnum("2065")]
		Item2065,

		/// <remarks/>
		[XmlEnum("2066")]
		Item2066,

		/// <remarks/>
		[XmlEnum("2067")]
		Item2067,

		/// <remarks/>
		[XmlEnum("2068")]
		Item2068,

		/// <remarks/>
		[XmlEnum("2069")]
		Item2069,

		/// <remarks/>
		[XmlEnum("2070")]
		Item2070,

		/// <remarks/>
		[XmlEnum("2071")]
		Item2071,

		/// <remarks/>
		[XmlEnum("2072")]
		Item2072,

		/// <remarks/>
		[XmlEnum("2073")]
		Item2073,

		/// <remarks/>
		[XmlEnum("2074")]
		Item2074,

		/// <remarks/>
		[XmlEnum("2075")]
		Item2075,

		/// <remarks/>
		[XmlEnum("2076")]
		Item2076,

		/// <remarks/>
		[XmlEnum("2077")]
		Item2077,

		/// <remarks/>
		[XmlEnum("2078")]
		Item2078,

		/// <remarks/>
		[XmlEnum("2079")]
		Item2079,

		/// <remarks/>
		[XmlEnum("2080")]
		Item2080,

		/// <remarks/>
		[XmlEnum("2081")]
		Item2081,

		/// <remarks/>
		[XmlEnum("2082")]
		Item2082,

		/// <remarks/>
		[XmlEnum("2083")]
		Item2083,

		/// <remarks/>
		[XmlEnum("2084")]
		Item2084,

		/// <remarks/>
		[XmlEnum("2085")]
		Item2085,

		/// <remarks/>
		[XmlEnum("2086")]
		Item2086,

		/// <remarks/>
		[XmlEnum("2087")]
		Item2087,

		/// <remarks/>
		[XmlEnum("2088")]
		Item2088,

		/// <remarks/>
		[XmlEnum("2089")]
		Item2089,

		/// <remarks/>
		[XmlEnum("2090")]
		Item2090,

		/// <remarks/>
		[XmlEnum("2091")]
		Item2091,

		/// <remarks/>
		[XmlEnum("2092")]
		Item2092,

		/// <remarks/>
		[XmlEnum("2093")]
		Item2093,

		/// <remarks/>
		[XmlEnum("2094")]
		Item2094,

		/// <remarks/>
		[XmlEnum("2095")]
		Item2095,

		/// <remarks/>
		[XmlEnum("2096")]
		Item2096,

		/// <remarks/>
		[XmlEnum("2097")]
		Item2097,

		/// <remarks/>
		[XmlEnum("2098")]
		Item2098,

		/// <remarks/>
		[XmlEnum("2099")]
		Item2099,

		/// <remarks/>
		[XmlEnum("2100")]
		Item2100,

		/// <remarks/>
		[XmlEnum("2101")]
		Item2101,

		/// <remarks/>
		[XmlEnum("2102")]
		Item2102,

		/// <remarks/>
		[XmlEnum("2103")]
		Item2103,

		/// <remarks/>
		[XmlEnum("2104")]
		Item2104,

		/// <remarks/>
		[XmlEnum("2105")]
		Item2105,

		/// <remarks/>
		[XmlEnum("2106")]
		Item2106,

		/// <remarks/>
		[XmlEnum("2107")]
		Item2107,

		/// <remarks/>
		[XmlEnum("2250")]
		Item2250,

		/// <remarks/>
		[XmlEnum("2251")]
		Item2251,

		/// <remarks/>
		[XmlEnum("2252")]
		Item2252,

		/// <remarks/>
		[XmlEnum("2253")]
		Item2253,

		/// <remarks/>
		[XmlEnum("2254")]
		Item2254,

		/// <remarks/>
		[XmlEnum("2255")]
		Item2255,

		/// <remarks/>
		[XmlEnum("2256")]
		Item2256,

		/// <remarks/>
		[XmlEnum("2257")]
		Item2257,

		/// <remarks/>
		[XmlEnum("2258")]
		Item2258,

		/// <remarks/>
		[XmlEnum("2259")]
		Item2259,
	}
}