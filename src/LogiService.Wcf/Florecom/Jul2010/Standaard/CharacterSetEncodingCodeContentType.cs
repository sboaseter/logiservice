﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:6:0133:20061205")]
	public enum CharacterSetEncodingCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		zzz,
	}
}