﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4")]
	public class CodeType
	{
		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public AgencyIdentificationCodeContentType listAgencyID { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool listAgencyIDSpecified { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string listAgencyName { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string listName { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listVersionID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string name { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "language")]
		public string languageCode { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "anyURI")]
		public string listURI { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "anyURI")]
		public string listSchemeURI { get; set; }

		/// <remarks/>
		[XmlText(DataType = "token")]
		public string Value { get; set; }
	}
}