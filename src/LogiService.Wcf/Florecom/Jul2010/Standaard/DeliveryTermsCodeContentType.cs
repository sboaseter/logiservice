﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:DeliveryTermsCode:D07A")]
	public enum DeliveryTermsCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		CFR,

		/// <remarks/>
		CIF,

		/// <remarks/>
		CIP,

		/// <remarks/>
		CPT,

		/// <remarks/>
		DAF,

		/// <remarks/>
		DDP,

		/// <remarks/>
		DDU,

		/// <remarks/>
		DEQ,

		/// <remarks/>
		DES,

		/// <remarks/>
		EXW,

		/// <remarks/>
		FAS,

		/// <remarks/>
		FCA,

		/// <remarks/>
		FOB,
	}
}