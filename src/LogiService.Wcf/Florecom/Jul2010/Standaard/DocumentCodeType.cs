﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:3")]
	public class DocumentCodeType
	{
		public DocumentCodeType()
		{
			listID = "1001";
			listAgencyID = AgencyIdentificationCodeContentType.Item6;
			listVersionID = "D07A";
		}

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public AgencyIdentificationCodeContentType listAgencyID { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool listAgencyIDSpecified { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listVersionID { get; set; }

		/// <remarks/>
		[XmlText]
		public DocumentNameCodeContentType Value { get; set; }
	}
}