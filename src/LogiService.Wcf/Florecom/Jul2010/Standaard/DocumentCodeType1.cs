﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(TypeName = "DocumentCodeType", Namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:7")]
	public class DocumentCodeType1
	{
		public DocumentCodeType1()
		{
			listID = "1001";
			listAgencyID = AgencyIdentificationCodeContentType1.Item6;
			listVersionID = "D09A";
		}

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public AgencyIdentificationCodeContentType1 listAgencyID { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool listAgencyIDSpecified { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string listVersionID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string name { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "anyURI")]
		public string listURI { get; set; }

		/// <remarks/>
		[XmlText]
		public DocumentNameCodeContentType1 Value { get; set; }
	}
}