﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(TypeName = "DocumentNameCodeContentType", Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:DocumentNameCode:D09A")]
	public enum DocumentNameCodeContentType1
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		[XmlEnum("9")]
		Item9,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("12")]
		Item12,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("16")]
		Item16,

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("18")]
		Item18,

		/// <remarks/>
		[XmlEnum("19")]
		Item19,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("26")]
		Item26,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("39")]
		Item39,

		/// <remarks/>
		[XmlEnum("40")]
		Item40,

		/// <remarks/>
		[XmlEnum("41")]
		Item41,

		/// <remarks/>
		[XmlEnum("42")]
		Item42,

		/// <remarks/>
		[XmlEnum("43")]
		Item43,

		/// <remarks/>
		[XmlEnum("44")]
		Item44,

		/// <remarks/>
		[XmlEnum("45")]
		Item45,

		/// <remarks/>
		[XmlEnum("46")]
		Item46,

		/// <remarks/>
		[XmlEnum("47")]
		Item47,

		/// <remarks/>
		[XmlEnum("48")]
		Item48,

		/// <remarks/>
		[XmlEnum("49")]
		Item49,

		/// <remarks/>
		[XmlEnum("50")]
		Item50,

		/// <remarks/>
		[XmlEnum("51")]
		Item51,

		/// <remarks/>
		[XmlEnum("52")]
		Item52,

		/// <remarks/>
		[XmlEnum("53")]
		Item53,

		/// <remarks/>
		[XmlEnum("54")]
		Item54,

		/// <remarks/>
		[XmlEnum("55")]
		Item55,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("58")]
		Item58,

		/// <remarks/>
		[XmlEnum("59")]
		Item59,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("61")]
		Item61,

		/// <remarks/>
		[XmlEnum("62")]
		Item62,

		/// <remarks/>
		[XmlEnum("63")]
		Item63,

		/// <remarks/>
		[XmlEnum("64")]
		Item64,

		/// <remarks/>
		[XmlEnum("65")]
		Item65,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,

		/// <remarks/>
		[XmlEnum("67")]
		Item67,

		/// <remarks/>
		[XmlEnum("68")]
		Item68,

		/// <remarks/>
		[XmlEnum("69")]
		Item69,

		/// <remarks/>
		[XmlEnum("70")]
		Item70,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("72")]
		Item72,

		/// <remarks/>
		[XmlEnum("73")]
		Item73,

		/// <remarks/>
		[XmlEnum("74")]
		Item74,

		/// <remarks/>
		[XmlEnum("75")]
		Item75,

		/// <remarks/>
		[XmlEnum("76")]
		Item76,

		/// <remarks/>
		[XmlEnum("77")]
		Item77,

		/// <remarks/>
		[XmlEnum("78")]
		Item78,

		/// <remarks/>
		[XmlEnum("79")]
		Item79,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("82")]
		Item82,

		/// <remarks/>
		[XmlEnum("83")]
		Item83,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("86")]
		Item86,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		[XmlEnum("88")]
		Item88,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("90")]
		Item90,

		/// <remarks/>
		[XmlEnum("91")]
		Item91,

		/// <remarks/>
		[XmlEnum("92")]
		Item92,

		/// <remarks/>
		[XmlEnum("93")]
		Item93,

		/// <remarks/>
		[XmlEnum("94")]
		Item94,

		/// <remarks/>
		[XmlEnum("95")]
		Item95,

		/// <remarks/>
		[XmlEnum("96")]
		Item96,

		/// <remarks/>
		[XmlEnum("97")]
		Item97,

		/// <remarks/>
		[XmlEnum("98")]
		Item98,

		/// <remarks/>
		[XmlEnum("99")]
		Item99,

		/// <remarks/>
		[XmlEnum("100")]
		Item100,

		/// <remarks/>
		[XmlEnum("101")]
		Item101,

		/// <remarks/>
		[XmlEnum("102")]
		Item102,

		/// <remarks/>
		[XmlEnum("103")]
		Item103,

		/// <remarks/>
		[XmlEnum("104")]
		Item104,

		/// <remarks/>
		[XmlEnum("105")]
		Item105,

		/// <remarks/>
		[XmlEnum("106")]
		Item106,

		/// <remarks/>
		[XmlEnum("107")]
		Item107,

		/// <remarks/>
		[XmlEnum("108")]
		Item108,

		/// <remarks/>
		[XmlEnum("109")]
		Item109,

		/// <remarks/>
		[XmlEnum("110")]
		Item110,

		/// <remarks/>
		[XmlEnum("111")]
		Item111,

		/// <remarks/>
		[XmlEnum("112")]
		Item112,

		/// <remarks/>
		[XmlEnum("113")]
		Item113,

		/// <remarks/>
		[XmlEnum("114")]
		Item114,

		/// <remarks/>
		[XmlEnum("115")]
		Item115,

		/// <remarks/>
		[XmlEnum("116")]
		Item116,

		/// <remarks/>
		[XmlEnum("117")]
		Item117,

		/// <remarks/>
		[XmlEnum("118")]
		Item118,

		/// <remarks/>
		[XmlEnum("119")]
		Item119,

		/// <remarks/>
		[XmlEnum("120")]
		Item120,

		/// <remarks/>
		[XmlEnum("121")]
		Item121,

		/// <remarks/>
		[XmlEnum("122")]
		Item122,

		/// <remarks/>
		[XmlEnum("123")]
		Item123,

		/// <remarks/>
		[XmlEnum("124")]
		Item124,

		/// <remarks/>
		[XmlEnum("125")]
		Item125,

		/// <remarks/>
		[XmlEnum("126")]
		Item126,

		/// <remarks/>
		[XmlEnum("127")]
		Item127,

		/// <remarks/>
		[XmlEnum("128")]
		Item128,

		/// <remarks/>
		[XmlEnum("129")]
		Item129,

		/// <remarks/>
		[XmlEnum("130")]
		Item130,

		/// <remarks/>
		[XmlEnum("131")]
		Item131,

		/// <remarks/>
		[XmlEnum("132")]
		Item132,

		/// <remarks/>
		[XmlEnum("133")]
		Item133,

		/// <remarks/>
		[XmlEnum("134")]
		Item134,

		/// <remarks/>
		[XmlEnum("135")]
		Item135,

		/// <remarks/>
		[XmlEnum("136")]
		Item136,

		/// <remarks/>
		[XmlEnum("137")]
		Item137,

		/// <remarks/>
		[XmlEnum("138")]
		Item138,

		/// <remarks/>
		[XmlEnum("139")]
		Item139,

		/// <remarks/>
		[XmlEnum("140")]
		Item140,

		/// <remarks/>
		[XmlEnum("141")]
		Item141,

		/// <remarks/>
		[XmlEnum("142")]
		Item142,

		/// <remarks/>
		[XmlEnum("143")]
		Item143,

		/// <remarks/>
		[XmlEnum("144")]
		Item144,

		/// <remarks/>
		[XmlEnum("145")]
		Item145,

		/// <remarks/>
		[XmlEnum("146")]
		Item146,

		/// <remarks/>
		[XmlEnum("147")]
		Item147,

		/// <remarks/>
		[XmlEnum("148")]
		Item148,

		/// <remarks/>
		[XmlEnum("149")]
		Item149,

		/// <remarks/>
		[XmlEnum("150")]
		Item150,

		/// <remarks/>
		[XmlEnum("151")]
		Item151,

		/// <remarks/>
		[XmlEnum("152")]
		Item152,

		/// <remarks/>
		[XmlEnum("153")]
		Item153,

		/// <remarks/>
		[XmlEnum("154")]
		Item154,

		/// <remarks/>
		[XmlEnum("155")]
		Item155,

		/// <remarks/>
		[XmlEnum("156")]
		Item156,

		/// <remarks/>
		[XmlEnum("157")]
		Item157,

		/// <remarks/>
		[XmlEnum("158")]
		Item158,

		/// <remarks/>
		[XmlEnum("159")]
		Item159,

		/// <remarks/>
		[XmlEnum("160")]
		Item160,

		/// <remarks/>
		[XmlEnum("161")]
		Item161,

		/// <remarks/>
		[XmlEnum("162")]
		Item162,

		/// <remarks/>
		[XmlEnum("163")]
		Item163,

		/// <remarks/>
		[XmlEnum("164")]
		Item164,

		/// <remarks/>
		[XmlEnum("165")]
		Item165,

		/// <remarks/>
		[XmlEnum("166")]
		Item166,

		/// <remarks/>
		[XmlEnum("167")]
		Item167,

		/// <remarks/>
		[XmlEnum("168")]
		Item168,

		/// <remarks/>
		[XmlEnum("169")]
		Item169,

		/// <remarks/>
		[XmlEnum("170")]
		Item170,

		/// <remarks/>
		[XmlEnum("171")]
		Item171,

		/// <remarks/>
		[XmlEnum("172")]
		Item172,

		/// <remarks/>
		[XmlEnum("173")]
		Item173,

		/// <remarks/>
		[XmlEnum("174")]
		Item174,

		/// <remarks/>
		[XmlEnum("175")]
		Item175,

		/// <remarks/>
		[XmlEnum("176")]
		Item176,

		/// <remarks/>
		[XmlEnum("177")]
		Item177,

		/// <remarks/>
		[XmlEnum("178")]
		Item178,

		/// <remarks/>
		[XmlEnum("179")]
		Item179,

		/// <remarks/>
		[XmlEnum("180")]
		Item180,

		/// <remarks/>
		[XmlEnum("181")]
		Item181,

		/// <remarks/>
		[XmlEnum("182")]
		Item182,

		/// <remarks/>
		[XmlEnum("183")]
		Item183,

		/// <remarks/>
		[XmlEnum("184")]
		Item184,

		/// <remarks/>
		[XmlEnum("185")]
		Item185,

		/// <remarks/>
		[XmlEnum("186")]
		Item186,

		/// <remarks/>
		[XmlEnum("187")]
		Item187,

		/// <remarks/>
		[XmlEnum("188")]
		Item188,

		/// <remarks/>
		[XmlEnum("189")]
		Item189,

		/// <remarks/>
		[XmlEnum("190")]
		Item190,

		/// <remarks/>
		[XmlEnum("191")]
		Item191,

		/// <remarks/>
		[XmlEnum("192")]
		Item192,

		/// <remarks/>
		[XmlEnum("193")]
		Item193,

		/// <remarks/>
		[XmlEnum("194")]
		Item194,

		/// <remarks/>
		[XmlEnum("195")]
		Item195,

		/// <remarks/>
		[XmlEnum("196")]
		Item196,

		/// <remarks/>
		[XmlEnum("197")]
		Item197,

		/// <remarks/>
		[XmlEnum("198")]
		Item198,

		/// <remarks/>
		[XmlEnum("199")]
		Item199,

		/// <remarks/>
		[XmlEnum("200")]
		Item200,

		/// <remarks/>
		[XmlEnum("201")]
		Item201,

		/// <remarks/>
		[XmlEnum("202")]
		Item202,

		/// <remarks/>
		[XmlEnum("203")]
		Item203,

		/// <remarks/>
		[XmlEnum("204")]
		Item204,

		/// <remarks/>
		[XmlEnum("205")]
		Item205,

		/// <remarks/>
		[XmlEnum("206")]
		Item206,

		/// <remarks/>
		[XmlEnum("207")]
		Item207,

		/// <remarks/>
		[XmlEnum("208")]
		Item208,

		/// <remarks/>
		[XmlEnum("209")]
		Item209,

		/// <remarks/>
		[XmlEnum("210")]
		Item210,

		/// <remarks/>
		[XmlEnum("211")]
		Item211,

		/// <remarks/>
		[XmlEnum("212")]
		Item212,

		/// <remarks/>
		[XmlEnum("213")]
		Item213,

		/// <remarks/>
		[XmlEnum("214")]
		Item214,

		/// <remarks/>
		[XmlEnum("215")]
		Item215,

		/// <remarks/>
		[XmlEnum("216")]
		Item216,

		/// <remarks/>
		[XmlEnum("217")]
		Item217,

		/// <remarks/>
		[XmlEnum("218")]
		Item218,

		/// <remarks/>
		[XmlEnum("219")]
		Item219,

		/// <remarks/>
		[XmlEnum("220")]
		Item220,

		/// <remarks/>
		[XmlEnum("221")]
		Item221,

		/// <remarks/>
		[XmlEnum("222")]
		Item222,

		/// <remarks/>
		[XmlEnum("223")]
		Item223,

		/// <remarks/>
		[XmlEnum("224")]
		Item224,

		/// <remarks/>
		[XmlEnum("225")]
		Item225,

		/// <remarks/>
		[XmlEnum("226")]
		Item226,

		/// <remarks/>
		[XmlEnum("227")]
		Item227,

		/// <remarks/>
		[XmlEnum("228")]
		Item228,

		/// <remarks/>
		[XmlEnum("229")]
		Item229,

		/// <remarks/>
		[XmlEnum("230")]
		Item230,

		/// <remarks/>
		[XmlEnum("231")]
		Item231,

		/// <remarks/>
		[XmlEnum("232")]
		Item232,

		/// <remarks/>
		[XmlEnum("233")]
		Item233,

		/// <remarks/>
		[XmlEnum("234")]
		Item234,

		/// <remarks/>
		[XmlEnum("235")]
		Item235,

		/// <remarks/>
		[XmlEnum("236")]
		Item236,

		/// <remarks/>
		[XmlEnum("237")]
		Item237,

		/// <remarks/>
		[XmlEnum("238")]
		Item238,

		/// <remarks/>
		[XmlEnum("239")]
		Item239,

		/// <remarks/>
		[XmlEnum("240")]
		Item240,

		/// <remarks/>
		[XmlEnum("241")]
		Item241,

		/// <remarks/>
		[XmlEnum("242")]
		Item242,

		/// <remarks/>
		[XmlEnum("243")]
		Item243,

		/// <remarks/>
		[XmlEnum("244")]
		Item244,

		/// <remarks/>
		[XmlEnum("245")]
		Item245,

		/// <remarks/>
		[XmlEnum("246")]
		Item246,

		/// <remarks/>
		[XmlEnum("247")]
		Item247,

		/// <remarks/>
		[XmlEnum("248")]
		Item248,

		/// <remarks/>
		[XmlEnum("249")]
		Item249,

		/// <remarks/>
		[XmlEnum("250")]
		Item250,

		/// <remarks/>
		[XmlEnum("251")]
		Item251,

		/// <remarks/>
		[XmlEnum("252")]
		Item252,

		/// <remarks/>
		[XmlEnum("253")]
		Item253,

		/// <remarks/>
		[XmlEnum("254")]
		Item254,

		/// <remarks/>
		[XmlEnum("255")]
		Item255,

		/// <remarks/>
		[XmlEnum("256")]
		Item256,

		/// <remarks/>
		[XmlEnum("257")]
		Item257,

		/// <remarks/>
		[XmlEnum("258")]
		Item258,

		/// <remarks/>
		[XmlEnum("259")]
		Item259,

		/// <remarks/>
		[XmlEnum("260")]
		Item260,

		/// <remarks/>
		[XmlEnum("261")]
		Item261,

		/// <remarks/>
		[XmlEnum("262")]
		Item262,

		/// <remarks/>
		[XmlEnum("263")]
		Item263,

		/// <remarks/>
		[XmlEnum("264")]
		Item264,

		/// <remarks/>
		[XmlEnum("265")]
		Item265,

		/// <remarks/>
		[XmlEnum("266")]
		Item266,

		/// <remarks/>
		[XmlEnum("267")]
		Item267,

		/// <remarks/>
		[XmlEnum("268")]
		Item268,

		/// <remarks/>
		[XmlEnum("269")]
		Item269,

		/// <remarks/>
		[XmlEnum("270")]
		Item270,

		/// <remarks/>
		[XmlEnum("271")]
		Item271,

		/// <remarks/>
		[XmlEnum("272")]
		Item272,

		/// <remarks/>
		[XmlEnum("273")]
		Item273,

		/// <remarks/>
		[XmlEnum("274")]
		Item274,

		/// <remarks/>
		[XmlEnum("275")]
		Item275,

		/// <remarks/>
		[XmlEnum("276")]
		Item276,

		/// <remarks/>
		[XmlEnum("277")]
		Item277,

		/// <remarks/>
		[XmlEnum("278")]
		Item278,

		/// <remarks/>
		[XmlEnum("279")]
		Item279,

		/// <remarks/>
		[XmlEnum("280")]
		Item280,

		/// <remarks/>
		[XmlEnum("281")]
		Item281,

		/// <remarks/>
		[XmlEnum("282")]
		Item282,

		/// <remarks/>
		[XmlEnum("283")]
		Item283,

		/// <remarks/>
		[XmlEnum("284")]
		Item284,

		/// <remarks/>
		[XmlEnum("285")]
		Item285,

		/// <remarks/>
		[XmlEnum("286")]
		Item286,

		/// <remarks/>
		[XmlEnum("287")]
		Item287,

		/// <remarks/>
		[XmlEnum("288")]
		Item288,

		/// <remarks/>
		[XmlEnum("289")]
		Item289,

		/// <remarks/>
		[XmlEnum("290")]
		Item290,

		/// <remarks/>
		[XmlEnum("291")]
		Item291,

		/// <remarks/>
		[XmlEnum("292")]
		Item292,

		/// <remarks/>
		[XmlEnum("293")]
		Item293,

		/// <remarks/>
		[XmlEnum("294")]
		Item294,

		/// <remarks/>
		[XmlEnum("295")]
		Item295,

		/// <remarks/>
		[XmlEnum("296")]
		Item296,

		/// <remarks/>
		[XmlEnum("297")]
		Item297,

		/// <remarks/>
		[XmlEnum("298")]
		Item298,

		/// <remarks/>
		[XmlEnum("299")]
		Item299,

		/// <remarks/>
		[XmlEnum("300")]
		Item300,

		/// <remarks/>
		[XmlEnum("301")]
		Item301,

		/// <remarks/>
		[XmlEnum("302")]
		Item302,

		/// <remarks/>
		[XmlEnum("303")]
		Item303,

		/// <remarks/>
		[XmlEnum("304")]
		Item304,

		/// <remarks/>
		[XmlEnum("305")]
		Item305,

		/// <remarks/>
		[XmlEnum("306")]
		Item306,

		/// <remarks/>
		[XmlEnum("307")]
		Item307,

		/// <remarks/>
		[XmlEnum("308")]
		Item308,

		/// <remarks/>
		[XmlEnum("309")]
		Item309,

		/// <remarks/>
		[XmlEnum("310")]
		Item310,

		/// <remarks/>
		[XmlEnum("311")]
		Item311,

		/// <remarks/>
		[XmlEnum("312")]
		Item312,

		/// <remarks/>
		[XmlEnum("313")]
		Item313,

		/// <remarks/>
		[XmlEnum("314")]
		Item314,

		/// <remarks/>
		[XmlEnum("315")]
		Item315,

		/// <remarks/>
		[XmlEnum("316")]
		Item316,

		/// <remarks/>
		[XmlEnum("317")]
		Item317,

		/// <remarks/>
		[XmlEnum("318")]
		Item318,

		/// <remarks/>
		[XmlEnum("319")]
		Item319,

		/// <remarks/>
		[XmlEnum("320")]
		Item320,

		/// <remarks/>
		[XmlEnum("321")]
		Item321,

		/// <remarks/>
		[XmlEnum("322")]
		Item322,

		/// <remarks/>
		[XmlEnum("323")]
		Item323,

		/// <remarks/>
		[XmlEnum("324")]
		Item324,

		/// <remarks/>
		[XmlEnum("325")]
		Item325,

		/// <remarks/>
		[XmlEnum("326")]
		Item326,

		/// <remarks/>
		[XmlEnum("327")]
		Item327,

		/// <remarks/>
		[XmlEnum("328")]
		Item328,

		/// <remarks/>
		[XmlEnum("329")]
		Item329,

		/// <remarks/>
		[XmlEnum("330")]
		Item330,

		/// <remarks/>
		[XmlEnum("331")]
		Item331,

		/// <remarks/>
		[XmlEnum("332")]
		Item332,

		/// <remarks/>
		[XmlEnum("333")]
		Item333,

		/// <remarks/>
		[XmlEnum("334")]
		Item334,

		/// <remarks/>
		[XmlEnum("335")]
		Item335,

		/// <remarks/>
		[XmlEnum("336")]
		Item336,

		/// <remarks/>
		[XmlEnum("337")]
		Item337,

		/// <remarks/>
		[XmlEnum("338")]
		Item338,

		/// <remarks/>
		[XmlEnum("339")]
		Item339,

		/// <remarks/>
		[XmlEnum("340")]
		Item340,

		/// <remarks/>
		[XmlEnum("341")]
		Item341,

		/// <remarks/>
		[XmlEnum("342")]
		Item342,

		/// <remarks/>
		[XmlEnum("343")]
		Item343,

		/// <remarks/>
		[XmlEnum("344")]
		Item344,

		/// <remarks/>
		[XmlEnum("345")]
		Item345,

		/// <remarks/>
		[XmlEnum("346")]
		Item346,

		/// <remarks/>
		[XmlEnum("347")]
		Item347,

		/// <remarks/>
		[XmlEnum("348")]
		Item348,

		/// <remarks/>
		[XmlEnum("349")]
		Item349,

		/// <remarks/>
		[XmlEnum("350")]
		Item350,

		/// <remarks/>
		[XmlEnum("351")]
		Item351,

		/// <remarks/>
		[XmlEnum("352")]
		Item352,

		/// <remarks/>
		[XmlEnum("353")]
		Item353,

		/// <remarks/>
		[XmlEnum("354")]
		Item354,

		/// <remarks/>
		[XmlEnum("355")]
		Item355,

		/// <remarks/>
		[XmlEnum("356")]
		Item356,

		/// <remarks/>
		[XmlEnum("357")]
		Item357,

		/// <remarks/>
		[XmlEnum("358")]
		Item358,

		/// <remarks/>
		[XmlEnum("359")]
		Item359,

		/// <remarks/>
		[XmlEnum("360")]
		Item360,

		/// <remarks/>
		[XmlEnum("361")]
		Item361,

		/// <remarks/>
		[XmlEnum("362")]
		Item362,

		/// <remarks/>
		[XmlEnum("363")]
		Item363,

		/// <remarks/>
		[XmlEnum("364")]
		Item364,

		/// <remarks/>
		[XmlEnum("365")]
		Item365,

		/// <remarks/>
		[XmlEnum("366")]
		Item366,

		/// <remarks/>
		[XmlEnum("367")]
		Item367,

		/// <remarks/>
		[XmlEnum("368")]
		Item368,

		/// <remarks/>
		[XmlEnum("369")]
		Item369,

		/// <remarks/>
		[XmlEnum("370")]
		Item370,

		/// <remarks/>
		[XmlEnum("371")]
		Item371,

		/// <remarks/>
		[XmlEnum("372")]
		Item372,

		/// <remarks/>
		[XmlEnum("373")]
		Item373,

		/// <remarks/>
		[XmlEnum("374")]
		Item374,

		/// <remarks/>
		[XmlEnum("375")]
		Item375,

		/// <remarks/>
		[XmlEnum("376")]
		Item376,

		/// <remarks/>
		[XmlEnum("377")]
		Item377,

		/// <remarks/>
		[XmlEnum("378")]
		Item378,

		/// <remarks/>
		[XmlEnum("379")]
		Item379,

		/// <remarks/>
		[XmlEnum("380")]
		Item380,

		/// <remarks/>
		[XmlEnum("381")]
		Item381,

		/// <remarks/>
		[XmlEnum("382")]
		Item382,

		/// <remarks/>
		[XmlEnum("383")]
		Item383,

		/// <remarks/>
		[XmlEnum("384")]
		Item384,

		/// <remarks/>
		[XmlEnum("385")]
		Item385,

		/// <remarks/>
		[XmlEnum("386")]
		Item386,

		/// <remarks/>
		[XmlEnum("387")]
		Item387,

		/// <remarks/>
		[XmlEnum("388")]
		Item388,

		/// <remarks/>
		[XmlEnum("389")]
		Item389,

		/// <remarks/>
		[XmlEnum("390")]
		Item390,

		/// <remarks/>
		[XmlEnum("391")]
		Item391,

		/// <remarks/>
		[XmlEnum("392")]
		Item392,

		/// <remarks/>
		[XmlEnum("393")]
		Item393,

		/// <remarks/>
		[XmlEnum("394")]
		Item394,

		/// <remarks/>
		[XmlEnum("395")]
		Item395,

		/// <remarks/>
		[XmlEnum("396")]
		Item396,

		/// <remarks/>
		[XmlEnum("397")]
		Item397,

		/// <remarks/>
		[XmlEnum("398")]
		Item398,

		/// <remarks/>
		[XmlEnum("399")]
		Item399,

		/// <remarks/>
		[XmlEnum("400")]
		Item400,

		/// <remarks/>
		[XmlEnum("401")]
		Item401,

		/// <remarks/>
		[XmlEnum("402")]
		Item402,

		/// <remarks/>
		[XmlEnum("403")]
		Item403,

		/// <remarks/>
		[XmlEnum("404")]
		Item404,

		/// <remarks/>
		[XmlEnum("405")]
		Item405,

		/// <remarks/>
		[XmlEnum("406")]
		Item406,

		/// <remarks/>
		[XmlEnum("407")]
		Item407,

		/// <remarks/>
		[XmlEnum("408")]
		Item408,

		/// <remarks/>
		[XmlEnum("409")]
		Item409,

		/// <remarks/>
		[XmlEnum("410")]
		Item410,

		/// <remarks/>
		[XmlEnum("411")]
		Item411,

		/// <remarks/>
		[XmlEnum("412")]
		Item412,

		/// <remarks/>
		[XmlEnum("413")]
		Item413,

		/// <remarks/>
		[XmlEnum("414")]
		Item414,

		/// <remarks/>
		[XmlEnum("415")]
		Item415,

		/// <remarks/>
		[XmlEnum("416")]
		Item416,

		/// <remarks/>
		[XmlEnum("417")]
		Item417,

		/// <remarks/>
		[XmlEnum("418")]
		Item418,

		/// <remarks/>
		[XmlEnum("419")]
		Item419,

		/// <remarks/>
		[XmlEnum("420")]
		Item420,

		/// <remarks/>
		[XmlEnum("421")]
		Item421,

		/// <remarks/>
		[XmlEnum("422")]
		Item422,

		/// <remarks/>
		[XmlEnum("423")]
		Item423,

		/// <remarks/>
		[XmlEnum("424")]
		Item424,

		/// <remarks/>
		[XmlEnum("425")]
		Item425,

		/// <remarks/>
		[XmlEnum("426")]
		Item426,

		/// <remarks/>
		[XmlEnum("427")]
		Item427,

		/// <remarks/>
		[XmlEnum("428")]
		Item428,

		/// <remarks/>
		[XmlEnum("429")]
		Item429,

		/// <remarks/>
		[XmlEnum("430")]
		Item430,

		/// <remarks/>
		[XmlEnum("431")]
		Item431,

		/// <remarks/>
		[XmlEnum("432")]
		Item432,

		/// <remarks/>
		[XmlEnum("433")]
		Item433,

		/// <remarks/>
		[XmlEnum("434")]
		Item434,

		/// <remarks/>
		[XmlEnum("435")]
		Item435,

		/// <remarks/>
		[XmlEnum("436")]
		Item436,

		/// <remarks/>
		[XmlEnum("437")]
		Item437,

		/// <remarks/>
		[XmlEnum("438")]
		Item438,

		/// <remarks/>
		[XmlEnum("439")]
		Item439,

		/// <remarks/>
		[XmlEnum("440")]
		Item440,

		/// <remarks/>
		[XmlEnum("441")]
		Item441,

		/// <remarks/>
		[XmlEnum("442")]
		Item442,

		/// <remarks/>
		[XmlEnum("443")]
		Item443,

		/// <remarks/>
		[XmlEnum("444")]
		Item444,

		/// <remarks/>
		[XmlEnum("445")]
		Item445,

		/// <remarks/>
		[XmlEnum("446")]
		Item446,

		/// <remarks/>
		[XmlEnum("447")]
		Item447,

		/// <remarks/>
		[XmlEnum("448")]
		Item448,

		/// <remarks/>
		[XmlEnum("449")]
		Item449,

		/// <remarks/>
		[XmlEnum("450")]
		Item450,

		/// <remarks/>
		[XmlEnum("451")]
		Item451,

		/// <remarks/>
		[XmlEnum("452")]
		Item452,

		/// <remarks/>
		[XmlEnum("453")]
		Item453,

		/// <remarks/>
		[XmlEnum("454")]
		Item454,

		/// <remarks/>
		[XmlEnum("455")]
		Item455,

		/// <remarks/>
		[XmlEnum("456")]
		Item456,

		/// <remarks/>
		[XmlEnum("457")]
		Item457,

		/// <remarks/>
		[XmlEnum("458")]
		Item458,

		/// <remarks/>
		[XmlEnum("459")]
		Item459,

		/// <remarks/>
		[XmlEnum("460")]
		Item460,

		/// <remarks/>
		[XmlEnum("461")]
		Item461,

		/// <remarks/>
		[XmlEnum("462")]
		Item462,

		/// <remarks/>
		[XmlEnum("463")]
		Item463,

		/// <remarks/>
		[XmlEnum("464")]
		Item464,

		/// <remarks/>
		[XmlEnum("465")]
		Item465,

		/// <remarks/>
		[XmlEnum("466")]
		Item466,

		/// <remarks/>
		[XmlEnum("467")]
		Item467,

		/// <remarks/>
		[XmlEnum("468")]
		Item468,

		/// <remarks/>
		[XmlEnum("469")]
		Item469,

		/// <remarks/>
		[XmlEnum("470")]
		Item470,

		/// <remarks/>
		[XmlEnum("481")]
		Item481,

		/// <remarks/>
		[XmlEnum("482")]
		Item482,

		/// <remarks/>
		[XmlEnum("483")]
		Item483,

		/// <remarks/>
		[XmlEnum("484")]
		Item484,

		/// <remarks/>
		[XmlEnum("485")]
		Item485,

		/// <remarks/>
		[XmlEnum("486")]
		Item486,

		/// <remarks/>
		[XmlEnum("487")]
		Item487,

		/// <remarks/>
		[XmlEnum("488")]
		Item488,

		/// <remarks/>
		[XmlEnum("489")]
		Item489,

		/// <remarks/>
		[XmlEnum("490")]
		Item490,

		/// <remarks/>
		[XmlEnum("491")]
		Item491,

		/// <remarks/>
		[XmlEnum("493")]
		Item493,

		/// <remarks/>
		[XmlEnum("494")]
		Item494,

		/// <remarks/>
		[XmlEnum("495")]
		Item495,

		/// <remarks/>
		[XmlEnum("496")]
		Item496,

		/// <remarks/>
		[XmlEnum("497")]
		Item497,

		/// <remarks/>
		[XmlEnum("498")]
		Item498,

		/// <remarks/>
		[XmlEnum("499")]
		Item499,

		/// <remarks/>
		[XmlEnum("520")]
		Item520,

		/// <remarks/>
		[XmlEnum("521")]
		Item521,

		/// <remarks/>
		[XmlEnum("522")]
		Item522,

		/// <remarks/>
		[XmlEnum("523")]
		Item523,

		/// <remarks/>
		[XmlEnum("524")]
		Item524,

		/// <remarks/>
		[XmlEnum("525")]
		Item525,

		/// <remarks/>
		[XmlEnum("526")]
		Item526,

		/// <remarks/>
		[XmlEnum("527")]
		Item527,

		/// <remarks/>
		[XmlEnum("528")]
		Item528,

		/// <remarks/>
		[XmlEnum("529")]
		Item529,

		/// <remarks/>
		[XmlEnum("530")]
		Item530,

		/// <remarks/>
		[XmlEnum("531")]
		Item531,

		/// <remarks/>
		[XmlEnum("532")]
		Item532,

		/// <remarks/>
		[XmlEnum("533")]
		Item533,

		/// <remarks/>
		[XmlEnum("534")]
		Item534,

		/// <remarks/>
		[XmlEnum("535")]
		Item535,

		/// <remarks/>
		[XmlEnum("536")]
		Item536,

		/// <remarks/>
		[XmlEnum("537")]
		Item537,

		/// <remarks/>
		[XmlEnum("538")]
		Item538,

		/// <remarks/>
		[XmlEnum("550")]
		Item550,

		/// <remarks/>
		[XmlEnum("552")]
		Item552,

		/// <remarks/>
		[XmlEnum("553")]
		Item553,

		/// <remarks/>
		[XmlEnum("575")]
		Item575,

		/// <remarks/>
		[XmlEnum("580")]
		Item580,

		/// <remarks/>
		[XmlEnum("610")]
		Item610,

		/// <remarks/>
		[XmlEnum("621")]
		Item621,

		/// <remarks/>
		[XmlEnum("622")]
		Item622,

		/// <remarks/>
		[XmlEnum("623")]
		Item623,

		/// <remarks/>
		[XmlEnum("624")]
		Item624,

		/// <remarks/>
		[XmlEnum("630")]
		Item630,

		/// <remarks/>
		[XmlEnum("631")]
		Item631,

		/// <remarks/>
		[XmlEnum("632")]
		Item632,

		/// <remarks/>
		[XmlEnum("633")]
		Item633,

		/// <remarks/>
		[XmlEnum("635")]
		Item635,

		/// <remarks/>
		[XmlEnum("640")]
		Item640,

		/// <remarks/>
		[XmlEnum("650")]
		Item650,

		/// <remarks/>
		[XmlEnum("655")]
		Item655,

		/// <remarks/>
		[XmlEnum("700")]
		Item700,

		/// <remarks/>
		[XmlEnum("701")]
		Item701,

		/// <remarks/>
		[XmlEnum("702")]
		Item702,

		/// <remarks/>
		[XmlEnum("703")]
		Item703,

		/// <remarks/>
		[XmlEnum("704")]
		Item704,

		/// <remarks/>
		[XmlEnum("705")]
		Item705,

		/// <remarks/>
		[XmlEnum("706")]
		Item706,

		/// <remarks/>
		[XmlEnum("707")]
		Item707,

		/// <remarks/>
		[XmlEnum("708")]
		Item708,

		/// <remarks/>
		[XmlEnum("709")]
		Item709,

		/// <remarks/>
		[XmlEnum("710")]
		Item710,

		/// <remarks/>
		[XmlEnum("711")]
		Item711,

		/// <remarks/>
		[XmlEnum("712")]
		Item712,

		/// <remarks/>
		[XmlEnum("713")]
		Item713,

		/// <remarks/>
		[XmlEnum("714")]
		Item714,

		/// <remarks/>
		[XmlEnum("715")]
		Item715,

		/// <remarks/>
		[XmlEnum("716")]
		Item716,

		/// <remarks/>
		[XmlEnum("720")]
		Item720,

		/// <remarks/>
		[XmlEnum("722")]
		Item722,

		/// <remarks/>
		[XmlEnum("723")]
		Item723,

		/// <remarks/>
		[XmlEnum("724")]
		Item724,

		/// <remarks/>
		[XmlEnum("730")]
		Item730,

		/// <remarks/>
		[XmlEnum("740")]
		Item740,

		/// <remarks/>
		[XmlEnum("741")]
		Item741,

		/// <remarks/>
		[XmlEnum("743")]
		Item743,

		/// <remarks/>
		[XmlEnum("744")]
		Item744,

		/// <remarks/>
		[XmlEnum("745")]
		Item745,

		/// <remarks/>
		[XmlEnum("746")]
		Item746,

		/// <remarks/>
		[XmlEnum("750")]
		Item750,

		/// <remarks/>
		[XmlEnum("760")]
		Item760,

		/// <remarks/>
		[XmlEnum("761")]
		Item761,

		/// <remarks/>
		[XmlEnum("763")]
		Item763,

		/// <remarks/>
		[XmlEnum("764")]
		Item764,

		/// <remarks/>
		[XmlEnum("765")]
		Item765,

		/// <remarks/>
		[XmlEnum("766")]
		Item766,

		/// <remarks/>
		[XmlEnum("770")]
		Item770,

		/// <remarks/>
		[XmlEnum("775")]
		Item775,

		/// <remarks/>
		[XmlEnum("780")]
		Item780,

		/// <remarks/>
		[XmlEnum("781")]
		Item781,

		/// <remarks/>
		[XmlEnum("782")]
		Item782,

		/// <remarks/>
		[XmlEnum("783")]
		Item783,

		/// <remarks/>
		[XmlEnum("784")]
		Item784,

		/// <remarks/>
		[XmlEnum("785")]
		Item785,

		/// <remarks/>
		[XmlEnum("786")]
		Item786,

		/// <remarks/>
		[XmlEnum("787")]
		Item787,

		/// <remarks/>
		[XmlEnum("788")]
		Item788,

		/// <remarks/>
		[XmlEnum("789")]
		Item789,

		/// <remarks/>
		[XmlEnum("790")]
		Item790,

		/// <remarks/>
		[XmlEnum("791")]
		Item791,

		/// <remarks/>
		[XmlEnum("792")]
		Item792,

		/// <remarks/>
		[XmlEnum("793")]
		Item793,

		/// <remarks/>
		[XmlEnum("794")]
		Item794,

		/// <remarks/>
		[XmlEnum("795")]
		Item795,

		/// <remarks/>
		[XmlEnum("796")]
		Item796,

		/// <remarks/>
		[XmlEnum("797")]
		Item797,

		/// <remarks/>
		[XmlEnum("798")]
		Item798,

		/// <remarks/>
		[XmlEnum("799")]
		Item799,

		/// <remarks/>
		[XmlEnum("810")]
		Item810,

		/// <remarks/>
		[XmlEnum("811")]
		Item811,

		/// <remarks/>
		[XmlEnum("812")]
		Item812,

		/// <remarks/>
		[XmlEnum("820")]
		Item820,

		/// <remarks/>
		[XmlEnum("821")]
		Item821,

		/// <remarks/>
		[XmlEnum("822")]
		Item822,

		/// <remarks/>
		[XmlEnum("823")]
		Item823,

		/// <remarks/>
		[XmlEnum("824")]
		Item824,

		/// <remarks/>
		[XmlEnum("825")]
		Item825,

		/// <remarks/>
		[XmlEnum("830")]
		Item830,

		/// <remarks/>
		[XmlEnum("833")]
		Item833,

		/// <remarks/>
		[XmlEnum("840")]
		Item840,

		/// <remarks/>
		[XmlEnum("841")]
		Item841,

		/// <remarks/>
		[XmlEnum("850")]
		Item850,

		/// <remarks/>
		[XmlEnum("851")]
		Item851,

		/// <remarks/>
		[XmlEnum("852")]
		Item852,

		/// <remarks/>
		[XmlEnum("853")]
		Item853,

		/// <remarks/>
		[XmlEnum("855")]
		Item855,

		/// <remarks/>
		[XmlEnum("856")]
		Item856,

		/// <remarks/>
		[XmlEnum("860")]
		Item860,

		/// <remarks/>
		[XmlEnum("861")]
		Item861,

		/// <remarks/>
		[XmlEnum("862")]
		Item862,

		/// <remarks/>
		[XmlEnum("863")]
		Item863,

		/// <remarks/>
		[XmlEnum("864")]
		Item864,

		/// <remarks/>
		[XmlEnum("865")]
		Item865,

		/// <remarks/>
		[XmlEnum("870")]
		Item870,

		/// <remarks/>
		[XmlEnum("890")]
		Item890,

		/// <remarks/>
		[XmlEnum("895")]
		Item895,

		/// <remarks/>
		[XmlEnum("896")]
		Item896,

		/// <remarks/>
		[XmlEnum("901")]
		Item901,

		/// <remarks/>
		[XmlEnum("910")]
		Item910,

		/// <remarks/>
		[XmlEnum("911")]
		Item911,

		/// <remarks/>
		[XmlEnum("913")]
		Item913,

		/// <remarks/>
		[XmlEnum("914")]
		Item914,

		/// <remarks/>
		[XmlEnum("915")]
		Item915,

		/// <remarks/>
		[XmlEnum("916")]
		Item916,

		/// <remarks/>
		[XmlEnum("917")]
		Item917,

		/// <remarks/>
		[XmlEnum("925")]
		Item925,

		/// <remarks/>
		[XmlEnum("926")]
		Item926,

		/// <remarks/>
		[XmlEnum("927")]
		Item927,

		/// <remarks/>
		[XmlEnum("929")]
		Item929,

		/// <remarks/>
		[XmlEnum("930")]
		Item930,

		/// <remarks/>
		[XmlEnum("931")]
		Item931,

		/// <remarks/>
		[XmlEnum("932")]
		Item932,

		/// <remarks/>
		[XmlEnum("933")]
		Item933,

		/// <remarks/>
		[XmlEnum("934")]
		Item934,

		/// <remarks/>
		[XmlEnum("935")]
		Item935,

		/// <remarks/>
		[XmlEnum("936")]
		Item936,

		/// <remarks/>
		[XmlEnum("937")]
		Item937,

		/// <remarks/>
		[XmlEnum("938")]
		Item938,

		/// <remarks/>
		[XmlEnum("940")]
		Item940,

		/// <remarks/>
		[XmlEnum("941")]
		Item941,

		/// <remarks/>
		[XmlEnum("950")]
		Item950,

		/// <remarks/>
		[XmlEnum("951")]
		Item951,

		/// <remarks/>
		[XmlEnum("952")]
		Item952,

		/// <remarks/>
		[XmlEnum("953")]
		Item953,

		/// <remarks/>
		[XmlEnum("954")]
		Item954,

		/// <remarks/>
		[XmlEnum("955")]
		Item955,

		/// <remarks/>
		[XmlEnum("960")]
		Item960,

		/// <remarks/>
		[XmlEnum("961")]
		Item961,

		/// <remarks/>
		[XmlEnum("962")]
		Item962,

		/// <remarks/>
		[XmlEnum("963")]
		Item963,

		/// <remarks/>
		[XmlEnum("964")]
		Item964,

		/// <remarks/>
		[XmlEnum("965")]
		Item965,

		/// <remarks/>
		[XmlEnum("966")]
		Item966,

		/// <remarks/>
		[XmlEnum("970")]
		Item970,

		/// <remarks/>
		[XmlEnum("971")]
		Item971,

		/// <remarks/>
		[XmlEnum("972")]
		Item972,

		/// <remarks/>
		[XmlEnum("974")]
		Item974,

		/// <remarks/>
		[XmlEnum("975")]
		Item975,

		/// <remarks/>
		[XmlEnum("976")]
		Item976,

		/// <remarks/>
		[XmlEnum("977")]
		Item977,

		/// <remarks/>
		[XmlEnum("978")]
		Item978,

		/// <remarks/>
		[XmlEnum("979")]
		Item979,

		/// <remarks/>
		[XmlEnum("990")]
		Item990,

		/// <remarks/>
		[XmlEnum("991")]
		Item991,

		/// <remarks/>
		[XmlEnum("995")]
		Item995,

		/// <remarks/>
		[XmlEnum("996")]
		Item996,

		/// <remarks/>
		[XmlEnum("998")]
		Item998,
	}
}