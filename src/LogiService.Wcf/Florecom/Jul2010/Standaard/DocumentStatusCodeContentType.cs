﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:DocumentStatusCode:D09A")]
	public enum DocumentStatusCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		[XmlEnum("9")]
		Item9,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("12")]
		Item12,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("16")]
		Item16,

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("18")]
		Item18,

		/// <remarks/>
		[XmlEnum("19")]
		Item19,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("26")]
		Item26,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,
	}
}