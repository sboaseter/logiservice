﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:EventTimeReferenceCodePaymentTermsEvent:D07A")]
	public enum EventTimeReferenceCodePaymentTermsEventContentType
	{

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("45")]
		Item45,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,
	}
}