﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:standard:6:Recommendation20:4")]
	public enum FEC_MeasurementUnitCommonCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("2Y")]
		Item2Y,

		/// <remarks/>
		[XmlEnum("2A")]
		Item2A,

		/// <remarks/>
		[XmlEnum("2B")]
		Item2B,

		/// <remarks/>
		[XmlEnum("2P")]
		Item2P,

		/// <remarks/>
		[XmlEnum("2R")]
		Item2R,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("4L")]
		Item4L,

		/// <remarks/>
		[XmlEnum("4W")]
		Item4W,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		A28,

		/// <remarks/>
		A29,

		/// <remarks/>
		A30,

		/// <remarks/>
		A33,

		/// <remarks/>
		A34,

		/// <remarks/>
		A35,

		/// <remarks/>
		A36,

		/// <remarks/>
		B21,

		/// <remarks/>
		B24,

		/// <remarks/>
		B56,

		/// <remarks/>
		B59,

		/// <remarks/>
		B63,

		/// <remarks/>
		BHP,

		/// <remarks/>
		C17,

		/// <remarks/>
		C35,

		/// <remarks/>
		C38,

		/// <remarks/>
		C94,

		/// <remarks/>
		CMQ,

		/// <remarks/>
		D32,

		/// <remarks/>
		D42,

		/// <remarks/>
		D48,

		/// <remarks/>
		D60,

		/// <remarks/>
		DD,

		/// <remarks/>
		DMQ,

		/// <remarks/>
		E33,

		/// <remarks/>
		FAR,

		/// <remarks/>
		G2,

		/// <remarks/>
		GE,

		/// <remarks/>
		GWH,

		/// <remarks/>
		HGM,

		/// <remarks/>
		HLT,

		/// <remarks/>
		IA,

		/// <remarks/>
		KI,

		/// <remarks/>
		KJ,

		/// <remarks/>
		KMK,

		/// <remarks/>
		KR,

		/// <remarks/>
		KVR,

		/// <remarks/>
		KWH,

		/// <remarks/>
		LA,

		/// <remarks/>
		M9,

		/// <remarks/>
		MMQ,

		/// <remarks/>
		MON,

		/// <remarks/>
		MWH,

		/// <remarks/>
		PO,

		/// <remarks/>
		RPM,

		/// <remarks/>
		RPS,

		/// <remarks/>
		TAH,

		/// <remarks/>
		TQD,

		/// <remarks/>
		WEE,

		/// <remarks/>
		YDK,

		/// <remarks/>
		E45,

		/// <remarks/>
		E46,

		/// <remarks/>
		E47,

		/// <remarks/>
		E48,

		/// <remarks/>
		E49,

		/// <remarks/>
		E50,

		/// <remarks/>
		[XmlEnum("64")]
		Item64,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,

		/// <remarks/>
		[XmlEnum("76")]
		Item76,

		/// <remarks/>
		[XmlEnum("93")]
		Item93,

		/// <remarks/>
		A1,

		/// <remarks/>
		A25,

		/// <remarks/>
		A50,

		/// <remarks/>
		A51,

		/// <remarks/>
		A52,

		/// <remarks/>
		A57,

		/// <remarks/>
		A58,

		/// <remarks/>
		A60,

		/// <remarks/>
		A61,

		/// <remarks/>
		A62,

		/// <remarks/>
		A63,

		/// <remarks/>
		A64,

		/// <remarks/>
		A65,

		/// <remarks/>
		A66,

		/// <remarks/>
		A67,

		/// <remarks/>
		A77,

		/// <remarks/>
		A78,

		/// <remarks/>
		A79,

		/// <remarks/>
		A80,

		/// <remarks/>
		A81,

		/// <remarks/>
		A82,

		/// <remarks/>
		A83,

		/// <remarks/>
		ATT,

		/// <remarks/>
		B36,

		/// <remarks/>
		B39,

		/// <remarks/>
		B40,

		/// <remarks/>
		B65,

		/// <remarks/>
		D35,

		/// <remarks/>
		D37,

		/// <remarks/>
		D38,

		/// <remarks/>
		D39,

		/// <remarks/>
		D70,

		/// <remarks/>
		D71,

		/// <remarks/>
		D72,

		/// <remarks/>
		D75,

		/// <remarks/>
		D76,

		/// <remarks/>
		D9,

		/// <remarks/>
		DU,

		/// <remarks/>
		DX,

		/// <remarks/>
		E11,

		/// <remarks/>
		GRT,

		/// <remarks/>
		HJ,

		/// <remarks/>
		HN,

		/// <remarks/>
		HP,

		/// <remarks/>
		IF,

		/// <remarks/>
		NPR,

		/// <remarks/>
		NQ,

		/// <remarks/>
		NR,

		/// <remarks/>
		NTT,

		/// <remarks/>
		OZ,

		/// <remarks/>
		R4,

		/// <remarks/>
		SHT,

		/// <remarks/>
		UA,

		/// <remarks/>
		WW,

		/// <remarks/>
		B37,

		/// <remarks/>
		B38,

		/// <remarks/>
		B51,

		/// <remarks/>
		[XmlEnum("78")]
		Item78,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("1I")]
		Item1I,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("2C")]
		Item2C,

		/// <remarks/>
		[XmlEnum("2I")]
		Item2I,

		/// <remarks/>
		[XmlEnum("2J")]
		Item2J,

		/// <remarks/>
		[XmlEnum("2K")]
		Item2K,

		/// <remarks/>
		[XmlEnum("2L")]
		Item2L,

		/// <remarks/>
		[XmlEnum("2M")]
		Item2M,

		/// <remarks/>
		[XmlEnum("2N")]
		Item2N,

		/// <remarks/>
		[XmlEnum("2Q")]
		Item2Q,

		/// <remarks/>
		[XmlEnum("2U")]
		Item2U,

		/// <remarks/>
		[XmlEnum("2X")]
		Item2X,

		/// <remarks/>
		[XmlEnum("2Z")]
		Item2Z,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("3B")]
		Item3B,

		/// <remarks/>
		[XmlEnum("3C")]
		Item3C,

		/// <remarks/>
		[XmlEnum("40")]
		Item40,

		/// <remarks/>
		[XmlEnum("41")]
		Item41,

		/// <remarks/>
		[XmlEnum("4C")]
		Item4C,

		/// <remarks/>
		[XmlEnum("4G")]
		Item4G,

		/// <remarks/>
		[XmlEnum("4H")]
		Item4H,

		/// <remarks/>
		[XmlEnum("4K")]
		Item4K,

		/// <remarks/>
		[XmlEnum("4M")]
		Item4M,

		/// <remarks/>
		[XmlEnum("4N")]
		Item4N,

		/// <remarks/>
		[XmlEnum("4O")]
		Item4O,

		/// <remarks/>
		[XmlEnum("4P")]
		Item4P,

		/// <remarks/>
		[XmlEnum("4Q")]
		Item4Q,

		/// <remarks/>
		[XmlEnum("4R")]
		Item4R,

		/// <remarks/>
		[XmlEnum("4T")]
		Item4T,

		/// <remarks/>
		[XmlEnum("4U")]
		Item4U,

		/// <remarks/>
		[XmlEnum("4X")]
		Item4X,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("58")]
		Item58,

		/// <remarks/>
		[XmlEnum("59")]
		Item59,

		/// <remarks/>
		[XmlEnum("5A")]
		Item5A,

		/// <remarks/>
		[XmlEnum("5B")]
		Item5B,

		/// <remarks/>
		[XmlEnum("5E")]
		Item5E,

		/// <remarks/>
		[XmlEnum("5I")]
		Item5I,

		/// <remarks/>
		[XmlEnum("5J")]
		Item5J,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("61")]
		Item61,

		/// <remarks/>
		[XmlEnum("74")]
		Item74,

		/// <remarks/>
		[XmlEnum("77")]
		Item77,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("91")]
		Item91,

		/// <remarks/>
		A10,

		/// <remarks/>
		A11,

		/// <remarks/>
		A12,

		/// <remarks/>
		A13,

		/// <remarks/>
		A14,

		/// <remarks/>
		A15,

		/// <remarks/>
		A16,

		/// <remarks/>
		A17,

		/// <remarks/>
		A18,

		/// <remarks/>
		A19,

		/// <remarks/>
		A2,

		/// <remarks/>
		A20,

		/// <remarks/>
		A21,

		/// <remarks/>
		A22,

		/// <remarks/>
		A23,

		/// <remarks/>
		A24,

		/// <remarks/>
		A26,

		/// <remarks/>
		A27,

		/// <remarks/>
		A3,

		/// <remarks/>
		A31,

		/// <remarks/>
		A32,

		/// <remarks/>
		A37,

		/// <remarks/>
		A38,

		/// <remarks/>
		A39,

		/// <remarks/>
		A4,

		/// <remarks/>
		A40,

		/// <remarks/>
		A41,

		/// <remarks/>
		A42,

		/// <remarks/>
		A43,

		/// <remarks/>
		A44,

		/// <remarks/>
		A45,

		/// <remarks/>
		A47,

		/// <remarks/>
		A48,

		/// <remarks/>
		A49,

		/// <remarks/>
		A5,

		/// <remarks/>
		A53,

		/// <remarks/>
		A54,

		/// <remarks/>
		A55,

		/// <remarks/>
		A56,

		/// <remarks/>
		A59,

		/// <remarks/>
		A6,

		/// <remarks/>
		A68,

		/// <remarks/>
		A69,

		/// <remarks/>
		A7,

		/// <remarks/>
		A70,

		/// <remarks/>
		A71,

		/// <remarks/>
		A73,

		/// <remarks/>
		A74,

		/// <remarks/>
		A75,

		/// <remarks/>
		A76,

		/// <remarks/>
		A8,

		/// <remarks/>
		A84,

		/// <remarks/>
		A85,

		/// <remarks/>
		A86,

		/// <remarks/>
		A87,

		/// <remarks/>
		A88,

		/// <remarks/>
		A89,

		/// <remarks/>
		A9,

		/// <remarks/>
		A90,

		/// <remarks/>
		A91,

		/// <remarks/>
		A93,

		/// <remarks/>
		A94,

		/// <remarks/>
		A95,

		/// <remarks/>
		A96,

		/// <remarks/>
		A97,

		/// <remarks/>
		A98,

		/// <remarks/>
		A99,

		/// <remarks/>
		AA,

		/// <remarks/>
		AB,

		/// <remarks/>
		ACR,

		/// <remarks/>
		ACT,

		/// <remarks/>
		AD,

		/// <remarks/>
		AE,

		/// <remarks/>
		AH,

		/// <remarks/>
		AI,

		/// <remarks/>
		AK,

		/// <remarks/>
		AL,

		/// <remarks/>
		AMH,

		/// <remarks/>
		AMP,

		/// <remarks/>
		ANN,

		/// <remarks/>
		APZ,

		/// <remarks/>
		AQ,

		/// <remarks/>
		ARE,

		/// <remarks/>
		AS,

		/// <remarks/>
		ASM,

		/// <remarks/>
		ASU,

		/// <remarks/>
		ATM,

		/// <remarks/>
		AY,

		/// <remarks/>
		AZ,

		/// <remarks/>
		B1,

		/// <remarks/>
		B10,

		/// <remarks/>
		B11,

		/// <remarks/>
		B12,

		/// <remarks/>
		B13,

		/// <remarks/>
		B14,

		/// <remarks/>
		B15,

		/// <remarks/>
		B16,

		/// <remarks/>
		B17,

		/// <remarks/>
		B18,

		/// <remarks/>
		B19,

		/// <remarks/>
		B20,

		/// <remarks/>
		B22,

		/// <remarks/>
		B23,

		/// <remarks/>
		B25,

		/// <remarks/>
		B26,

		/// <remarks/>
		B27,

		/// <remarks/>
		B28,

		/// <remarks/>
		B29,

		/// <remarks/>
		B3,

		/// <remarks/>
		B30,

		/// <remarks/>
		B31,

		/// <remarks/>
		B32,

		/// <remarks/>
		B33,

		/// <remarks/>
		B34,

		/// <remarks/>
		B35,

		/// <remarks/>
		B4,

		/// <remarks/>
		B41,

		/// <remarks/>
		B42,

		/// <remarks/>
		B43,

		/// <remarks/>
		B44,

		/// <remarks/>
		B45,

		/// <remarks/>
		B46,

		/// <remarks/>
		B47,

		/// <remarks/>
		B48,

		/// <remarks/>
		B49,

		/// <remarks/>
		B50,

		/// <remarks/>
		B52,

		/// <remarks/>
		B53,

		/// <remarks/>
		B54,

		/// <remarks/>
		B55,

		/// <remarks/>
		B57,

		/// <remarks/>
		B58,

		/// <remarks/>
		B60,

		/// <remarks/>
		B61,

		/// <remarks/>
		B62,

		/// <remarks/>
		B64,

		/// <remarks/>
		B66,

		/// <remarks/>
		B67,

		/// <remarks/>
		B68,

		/// <remarks/>
		B69,

		/// <remarks/>
		B7,

		/// <remarks/>
		B70,

		/// <remarks/>
		B71,

		/// <remarks/>
		B72,

		/// <remarks/>
		B73,

		/// <remarks/>
		B74,

		/// <remarks/>
		B75,

		/// <remarks/>
		B76,

		/// <remarks/>
		B77,

		/// <remarks/>
		B78,

		/// <remarks/>
		B79,

		/// <remarks/>
		B8,

		/// <remarks/>
		B80,

		/// <remarks/>
		B81,

		/// <remarks/>
		B82,

		/// <remarks/>
		B83,

		/// <remarks/>
		B84,

		/// <remarks/>
		B85,

		/// <remarks/>
		B86,

		/// <remarks/>
		B87,

		/// <remarks/>
		B88,

		/// <remarks/>
		B89,

		/// <remarks/>
		B90,

		/// <remarks/>
		B91,

		/// <remarks/>
		B92,

		/// <remarks/>
		B93,

		/// <remarks/>
		B94,

		/// <remarks/>
		B95,

		/// <remarks/>
		B96,

		/// <remarks/>
		B97,

		/// <remarks/>
		B98,

		/// <remarks/>
		B99,

		/// <remarks/>
		BAR,

		/// <remarks/>
		BB,

		/// <remarks/>
		BFT,

		/// <remarks/>
		BIL,

		/// <remarks/>
		BLD,

		/// <remarks/>
		BLL,

		/// <remarks/>
		BP,

		/// <remarks/>
		BQL,

		/// <remarks/>
		BTU,

		/// <remarks/>
		BUA,

		/// <remarks/>
		BUI,

		/// <remarks/>
		C0,

		/// <remarks/>
		C10,

		/// <remarks/>
		C11,

		/// <remarks/>
		C12,

		/// <remarks/>
		C13,

		/// <remarks/>
		C14,

		/// <remarks/>
		C15,

		/// <remarks/>
		C16,

		/// <remarks/>
		C18,

		/// <remarks/>
		C19,

		/// <remarks/>
		C20,

		/// <remarks/>
		C21,

		/// <remarks/>
		C22,

		/// <remarks/>
		C23,

		/// <remarks/>
		C24,

		/// <remarks/>
		C25,

		/// <remarks/>
		C26,

		/// <remarks/>
		C27,

		/// <remarks/>
		C28,

		/// <remarks/>
		C29,

		/// <remarks/>
		C3,

		/// <remarks/>
		C30,

		/// <remarks/>
		C31,

		/// <remarks/>
		C32,

		/// <remarks/>
		C33,

		/// <remarks/>
		C34,

		/// <remarks/>
		C36,

		/// <remarks/>
		C37,

		/// <remarks/>
		C39,

		/// <remarks/>
		C40,

		/// <remarks/>
		C41,

		/// <remarks/>
		C42,

		/// <remarks/>
		C43,

		/// <remarks/>
		C44,

		/// <remarks/>
		C45,

		/// <remarks/>
		C46,

		/// <remarks/>
		C47,

		/// <remarks/>
		C48,

		/// <remarks/>
		C49,

		/// <remarks/>
		C50,

		/// <remarks/>
		C51,

		/// <remarks/>
		C52,

		/// <remarks/>
		C53,

		/// <remarks/>
		C54,

		/// <remarks/>
		C55,

		/// <remarks/>
		C56,

		/// <remarks/>
		C57,

		/// <remarks/>
		C58,

		/// <remarks/>
		C59,

		/// <remarks/>
		C60,

		/// <remarks/>
		C61,

		/// <remarks/>
		C62,

		/// <remarks/>
		C63,

		/// <remarks/>
		C64,

		/// <remarks/>
		C65,

		/// <remarks/>
		C66,

		/// <remarks/>
		C67,

		/// <remarks/>
		C68,

		/// <remarks/>
		C69,

		/// <remarks/>
		C7,

		/// <remarks/>
		C70,

		/// <remarks/>
		C71,

		/// <remarks/>
		C72,

		/// <remarks/>
		C73,

		/// <remarks/>
		C74,

		/// <remarks/>
		C75,

		/// <remarks/>
		C76,

		/// <remarks/>
		C78,

		/// <remarks/>
		C79,

		/// <remarks/>
		C8,

		/// <remarks/>
		C80,

		/// <remarks/>
		C81,

		/// <remarks/>
		C82,

		/// <remarks/>
		C83,

		/// <remarks/>
		C84,

		/// <remarks/>
		C85,

		/// <remarks/>
		C86,

		/// <remarks/>
		C87,

		/// <remarks/>
		C88,

		/// <remarks/>
		C89,

		/// <remarks/>
		C9,

		/// <remarks/>
		C90,

		/// <remarks/>
		C91,

		/// <remarks/>
		C92,

		/// <remarks/>
		C93,

		/// <remarks/>
		C95,

		/// <remarks/>
		C96,

		/// <remarks/>
		C97,

		/// <remarks/>
		C98,

		/// <remarks/>
		C99,

		/// <remarks/>
		CCT,

		/// <remarks/>
		CDL,

		/// <remarks/>
		CEL,

		/// <remarks/>
		CEN,

		/// <remarks/>
		CG,

		/// <remarks/>
		CGM,

		/// <remarks/>
		CKG,

		/// <remarks/>
		CLF,

		/// <remarks/>
		CLT,

		/// <remarks/>
		CMK,

		/// <remarks/>
		CMT,

		/// <remarks/>
		CNP,

		/// <remarks/>
		CNT,

		/// <remarks/>
		COU,

		/// <remarks/>
		CTG,

		/// <remarks/>
		CTM,

		/// <remarks/>
		CTN,

		/// <remarks/>
		CUR,

		/// <remarks/>
		CWA,

		/// <remarks/>
		CWI,

		/// <remarks/>
		D03,

		/// <remarks/>
		D04,

		/// <remarks/>
		D1,

		/// <remarks/>
		D10,

		/// <remarks/>
		D11,

		/// <remarks/>
		D12,

		/// <remarks/>
		D13,

		/// <remarks/>
		D15,

		/// <remarks/>
		D16,

		/// <remarks/>
		D17,

		/// <remarks/>
		D18,

		/// <remarks/>
		D19,

		/// <remarks/>
		D2,

		/// <remarks/>
		D20,

		/// <remarks/>
		D21,

		/// <remarks/>
		D22,

		/// <remarks/>
		D23,

		/// <remarks/>
		D24,

		/// <remarks/>
		D25,

		/// <remarks/>
		D26,

		/// <remarks/>
		D27,

		/// <remarks/>
		D29,

		/// <remarks/>
		D30,

		/// <remarks/>
		D31,

		/// <remarks/>
		D33,

		/// <remarks/>
		D34,

		/// <remarks/>
		D36,

		/// <remarks/>
		D41,

		/// <remarks/>
		D43,

		/// <remarks/>
		D44,

		/// <remarks/>
		D45,

		/// <remarks/>
		D46,

		/// <remarks/>
		D47,

		/// <remarks/>
		D49,

		/// <remarks/>
		D5,

		/// <remarks/>
		D50,

		/// <remarks/>
		D51,

		/// <remarks/>
		D52,

		/// <remarks/>
		D53,

		/// <remarks/>
		D54,

		/// <remarks/>
		D55,

		/// <remarks/>
		D56,

		/// <remarks/>
		D57,

		/// <remarks/>
		D58,

		/// <remarks/>
		D59,

		/// <remarks/>
		D6,

		/// <remarks/>
		D61,

		/// <remarks/>
		D62,

		/// <remarks/>
		D63,

		/// <remarks/>
		D65,

		/// <remarks/>
		D68,

		/// <remarks/>
		D69,

		/// <remarks/>
		D73,

		/// <remarks/>
		D74,

		/// <remarks/>
		D77,

		/// <remarks/>
		D78,

		/// <remarks/>
		D80,

		/// <remarks/>
		D81,

		/// <remarks/>
		D82,

		/// <remarks/>
		D83,

		/// <remarks/>
		D85,

		/// <remarks/>
		D86,

		/// <remarks/>
		D87,

		/// <remarks/>
		D88,

		/// <remarks/>
		D89,

		/// <remarks/>
		D91,

		/// <remarks/>
		D93,

		/// <remarks/>
		D94,

		/// <remarks/>
		D95,

		/// <remarks/>
		DAA,

		/// <remarks/>
		DAD,

		/// <remarks/>
		DAY,

		/// <remarks/>
		DB,

		/// <remarks/>
		DEC,

		/// <remarks/>
		DG,

		/// <remarks/>
		DJ,

		/// <remarks/>
		DLT,

		/// <remarks/>
		DMK,

		/// <remarks/>
		DMO,

		/// <remarks/>
		DMT,

		/// <remarks/>
		DN,

		/// <remarks/>
		DPC,

		/// <remarks/>
		DPR,

		/// <remarks/>
		DPT,

		/// <remarks/>
		DRA,

		/// <remarks/>
		DRI,

		/// <remarks/>
		DRL,

		/// <remarks/>
		DRM,

		/// <remarks/>
		DT,

		/// <remarks/>
		DTN,

		/// <remarks/>
		DWT,

		/// <remarks/>
		DZN,

		/// <remarks/>
		DZP,

		/// <remarks/>
		E01,

		/// <remarks/>
		E07,

		/// <remarks/>
		E08,

		/// <remarks/>
		E09,

		/// <remarks/>
		E10,

		/// <remarks/>
		E12,

		/// <remarks/>
		E14,

		/// <remarks/>
		E15,

		/// <remarks/>
		E16,

		/// <remarks/>
		E17,

		/// <remarks/>
		E18,

		/// <remarks/>
		E19,

		/// <remarks/>
		E20,

		/// <remarks/>
		E21,

		/// <remarks/>
		E22,

		/// <remarks/>
		E23,

		/// <remarks/>
		E25,

		/// <remarks/>
		E27,

		/// <remarks/>
		E28,

		/// <remarks/>
		E30,

		/// <remarks/>
		E31,

		/// <remarks/>
		E32,

		/// <remarks/>
		E34,

		/// <remarks/>
		E35,

		/// <remarks/>
		E36,

		/// <remarks/>
		E37,

		/// <remarks/>
		E38,

		/// <remarks/>
		E39,

		/// <remarks/>
		E4,

		/// <remarks/>
		E40,

		/// <remarks/>
		E41,

		/// <remarks/>
		E42,

		/// <remarks/>
		E43,

		/// <remarks/>
		E44,

		/// <remarks/>
		E5,

		/// <remarks/>
		EA,

		/// <remarks/>
		EB,

		/// <remarks/>
		EQ,

		/// <remarks/>
		FAH,

		/// <remarks/>
		FBM,

		/// <remarks/>
		FC,

		/// <remarks/>
		FF,

		/// <remarks/>
		FH,

		/// <remarks/>
		FL,

		/// <remarks/>
		FOT,

		/// <remarks/>
		FP,

		/// <remarks/>
		FR,

		/// <remarks/>
		FS,

		/// <remarks/>
		FTK,

		/// <remarks/>
		FTQ,

		/// <remarks/>
		G3,

		/// <remarks/>
		GB,

		/// <remarks/>
		GBQ,

		/// <remarks/>
		GDW,

		/// <remarks/>
		GF,

		/// <remarks/>
		GFI,

		/// <remarks/>
		GGR,

		/// <remarks/>
		GIA,

		/// <remarks/>
		GIC,

		/// <remarks/>
		GII,

		/// <remarks/>
		GIP,

		/// <remarks/>
		GJ,

		/// <remarks/>
		GL,

		/// <remarks/>
		GLD,

		/// <remarks/>
		GLI,

		/// <remarks/>
		GLL,

		/// <remarks/>
		GM,

		/// <remarks/>
		GO,

		/// <remarks/>
		GP,

		/// <remarks/>
		GQ,

		/// <remarks/>
		GRM,

		/// <remarks/>
		GRN,

		/// <remarks/>
		GRO,

		/// <remarks/>
		GT,

		/// <remarks/>
		GV,

		/// <remarks/>
		HA,

		/// <remarks/>
		HAR,

		/// <remarks/>
		HBA,

		/// <remarks/>
		HBX,

		/// <remarks/>
		HC,

		/// <remarks/>
		HDW,

		/// <remarks/>
		HIU,

		/// <remarks/>
		HKM,

		/// <remarks/>
		HM,

		/// <remarks/>
		HMQ,

		/// <remarks/>
		HMT,

		/// <remarks/>
		HPA,

		/// <remarks/>
		HTZ,

		/// <remarks/>
		HUR,

		/// <remarks/>
		IE,

		/// <remarks/>
		INH,

		/// <remarks/>
		INK,

		/// <remarks/>
		INQ,

		/// <remarks/>
		ISD,

		/// <remarks/>
		IU,

		/// <remarks/>
		IV,

		/// <remarks/>
		J2,

		/// <remarks/>
		JE,

		/// <remarks/>
		JK,

		/// <remarks/>
		JM,

		/// <remarks/>
		JOU,

		/// <remarks/>
		JPS,

		/// <remarks/>
		JWL,

		/// <remarks/>
		K1,

		/// <remarks/>
		K2,

		/// <remarks/>
		K3,

		/// <remarks/>
		K5,

		/// <remarks/>
		K6,

		/// <remarks/>
		KA,

		/// <remarks/>
		KB,

		/// <remarks/>
		KBA,

		/// <remarks/>
		KCC,

		/// <remarks/>
		KDW,

		/// <remarks/>
		KEL,

		/// <remarks/>
		KGM,

		/// <remarks/>
		KGS,

		/// <remarks/>
		KHY,

		/// <remarks/>
		KHZ,

		/// <remarks/>
		KIC,

		/// <remarks/>
		KIP,

		/// <remarks/>
		KJO,

		/// <remarks/>
		KL,

		/// <remarks/>
		KLK,

		/// <remarks/>
		KMA,

		/// <remarks/>
		KMH,

		/// <remarks/>
		KMQ,

		/// <remarks/>
		KMT,

		/// <remarks/>
		KNI,

		/// <remarks/>
		KNS,

		/// <remarks/>
		KNT,

		/// <remarks/>
		KO,

		/// <remarks/>
		KPA,

		/// <remarks/>
		KPH,

		/// <remarks/>
		KPO,

		/// <remarks/>
		KPP,

		/// <remarks/>
		KSD,

		/// <remarks/>
		KSH,

		/// <remarks/>
		KT,

		/// <remarks/>
		KTN,

		/// <remarks/>
		KUR,

		/// <remarks/>
		KVA,

		/// <remarks/>
		KVT,

		/// <remarks/>
		KW,

		/// <remarks/>
		KWO,

		/// <remarks/>
		KWT,

		/// <remarks/>
		KX,

		/// <remarks/>
		L2,

		/// <remarks/>
		LAC,

		/// <remarks/>
		LBR,

		/// <remarks/>
		LBT,

		/// <remarks/>
		LD,

		/// <remarks/>
		LEF,

		/// <remarks/>
		LF,

		/// <remarks/>
		LH,

		/// <remarks/>
		LK,

		/// <remarks/>
		LM,

		/// <remarks/>
		LN,

		/// <remarks/>
		LO,

		/// <remarks/>
		LP,

		/// <remarks/>
		LPA,

		/// <remarks/>
		LR,

		/// <remarks/>
		LS,

		/// <remarks/>
		LTN,

		/// <remarks/>
		LTR,

		/// <remarks/>
		LUB,

		/// <remarks/>
		LUM,

		/// <remarks/>
		LUX,

		/// <remarks/>
		LY,

		/// <remarks/>
		M1,

		/// <remarks/>
		M4,

		/// <remarks/>
		M5,

		/// <remarks/>
		M7,

		/// <remarks/>
		MAH,

		/// <remarks/>
		MAL,

		/// <remarks/>
		MAM,

		/// <remarks/>
		MAR,

		/// <remarks/>
		MAW,

		/// <remarks/>
		MBE,

		/// <remarks/>
		MBF,

		/// <remarks/>
		MBR,

		/// <remarks/>
		MC,

		/// <remarks/>
		MCU,

		/// <remarks/>
		MD,

		/// <remarks/>
		MGM,

		/// <remarks/>
		MHZ,

		/// <remarks/>
		MIK,

		/// <remarks/>
		MIL,

		/// <remarks/>
		MIN,

		/// <remarks/>
		MIO,

		/// <remarks/>
		MIU,

		/// <remarks/>
		MLD,

		/// <remarks/>
		MLT,

		/// <remarks/>
		MMK,

		/// <remarks/>
		MMT,

		/// <remarks/>
		MND,

		/// <remarks/>
		MPA,

		/// <remarks/>
		MQH,

		/// <remarks/>
		MQS,

		/// <remarks/>
		MSK,

		/// <remarks/>
		MTK,

		/// <remarks/>
		MTQ,

		/// <remarks/>
		MTR,

		/// <remarks/>
		MTS,

		/// <remarks/>
		MVA,

		/// <remarks/>
		N1,

		/// <remarks/>
		N3,

		/// <remarks/>
		NAR,

		/// <remarks/>
		NCL,

		/// <remarks/>
		NEW,

		/// <remarks/>
		NF,

		/// <remarks/>
		NIU,

		/// <remarks/>
		NL,

		/// <remarks/>
		NMI,

		/// <remarks/>
		NMP,

		/// <remarks/>
		NPT,

		/// <remarks/>
		NT,

		/// <remarks/>
		NU,

		/// <remarks/>
		NX,

		/// <remarks/>
		OA,

		/// <remarks/>
		ODE,

		/// <remarks/>
		OHM,

		/// <remarks/>
		ON,

		/// <remarks/>
		ONZ,

		/// <remarks/>
		OT,

		/// <remarks/>
		OZA,

		/// <remarks/>
		OZI,

		/// <remarks/>
		P1,

		/// <remarks/>
		P2,

		/// <remarks/>
		P5,

		/// <remarks/>
		PAL,

		/// <remarks/>
		PD,

		/// <remarks/>
		PFL,

		/// <remarks/>
		PGL,

		/// <remarks/>
		PI,

		/// <remarks/>
		PLA,

		/// <remarks/>
		PQ,

		/// <remarks/>
		PR,

		/// <remarks/>
		PS,

		/// <remarks/>
		PT,

		/// <remarks/>
		PTD,

		/// <remarks/>
		PTI,

		/// <remarks/>
		PTL,

		/// <remarks/>
		Q3,

		/// <remarks/>
		QA,

		/// <remarks/>
		QAN,

		/// <remarks/>
		QB,

		/// <remarks/>
		QR,

		/// <remarks/>
		QT,

		/// <remarks/>
		QTD,

		/// <remarks/>
		QTI,

		/// <remarks/>
		QTL,

		/// <remarks/>
		QTR,

		/// <remarks/>
		R1,

		/// <remarks/>
		R9,

		/// <remarks/>
		RH,

		/// <remarks/>
		RM,

		/// <remarks/>
		RP,

		/// <remarks/>
		RT,

		/// <remarks/>
		S3,

		/// <remarks/>
		S4,

		/// <remarks/>
		SAN,

		/// <remarks/>
		SCO,

		/// <remarks/>
		SCR,

		/// <remarks/>
		SEC,

		/// <remarks/>
		SET,

		/// <remarks/>
		SG,

		/// <remarks/>
		SIE,

		/// <remarks/>
		SMI,

		/// <remarks/>
		SQ,

		/// <remarks/>
		SQR,

		/// <remarks/>
		SR,

		/// <remarks/>
		STI,

		/// <remarks/>
		STK,

		/// <remarks/>
		STL,

		/// <remarks/>
		STN,

		/// <remarks/>
		SW,

		/// <remarks/>
		SX,

		/// <remarks/>
		T0,

		/// <remarks/>
		T3,

		/// <remarks/>
		TIC,

		/// <remarks/>
		TIP,

		/// <remarks/>
		TMS,

		/// <remarks/>
		TNE,

		/// <remarks/>
		TP,

		/// <remarks/>
		TPR,

		/// <remarks/>
		TRL,

		/// <remarks/>
		U1,

		/// <remarks/>
		U2,

		/// <remarks/>
		UB,

		/// <remarks/>
		UC,

		/// <remarks/>
		VA,

		/// <remarks/>
		VLT,

		/// <remarks/>
		W2,

		/// <remarks/>
		WA,

		/// <remarks/>
		WB,

		/// <remarks/>
		WCD,

		/// <remarks/>
		WE,

		/// <remarks/>
		WEB,

		/// <remarks/>
		WG,

		/// <remarks/>
		WHR,

		/// <remarks/>
		WM,

		/// <remarks/>
		WSD,

		/// <remarks/>
		WTT,

		/// <remarks/>
		X1,

		/// <remarks/>
		YDQ,

		/// <remarks/>
		YRD,

		/// <remarks/>
		ZP,

		/// <remarks/>
		ZZ,
	}
}