﻿using System.CodeDom.Compiler;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[WebServiceBinding(Name="CommercialCustomer", Namespace="http://webservice.florecom.org/CommercialCustomer")]
	public interface ICommercialCustomer {
    
		/// <remarks/>
		[WebMethod]
		[SoapDocumentMethod("http://webservices.florecom.org/commercial/customer/GetSupply", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Bare)]
		[return: XmlElement("SupplyResponse", Namespace="urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")]
		SupplyResponseMessage GetSupply([XmlElement(Namespace="urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")] SupplyRequestMessage SupplyRequest);
    
		/// <remarks/>
		[WebMethod]
		[SoapDocumentMethod("http://webservices.florecom.org/commercial/customer/GetImage", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Bare)]
		[return: XmlElement("ImagesResponse", Namespace="urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
		ImagesResponseMessage GetImage([XmlElement(Namespace="urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")] ImagesRequestMessage ImagesRequest);
    
		/// <remarks/>
		[WebMethod]
		[SoapDocumentMethod("http://webservices.florecom.org/commercial/customer/GetStatus", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Bare)]
		[return: XmlElement("StatusResponse", Namespace="urn:fec:florecom:xml:data:draft:StatusStandardMessage:3")]
		StatusResponseMessage GetStatus([XmlElement(Namespace="urn:fec:florecom:xml:data:draft:StatusStandardMessage:3")] StatusRequestMessage StatusRequest);
    
		/// <remarks/>
		[WebMethod]
		[SoapDocumentMethod("http://webservices.florecom.org/commercial/customer/PutOrder", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=SoapParameterStyle.Bare)]
		[return: XmlElement("PutOrderResponse", Namespace="urn:fec:florecom:xml:data:draft:OrderStandardMessage:5")]
		OrderResponseMessage PutOrder([XmlElement(Namespace="urn:fec:florecom:xml:data:draft:OrderStandardMessage:5")] OrderRequestMessage PutOrderRequest);
	}
}