﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4")]
	public class IDType
	{
		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string schemeID { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string schemeName { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public AgencyIdentificationCodeContentType schemeAgencyID { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool schemeAgencyIDSpecified { get; set; }

		/// <remarks/>
		[XmlAttribute]
		public string schemeAgencyName { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "token")]
		public string schemeVersionID { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "anyURI")]
		public string schemeDataURI { get; set; }

		/// <remarks/>
		[XmlAttribute(DataType = "anyURI")]
		public string schemeURI { get; set; }

		/// <remarks/>
		[XmlText(DataType = "token")]
		public string Value { get; set; }
	}
}