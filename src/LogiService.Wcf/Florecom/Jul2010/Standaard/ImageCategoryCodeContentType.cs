﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:ImageCategory:1:0")]
	public enum ImageCategoryCodeContentType
	{

		/// <remarks/>
		Standard,

		/// <remarks/>
		Commercial,

		/// <remarks/>
		Batch,

		/// <remarks/>
		Manufacturer,

		/// <remarks/>
		Supplier,
	}
}