﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class ImageRequestLineItemType
	{
		/// <remarks/>
		[XmlElement("ID")]
		public IDType[] ID { get; set; }

		/// <remarks/>
		[XmlElement("CategoryCode")]
		public ImageCategoryCodeType[] CategoryCode { get; set; }

		/// <remarks/>
		[XmlElement("DocumentStatusCode")]
		public DocumentStatusCodeType[] DocumentStatusCode { get; set; }

		/// <remarks/>
		[XmlElement("TakenDateTime")]
		public DateTime[] TakenDateTime { get; set; }

		/// <remarks/>
		[XmlElement("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }

		/// <remarks/>
		[XmlElement("AdditionalInformationTradeNote")]
		public TradeNoteType[] AdditionalInformationTradeNote { get; set; }

		/// <remarks/>
		[XmlElement("SupplierParty")]
		public SupplierPartyType[] SupplierParty { get; set; }

		/// <remarks/>
		[XmlElement("CopyrightOwnerParty")]
		public OwnerPartyType[] CopyrightOwnerParty { get; set; }

		/// <remarks/>
		[XmlElement("Product")]
		public ProductType[] Product { get; set; }

		/// <remarks/>
		[XmlElement("ValidityPeriod")]
		public PeriodType[] ValidityPeriod { get; set; }

		/// <remarks/>
		[XmlElement("SubjectCode")]
		public ImageSubjectCodeType[] SubjectCode { get; set; }

		/// <remarks/>
		public bool DigitalImageBinaryObjectRequestIndicator { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool DigitalImageBinaryObjectRequestIndicatorSpecified { get; set; }
	}
}