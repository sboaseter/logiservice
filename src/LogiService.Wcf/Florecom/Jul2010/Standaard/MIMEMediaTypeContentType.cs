﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:IANA:MIMEMediaType:2007-07-09")]
	public enum MIMEMediaTypeContentType
	{

		/// <remarks/>
		application,

		/// <remarks/>
		[XmlEnum("application/activemessage")]
		applicationactivemessage,

		/// <remarks/>
		[XmlEnum("application/andrew-inset")]
		applicationandrewinset,

		/// <remarks/>
		[XmlEnum("application/applefile")]
		applicationapplefile,

		/// <remarks/>
		[XmlEnum("application/atom+xml")]
		applicationatomxml,

		/// <remarks/>
		[XmlEnum("application/atomicmail")]
		applicationatomicmail,

		/// <remarks/>
		[XmlEnum("application/auth-policy+xml")]
		applicationauthpolicyxml,

		/// <remarks/>
		[XmlEnum("application/batch-SMTP")]
		applicationbatchSMTP,

		/// <remarks/>
		[XmlEnum("application/beep+xml")]
		applicationbeepxml,

		/// <remarks/>
		[XmlEnum("application/cals-1840")]
		applicationcals1840,

		/// <remarks/>
		[XmlEnum("application/ccxml+xml")]
		applicationccxmlxml,

		/// <remarks/>
		[XmlEnum("application/cellml+xml")]
		applicationcellmlxml,

		/// <remarks/>
		[XmlEnum("application/cnrp+xml")]
		applicationcnrpxml,

		/// <remarks/>
		[XmlEnum("application/commonground")]
		applicationcommonground,

		/// <remarks/>
		[XmlEnum("application/conference-info+xml")]
		applicationconferenceinfoxml,

		/// <remarks/>
		[XmlEnum("application/cpl+xml")]
		applicationcplxml,

		/// <remarks/>
		[XmlEnum("application/csta+xml")]
		applicationcstaxml,

		/// <remarks/>
		[XmlEnum("application/CSTAdata+xml")]
		applicationCSTAdataxml,

		/// <remarks/>
		[XmlEnum("application/cybercash")]
		applicationcybercash,

		/// <remarks/>
		[XmlEnum("application/davmount+xml")]
		applicationdavmountxml,

		/// <remarks/>
		[XmlEnum("application/dca-rft")]
		applicationdcarft,

		/// <remarks/>
		[XmlEnum("application/dec-dx")]
		applicationdecdx,

		/// <remarks/>
		[XmlEnum("application/dialog-info+xml")]
		applicationdialoginfoxml,

		/// <remarks/>
		[XmlEnum("application/dicom")]
		applicationdicom,

		/// <remarks/>
		[XmlEnum("application/dns")]
		applicationdns,

		/// <remarks/>
		[XmlEnum("application/dvcs")]
		applicationdvcs,

		/// <remarks/>
		[XmlEnum("application/ecmascript")]
		applicationecmascript,

		/// <remarks/>
		[XmlEnum("application/EDI-Consent")]
		applicationEDIConsent,

		/// <remarks/>
		[XmlEnum("application/EDIFACT")]
		applicationEDIFACT,

		/// <remarks/>
		[XmlEnum("application/EDI-X12")]
		applicationEDIX12,

		/// <remarks/>
		[XmlEnum("application/epp+xml")]
		applicationeppxml,

		/// <remarks/>
		[XmlEnum("application/eshop")]
		applicationeshop,

		/// <remarks/>
		[XmlEnum("application/example")]
		applicationexample,

		/// <remarks/>
		[XmlEnum("application/fastinfoset")]
		applicationfastinfoset,

		/// <remarks/>
		[XmlEnum("application/fastsoap")]
		applicationfastsoap,

		/// <remarks/>
		[XmlEnum("application/fits")]
		applicationfits,

		/// <remarks/>
		[XmlEnum("application/font-tdpfr")]
		applicationfonttdpfr,

		/// <remarks/>
		[XmlEnum("application/H224")]
		applicationH224,

		/// <remarks/>
		[XmlEnum("application/http")]
		applicationhttp,

		/// <remarks/>
		[XmlEnum("application/hyperstudio")]
		applicationhyperstudio,

		/// <remarks/>
		[XmlEnum("application/iges")]
		applicationiges,

		/// <remarks/>
		[XmlEnum("application/im-iscomposing+xml")]
		applicationimiscomposingxml,

		/// <remarks/>
		[XmlEnum("application/index")]
		applicationindex,

		/// <remarks/>
		[XmlEnum("application/index.cmd")]
		applicationindexcmd,

		/// <remarks/>
		[XmlEnum("application/index.obj")]
		applicationindexobj,

		/// <remarks/>
		[XmlEnum("application/index.response")]
		applicationindexresponse,

		/// <remarks/>
		[XmlEnum("application/index.vnd")]
		applicationindexvnd,

		/// <remarks/>
		[XmlEnum("application/iotp")]
		applicationiotp,

		/// <remarks/>
		[XmlEnum("application/ipp")]
		applicationipp,

		/// <remarks/>
		[XmlEnum("application/isup")]
		applicationisup,

		/// <remarks/>
		[XmlEnum("application/javascript")]
		applicationjavascript,

		/// <remarks/>
		[XmlEnum("application/json")]
		applicationjson,

		/// <remarks/>
		[XmlEnum("application/kpml-request+xml")]
		applicationkpmlrequestxml,

		/// <remarks/>
		[XmlEnum("application/kpml-response+xml")]
		applicationkpmlresponsexml,

		/// <remarks/>
		[XmlEnum("application/mac-binhex40")]
		applicationmacbinhex40,

		/// <remarks/>
		[XmlEnum("application/macwriteii")]
		applicationmacwriteii,

		/// <remarks/>
		[XmlEnum("application/marc")]
		applicationmarc,

		/// <remarks/>
		[XmlEnum("application/mathematica")]
		applicationmathematica,

		/// <remarks/>
		[XmlEnum("application/mbms-associated-procedure-description+xml")]
		applicationmbmsassociatedproceduredescriptionxml,

		/// <remarks/>
		[XmlEnum("application/mbms-deregister+xml")]
		applicationmbmsderegisterxml,

		/// <remarks/>
		[XmlEnum("application/mbms-envelope+xml")]
		applicationmbmsenvelopexml,

		/// <remarks/>
		[XmlEnum("application/mbms-msk-response+xml")]
		applicationmbmsmskresponsexml,

		/// <remarks/>
		[XmlEnum("application/mbms-msk+xml")]
		applicationmbmsmskxml,

		/// <remarks/>
		[XmlEnum("application/mbms-protection-description+xml")]
		applicationmbmsprotectiondescriptionxml,

		/// <remarks/>
		[XmlEnum("application/mbms-reception-report+xml")]
		applicationmbmsreceptionreportxml,

		/// <remarks/>
		[XmlEnum("application/mbms-register-response+xml")]
		applicationmbmsregisterresponsexml,

		/// <remarks/>
		[XmlEnum("application/mbms-register+xml")]
		applicationmbmsregisterxml,

		/// <remarks/>
		[XmlEnum("application/mbms-user-service-description+xml")]
		applicationmbmsuserservicedescriptionxml,

		/// <remarks/>
		[XmlEnum("application/mbox")]
		applicationmbox,

		/// <remarks/>
		[XmlEnum("application/mediaservercontrol+xml")]
		applicationmediaservercontrolxml,

		/// <remarks/>
		[XmlEnum("application/mikey")]
		applicationmikey,

		/// <remarks/>
		[XmlEnum("application/mpeg4-generic")]
		applicationmpeg4generic,

		/// <remarks/>
		[XmlEnum("application/mpeg4-iod")]
		applicationmpeg4iod,

		/// <remarks/>
		[XmlEnum("application/mpeg4-iod-xmt")]
		applicationmpeg4iodxmt,

		/// <remarks/>
		[XmlEnum("application/mp4")]
		applicationmp4,

		/// <remarks/>
		[XmlEnum("application/msword")]
		applicationmsword,

		/// <remarks/>
		[XmlEnum("application/mxf")]
		applicationmxf,

		/// <remarks/>
		[XmlEnum("application/nasdata")]
		applicationnasdata,

		/// <remarks/>
		[XmlEnum("application/news-message-id")]
		applicationnewsmessageid,

		/// <remarks/>
		[XmlEnum("application/news-transmission")]
		applicationnewstransmission,

		/// <remarks/>
		[XmlEnum("application/nss")]
		applicationnss,

		/// <remarks/>
		[XmlEnum("application/ocsp-request")]
		applicationocsprequest,

		/// <remarks/>
		[XmlEnum("application/ocsp-response")]
		applicationocspresponse,

		/// <remarks/>
		[XmlEnum("application/octet-stream")]
		applicationoctetstream,

		/// <remarks/>
		[XmlEnum("application/oda")]
		applicationoda,

		/// <remarks/>
		[XmlEnum("application/oebps-package+xml")]
		applicationoebpspackagexml,

		/// <remarks/>
		[XmlEnum("application/ogg")]
		applicationogg,

		/// <remarks/>
		[XmlEnum("application/parityfec")]
		applicationparityfec,

		/// <remarks/>
		[XmlEnum("application/pdf")]
		applicationpdf,

		/// <remarks/>
		[XmlEnum("application/pgp-encrypted")]
		applicationpgpencrypted,

		/// <remarks/>
		[XmlEnum("application/pgp-keys")]
		applicationpgpkeys,

		/// <remarks/>
		[XmlEnum("application/pgp-signature")]
		applicationpgpsignature,

		/// <remarks/>
		[XmlEnum("application/pidf+xml")]
		applicationpidfxml,

		/// <remarks/>
		[XmlEnum("application/pkcs10")]
		applicationpkcs10,

		/// <remarks/>
		[XmlEnum("application/pkcs7-mime")]
		applicationpkcs7mime,

		/// <remarks/>
		[XmlEnum("application/pkcs7-signature")]
		applicationpkcs7signature,

		/// <remarks/>
		[XmlEnum("application/pkix-cert")]
		applicationpkixcert,

		/// <remarks/>
		[XmlEnum("application/pkixcmp")]
		applicationpkixcmp,

		/// <remarks/>
		[XmlEnum("application/pkix-crl")]
		applicationpkixcrl,

		/// <remarks/>
		[XmlEnum("application/pkix-pkipath")]
		applicationpkixpkipath,

		/// <remarks/>
		[XmlEnum("application/pls+xml")]
		applicationplsxml,

		/// <remarks/>
		[XmlEnum("application/poc-settings+xml")]
		applicationpocsettingsxml,

		/// <remarks/>
		[XmlEnum("application/postscript")]
		applicationpostscript,

		/// <remarks/>
		[XmlEnum("application/prs.alvestrand.titrax-sheet")]
		applicationprsalvestrandtitraxsheet,

		/// <remarks/>
		[XmlEnum("application/prs.cww")]
		applicationprscww,

		/// <remarks/>
		[XmlEnum("application/prs.nprend")]
		applicationprsnprend,

		/// <remarks/>
		[XmlEnum("application/prs.plucker")]
		applicationprsplucker,

		/// <remarks/>
		[XmlEnum("application/rdf+xml")]
		applicationrdfxml,

		/// <remarks/>
		[XmlEnum("application/qsig")]
		applicationqsig,

		/// <remarks/>
		[XmlEnum("application/reginfo+xml")]
		applicationreginfoxml,

		/// <remarks/>
		[XmlEnum("application/relax-ng-compact-syntax")]
		applicationrelaxngcompactsyntax,

		/// <remarks/>
		[XmlEnum("application/remote-printing")]
		applicationremoteprinting,

		/// <remarks/>
		[XmlEnum("application/resource-lists+xml")]
		applicationresourcelistsxml,

		/// <remarks/>
		[XmlEnum("application/riscos")]
		applicationriscos,

		/// <remarks/>
		[XmlEnum("application/rlmi+xml")]
		applicationrlmixml,

		/// <remarks/>
		[XmlEnum("application/rls-services+xml")]
		applicationrlsservicesxml,

		/// <remarks/>
		[XmlEnum("application/rtf")]
		applicationrtf,

		/// <remarks/>
		[XmlEnum("application/rtx")]
		applicationrtx,

		/// <remarks/>
		[XmlEnum("application/samlassertion+xml")]
		applicationsamlassertionxml,

		/// <remarks/>
		[XmlEnum("application/samlmetadata+xml")]
		applicationsamlmetadataxml,

		/// <remarks/>
		[XmlEnum("application/sbml+xml")]
		applicationsbmlxml,

		/// <remarks/>
		[XmlEnum("application/sdp")]
		applicationsdp,

		/// <remarks/>
		[XmlEnum("application/set-payment")]
		applicationsetpayment,

		/// <remarks/>
		[XmlEnum("application/set-payment-initiation")]
		applicationsetpaymentinitiation,

		/// <remarks/>
		[XmlEnum("application/set-registration")]
		applicationsetregistration,

		/// <remarks/>
		[XmlEnum("application/set-registration-initiation")]
		applicationsetregistrationinitiation,

		/// <remarks/>
		[XmlEnum("application/sgml")]
		applicationsgml,

		/// <remarks/>
		[XmlEnum("application/sgml-open-catalog")]
		applicationsgmlopencatalog,

		/// <remarks/>
		[XmlEnum("application/shf+xml")]
		applicationshfxml,

		/// <remarks/>
		[XmlEnum("application/sieve")]
		applicationsieve,

		/// <remarks/>
		[XmlEnum("application/simple-filter+xml")]
		applicationsimplefilterxml,

		/// <remarks/>
		[XmlEnum("application/simple-message-summary")]
		applicationsimplemessagesummary,

		/// <remarks/>
		[XmlEnum("application/simpleSymbolContainer")]
		applicationsimpleSymbolContainer,

		/// <remarks/>
		[XmlEnum("application/slate")]
		applicationslate,

		/// <remarks/>
		[XmlEnum("application/smil (OBSOLETE)")]
		applicationsmilOBSOLETE,

		/// <remarks/>
		[XmlEnum("application/smil+xml")]
		applicationsmilxml,

		/// <remarks/>
		[XmlEnum("application/soap+fastinfoset")]
		applicationsoapfastinfoset,

		/// <remarks/>
		[XmlEnum("application/soap+xml")]
		applicationsoapxml,

		/// <remarks/>
		[XmlEnum("application/spirits-event+xml")]
		applicationspiritseventxml,

		/// <remarks/>
		[XmlEnum("application/srgs")]
		applicationsrgs,

		/// <remarks/>
		[XmlEnum("application/srgs+xml")]
		applicationsrgsxml,

		/// <remarks/>
		[XmlEnum("application/ssml+xml")]
		applicationssmlxml,

		/// <remarks/>
		[XmlEnum("application/timestamp-query")]
		applicationtimestampquery,

		/// <remarks/>
		[XmlEnum("application/timestamp-reply")]
		applicationtimestampreply,

		/// <remarks/>
		[XmlEnum("application/tve-trigger")]
		applicationtvetrigger,

		/// <remarks/>
		[XmlEnum("application/vemmi")]
		applicationvemmi,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp.bsf+xml")]
		applicationvnd3gppbsfxml,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp.pic-bw-large")]
		applicationvnd3gpppicbwlarge,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp.pic-bw-small")]
		applicationvnd3gpppicbwsmall,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp.pic-bw-var")]
		applicationvnd3gpppicbwvar,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp.sms")]
		applicationvnd3gppsms,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp2.bcmcsinfo+xml")]
		applicationvnd3gpp2bcmcsinfoxml,

		/// <remarks/>
		[XmlEnum("application/vnd.3gpp2.sms")]
		applicationvnd3gpp2sms,

		/// <remarks/>
		[XmlEnum("application/vnd.3M.Post-it-Notes")]
		applicationvnd3MPostitNotes,

		/// <remarks/>
		[XmlEnum("application/vnd.accpac.simply.aso")]
		applicationvndaccpacsimplyaso,

		/// <remarks/>
		[XmlEnum("application/vnd.accpac.simply.imp")]
		applicationvndaccpacsimplyimp,

		/// <remarks/>
		[XmlEnum("application/vnd.acucobol")]
		applicationvndacucobol,

		/// <remarks/>
		[XmlEnum("application/vnd.acucorp")]
		applicationvndacucorp,

		/// <remarks/>
		[XmlEnum("application/vnd.adobe.xdp+xml")]
		applicationvndadobexdpxml,

		/// <remarks/>
		[XmlEnum("application/vnd.adobe.xfdf")]
		applicationvndadobexfdf,

		/// <remarks/>
		[XmlEnum("application/vnd.aether.imp")]
		applicationvndaetherimp,

		/// <remarks/>
		[XmlEnum("application/vnd.amiga.ami")]
		applicationvndamigaami,

		/// <remarks/>
		[XmlEnum("application/vnd.anser-web-certificate-issue-initiation")]
		applicationvndanserwebcertificateissueinitiation,

		/// <remarks/>
		[XmlEnum("application/vnd.apple.installer+xml")]
		applicationvndappleinstallerxml,

		/// <remarks/>
		[XmlEnum("application/vnd.audiograph")]
		applicationvndaudiograph,

		/// <remarks/>
		[XmlEnum("application/vnd.autopackage")]
		applicationvndautopackage,

		/// <remarks/>
		[XmlEnum("application/vnd.avistar+xml")]
		applicationvndavistarxml,

		/// <remarks/>
		[XmlEnum("application/vnd.blueice.multipass")]
		applicationvndblueicemultipass,

		/// <remarks/>
		[XmlEnum("application/vnd.bmi")]
		applicationvndbmi,

		/// <remarks/>
		[XmlEnum("application/vnd.businessobjects")]
		applicationvndbusinessobjects,

		/// <remarks/>
		[XmlEnum("application/vnd.cab-jscript")]
		applicationvndcabjscript,

		/// <remarks/>
		[XmlEnum("application/vnd.canon-cpdl")]
		applicationvndcanoncpdl,

		/// <remarks/>
		[XmlEnum("application/vnd.canon-lips")]
		applicationvndcanonlips,

		/// <remarks/>
		[XmlEnum("application/vnd.cendio.thinlinc.clientconf")]
		applicationvndcendiothinlincclientconf,

		/// <remarks/>
		[XmlEnum("application/vnd.chemdraw+xml")]
		applicationvndchemdrawxml,

		/// <remarks/>
		[XmlEnum("application/vnd.chipnuts.karaoke-mmd")]
		applicationvndchipnutskaraokemmd,

		/// <remarks/>
		[XmlEnum("application/vnd.cinderella")]
		applicationvndcinderella,

		/// <remarks/>
		[XmlEnum("application/vnd.cirpack.isdn-ext")]
		applicationvndcirpackisdnext,

		/// <remarks/>
		[XmlEnum("application/vnd.claymore")]
		applicationvndclaymore,

		/// <remarks/>
		[XmlEnum("application/vnd.clonk.c4group")]
		applicationvndclonkc4group,

		/// <remarks/>
		[XmlEnum("application/vnd.commerce-battelle")]
		applicationvndcommercebattelle,

		/// <remarks/>
		[XmlEnum("application/vnd.commonspace")]
		applicationvndcommonspace,

		/// <remarks/>
		[XmlEnum("application/vnd.cosmocaller")]
		applicationvndcosmocaller,

		/// <remarks/>
		[XmlEnum("application/vnd.contact.cmsg")]
		applicationvndcontactcmsg,

		/// <remarks/>
		[XmlEnum("application/vnd.crick.clicker")]
		applicationvndcrickclicker,

		/// <remarks/>
		[XmlEnum("application/vnd.crick.clicker.keyboard")]
		applicationvndcrickclickerkeyboard,

		/// <remarks/>
		[XmlEnum("application/vnd.crick.clicker.palette")]
		applicationvndcrickclickerpalette,

		/// <remarks/>
		[XmlEnum("application/vnd.crick.clicker.template")]
		applicationvndcrickclickertemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.crick.clicker.wordbank")]
		applicationvndcrickclickerwordbank,

		/// <remarks/>
		[XmlEnum("application/vnd.criticaltools.wbs+xml")]
		applicationvndcriticaltoolswbsxml,

		/// <remarks/>
		[XmlEnum("application/vnd.ctc-posml")]
		applicationvndctcposml,

		/// <remarks/>
		[XmlEnum("application/vnd.cups-pdf")]
		applicationvndcupspdf,

		/// <remarks/>
		[XmlEnum("application/vnd.cups-postscript")]
		applicationvndcupspostscript,

		/// <remarks/>
		[XmlEnum("application/vnd.cups-ppd")]
		applicationvndcupsppd,

		/// <remarks/>
		[XmlEnum("application/vnd.cups-raster")]
		applicationvndcupsraster,

		/// <remarks/>
		[XmlEnum("application/vnd.cups-raw")]
		applicationvndcupsraw,

		/// <remarks/>
		[XmlEnum("application/vnd.curl")]
		applicationvndcurl,

		/// <remarks/>
		[XmlEnum("application/vnd.cybank")]
		applicationvndcybank,

		/// <remarks/>
		[XmlEnum("application/vnd.data-vision.rdz")]
		applicationvnddatavisionrdz,

		/// <remarks/>
		[XmlEnum("application/vnd.denovo.fcselayout-link")]
		applicationvnddenovofcselayoutlink,

		/// <remarks/>
		[XmlEnum("application/vnd.dna")]
		applicationvnddna,

		/// <remarks/>
		[XmlEnum("application/vnd.dpgraph")]
		applicationvnddpgraph,

		/// <remarks/>
		[XmlEnum("application/vnd.dreamfactory")]
		applicationvnddreamfactory,

		/// <remarks/>
		[XmlEnum("application/vnd.dvb.esgcontainer")]
		applicationvnddvbesgcontainer,

		/// <remarks/>
		[XmlEnum("application/vnd.dvb.ipdcesgaccess")]
		applicationvnddvbipdcesgaccess,

		/// <remarks/>
		[XmlEnum("application/vnd.dxr")]
		applicationvnddxr,

		/// <remarks/>
		[XmlEnum("application/vnd.ecdis-update")]
		applicationvndecdisupdate,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.chart")]
		applicationvndecowinchart,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.filerequest")]
		applicationvndecowinfilerequest,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.fileupdate")]
		applicationvndecowinfileupdate,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.series")]
		applicationvndecowinseries,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.seriesrequest")]
		applicationvndecowinseriesrequest,

		/// <remarks/>
		[XmlEnum("application/vnd.ecowin.seriesupdate")]
		applicationvndecowinseriesupdate,

		/// <remarks/>
		[XmlEnum("application/vnd.enliven")]
		applicationvndenliven,

		/// <remarks/>
		[XmlEnum("application/vnd.epson.esf")]
		applicationvndepsonesf,

		/// <remarks/>
		[XmlEnum("application/vnd.epson.msf")]
		applicationvndepsonmsf,

		/// <remarks/>
		[XmlEnum("application/vnd.epson.quickanime")]
		applicationvndepsonquickanime,

		/// <remarks/>
		[XmlEnum("application/vnd.epson.salt")]
		applicationvndepsonsalt,

		/// <remarks/>
		[XmlEnum("application/vnd.epson.ssf")]
		applicationvndepsonssf,

		/// <remarks/>
		[XmlEnum("application/vnd.ericsson.quickcall")]
		applicationvndericssonquickcall,

		/// <remarks/>
		[XmlEnum("application/vnd.eudora.data")]
		applicationvndeudoradata,

		/// <remarks/>
		[XmlEnum("application/vnd.ezpix-album")]
		applicationvndezpixalbum,

		/// <remarks/>
		[XmlEnum("application/vnd.ezpix-package")]
		applicationvndezpixpackage,

		/// <remarks/>
		[XmlEnum("application/vnd.fdf")]
		applicationvndfdf,

		/// <remarks/>
		[XmlEnum("application/vnd.ffsns")]
		applicationvndffsns,

		/// <remarks/>
		[XmlEnum("application/vnd.fints")]
		applicationvndfints,

		/// <remarks/>
		[XmlEnum("application/vnd.FloGraphIt")]
		applicationvndFloGraphIt,

		/// <remarks/>
		[XmlEnum("application/vnd.fluxtime.clip")]
		applicationvndfluxtimeclip,

		/// <remarks/>
		[XmlEnum("application/vnd.framemaker")]
		applicationvndframemaker,

		/// <remarks/>
		[XmlEnum("application/vnd.frogans.fnc")]
		applicationvndfrogansfnc,

		/// <remarks/>
		[XmlEnum("application/vnd.frogans.ltf")]
		applicationvndfrogansltf,

		/// <remarks/>
		[XmlEnum("application/vnd.fsc.weblaunch")]
		applicationvndfscweblaunch,

		/// <remarks/>
		[XmlEnum("application/vnd.fujitsu.oasys")]
		applicationvndfujitsuoasys,

		/// <remarks/>
		[XmlEnum("application/vnd.fujitsu.oasys2")]
		applicationvndfujitsuoasys2,

		/// <remarks/>
		[XmlEnum("application/vnd.fujitsu.oasys3")]
		applicationvndfujitsuoasys3,

		/// <remarks/>
		[XmlEnum("application/vnd.fujitsu.oasysgp")]
		applicationvndfujitsuoasysgp,

		/// <remarks/>
		[XmlEnum("application/vnd.fujitsu.oasysprs")]
		applicationvndfujitsuoasysprs,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.ART4")]
		applicationvndfujixeroxART4,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.ART-EX")]
		applicationvndfujixeroxARTEX,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.ddd")]
		applicationvndfujixeroxddd,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.docuworks")]
		applicationvndfujixeroxdocuworks,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.docuworks.binder")]
		applicationvndfujixeroxdocuworksbinder,

		/// <remarks/>
		[XmlEnum("application/vnd.fujixerox.HBPL")]
		applicationvndfujixeroxHBPL,

		/// <remarks/>
		[XmlEnum("application/vnd.fut-misnet")]
		applicationvndfutmisnet,

		/// <remarks/>
		[XmlEnum("application/vnd.fuzzysheet")]
		applicationvndfuzzysheet,

		/// <remarks/>
		[XmlEnum("application/vnd.genomatix.tuxedo")]
		applicationvndgenomatixtuxedo,

		/// <remarks/>
		[XmlEnum("application/vnd.google-earth.kml+xml")]
		applicationvndgoogleearthkmlxml,

		/// <remarks/>
		[XmlEnum("application/vnd.google-earth.kmz")]
		applicationvndgoogleearthkmz,

		/// <remarks/>
		[XmlEnum("application/vnd.grafeq")]
		applicationvndgrafeq,

		/// <remarks/>
		[XmlEnum("application/vnd.gridmp")]
		applicationvndgridmp,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-account")]
		applicationvndgrooveaccount,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-help")]
		applicationvndgroovehelp,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-identity-message")]
		applicationvndgrooveidentitymessage,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-injector")]
		applicationvndgrooveinjector,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-tool-message")]
		applicationvndgroovetoolmessage,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-tool-template")]
		applicationvndgroovetooltemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.groove-vcard")]
		applicationvndgroovevcard,

		/// <remarks/>
		[XmlEnum("application/vnd.HandHeld-Entertainment+xml")]
		applicationvndHandHeldEntertainmentxml,

		/// <remarks/>
		[XmlEnum("application/vnd.hbci")]
		applicationvndhbci,

		/// <remarks/>
		[XmlEnum("application/vnd.hcl-bireports")]
		applicationvndhclbireports,

		/// <remarks/>
		[XmlEnum("application/vnd.hhe.lesson-player")]
		applicationvndhhelessonplayer,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-HPGL")]
		applicationvndhpHPGL,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-hpid")]
		applicationvndhphpid,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-hps")]
		applicationvndhphps,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-jlyt")]
		applicationvndhpjlyt,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-PCL")]
		applicationvndhpPCL,

		/// <remarks/>
		[XmlEnum("application/vnd.hp-PCLXL")]
		applicationvndhpPCLXL,

		/// <remarks/>
		[XmlEnum("application/vnd.httphone")]
		applicationvndhttphone,

		/// <remarks/>
		[XmlEnum("application/vnd.hzn-3d-crossword")]
		applicationvndhzn3dcrossword,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.afplinedata")]
		applicationvndibmafplinedata,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.electronic-media")]
		applicationvndibmelectronicmedia,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.MiniPay")]
		applicationvndibmMiniPay,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.modcap")]
		applicationvndibmmodcap,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.rights-management")]
		applicationvndibmrightsmanagement,

		/// <remarks/>
		[XmlEnum("application/vnd.ibm.secure-container")]
		applicationvndibmsecurecontainer,

		/// <remarks/>
		[XmlEnum("application/vnd.igloader")]
		applicationvndigloader,

		/// <remarks/>
		[XmlEnum("application/vnd.immervision-ivp")]
		applicationvndimmervisionivp,

		/// <remarks/>
		[XmlEnum("application/vnd.immervision-ivu")]
		applicationvndimmervisionivu,

		/// <remarks/>
		[XmlEnum("application/vnd.informedcontrol.rms+xml")]
		applicationvndinformedcontrolrmsxml,

		/// <remarks/>
		[XmlEnum("application/vnd.informix-visionary")]
		applicationvndinformixvisionary,

		/// <remarks/>
		[XmlEnum("application/vnd.intercon.formnet")]
		applicationvndinterconformnet,

		/// <remarks/>
		[XmlEnum("application/vnd.intertrust.digibox")]
		applicationvndintertrustdigibox,

		/// <remarks/>
		[XmlEnum("application/vnd.intertrust.nncp")]
		applicationvndintertrustnncp,

		/// <remarks/>
		[XmlEnum("application/vnd.intu.qbo")]
		applicationvndintuqbo,

		/// <remarks/>
		[XmlEnum("application/vnd.intu.qfx")]
		applicationvndintuqfx,

		/// <remarks/>
		[XmlEnum("application/vnd.ipunplugged.rcprofile")]
		applicationvndipunpluggedrcprofile,

		/// <remarks/>
		[XmlEnum("application/vnd.irepository.package+xml")]
		applicationvndirepositorypackagexml,

		/// <remarks/>
		[XmlEnum("application/vnd.is-xpr")]
		applicationvndisxpr,

		/// <remarks/>
		[XmlEnum("application/vnd.jam")]
		applicationvndjam,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-directory-service")]
		applicationvndjapannetdirectoryservice,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-jpnstore-wakeup")]
		applicationvndjapannetjpnstorewakeup,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-payment-wakeup")]
		applicationvndjapannetpaymentwakeup,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-registration")]
		applicationvndjapannetregistration,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-registration-wakeup")]
		applicationvndjapannetregistrationwakeup,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-setstore-wakeup")]
		applicationvndjapannetsetstorewakeup,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-verification")]
		applicationvndjapannetverification,

		/// <remarks/>
		[XmlEnum("application/vnd.japannet-verification-wakeup")]
		applicationvndjapannetverificationwakeup,

		/// <remarks/>
		[XmlEnum("application/vnd.jcp.javame.midlet-rms")]
		applicationvndjcpjavamemidletrms,

		/// <remarks/>
		[XmlEnum("application/vnd.jisp")]
		applicationvndjisp,

		/// <remarks/>
		[XmlEnum("application/vnd.kahootz")]
		applicationvndkahootz,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.karbon")]
		applicationvndkdekarbon,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kchart")]
		applicationvndkdekchart,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kformula")]
		applicationvndkdekformula,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kivio")]
		applicationvndkdekivio,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kontour")]
		applicationvndkdekontour,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kpresenter")]
		applicationvndkdekpresenter,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kspread")]
		applicationvndkdekspread,

		/// <remarks/>
		[XmlEnum("application/vnd.kde.kword")]
		applicationvndkdekword,

		/// <remarks/>
		[XmlEnum("application/vnd.kenameaapp")]
		applicationvndkenameaapp,

		/// <remarks/>
		[XmlEnum("application/vnd.kidspiration")]
		applicationvndkidspiration,

		/// <remarks/>
		[XmlEnum("application/vnd.Kinar")]
		applicationvndKinar,

		/// <remarks/>
		[XmlEnum("application/vnd.koan")]
		applicationvndkoan,

		/// <remarks/>
		[XmlEnum("application/vnd.liberty-request+xml")]
		applicationvndlibertyrequestxml,

		/// <remarks/>
		[XmlEnum("application/vnd.llamagraphics.life-balance.desktop")]
		applicationvndllamagraphicslifebalancedesktop,

		/// <remarks/>
		[XmlEnum("application/vnd.llamagraphics.life-balance.exchange+xml")]
		applicationvndllamagraphicslifebalanceexchangexml,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-1-2-3")]
		applicationvndlotus123,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-approach")]
		applicationvndlotusapproach,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-freelance")]
		applicationvndlotusfreelance,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-notes")]
		applicationvndlotusnotes,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-organizer")]
		applicationvndlotusorganizer,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-screencam")]
		applicationvndlotusscreencam,

		/// <remarks/>
		[XmlEnum("application/vnd.lotus-wordpro")]
		applicationvndlotuswordpro,

		/// <remarks/>
		[XmlEnum("application/vnd.macports.portpkg")]
		applicationvndmacportsportpkg,

		/// <remarks/>
		[XmlEnum("application/vnd.marlin.drm.actiontoken+xml")]
		applicationvndmarlindrmactiontokenxml,

		/// <remarks/>
		[XmlEnum("application/vnd.marlin.drm.conftoken+xml")]
		applicationvndmarlindrmconftokenxml,

		/// <remarks/>
		[XmlEnum("application/vnd.marlin.drm.mdcf")]
		applicationvndmarlindrmmdcf,

		/// <remarks/>
		[XmlEnum("application/vnd.mcd")]
		applicationvndmcd,

		/// <remarks/>
		[XmlEnum("application/vnd.medcalcdata")]
		applicationvndmedcalcdata,

		/// <remarks/>
		[XmlEnum("application/vnd.mediastation.cdkey")]
		applicationvndmediastationcdkey,

		/// <remarks/>
		[XmlEnum("application/vnd.meridian-slingshot")]
		applicationvndmeridianslingshot,

		/// <remarks/>
		[XmlEnum("application/vnd.MFER")]
		applicationvndMFER,

		/// <remarks/>
		[XmlEnum("application/vnd.mfmp")]
		applicationvndmfmp,

		/// <remarks/>
		[XmlEnum("application/vnd.micrografx.flo")]
		applicationvndmicrografxflo,

		/// <remarks/>
		[XmlEnum("application/vnd.micrografx.igx")]
		applicationvndmicrografxigx,

		/// <remarks/>
		[XmlEnum("application/vnd.mif")]
		applicationvndmif,

		/// <remarks/>
		[XmlEnum("application/vnd.minisoft-hp3000-save")]
		applicationvndminisofthp3000save,

		/// <remarks/>
		[XmlEnum("application/vnd.mitsubishi.misty-guard.trustweb")]
		applicationvndmitsubishimistyguardtrustweb,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.DAF")]
		applicationvndMobiusDAF,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.DIS")]
		applicationvndMobiusDIS,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.MBK")]
		applicationvndMobiusMBK,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.MQY")]
		applicationvndMobiusMQY,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.MSL")]
		applicationvndMobiusMSL,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.PLC")]
		applicationvndMobiusPLC,

		/// <remarks/>
		[XmlEnum("application/vnd.Mobius.TXF")]
		applicationvndMobiusTXF,

		/// <remarks/>
		[XmlEnum("application/vnd.mophun.application")]
		applicationvndmophunapplication,

		/// <remarks/>
		[XmlEnum("application/vnd.mophun.certificate")]
		applicationvndmophuncertificate,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite")]
		applicationvndmotorolaflexsuite,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.adsi")]
		applicationvndmotorolaflexsuiteadsi,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.fis")]
		applicationvndmotorolaflexsuitefis,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.gotap")]
		applicationvndmotorolaflexsuitegotap,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.kmr")]
		applicationvndmotorolaflexsuitekmr,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.ttc")]
		applicationvndmotorolaflexsuitettc,

		/// <remarks/>
		[XmlEnum("application/vnd.motorola.flexsuite.wem")]
		applicationvndmotorolaflexsuitewem,

		/// <remarks/>
		[XmlEnum("application/vnd.mozilla.xul+xml")]
		applicationvndmozillaxulxml,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-artgalry")]
		applicationvndmsartgalry,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-asf")]
		applicationvndmsasf,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-cab-compressed")]
		applicationvndmscabcompressed,

		/// <remarks/>
		[XmlEnum("application/vnd.mseq")]
		applicationvndmseq,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-excel")]
		applicationvndmsexcel,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-fontobject")]
		applicationvndmsfontobject,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-htmlhelp")]
		applicationvndmshtmlhelp,

		/// <remarks/>
		[XmlEnum("application/vnd.msign")]
		applicationvndmsign,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-ims")]
		applicationvndmsims,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-lrm")]
		applicationvndmslrm,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-playready.initiator+xml")]
		applicationvndmsplayreadyinitiatorxml,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-powerpoint")]
		applicationvndmspowerpoint,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-project")]
		applicationvndmsproject,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-tnef")]
		applicationvndmstnef,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-wmdrm.lic-chlg-req")]
		applicationvndmswmdrmlicchlgreq,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-wmdrm.lic-resp")]
		applicationvndmswmdrmlicresp,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-wmdrm.meter-chlg-req")]
		applicationvndmswmdrmmeterchlgreq,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-wmdrm.meter-resp")]
		applicationvndmswmdrmmeterresp,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-works")]
		applicationvndmsworks,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-wpl")]
		applicationvndmswpl,

		/// <remarks/>
		[XmlEnum("application/vnd.ms-xpsdocument")]
		applicationvndmsxpsdocument,

		/// <remarks/>
		[XmlEnum("application/vnd.musician")]
		applicationvndmusician,

		/// <remarks/>
		[XmlEnum("application/vnd.music-niff")]
		applicationvndmusicniff,

		/// <remarks/>
		[XmlEnum("application/vnd.ncd.control")]
		applicationvndncdcontrol,

		/// <remarks/>
		[XmlEnum("application/vnd.nervana")]
		applicationvndnervana,

		/// <remarks/>
		[XmlEnum("application/vnd.netfpx")]
		applicationvndnetfpx,

		/// <remarks/>
		[XmlEnum("application/vnd.neurolanguage.nlu")]
		applicationvndneurolanguagenlu,

		/// <remarks/>
		[XmlEnum("application/vnd.noblenet-directory")]
		applicationvndnoblenetdirectory,

		/// <remarks/>
		[XmlEnum("application/vnd.noblenet-sealer")]
		applicationvndnoblenetsealer,

		/// <remarks/>
		[XmlEnum("application/vnd.noblenet-web")]
		applicationvndnoblenetweb,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.catalogs")]
		applicationvndnokiacatalogs,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.conml+wbxml")]
		applicationvndnokiaconmlwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.conml+xml")]
		applicationvndnokiaconmlxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.iptv.config+xml")]
		applicationvndnokiaiptvconfigxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.iSDS-radio-presets")]
		applicationvndnokiaiSDSradiopresets,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.landmark+wbxml")]
		applicationvndnokialandmarkwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.landmark+xml")]
		applicationvndnokialandmarkxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.landmarkcollection+xml")]
		applicationvndnokialandmarkcollectionxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.ncd")]
		applicationvndnokiancd,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.n-gage.ac+xml")]
		applicationvndnokiangageacxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.n-gage.data")]
		applicationvndnokiangagedata,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.n-gage.symbian.install")]
		applicationvndnokiangagesymbianinstall,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.pcd+wbxml")]
		applicationvndnokiapcdwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.pcd+xml")]
		applicationvndnokiapcdxml,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.radio-preset")]
		applicationvndnokiaradiopreset,

		/// <remarks/>
		[XmlEnum("application/vnd.nokia.radio-presets")]
		applicationvndnokiaradiopresets,

		/// <remarks/>
		[XmlEnum("application/vnd.novadigm.EDM")]
		applicationvndnovadigmEDM,

		/// <remarks/>
		[XmlEnum("application/vnd.novadigm.EDX")]
		applicationvndnovadigmEDX,

		/// <remarks/>
		[XmlEnum("application/vnd.novadigm.EXT")]
		applicationvndnovadigmEXT,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.chart")]
		applicationvndoasisopendocumentchart,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.chart-template")]
		applicationvndoasisopendocumentcharttemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.formula")]
		applicationvndoasisopendocumentformula,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.formula-template")]
		applicationvndoasisopendocumentformulatemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.graphics")]
		applicationvndoasisopendocumentgraphics,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.graphics-template")]
		applicationvndoasisopendocumentgraphicstemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.image")]
		applicationvndoasisopendocumentimage,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.image-template")]
		applicationvndoasisopendocumentimagetemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.presentation")]
		applicationvndoasisopendocumentpresentation,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.presentation-template")]
		applicationvndoasisopendocumentpresentationtemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.spreadsheet")]
		applicationvndoasisopendocumentspreadsheet,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.spreadsheet-template")]
		applicationvndoasisopendocumentspreadsheettemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.text")]
		applicationvndoasisopendocumenttext,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.text-master")]
		applicationvndoasisopendocumenttextmaster,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.text-template")]
		applicationvndoasisopendocumenttexttemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.oasis.opendocument.text-web")]
		applicationvndoasisopendocumenttextweb,

		/// <remarks/>
		[XmlEnum("application/vnd.obn")]
		applicationvndobn,

		/// <remarks/>
		[XmlEnum("application/vnd.olpc-sugar")]
		applicationvndolpcsugar,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.associated-procedure-parameter+xml")]
		applicationvndomabcastassociatedprocedureparameterxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.drm-trigger+xml")]
		applicationvndomabcastdrmtriggerxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.imd+xml")]
		applicationvndomabcastimdxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.notification+xml")]
		applicationvndomabcastnotificationxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.sgboot")]
		applicationvndomabcastsgboot,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.sgdd+xml")]
		applicationvndomabcastsgddxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.sgdu")]
		applicationvndomabcastsgdu,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.simple-symbol-container")]
		applicationvndomabcastsimplesymbolcontainer,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.smartcard-trigger+xml")]
		applicationvndomabcastsmartcardtriggerxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.bcast.sprov+xml")]
		applicationvndomabcastsprovxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.dd2+xml")]
		applicationvndomadd2xml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.drm.risd+xml")]
		applicationvndomadrmrisdxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.group-usage-list+xml")]
		applicationvndomagroupusagelistxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.poc.groups+xml")]
		applicationvndomapocgroupsxml,

		/// <remarks/>
		[XmlEnum("application/vnd.oma.xcap-directory+xml")]
		applicationvndomaxcapdirectoryxml,

		/// <remarks/>
		[XmlEnum("application/vnd.omads-email+xml")]
		applicationvndomadsemailxml,

		/// <remarks/>
		[XmlEnum("application/vnd.omads-file+xml")]
		applicationvndomadsfilexml,

		/// <remarks/>
		[XmlEnum("application/vnd.omads-folder+xml")]
		applicationvndomadsfolderxml,

		/// <remarks/>
		[XmlEnum("application/vnd.omaloc-supl-init")]
		applicationvndomalocsuplinit,

		/// <remarks/>
		[XmlEnum("application/vnd.oma-scws-config")]
		applicationvndomascwsconfig,

		/// <remarks/>
		[XmlEnum("application/vnd.oma-scws-http-request")]
		applicationvndomascwshttprequest,

		/// <remarks/>
		[XmlEnum("application/vnd.oma-scws-http-response")]
		applicationvndomascwshttpresponse,

		/// <remarks/>
		[XmlEnum("application/vnd.openofficeorg.extension")]
		applicationvndopenofficeorgextension,

		/// <remarks/>
		[XmlEnum("application/vnd.osa.netdeploy")]
		applicationvndosanetdeploy,

		/// <remarks/>
		[XmlEnum("application/vnd.osgi.dp")]
		applicationvndosgidp,

		/// <remarks/>
		[XmlEnum("application/vnd.otps.ct-kip+xml")]
		applicationvndotpsctkipxml,

		/// <remarks/>
		[XmlEnum("application/vnd.palm")]
		applicationvndpalm,

		/// <remarks/>
		[XmlEnum("application/vnd.paos.xml")]
		applicationvndpaosxml,

		/// <remarks/>
		[XmlEnum("application/vnd.pg.format")]
		applicationvndpgformat,

		/// <remarks/>
		[XmlEnum("application/vnd.pg.osasli")]
		applicationvndpgosasli,

		/// <remarks/>
		[XmlEnum("application/vnd.piaccess.application-licence")]
		applicationvndpiaccessapplicationlicence,

		/// <remarks/>
		[XmlEnum("application/vnd.picsel")]
		applicationvndpicsel,

		/// <remarks/>
		[XmlEnum("application/vnd.poc.group-advertisement+xml")]
		applicationvndpocgroupadvertisementxml,

		/// <remarks/>
		[XmlEnum("application/vnd.pocketlearn")]
		applicationvndpocketlearn,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder6")]
		applicationvndpowerbuilder6,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder6-s")]
		applicationvndpowerbuilder6s,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder7")]
		applicationvndpowerbuilder7,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder75")]
		applicationvndpowerbuilder75,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder75-s")]
		applicationvndpowerbuilder75s,

		/// <remarks/>
		[XmlEnum("application/vnd.powerbuilder7-s")]
		applicationvndpowerbuilder7s,

		/// <remarks/>
		[XmlEnum("application/vnd.preminet")]
		applicationvndpreminet,

		/// <remarks/>
		[XmlEnum("application/vnd.previewsystems.box")]
		applicationvndpreviewsystemsbox,

		/// <remarks/>
		[XmlEnum("application/vnd.proteus.magazine")]
		applicationvndproteusmagazine,

		/// <remarks/>
		[XmlEnum("application/vnd.publishare-delta-tree")]
		applicationvndpublisharedeltatree,

		/// <remarks/>
		[XmlEnum("application/vnd.pvi.ptid1")]
		applicationvndpviptid1,

		/// <remarks/>
		[XmlEnum("application/vnd.pwg-multiplexed")]
		applicationvndpwgmultiplexed,

		/// <remarks/>
		[XmlEnum("application/vnd.pwg-xhtml-print+xml")]
		applicationvndpwgxhtmlprintxml,

		/// <remarks/>
		[XmlEnum("application/vnd.qualcomm.brew-app-res")]
		applicationvndqualcommbrewappres,

		/// <remarks/>
		[XmlEnum("application/vnd.Quark.QuarkXPress")]
		applicationvndQuarkQuarkXPress,

		/// <remarks/>
		[XmlEnum("application/vnd.rapid")]
		applicationvndrapid,

		/// <remarks/>
		[XmlEnum("application/vnd.RenLearn.rlprint")]
		applicationvndRenLearnrlprint,

		/// <remarks/>
		[XmlEnum("application/vnd.ruckus.download")]
		applicationvndruckusdownload,

		/// <remarks/>
		[XmlEnum("application/vnd.s3sms")]
		applicationvnds3sms,

		/// <remarks/>
		[XmlEnum("application/vnd.scribus")]
		applicationvndscribus,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.3df")]
		applicationvndsealed3df,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.csf")]
		applicationvndsealedcsf,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.doc")]
		applicationvndsealeddoc,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.eml")]
		applicationvndsealedeml,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.mht")]
		applicationvndsealedmht,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.net")]
		applicationvndsealednet,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.ppt")]
		applicationvndsealedppt,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.tiff")]
		applicationvndsealedtiff,

		/// <remarks/>
		[XmlEnum("application/vnd.sealed.xls")]
		applicationvndsealedxls,

		/// <remarks/>
		[XmlEnum("application/vnd.sealedmedia.softseal.html")]
		applicationvndsealedmediasoftsealhtml,

		/// <remarks/>
		[XmlEnum("application/vnd.sealedmedia.softseal.pdf")]
		applicationvndsealedmediasoftsealpdf,

		/// <remarks/>
		[XmlEnum("application/vnd.seemail")]
		applicationvndseemail,

		/// <remarks/>
		[XmlEnum("application/vnd.sema")]
		applicationvndsema,

		/// <remarks/>
		[XmlEnum("application/vnd.semd")]
		applicationvndsemd,

		/// <remarks/>
		[XmlEnum("application/vnd.semf")]
		applicationvndsemf,

		/// <remarks/>
		[XmlEnum("application/vnd.shana.informed.formdata")]
		applicationvndshanainformedformdata,

		/// <remarks/>
		[XmlEnum("application/vnd.shana.informed.formtemplate")]
		applicationvndshanainformedformtemplate,

		/// <remarks/>
		[XmlEnum("application/vnd.shana.informed.interchange")]
		applicationvndshanainformedinterchange,

		/// <remarks/>
		[XmlEnum("application/vnd.shana.informed.package")]
		applicationvndshanainformedpackage,

		/// <remarks/>
		[XmlEnum("application/vnd.SimTech-MindMapper")]
		applicationvndSimTechMindMapper,

		/// <remarks/>
		[XmlEnum("application/vnd.smaf")]
		applicationvndsmaf,

		/// <remarks/>
		[XmlEnum("application/vnd.solent.sdkm+xml")]
		applicationvndsolentsdkmxml,

		/// <remarks/>
		[XmlEnum("application/vnd.spotfire.dxp")]
		applicationvndspotfiredxp,

		/// <remarks/>
		[XmlEnum("application/vnd.spotfire.sfs")]
		applicationvndspotfiresfs,

		/// <remarks/>
		[XmlEnum("application/vnd.sss-cod")]
		applicationvndssscod,

		/// <remarks/>
		[XmlEnum("application/vnd.sss-dtf")]
		applicationvndsssdtf,

		/// <remarks/>
		[XmlEnum("application/vnd.sss-ntf")]
		applicationvndsssntf,

		/// <remarks/>
		[XmlEnum("application/vnd.street-stream")]
		applicationvndstreetstream,

		/// <remarks/>
		[XmlEnum("application/vnd.sun.wadl+xml")]
		applicationvndsunwadlxml,

		/// <remarks/>
		[XmlEnum("application/vnd.sus-calendar")]
		applicationvndsuscalendar,

		/// <remarks/>
		[XmlEnum("application/vnd.svd")]
		applicationvndsvd,

		/// <remarks/>
		[XmlEnum("application/vnd.swiftview-ics")]
		applicationvndswiftviewics,

		/// <remarks/>
		[XmlEnum("application/vnd.syncml.dm+wbxml")]
		applicationvndsyncmldmwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.syncml.dm+xml")]
		applicationvndsyncmldmxml,

		/// <remarks/>
		[XmlEnum("application/vnd.syncml.ds.notification")]
		applicationvndsyncmldsnotification,

		/// <remarks/>
		[XmlEnum("application/vnd.syncml+xml")]
		applicationvndsyncmlxml,

		/// <remarks/>
		[XmlEnum("application/vnd.tao.intent-module-archive")]
		applicationvndtaointentmodulearchive,

		/// <remarks/>
		[XmlEnum("application/vnd.tmobile-livetv")]
		applicationvndtmobilelivetv,

		/// <remarks/>
		[XmlEnum("application/vnd.trid.tpt")]
		applicationvndtridtpt,

		/// <remarks/>
		[XmlEnum("application/vnd.triscape.mxs")]
		applicationvndtriscapemxs,

		/// <remarks/>
		[XmlEnum("application/vnd.trueapp")]
		applicationvndtrueapp,

		/// <remarks/>
		[XmlEnum("application/vnd.truedoc")]
		applicationvndtruedoc,

		/// <remarks/>
		[XmlEnum("application/vnd.ufdl")]
		applicationvndufdl,

		/// <remarks/>
		[XmlEnum("application/vnd.uiq.theme")]
		applicationvnduiqtheme,

		/// <remarks/>
		[XmlEnum("application/vnd.umajin")]
		applicationvndumajin,

		/// <remarks/>
		[XmlEnum("application/vnd.unity")]
		applicationvndunity,

		/// <remarks/>
		[XmlEnum("application/vnd.uoml+xml")]
		applicationvnduomlxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.alert")]
		applicationvnduplanetalert,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.alert-wbxml")]
		applicationvnduplanetalertwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.bearer-choice")]
		applicationvnduplanetbearerchoice,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.bearer-choice-wbxml")]
		applicationvnduplanetbearerchoicewbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.cacheop")]
		applicationvnduplanetcacheop,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.cacheop-wbxml")]
		applicationvnduplanetcacheopwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.channel")]
		applicationvnduplanetchannel,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.channel-wbxml")]
		applicationvnduplanetchannelwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.list")]
		applicationvnduplanetlist,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.listcmd")]
		applicationvnduplanetlistcmd,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.listcmd-wbxml")]
		applicationvnduplanetlistcmdwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.list-wbxml")]
		applicationvnduplanetlistwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.uplanet.signal")]
		applicationvnduplanetsignal,

		/// <remarks/>
		[XmlEnum("application/vnd.vcx")]
		applicationvndvcx,

		/// <remarks/>
		[XmlEnum("application/vnd.vectorworks")]
		applicationvndvectorworks,

		/// <remarks/>
		[XmlEnum("application/vnd.vd-study")]
		applicationvndvdstudy,

		/// <remarks/>
		[XmlEnum("application/vnd.vidsoft.vidconference")]
		applicationvndvidsoftvidconference,

		/// <remarks/>
		[XmlEnum("application/vnd.visio")]
		applicationvndvisio,

		/// <remarks/>
		[XmlEnum("application/vnd.visionary")]
		applicationvndvisionary,

		/// <remarks/>
		[XmlEnum("application/vnd.vividence.scriptfile")]
		applicationvndvividencescriptfile,

		/// <remarks/>
		[XmlEnum("application/vnd.vsf")]
		applicationvndvsf,

		/// <remarks/>
		[XmlEnum("application/vnd.wap.sic")]
		applicationvndwapsic,

		/// <remarks/>
		[XmlEnum("application/vnd.wap.slc")]
		applicationvndwapslc,

		/// <remarks/>
		[XmlEnum("application/vnd.wap.wbxml")]
		applicationvndwapwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.wap.wmlc")]
		applicationvndwapwmlc,

		/// <remarks/>
		[XmlEnum("application/vnd.wap.wmlscriptc")]
		applicationvndwapwmlscriptc,

		/// <remarks/>
		[XmlEnum("application/vnd.webturbo")]
		applicationvndwebturbo,

		/// <remarks/>
		[XmlEnum("application/vnd.wfa.wsc")]
		applicationvndwfawsc,

		/// <remarks/>
		[XmlEnum("application/vnd.wordperfect")]
		applicationvndwordperfect,

		/// <remarks/>
		[XmlEnum("application/vnd.wqd")]
		applicationvndwqd,

		/// <remarks/>
		[XmlEnum("application/vnd.wrq-hp3000-labelled")]
		applicationvndwrqhp3000labelled,

		/// <remarks/>
		[XmlEnum("application/vnd.wt.stf")]
		applicationvndwtstf,

		/// <remarks/>
		[XmlEnum("application/vnd.wv.csp+xml")]
		applicationvndwvcspxml,

		/// <remarks/>
		[XmlEnum("application/vnd.wv.csp+wbxml")]
		applicationvndwvcspwbxml,

		/// <remarks/>
		[XmlEnum("application/vnd.wv.ssp+xml")]
		applicationvndwvsspxml,

		/// <remarks/>
		[XmlEnum("application/vnd.xara")]
		applicationvndxara,

		/// <remarks/>
		[XmlEnum("application/vnd.xfdl")]
		applicationvndxfdl,

		/// <remarks/>
		[XmlEnum("application/vnd.xmpie.cpkg")]
		applicationvndxmpiecpkg,

		/// <remarks/>
		[XmlEnum("application/vnd.xmpie.dpkg")]
		applicationvndxmpiedpkg,

		/// <remarks/>
		[XmlEnum("application/vnd.xmpie.plan")]
		applicationvndxmpieplan,

		/// <remarks/>
		[XmlEnum("application/vnd.xmpie.ppkg")]
		applicationvndxmpieppkg,

		/// <remarks/>
		[XmlEnum("application/vnd.xmpie.xlim")]
		applicationvndxmpiexlim,

		/// <remarks/>
		[XmlEnum("application/vnd.yamaha.hv-dic")]
		applicationvndyamahahvdic,

		/// <remarks/>
		[XmlEnum("application/vnd.yamaha.hv-script")]
		applicationvndyamahahvscript,

		/// <remarks/>
		[XmlEnum("application/vnd.yamaha.hv-voice")]
		applicationvndyamahahvvoice,

		/// <remarks/>
		[XmlEnum("application/vnd.yamaha.smaf-audio")]
		applicationvndyamahasmafaudio,

		/// <remarks/>
		[XmlEnum("application/vnd.yamaha.smaf-phrase")]
		applicationvndyamahasmafphrase,

		/// <remarks/>
		[XmlEnum("application/vnd.yellowriver-custom-menu")]
		applicationvndyellowrivercustommenu,

		/// <remarks/>
		[XmlEnum("application/vnd.zzazz.deck+xml")]
		applicationvndzzazzdeckxml,

		/// <remarks/>
		[XmlEnum("application/voicexml+xml")]
		applicationvoicexmlxml,

		/// <remarks/>
		[XmlEnum("application/watcherinfo+xml")]
		applicationwatcherinfoxml,

		/// <remarks/>
		[XmlEnum("application/whoispp-query")]
		applicationwhoisppquery,

		/// <remarks/>
		[XmlEnum("application/whoispp-response")]
		applicationwhoisppresponse,

		/// <remarks/>
		[XmlEnum("application/wita")]
		applicationwita,

		/// <remarks/>
		[XmlEnum("application/wordperfect5.1")]
		applicationwordperfect51,

		/// <remarks/>
		[XmlEnum("application/x400-bp")]
		applicationx400bp,

		/// <remarks/>
		[XmlEnum("application/xcap-att+xml")]
		applicationxcapattxml,

		/// <remarks/>
		[XmlEnum("application/xcap-caps+xml")]
		applicationxcapcapsxml,

		/// <remarks/>
		[XmlEnum("application/xcap-el+xml")]
		applicationxcapelxml,

		/// <remarks/>
		[XmlEnum("application/xcap-error+xml")]
		applicationxcaperrorxml,

		/// <remarks/>
		[XmlEnum("application/xcap-ns+xml")]
		applicationxcapnsxml,

		/// <remarks/>
		[XmlEnum("application/xenc+xml")]
		applicationxencxml,

		/// <remarks/>
		[XmlEnum("application/xhtml-voice+xml (Obsolete)")]
		applicationxhtmlvoicexmlObsolete,

		/// <remarks/>
		[XmlEnum("application/xhtml+xml")]
		applicationxhtmlxml,

		/// <remarks/>
		[XmlEnum("application/xml")]
		applicationxml,

		/// <remarks/>
		[XmlEnum("application/xml-dtd")]
		applicationxmldtd,

		/// <remarks/>
		[XmlEnum("application/xml-external-parsed-entity")]
		applicationxmlexternalparsedentity,

		/// <remarks/>
		[XmlEnum("application/xmpp+xml")]
		applicationxmppxml,

		/// <remarks/>
		[XmlEnum("application/xop+xml")]
		applicationxopxml,

		/// <remarks/>
		[XmlEnum("application/xv+xml")]
		applicationxvxml,

		/// <remarks/>
		[XmlEnum("application/zip")]
		applicationzip,

		/// <remarks/>
		audio,

		/// <remarks/>
		[XmlEnum("audio/32kadpcm")]
		audio32kadpcm,

		/// <remarks/>
		[XmlEnum("audio/3gpp")]
		audio3gpp,

		/// <remarks/>
		[XmlEnum("audio/3gpp2")]
		audio3gpp2,

		/// <remarks/>
		[XmlEnum("audio/ac3")]
		audioac3,

		/// <remarks/>
		[XmlEnum("audio/AMR")]
		audioAMR,

		/// <remarks/>
		[XmlEnum("audio/AMR-WB")]
		audioAMRWB,

		/// <remarks/>
		[XmlEnum("audio/amr-wb+")]
		audioamrwb,

		/// <remarks/>
		[XmlEnum("audio/asc")]
		audioasc,

		/// <remarks/>
		[XmlEnum("audio/basic")]
		audiobasic,

		/// <remarks/>
		[XmlEnum("audio/BV16")]
		audioBV16,

		/// <remarks/>
		[XmlEnum("audio/BV32")]
		audioBV32,

		/// <remarks/>
		[XmlEnum("audio/clearmode")]
		audioclearmode,

		/// <remarks/>
		[XmlEnum("audio/CN")]
		audioCN,

		/// <remarks/>
		[XmlEnum("audio/DAT12")]
		audioDAT12,

		/// <remarks/>
		[XmlEnum("audio/dls")]
		audiodls,

		/// <remarks/>
		[XmlEnum("audio/dsr-es201108")]
		audiodsres201108,

		/// <remarks/>
		[XmlEnum("audio/dsr-es202050")]
		audiodsres202050,

		/// <remarks/>
		[XmlEnum("audio/dsr-es202211")]
		audiodsres202211,

		/// <remarks/>
		[XmlEnum("audio/dsr-es202212")]
		audiodsres202212,

		/// <remarks/>
		[XmlEnum("audio/eac3")]
		audioeac3,

		/// <remarks/>
		[XmlEnum("audio/DVI4")]
		audioDVI4,

		/// <remarks/>
		[XmlEnum("audio/EVRC")]
		audioEVRC,

		/// <remarks/>
		[XmlEnum("audio/EVRC0")]
		audioEVRC0,

		/// <remarks/>
		[XmlEnum("audio/EVRC1")]
		audioEVRC1,

		/// <remarks/>
		[XmlEnum("audio/EVRCB")]
		audioEVRCB,

		/// <remarks/>
		[XmlEnum("audio/EVRCB0")]
		audioEVRCB0,

		/// <remarks/>
		[XmlEnum("audio/EVRCB1")]
		audioEVRCB1,

		/// <remarks/>
		[XmlEnum("audio/EVRC-QCP")]
		audioEVRCQCP,

		/// <remarks/>
		[XmlEnum("audio/example")]
		audioexample,

		/// <remarks/>
		[XmlEnum("audio/G722")]
		audioG722,

		/// <remarks/>
		[XmlEnum("audio/G7221")]
		audioG7221,

		/// <remarks/>
		[XmlEnum("audio/G723")]
		audioG723,

		/// <remarks/>
		[XmlEnum("audio/G726-16")]
		audioG72616,

		/// <remarks/>
		[XmlEnum("audio/G726-24")]
		audioG72624,

		/// <remarks/>
		[XmlEnum("audio/G726-32")]
		audioG72632,

		/// <remarks/>
		[XmlEnum("audio/G726-40")]
		audioG72640,

		/// <remarks/>
		[XmlEnum("audio/G728")]
		audioG728,

		/// <remarks/>
		[XmlEnum("audio/G729")]
		audioG729,

		/// <remarks/>
		[XmlEnum("audio/G7291")]
		audioG7291,

		/// <remarks/>
		[XmlEnum("audio/G729D")]
		audioG729D,

		/// <remarks/>
		[XmlEnum("audio/G729E")]
		audioG729E,

		/// <remarks/>
		[XmlEnum("audio/GSM")]
		audioGSM,

		/// <remarks/>
		[XmlEnum("audio/GSM-EFR")]
		audioGSMEFR,

		/// <remarks/>
		[XmlEnum("audio/iLBC")]
		audioiLBC,

		/// <remarks/>
		[XmlEnum("audio/L8")]
		audioL8,

		/// <remarks/>
		[XmlEnum("audio/L16")]
		audioL16,

		/// <remarks/>
		[XmlEnum("audio/L20")]
		audioL20,

		/// <remarks/>
		[XmlEnum("audio/L24")]
		audioL24,

		/// <remarks/>
		[XmlEnum("audio/LPC")]
		audioLPC,

		/// <remarks/>
		[XmlEnum("audio/mobile-xmf")]
		audiomobilexmf,

		/// <remarks/>
		[XmlEnum("audio/MPA")]
		audioMPA,

		/// <remarks/>
		[XmlEnum("audio/mp4")]
		audiomp4,

		/// <remarks/>
		[XmlEnum("audio/MP4A-LATM")]
		audioMP4ALATM,

		/// <remarks/>
		[XmlEnum("audio/mpa-robust")]
		audiomparobust,

		/// <remarks/>
		[XmlEnum("audio/mpeg")]
		audiompeg,

		/// <remarks/>
		[XmlEnum("audio/mpeg4-generic")]
		audiompeg4generic,

		/// <remarks/>
		[XmlEnum("audio/parityfec")]
		audioparityfec,

		/// <remarks/>
		[XmlEnum("audio/PCMA")]
		audioPCMA,

		/// <remarks/>
		[XmlEnum("audio/PCMU")]
		audioPCMU,

		/// <remarks/>
		[XmlEnum("audio/prs.sid")]
		audioprssid,

		/// <remarks/>
		[XmlEnum("audio/QCELP")]
		audioQCELP,

		/// <remarks/>
		[XmlEnum("audio/RED")]
		audioRED,

		/// <remarks/>
		[XmlEnum("audio/rtp-enc-aescm128")]
		audiortpencaescm128,

		/// <remarks/>
		[XmlEnum("audio/rtp-midi")]
		audiortpmidi,

		/// <remarks/>
		[XmlEnum("audio/rtx")]
		audiortx,

		/// <remarks/>
		[XmlEnum("audio/SMV")]
		audioSMV,

		/// <remarks/>
		[XmlEnum("audio/SMV0")]
		audioSMV0,

		/// <remarks/>
		[XmlEnum("audio/SMV-QCP")]
		audioSMVQCP,

		/// <remarks/>
		[XmlEnum("audio/sp-midi")]
		audiospmidi,

		/// <remarks/>
		[XmlEnum("audio/t140c")]
		audiot140c,

		/// <remarks/>
		[XmlEnum("audio/t38")]
		audiot38,

		/// <remarks/>
		[XmlEnum("audio/telephone-event")]
		audiotelephoneevent,

		/// <remarks/>
		[XmlEnum("audio/tone")]
		audiotone,

		/// <remarks/>
		[XmlEnum("audio/VDVI")]
		audioVDVI,

		/// <remarks/>
		[XmlEnum("audio/VMR-WB")]
		audioVMRWB,

		/// <remarks/>
		[XmlEnum("audio/vnd.3gpp.iufp")]
		audiovnd3gppiufp,

		/// <remarks/>
		[XmlEnum("audio/vnd.4SB")]
		audiovnd4SB,

		/// <remarks/>
		[XmlEnum("audio/vnd.audiokoz")]
		audiovndaudiokoz,

		/// <remarks/>
		[XmlEnum("audio/vnd.CELP")]
		audiovndCELP,

		/// <remarks/>
		[XmlEnum("audio/vnd.cisco.nse")]
		audiovndcisconse,

		/// <remarks/>
		[XmlEnum("audio/vnd.cmles.radio-events")]
		audiovndcmlesradioevents,

		/// <remarks/>
		[XmlEnum("audio/vnd.cns.anp1")]
		audiovndcnsanp1,

		/// <remarks/>
		[XmlEnum("audio/vnd.cns.inf1")]
		audiovndcnsinf1,

		/// <remarks/>
		[XmlEnum("audio/vnd.digital-winds")]
		audiovnddigitalwinds,

		/// <remarks/>
		[XmlEnum("audio/vnd.dlna.adts")]
		audiovnddlnaadts,

		/// <remarks/>
		[XmlEnum("audio/vnd.dolby.mlp")]
		audiovnddolbymlp,

		/// <remarks/>
		[XmlEnum("audio/vnd.everad.plj")]
		audiovndeveradplj,

		/// <remarks/>
		[XmlEnum("audio/vnd.hns.audio")]
		audiovndhnsaudio,

		/// <remarks/>
		[XmlEnum("audio/vnd.lucent.voice")]
		audiovndlucentvoice,

		/// <remarks/>
		[XmlEnum("audio/vnd.nokia.mobile-xmf")]
		audiovndnokiamobilexmf,

		/// <remarks/>
		[XmlEnum("audio/vnd.nortel.vbk")]
		audiovndnortelvbk,

		/// <remarks/>
		[XmlEnum("audio/vnd.nuera.ecelp4800")]
		audiovndnueraecelp4800,

		/// <remarks/>
		[XmlEnum("audio/vnd.nuera.ecelp7470")]
		audiovndnueraecelp7470,

		/// <remarks/>
		[XmlEnum("audio/vnd.nuera.ecelp9600")]
		audiovndnueraecelp9600,

		/// <remarks/>
		[XmlEnum("audio/vnd.octel.sbc")]
		audiovndoctelsbc,

		/// <remarks/>
		[XmlEnum("audio/vnd.qcelp - DEPRECATED - Please use audio/qcelp")]
		audiovndqcelpDEPRECATEDPleaseuseaudioqcelp,

		/// <remarks/>
		[XmlEnum("audio/vnd.rhetorex.32kadpcm")]
		audiovndrhetorex32kadpcm,

		/// <remarks/>
		[XmlEnum("audio/vnd.sealedmedia.softseal.mpeg")]
		audiovndsealedmediasoftsealmpeg,

		/// <remarks/>
		[XmlEnum("audio/vnd.vmx.cvsd")]
		audiovndvmxcvsd,

		/// <remarks/>
		example,

		/// <remarks/>
		image,

		/// <remarks/>
		[XmlEnum("image/cgm")]
		imagecgm,

		/// <remarks/>
		[XmlEnum("image/example")]
		imageexample,

		/// <remarks/>
		[XmlEnum("image/fits")]
		imagefits,

		/// <remarks/>
		[XmlEnum("image/g3fax")]
		imageg3fax,

		/// <remarks/>
		[XmlEnum("image/gif")]
		imagegif,

		/// <remarks/>
		[XmlEnum("image/ief")]
		imageief,

		/// <remarks/>
		[XmlEnum("image/jp2")]
		imagejp2,

		/// <remarks/>
		[XmlEnum("image/jpeg")]
		imagejpeg,

		/// <remarks/>
		[XmlEnum("image/jpm")]
		imagejpm,

		/// <remarks/>
		[XmlEnum("image/jpx")]
		imagejpx,

		/// <remarks/>
		[XmlEnum("image/naplps")]
		imagenaplps,

		/// <remarks/>
		[XmlEnum("image/png")]
		imagepng,

		/// <remarks/>
		[XmlEnum("image/prs.btif")]
		imageprsbtif,

		/// <remarks/>
		[XmlEnum("image/prs.pti")]
		imageprspti,

		/// <remarks/>
		[XmlEnum("image/t38")]
		imaget38,

		/// <remarks/>
		[XmlEnum("image/tiff")]
		imagetiff,

		/// <remarks/>
		[XmlEnum("image/tiff-fx")]
		imagetifffx,

		/// <remarks/>
		[XmlEnum("image/vnd.adobe.photoshop")]
		imagevndadobephotoshop,

		/// <remarks/>
		[XmlEnum("image/vnd.cns.inf2")]
		imagevndcnsinf2,

		/// <remarks/>
		[XmlEnum("image/vnd.djvu")]
		imagevnddjvu,

		/// <remarks/>
		[XmlEnum("image/vnd.dwg")]
		imagevnddwg,

		/// <remarks/>
		[XmlEnum("image/vnd.dxf")]
		imagevnddxf,

		/// <remarks/>
		[XmlEnum("image/vnd.fastbidsheet")]
		imagevndfastbidsheet,

		/// <remarks/>
		[XmlEnum("image/vnd.fpx")]
		imagevndfpx,

		/// <remarks/>
		[XmlEnum("image/vnd.fst")]
		imagevndfst,

		/// <remarks/>
		[XmlEnum("image/vnd.fujixerox.edmics-mmr")]
		imagevndfujixeroxedmicsmmr,

		/// <remarks/>
		[XmlEnum("image/vnd.fujixerox.edmics-rlc")]
		imagevndfujixeroxedmicsrlc,

		/// <remarks/>
		[XmlEnum("image/vnd.globalgraphics.pgb")]
		imagevndglobalgraphicspgb,

		/// <remarks/>
		[XmlEnum("image/vnd.microsoft.icon")]
		imagevndmicrosofticon,

		/// <remarks/>
		[XmlEnum("image/vnd.mix")]
		imagevndmix,

		/// <remarks/>
		[XmlEnum("image/vnd.ms-modi")]
		imagevndmsmodi,

		/// <remarks/>
		[XmlEnum("image/vnd.net-fpx")]
		imagevndnetfpx,

		/// <remarks/>
		[XmlEnum("image/vnd.sealed.png")]
		imagevndsealedpng,

		/// <remarks/>
		[XmlEnum("image/vnd.sealedmedia.softseal.gif")]
		imagevndsealedmediasoftsealgif,

		/// <remarks/>
		[XmlEnum("image/vnd.sealedmedia.softseal.jpg")]
		imagevndsealedmediasoftsealjpg,

		/// <remarks/>
		[XmlEnum("image/vnd.svf")]
		imagevndsvf,

		/// <remarks/>
		[XmlEnum("image/vnd.wap.wbmp")]
		imagevndwapwbmp,

		/// <remarks/>
		[XmlEnum("image/vnd.xiff")]
		imagevndxiff,

		/// <remarks/>
		message,

		/// <remarks/>
		[XmlEnum("message/CPIM")]
		messageCPIM,

		/// <remarks/>
		[XmlEnum("message/delivery-status")]
		messagedeliverystatus,

		/// <remarks/>
		[XmlEnum("message/disposition-notification")]
		messagedispositionnotification,

		/// <remarks/>
		[XmlEnum("message/example")]
		messageexample,

		/// <remarks/>
		[XmlEnum("message/external-body")]
		messageexternalbody,

		/// <remarks/>
		[XmlEnum("message/http")]
		messagehttp,

		/// <remarks/>
		[XmlEnum("message/news")]
		messagenews,

		/// <remarks/>
		[XmlEnum("message/partial")]
		messagepartial,

		/// <remarks/>
		[XmlEnum("message/rfc822")]
		messagerfc822,

		/// <remarks/>
		[XmlEnum("message/s-http")]
		messageshttp,

		/// <remarks/>
		[XmlEnum("message/sip")]
		messagesip,

		/// <remarks/>
		[XmlEnum("message/sipfrag")]
		messagesipfrag,

		/// <remarks/>
		[XmlEnum("message/tracking-status")]
		messagetrackingstatus,

		/// <remarks/>
		model,

		/// <remarks/>
		[XmlEnum("model/example")]
		modelexample,

		/// <remarks/>
		[XmlEnum("model/iges")]
		modeliges,

		/// <remarks/>
		[XmlEnum("model/mesh")]
		modelmesh,

		/// <remarks/>
		[XmlEnum("model/vnd.dwf")]
		modelvnddwf,

		/// <remarks/>
		[XmlEnum("model/vnd.flatland.3dml")]
		modelvndflatland3dml,

		/// <remarks/>
		[XmlEnum("model/vnd.gdl")]
		modelvndgdl,

		/// <remarks/>
		[XmlEnum("model/vnd.gs-gdl")]
		modelvndgsgdl,

		/// <remarks/>
		[XmlEnum("model/vnd.gtw")]
		modelvndgtw,

		/// <remarks/>
		[XmlEnum("model/vnd.moml+xml")]
		modelvndmomlxml,

		/// <remarks/>
		[XmlEnum("model/vnd.mts")]
		modelvndmts,

		/// <remarks/>
		[XmlEnum("model/vnd.parasolid.transmit.binary")]
		modelvndparasolidtransmitbinary,

		/// <remarks/>
		[XmlEnum("model/vnd.parasolid.transmit.text")]
		modelvndparasolidtransmittext,

		/// <remarks/>
		[XmlEnum("model/vnd.vtu")]
		modelvndvtu,

		/// <remarks/>
		[XmlEnum("model/vrml")]
		modelvrml,

		/// <remarks/>
		multipart,

		/// <remarks/>
		[XmlEnum("multipart/alternative")]
		multipartalternative,

		/// <remarks/>
		[XmlEnum("multipart/appledouble")]
		multipartappledouble,

		/// <remarks/>
		[XmlEnum("multipart/byteranges")]
		multipartbyteranges,

		/// <remarks/>
		[XmlEnum("multipart/digest")]
		multipartdigest,

		/// <remarks/>
		[XmlEnum("multipart/encrypted")]
		multipartencrypted,

		/// <remarks/>
		[XmlEnum("multipart/example")]
		multipartexample,

		/// <remarks/>
		[XmlEnum("multipart/form-data")]
		multipartformdata,

		/// <remarks/>
		[XmlEnum("multipart/header-set")]
		multipartheaderset,

		/// <remarks/>
		[XmlEnum("multipart/mixed")]
		multipartmixed,

		/// <remarks/>
		[XmlEnum("multipart/parallel")]
		multipartparallel,

		/// <remarks/>
		[XmlEnum("multipart/related")]
		multipartrelated,

		/// <remarks/>
		[XmlEnum("multipart/report")]
		multipartreport,

		/// <remarks/>
		[XmlEnum("multipart/signed")]
		multipartsigned,

		/// <remarks/>
		[XmlEnum("multipart/voice-message")]
		multipartvoicemessage,

		/// <remarks/>
		text,

		/// <remarks/>
		[XmlEnum("text/calendar")]
		textcalendar,

		/// <remarks/>
		[XmlEnum("text/css")]
		textcss,

		/// <remarks/>
		[XmlEnum("text/csv")]
		textcsv,

		/// <remarks/>
		[XmlEnum("text/directory")]
		textdirectory,

		/// <remarks/>
		[XmlEnum("text/dns")]
		textdns,

		/// <remarks/>
		[XmlEnum("text/ecmascript (obsolete)")]
		textecmascriptobsolete,

		/// <remarks/>
		[XmlEnum("text/enriched")]
		textenriched,

		/// <remarks/>
		[XmlEnum("text/example")]
		textexample,

		/// <remarks/>
		[XmlEnum("text/html")]
		texthtml,

		/// <remarks/>
		[XmlEnum("text/javascript (obsolete)")]
		textjavascriptobsolete,

		/// <remarks/>
		[XmlEnum("text/parityfec")]
		textparityfec,

		/// <remarks/>
		[XmlEnum("text/plain")]
		textplain,

		/// <remarks/>
		[XmlEnum("text/prs.fallenstein.rst")]
		textprsfallensteinrst,

		/// <remarks/>
		[XmlEnum("text/prs.lines.tag")]
		textprslinestag,

		/// <remarks/>
		[XmlEnum("text/RED")]
		textRED,

		/// <remarks/>
		[XmlEnum("text/rfc822-headers")]
		textrfc822headers,

		/// <remarks/>
		[XmlEnum("text/richtext")]
		textrichtext,

		/// <remarks/>
		[XmlEnum("text/rtf")]
		textrtf,

		/// <remarks/>
		[XmlEnum("text/rtp-enc-aescm128")]
		textrtpencaescm128,

		/// <remarks/>
		[XmlEnum("text/rtx")]
		textrtx,

		/// <remarks/>
		[XmlEnum("text/sgml")]
		textsgml,

		/// <remarks/>
		[XmlEnum("text/t140")]
		textt140,

		/// <remarks/>
		[XmlEnum("text/tab-separated-values")]
		texttabseparatedvalues,

		/// <remarks/>
		[XmlEnum("text/troff")]
		texttroff,

		/// <remarks/>
		[XmlEnum("text/uri-list")]
		texturilist,

		/// <remarks/>
		[XmlEnum("text/vnd.abc")]
		textvndabc,

		/// <remarks/>
		[XmlEnum("text/vnd.curl")]
		textvndcurl,

		/// <remarks/>
		[XmlEnum("text/vnd.DMClientScript")]
		textvndDMClientScript,

		/// <remarks/>
		[XmlEnum("text/vnd.esmertec.theme-descriptor")]
		textvndesmertecthemedescriptor,

		/// <remarks/>
		[XmlEnum("text/vnd.fly")]
		textvndfly,

		/// <remarks/>
		[XmlEnum("text/vnd.fmi.flexstor")]
		textvndfmiflexstor,

		/// <remarks/>
		[XmlEnum("text/vnd.in3d.3dml")]
		textvndin3d3dml,

		/// <remarks/>
		[XmlEnum("text/vnd.in3d.spot")]
		textvndin3dspot,

		/// <remarks/>
		[XmlEnum("text/vnd.IPTC.NewsML")]
		textvndIPTCNewsML,

		/// <remarks/>
		[XmlEnum("text/vnd.IPTC.NITF")]
		textvndIPTCNITF,

		/// <remarks/>
		[XmlEnum("text/vnd.latex-z")]
		textvndlatexz,

		/// <remarks/>
		[XmlEnum("text/vnd.motorola.reflex")]
		textvndmotorolareflex,

		/// <remarks/>
		[XmlEnum("text/vnd.ms-mediapackage")]
		textvndmsmediapackage,

		/// <remarks/>
		[XmlEnum("text/vnd.net2phone.commcenter.command")]
		textvndnet2phonecommcentercommand,

		/// <remarks/>
		[XmlEnum("text/vnd.sun.j2me.app-descriptor")]
		textvndsunj2meappdescriptor,

		/// <remarks/>
		[XmlEnum("text/vnd.trolltech.linguist")]
		textvndtrolltechlinguist,

		/// <remarks/>
		[XmlEnum("text/vnd.wap.si")]
		textvndwapsi,

		/// <remarks/>
		[XmlEnum("text/vnd.wap.sl")]
		textvndwapsl,

		/// <remarks/>
		[XmlEnum("text/vnd.wap.wml")]
		textvndwapwml,

		/// <remarks/>
		[XmlEnum("text/vnd.wap.wmlscript")]
		textvndwapwmlscript,

		/// <remarks/>
		[XmlEnum("text/xml")]
		textxml,

		/// <remarks/>
		[XmlEnum("text/xml-external-parsed-entity")]
		textxmlexternalparsedentity,

		/// <remarks/>
		video,

		/// <remarks/>
		[XmlEnum("video/3gpp")]
		video3gpp,

		/// <remarks/>
		[XmlEnum("video/3gpp2")]
		video3gpp2,

		/// <remarks/>
		[XmlEnum("video/3gpp-tt")]
		video3gpptt,

		/// <remarks/>
		[XmlEnum("video/BMPEG")]
		videoBMPEG,

		/// <remarks/>
		[XmlEnum("video/BT656")]
		videoBT656,

		/// <remarks/>
		[XmlEnum("video/CelB")]
		videoCelB,

		/// <remarks/>
		[XmlEnum("video/DV")]
		videoDV,

		/// <remarks/>
		[XmlEnum("video/example")]
		videoexample,

		/// <remarks/>
		[XmlEnum("video/H261")]
		videoH261,

		/// <remarks/>
		[XmlEnum("video/H263")]
		videoH263,

		/// <remarks/>
		[XmlEnum("video/H263-1998")]
		videoH2631998,

		/// <remarks/>
		[XmlEnum("video/H263-2000")]
		videoH2632000,

		/// <remarks/>
		[XmlEnum("video/H264")]
		videoH264,

		/// <remarks/>
		[XmlEnum("video/JPEG")]
		videoJPEG,

		/// <remarks/>
		[XmlEnum("video/MJ2")]
		videoMJ2,

		/// <remarks/>
		[XmlEnum("video/MP1S")]
		videoMP1S,

		/// <remarks/>
		[XmlEnum("video/MP2P")]
		videoMP2P,

		/// <remarks/>
		[XmlEnum("video/MP2T")]
		videoMP2T,

		/// <remarks/>
		[XmlEnum("video/mp4")]
		videomp4,

		/// <remarks/>
		[XmlEnum("video/MP4V-ES")]
		videoMP4VES,

		/// <remarks/>
		[XmlEnum("video/MPV")]
		videoMPV,

		/// <remarks/>
		[XmlEnum("video/mpeg")]
		videompeg,

		/// <remarks/>
		[XmlEnum("video/mpeg4-generic")]
		videompeg4generic,

		/// <remarks/>
		[XmlEnum("video/nv")]
		videonv,

		/// <remarks/>
		[XmlEnum("video/parityfec")]
		videoparityfec,

		/// <remarks/>
		[XmlEnum("video/pointer")]
		videopointer,

		/// <remarks/>
		[XmlEnum("video/quicktime")]
		videoquicktime,

		/// <remarks/>
		[XmlEnum("video/raw")]
		videoraw,

		/// <remarks/>
		[XmlEnum("video/rtp-enc-aescm128")]
		videortpencaescm128,

		/// <remarks/>
		[XmlEnum("video/rtx")]
		videortx,

		/// <remarks/>
		[XmlEnum("video/SMPTE292M")]
		videoSMPTE292M,

		/// <remarks/>
		[XmlEnum("video/vc1")]
		videovc1,

		/// <remarks/>
		[XmlEnum("video/vnd.dlna.mpeg-tts")]
		videovnddlnampegtts,

		/// <remarks/>
		[XmlEnum("video/vnd.fvt")]
		videovndfvt,

		/// <remarks/>
		[XmlEnum("video/vnd.hns.video")]
		videovndhnsvideo,

		/// <remarks/>
		[XmlEnum("video/vnd.motorola.video")]
		videovndmotorolavideo,

		/// <remarks/>
		[XmlEnum("video/vnd.motorola.videop")]
		videovndmotorolavideop,

		/// <remarks/>
		[XmlEnum("video/vnd.mpegurl")]
		videovndmpegurl,

		/// <remarks/>
		[XmlEnum("video/vnd.nokia.interleaved-multimedia")]
		videovndnokiainterleavedmultimedia,

		/// <remarks/>
		[XmlEnum("video/vnd.nokia.videovoip")]
		videovndnokiavideovoip,

		/// <remarks/>
		[XmlEnum("video/vnd.objectvideo")]
		videovndobjectvideo,

		/// <remarks/>
		[XmlEnum("video/vnd.sealed.mpeg1")]
		videovndsealedmpeg1,

		/// <remarks/>
		[XmlEnum("video/vnd.sealed.mpeg4")]
		videovndsealedmpeg4,

		/// <remarks/>
		[XmlEnum("video/vnd.sealed.swf")]
		videovndsealedswf,

		/// <remarks/>
		[XmlEnum("video/vnd.sealedmedia.softseal.mov")]
		videovndsealedmediasoftsealmov,

		/// <remarks/>
		[XmlEnum("video/vnd.vivo")]
		videovndvivo,
	}
}