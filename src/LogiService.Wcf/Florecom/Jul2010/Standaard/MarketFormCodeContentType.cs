﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:MarketForm:1:0")]
	public enum MarketFormCodeContentType
	{

		/// <remarks/>
		[XmlEnum("001")]
		Item001,

		/// <remarks/>
		[XmlEnum("002")]
		Item002,

		/// <remarks/>
		[XmlEnum("003")]
		Item003,

		/// <remarks/>
		[XmlEnum("004")]
		Item004,
	}
}