﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:3")]
	public class PostBoxAddressType
	{
		/// <remarks/>
		public CodeType PostcodeCode { get; set; }

		/// <remarks/>
		public TextType PostOfficeBox { get; set; }

		/// <remarks/>
		public TextType CityName { get; set; }

		/// <remarks/>
		public IDType CountryID { get; set; }

		/// <remarks/>
		public TextType CountrySubDivisionName { get; set; }

		/// <remarks/>
		public TextType CountryName { get; set; }
	}
}