﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:PriceTypeCode:D09A")]
	public enum PriceTypeCodeContentType
	{

		/// <remarks/>
		AA,

		/// <remarks/>
		AB,

		/// <remarks/>
		AC,

		/// <remarks/>
		AD,

		/// <remarks/>
		AE,

		/// <remarks/>
		AI,

		/// <remarks/>
		AQ,

		/// <remarks/>
		CA,

		/// <remarks/>
		CT,

		/// <remarks/>
		CU,

		/// <remarks/>
		DI,

		/// <remarks/>
		EC,

		/// <remarks/>
		NW,

		/// <remarks/>
		PC,

		/// <remarks/>
		PE,

		/// <remarks/>
		PK,

		/// <remarks/>
		PL,

		/// <remarks/>
		PT,

		/// <remarks/>
		PU,

		/// <remarks/>
		PV,

		/// <remarks/>
		PW,

		/// <remarks/>
		QT,

		/// <remarks/>
		SR,

		/// <remarks/>
		TB,

		/// <remarks/>
		TU,

		/// <remarks/>
		TW,

		/// <remarks/>
		WH,

		/// <remarks/>
		WI,
	}
}