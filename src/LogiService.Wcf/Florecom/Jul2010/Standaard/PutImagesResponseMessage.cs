﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class PutImagesResponseMessage
	{
		[XmlNamespaceDeclarations]
		public XmlSerializerNamespaces Namespaces;

		public PutImagesResponseMessage()
		{
			Namespaces = new XmlSerializerNamespaces();
			Namespaces.Add("ism", "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5");
			Namespaces.Add("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			Namespaces.Add("qdt", "urn:un:unece:uncefact:data:standard:QualifiedDataType:7");
			Namespaces.Add("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:8");
			Namespaces.Add("ram", "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:3");
			Namespaces.Add("qdt", "urn:un:unece:uncefact:data:standard:QualifiedDataType:3");
			Namespaces.Add("feram", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3");
			Namespaces.Add("feudt", "urn:fc:florecom:xml:data:draft:UnqualifiedDataType:1");
			Namespaces.Add("feqdt", "urn:fec:florecom:xml:data:draft:QualifiedDataType:5");
		}

		/// <remarks/>
		public PutImagesResponseMessageBody Body { get; set; }
	}
}