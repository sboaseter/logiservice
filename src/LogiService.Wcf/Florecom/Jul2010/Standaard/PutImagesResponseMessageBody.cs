﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class PutImagesResponseMessageBody
	{
		/// <remarks/>
		[XmlArrayItemAttribute("ImageAcknowledge", IsNullable = false)]
		public ImageAcknowledgementTypeImageAcknowledge[] PutImagesResponseDetails { get; set; }
	}
}