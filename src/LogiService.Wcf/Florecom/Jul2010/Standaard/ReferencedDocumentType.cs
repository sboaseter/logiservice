﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:3")]
	public class ReferencedDocumentType
	{
		/// <remarks/>
		public IDType IssuerAssignedID { get; set; }

		/// <remarks/>
		public IDType URIID { get; set; }

		/// <remarks/>
		public string IssueDateTime { get; set; }

		/// <remarks/>
		public CodeType StatusCode { get; set; }

		/// <remarks/>
		public bool CopyIndicator { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool CopyIndicatorSpecified { get; set; }

		/// <remarks/>
		public IDType LineID { get; set; }

		/// <remarks/>
		public CodeType LineStatusCode { get; set; }

		/// <remarks/>
		public DocumentCodeType TypeCode { get; set; }
	}
}