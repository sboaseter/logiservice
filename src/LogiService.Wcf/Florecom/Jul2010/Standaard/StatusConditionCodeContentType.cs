﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:StatusCondition:2:0")]
	public enum StatusConditionCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("2")]
		Item2,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("4")]
		Item4,

		/// <remarks/>
		[XmlEnum("5")]
		Item5,

		/// <remarks/>
		[XmlEnum("6")]
		Item6,

		/// <remarks/>
		[XmlEnum("7")]
		Item7,

		/// <remarks/>
		[XmlEnum("8")]
		Item8,

		/// <remarks/>
		[XmlEnum("9")]
		Item9,

		/// <remarks/>
		[XmlEnum("10")]
		Item10,

		/// <remarks/>
		[XmlEnum("11")]
		Item11,

		/// <remarks/>
		[XmlEnum("12")]
		Item12,

		/// <remarks/>
		[XmlEnum("13")]
		Item13,

		/// <remarks/>
		[XmlEnum("14")]
		Item14,

		/// <remarks/>
		[XmlEnum("15")]
		Item15,

		/// <remarks/>
		[XmlEnum("16")]
		Item16,

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("18")]
		Item18,

		/// <remarks/>
		[XmlEnum("19")]
		Item19,

		/// <remarks/>
		[XmlEnum("20")]
		Item20,

		/// <remarks/>
		[XmlEnum("21")]
		Item21,

		/// <remarks/>
		[XmlEnum("22")]
		Item22,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("24")]
		Item24,

		/// <remarks/>
		[XmlEnum("25")]
		Item25,

		/// <remarks/>
		[XmlEnum("26")]
		Item26,

		/// <remarks/>
		[XmlEnum("27")]
		Item27,

		/// <remarks/>
		[XmlEnum("28")]
		Item28,

		/// <remarks/>
		[XmlEnum("29")]
		Item29,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("39")]
		Item39,

		/// <remarks/>
		[XmlEnum("40")]
		Item40,

		/// <remarks/>
		[XmlEnum("41")]
		Item41,

		/// <remarks/>
		[XmlEnum("42")]
		Item42,

		/// <remarks/>
		[XmlEnum("43")]
		Item43,

		/// <remarks/>
		[XmlEnum("44")]
		Item44,

		/// <remarks/>
		[XmlEnum("45")]
		Item45,

		/// <remarks/>
		[XmlEnum("46")]
		Item46,

		/// <remarks/>
		[XmlEnum("47")]
		Item47,

		/// <remarks/>
		[XmlEnum("48")]
		Item48,

		/// <remarks/>
		[XmlEnum("49")]
		Item49,

		/// <remarks/>
		[XmlEnum("50")]
		Item50,

		/// <remarks/>
		[XmlEnum("51")]
		Item51,

		/// <remarks/>
		[XmlEnum("52")]
		Item52,

		/// <remarks/>
		[XmlEnum("53")]
		Item53,

		/// <remarks/>
		[XmlEnum("54")]
		Item54,

		/// <remarks/>
		[XmlEnum("55")]
		Item55,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("58")]
		Item58,

		/// <remarks/>
		[XmlEnum("59")]
		Item59,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("61")]
		Item61,

		/// <remarks/>
		[XmlEnum("62")]
		Item62,

		/// <remarks/>
		[XmlEnum("63")]
		Item63,

		/// <remarks/>
		[XmlEnum("64")]
		Item64,

		/// <remarks/>
		[XmlEnum("65")]
		Item65,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,

		/// <remarks/>
		[XmlEnum("67")]
		Item67,

		/// <remarks/>
		[XmlEnum("68")]
		Item68,

		/// <remarks/>
		[XmlEnum("69")]
		Item69,

		/// <remarks/>
		[XmlEnum("70")]
		Item70,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("72")]
		Item72,

		/// <remarks/>
		[XmlEnum("73")]
		Item73,

		/// <remarks/>
		[XmlEnum("74")]
		Item74,

		/// <remarks/>
		[XmlEnum("75")]
		Item75,

		/// <remarks/>
		[XmlEnum("76")]
		Item76,

		/// <remarks/>
		[XmlEnum("77")]
		Item77,

		/// <remarks/>
		[XmlEnum("78")]
		Item78,

		/// <remarks/>
		[XmlEnum("79")]
		Item79,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("82")]
		Item82,

		/// <remarks/>
		[XmlEnum("83")]
		Item83,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("86")]
		Item86,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		[XmlEnum("88")]
		Item88,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("90")]
		Item90,

		/// <remarks/>
		[XmlEnum("91")]
		Item91,

		/// <remarks/>
		[XmlEnum("92")]
		Item92,

		/// <remarks/>
		[XmlEnum("93")]
		Item93,

		/// <remarks/>
		[XmlEnum("94")]
		Item94,

		/// <remarks/>
		[XmlEnum("95")]
		Item95,

		/// <remarks/>
		[XmlEnum("96")]
		Item96,

		/// <remarks/>
		[XmlEnum("97")]
		Item97,

		/// <remarks/>
		[XmlEnum("98")]
		Item98,

		/// <remarks/>
		[XmlEnum("99")]
		Item99,

		/// <remarks/>
		[XmlEnum("100")]
		Item100,

		/// <remarks/>
		[XmlEnum("101")]
		Item101,

		/// <remarks/>
		[XmlEnum("102")]
		Item102,

		/// <remarks/>
		[XmlEnum("103")]
		Item103,

		/// <remarks/>
		[XmlEnum("104")]
		Item104,

		/// <remarks/>
		[XmlEnum("105")]
		Item105,

		/// <remarks/>
		[XmlEnum("106")]
		Item106,

		/// <remarks/>
		[XmlEnum("107")]
		Item107,

		/// <remarks/>
		[XmlEnum("108")]
		Item108,

		/// <remarks/>
		[XmlEnum("109")]
		Item109,

		/// <remarks/>
		[XmlEnum("110")]
		Item110,

		/// <remarks/>
		[XmlEnum("111")]
		Item111,

		/// <remarks/>
		[XmlEnum("112")]
		Item112,

		/// <remarks/>
		[XmlEnum("113")]
		Item113,

		/// <remarks/>
		[XmlEnum("114")]
		Item114,

		/// <remarks/>
		[XmlEnum("115")]
		Item115,

		/// <remarks/>
		[XmlEnum("116")]
		Item116,

		/// <remarks/>
		[XmlEnum("117")]
		Item117,

		/// <remarks/>
		[XmlEnum("118")]
		Item118,

		/// <remarks/>
		[XmlEnum("119")]
		Item119,

		/// <remarks/>
		[XmlEnum("120")]
		Item120,

		/// <remarks/>
		[XmlEnum("121")]
		Item121,

		/// <remarks/>
		[XmlEnum("122")]
		Item122,

		/// <remarks/>
		[XmlEnum("123")]
		Item123,

		/// <remarks/>
		[XmlEnum("124")]
		Item124,

		/// <remarks/>
		[XmlEnum("125")]
		Item125,

		/// <remarks/>
		[XmlEnum("126")]
		Item126,

		/// <remarks/>
		[XmlEnum("127")]
		Item127,

		/// <remarks/>
		[XmlEnum("128")]
		Item128,

		/// <remarks/>
		[XmlEnum("129")]
		Item129,

		/// <remarks/>
		[XmlEnum("130")]
		Item130,

		/// <remarks/>
		[XmlEnum("131")]
		Item131,

		/// <remarks/>
		[XmlEnum("132")]
		Item132,

		/// <remarks/>
		[XmlEnum("133")]
		Item133,

		/// <remarks/>
		[XmlEnum("134")]
		Item134,

		/// <remarks/>
		[XmlEnum("135")]
		Item135,

		/// <remarks/>
		[XmlEnum("136")]
		Item136,

		/// <remarks/>
		[XmlEnum("137")]
		Item137,

		/// <remarks/>
		[XmlEnum("138")]
		Item138,

		/// <remarks/>
		[XmlEnum("139")]
		Item139,

		/// <remarks/>
		[XmlEnum("140")]
		Item140,

		/// <remarks/>
		[XmlEnum("141")]
		Item141,

		/// <remarks/>
		[XmlEnum("142")]
		Item142,

		/// <remarks/>
		[XmlEnum("143")]
		Item143,

		/// <remarks/>
		[XmlEnum("144")]
		Item144,

		/// <remarks/>
		[XmlEnum("145")]
		Item145,

		/// <remarks/>
		[XmlEnum("301")]
		Item301,

		/// <remarks/>
		[XmlEnum("302")]
		Item302,

		/// <remarks/>
		[XmlEnum("303")]
		Item303,

		/// <remarks/>
		[XmlEnum("304")]
		Item304,

		/// <remarks/>
		[XmlEnum("310")]
		Item310,

		/// <remarks/>
		[XmlEnum("311")]
		Item311,

		/// <remarks/>
		[XmlEnum("312")]
		Item312,

		/// <remarks/>
		[XmlEnum("313")]
		Item313,

		/// <remarks/>
		[XmlEnum("314")]
		Item314,

		/// <remarks/>
		[XmlEnum("315")]
		Item315,

		/// <remarks/>
		[XmlEnum("316")]
		Item316,

		/// <remarks/>
		[XmlEnum("317")]
		Item317,

		/// <remarks/>
		[XmlEnum("320")]
		Item320,

		/// <remarks/>
		[XmlEnum("321")]
		Item321,

		/// <remarks/>
		[XmlEnum("330")]
		Item330,

		/// <remarks/>
		[XmlEnum("340")]
		Item340,

		/// <remarks/>
		[XmlEnum("341")]
		Item341,

		/// <remarks/>
		[XmlEnum("400")]
		Item400,

		/// <remarks/>
		[XmlEnum("401")]
		Item401,

		/// <remarks/>
		[XmlEnum("402")]
		Item402,

		/// <remarks/>
		[XmlEnum("403")]
		Item403,

		/// <remarks/>
		[XmlEnum("404")]
		Item404,

		/// <remarks/>
		[XmlEnum("405")]
		Item405,

		/// <remarks/>
		[XmlEnum("406")]
		Item406,

		/// <remarks/>
		[XmlEnum("407")]
		Item407,

		/// <remarks/>
		[XmlEnum("408")]
		Item408,

		/// <remarks/>
		[XmlEnum("500")]
		Item500,

		/// <remarks/>
		[XmlEnum("501")]
		Item501,

		/// <remarks/>
		[XmlEnum("502")]
		Item502,
	}
}