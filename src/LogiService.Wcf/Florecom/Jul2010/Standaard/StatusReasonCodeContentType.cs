﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:StatusReason:1:0")]
	public enum StatusReasonCodeContentType
	{

		/// <remarks/>
		[XmlEnum("17")]
		Item17,

		/// <remarks/>
		[XmlEnum("23")]
		Item23,

		/// <remarks/>
		[XmlEnum("30")]
		Item30,

		/// <remarks/>
		[XmlEnum("43")]
		Item43,

		/// <remarks/>
		[XmlEnum("56")]
		Item56,

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("62")]
		Item62,

		/// <remarks/>
		[XmlEnum("66")]
		Item66,
	}
}