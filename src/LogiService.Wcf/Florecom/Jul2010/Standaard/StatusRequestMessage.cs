﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:StatusStandardMessage:3")]
	public class StatusRequestMessage
	{
		/// <remarks/>
		public StatusRequestMessageHeader Header { get; set; }

		/// <remarks/>
		public StatusRequestMessageBody Body { get; set; }
	}
}