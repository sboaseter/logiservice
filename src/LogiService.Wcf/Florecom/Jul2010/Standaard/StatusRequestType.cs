﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:StatusStandardMessage:3")]
	public class StatusRequestType
	{
		/// <remarks/>
		public MarketPlaceType MarketPlace { get; set; }

		/// <remarks/>
		[XmlElement("DocumentStatus")]
		public DocumentStatusCodeType[] DocumentStatus { get; set; }

		/// <remarks/>
		[XmlElement("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }

		/// <remarks/>
		[XmlElement("Status")]
		public StatusType[] Status { get; set; }

		/// <remarks/>
		public DateTime MutationDateTime { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool MutationDateTimeSpecified { get; set; }
	}
}