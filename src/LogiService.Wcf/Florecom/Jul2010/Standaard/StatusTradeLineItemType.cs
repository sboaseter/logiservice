﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class StatusTradeLineItemType
	{
		/// <remarks/>
		public IDType ID { get; set; }

		/// <remarks/>
		public DocumentCodeType1 DocumentType { get; set; }

		/// <remarks/>
		public DocumentStatusCodeType DocumentStatusCode { get; set; }

		/// <remarks/>
		public DateTime LineDateTime { get; set; }

		/// <remarks/>
		public TradingTermsType TradingTerms { get; set; }

		/// <remarks/>
		[XmlElement("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }

		/// <remarks/>
		[XmlElement("AdditionalInformationTradeNote")]
		public TradeNoteType[] AdditionalInformationTradeNote { get; set; }

		/// <remarks/>
		public SellerPartyType SellerParty { get; set; }

		/// <remarks/>
		public BuyerPartyType BuyerParty { get; set; }

		/// <remarks/>
		public SupplierPartyType SupplierParty { get; set; }

		/// <remarks/>
		public CustomerPartyType CustomerParty { get; set; }

		/// <remarks/>
		public EndUserPartyType EndUserParty { get; set; }

		/// <remarks/>
		public ProductType Product { get; set; }

		/// <remarks/>
		public QuantityType Quantity { get; set; }

		/// <remarks/>
		public PackingType Packing { get; set; }

		/// <remarks/>
		[XmlElement("Delivery")]
		public DeliveryType[] Delivery { get; set; }

		/// <remarks/>
		[XmlElement("Status")]
		public StatusType[] Status { get; set; }

		/// <remarks/>
		[XmlElement("PhysicalLocation")]
		public LocationType[] PhysicalLocation { get; set; }

		/// <remarks/>
		[XmlElement("TransportUnit")]
		public TransportUnitType[] TransportUnit { get; set; }

		/// <remarks/>
		public ConsignmentType Consignment { get; set; }

		/// <remarks/>
		[XmlElement("Leg")]
		public LegType[] Leg { get; set; }

		/// <remarks/>
		[XmlElement("TradeTotal")]
		public TotalType[] TradeTotal { get; set; }

		/// <remarks/>
		[XmlElement("IncludedImageLine")]
		public ImageLineItemType[] IncludedImageLine { get; set; }
	}
}