﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class StatusType
	{
		/// <remarks/>
		[XmlElement("ConditionCode")]
		public StatusConditionCodeType[] ConditionCode { get; set; }

		/// <remarks/>
		[XmlElement("ReferenceDateTime")]
		public DateTime[] ReferenceDateTime { get; set; }

		/// <remarks/>
		[XmlElement("DescriptionText")]
		public TextType[] DescriptionText { get; set; }

		/// <remarks/>
		[XmlElement("ReasonCode")]
		public StatusReasonCodeType[] ReasonCode { get; set; }

		/// <remarks/>
		[XmlElement("ReasonText")]
		public TextType[] ReasonText { get; set; }

		/// <remarks/>
		[XmlElement("SequenceNumber")]
		public decimal[] SequenceNumber { get; set; }

		/// <remarks/>
		[XmlElement("ValidityPeriod")]
		public PeriodType[] ValidityPeriod { get; set; }
	}
}