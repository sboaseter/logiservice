﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(TypeName = "StatusType", Namespace = "urn:fec:florecom:xml:data:draft:StatusStandardMessage:3")]
	public class StatusType1
	{
		/// <remarks/>
		public AgentPartyType AgentParty { get; set; }

		/// <remarks/>
		[XmlElement("StatusTradeLineItem")]
		public StatusTradeLineItemType[] StatusTradeLineItem { get; set; }
	}
}