﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class SupplyRequestLineType
	{
		/// <remarks/>
		public IDType ID { get; set; }

		/// <remarks/>
		[XmlElement("DocumentType")]
		public DocumentCodeType1[] DocumentType { get; set; }

		/// <remarks/>
		[XmlElement("DocumentStatusCode")]
		public DocumentStatusCodeType[] DocumentStatusCode { get; set; }

		/// <remarks/>
		public DateTime MutationDateTime { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool MutationDateTimeSpecified { get; set; }

		/// <remarks/>
		[XmlElement("TradingTerms")]
		public TradingTermsType[] TradingTerms { get; set; }

		/// <remarks/>
		[XmlElement("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }

		/// <remarks/>
		[XmlElement("AdditionalInformationTradeNote")]
		public TradeNoteType[] AdditionalInformationTradeNote { get; set; }

		/// <remarks/>
		[XmlElement("SellerParty")]
		public SellerPartyType[] SellerParty { get; set; }

		/// <remarks/>
		[XmlElement("BuyerParty")]
		public BuyerPartyType[] BuyerParty { get; set; }

		/// <remarks/>
		[XmlElement("SupplierParty")]
		public SupplierPartyType[] SupplierParty { get; set; }

		/// <remarks/>
		[XmlElement("CustomerParty")]
		public CustomerPartyType[] CustomerParty { get; set; }

		/// <remarks/>
		[XmlElement("EndUserParty")]
		public EndUserPartyType[] EndUserParty { get; set; }

		/// <remarks/>
		[XmlElement("NotifyParty")]
		public NotifyPartyType[] NotifyParty { get; set; }

		/// <remarks/>
		[XmlElement("Product")]
		public ProductType[] Product { get; set; }

		/// <remarks/>
		[XmlElement("Quantity")]
		public QuantityType[] Quantity { get; set; }

		/// <remarks/>
		[XmlElement("Price")]
		public PriceType[] Price { get; set; }

		/// <remarks/>
		[XmlElement("Packing")]
		public PackingType[] Packing { get; set; }

		/// <remarks/>
		[XmlElement("Delivery")]
		public DeliveryType[] Delivery { get; set; }

		/// <remarks/>
		[XmlElement("ProductSampleLocation")]
		public TradeLocationType[] ProductSampleLocation { get; set; }

		/// <remarks/>
		[XmlElement("Status")]
		public StatusType[] Status { get; set; }

		/// <remarks/>
		[XmlElement("PhysicalLocation")]
		public LocationType[] PhysicalLocation { get; set; }
	}
}