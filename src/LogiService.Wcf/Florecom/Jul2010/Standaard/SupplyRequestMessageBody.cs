﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")]
	public class SupplyRequestMessageBody
	{
		/// <remarks/>
		public SupplyRequestType SupplyRequestDetails { get; set; }
	}
}