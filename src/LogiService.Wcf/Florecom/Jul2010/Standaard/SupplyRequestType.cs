﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class SupplyRequestType
	{
		/// <remarks/>
		public AgentPartyType AgentParty { get; set; }

		/// <remarks/>
		[XmlElement("SupplyRequestLine")]
		public SupplyRequestLineType[] SupplyRequestLine { get; set; }
	}
}