﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4")]
	public class TextType
	{
		/// <remarks/>
		[XmlAttribute(DataType = "language")]
		public string languageCode { get; set; }

		/// <remarks/>
		[XmlText]
		public string Value { get; set; }
	}
}