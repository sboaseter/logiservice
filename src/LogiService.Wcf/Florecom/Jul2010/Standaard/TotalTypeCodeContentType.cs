﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:TotalType:2:0")]
	public enum TotalTypeCodeContentType
	{

		/// <remarks/>
		[XmlEnum("1")]
		Item1,

		/// <remarks/>
		[XmlEnum("3")]
		Item3,

		/// <remarks/>
		[XmlEnum("55")]
		Item55,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("449")]
		Item449,
	}
}