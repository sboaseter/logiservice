﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class TradeAllowanceChargeType
	{
		/// <remarks/>
		public bool ChargeIndicator { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool ChargeIndicatorSpecified { get; set; }

		/// <remarks/>
		public IDType ID { get; set; }

		/// <remarks/>
		public decimal SequenceNumeric { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool SequenceNumericSpecified { get; set; }

		/// <remarks/>
		public decimal CalculationPercent { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool CalculationPercentSpecified { get; set; }

		/// <remarks/>
		public AmountType BasisAmount { get; set; }

		/// <remarks/>
		public QuantityType BasisQuantity { get; set; }

		/// <remarks/>
		public bool PrepaidIndicator { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool PrepaidIndicatorSpecified { get; set; }

		/// <remarks/>
		public AmountType ActualAmount { get; set; }

		/// <remarks/>
		public AllowanceChargeCurrencyExchangeType ActualAllowanceChargeCurrencyExchange { get; set; }
	}
}