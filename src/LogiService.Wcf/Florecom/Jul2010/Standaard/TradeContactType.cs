﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:3")]
	public class TradeContactType
	{
		/// <remarks/>
		public IDType ID { get; set; }

		/// <remarks/>
		public TextType PersonName { get; set; }

		/// <remarks/>
		public TextType DepartmentName { get; set; }

		/// <remarks/>
		public CodeType TypeCode { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType TelephoneSpecifiedCommunication { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType DirectTelephoneSpecifiedCommunication { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType MobileTelephoneSpecifiedCommunication { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType FaxSpecifiedCommunication { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType TelexSpecifiedCommunication { get; set; }

		/// <remarks/>
		public SpecifiedCommunicationType EmailURISpecifiedCommunication { get; set; }
	}
}