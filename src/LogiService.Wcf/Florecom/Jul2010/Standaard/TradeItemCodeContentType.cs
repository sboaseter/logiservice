﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:fc:florecom:xml:codelist:draft:TradeItem:1:0")]
	public enum TradeItemCodeContentType
	{

		/// <remarks/>
		[XmlEnum("57")]
		Item57,

		/// <remarks/>
		[XmlEnum("67")]
		Item67,

		/// <remarks/>
		[XmlEnum("128")]
		Item128,

		/// <remarks/>
		[XmlEnum("152")]
		Item152,
	}
}