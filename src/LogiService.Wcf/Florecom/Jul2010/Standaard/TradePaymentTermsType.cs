﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:3")]
	public class TradePaymentTermsType
	{
		/// <remarks/>
		public PaymentTermsIDType ID { get; set; }

		/// <remarks/>
		public PaymentTermsEventTimeReferenceCodeType FromEventCode { get; set; }

		/// <remarks/>
		public MeasureType SettlementPeriodMeasure { get; set; }
	}
}