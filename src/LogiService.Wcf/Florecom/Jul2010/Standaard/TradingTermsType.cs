﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:3")]
	public class TradingTermsType
	{
		/// <remarks/>
		public MarketPlaceType MarketPlace { get; set; }

		/// <remarks/>
		public MarketFormCodeType MarketFormCode { get; set; }

		/// <remarks/>
		public TradePaymentTermsType PaymentTerms { get; set; }

		/// <remarks/>
		[XmlElement("PaymentAgentParty")]
		public AgentPartyType[] PaymentAgentParty { get; set; }

		/// <remarks/>
		public PeriodType TradePeriod { get; set; }

		/// <remarks/>
		public decimal TradeSequence { get; set; }

		/// <remarks/>
		[XmlIgnore]
		public bool TradeSequenceSpecified { get; set; }

		/// <remarks/>
		[XmlElement("Condition")]
		public ConditionType[] Condition { get; set; }
	}
}
