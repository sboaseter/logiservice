﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2010
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[XmlType(Namespace = "urn:un:unece:uncefact:codelist:standard:UNECE:TransportMeansTypeCode:2007")]
	public enum TransportMeansTypeCodeContentType
	{

		/// <remarks/>
		[XmlEnum("31")]
		Item31,

		/// <remarks/>
		[XmlEnum("32")]
		Item32,

		/// <remarks/>
		[XmlEnum("33")]
		Item33,

		/// <remarks/>
		[XmlEnum("34")]
		Item34,

		/// <remarks/>
		[XmlEnum("35")]
		Item35,

		/// <remarks/>
		[XmlEnum("36")]
		Item36,

		/// <remarks/>
		[XmlEnum("37")]
		Item37,

		/// <remarks/>
		[XmlEnum("38")]
		Item38,

		/// <remarks/>
		[XmlEnum("39")]
		Item39,

		/// <remarks/>
		[XmlEnum("60")]
		Item60,

		/// <remarks/>
		[XmlEnum("70")]
		Item70,

		/// <remarks/>
		[XmlEnum("71")]
		Item71,

		/// <remarks/>
		[XmlEnum("72")]
		Item72,

		/// <remarks/>
		[XmlEnum("80")]
		Item80,

		/// <remarks/>
		[XmlEnum("81")]
		Item81,

		/// <remarks/>
		[XmlEnum("82")]
		Item82,

		/// <remarks/>
		[XmlEnum("83")]
		Item83,

		/// <remarks/>
		[XmlEnum("84")]
		Item84,

		/// <remarks/>
		[XmlEnum("85")]
		Item85,

		/// <remarks/>
		[XmlEnum("86")]
		Item86,

		/// <remarks/>
		[XmlEnum("87")]
		Item87,

		/// <remarks/>
		[XmlEnum("88")]
		Item88,

		/// <remarks/>
		[XmlEnum("89")]
		Item89,

		/// <remarks/>
		[XmlEnum("150")]
		Item150,

		/// <remarks/>
		[XmlEnum("151")]
		Item151,

		/// <remarks/>
		[XmlEnum("152")]
		Item152,

		/// <remarks/>
		[XmlEnum("153")]
		Item153,

		/// <remarks/>
		[XmlEnum("154")]
		Item154,

		/// <remarks/>
		[XmlEnum("155")]
		Item155,

		/// <remarks/>
		[XmlEnum("157")]
		Item157,

		/// <remarks/>
		[XmlEnum("159")]
		Item159,

		/// <remarks/>
		[XmlEnum("160")]
		Item160,

		/// <remarks/>
		[XmlEnum("170")]
		Item170,

		/// <remarks/>
		[XmlEnum("172")]
		Item172,

		/// <remarks/>
		[XmlEnum("173")]
		Item173,

		/// <remarks/>
		[XmlEnum("174")]
		Item174,

		/// <remarks/>
		[XmlEnum("175")]
		Item175,

		/// <remarks/>
		[XmlEnum("176")]
		Item176,

		/// <remarks/>
		[XmlEnum("177")]
		Item177,

		/// <remarks/>
		[XmlEnum("178")]
		Item178,

		/// <remarks/>
		[XmlEnum("180")]
		Item180,

		/// <remarks/>
		[XmlEnum("181")]
		Item181,

		/// <remarks/>
		[XmlEnum("182")]
		Item182,

		/// <remarks/>
		[XmlEnum("183")]
		Item183,

		/// <remarks/>
		[XmlEnum("184")]
		Item184,

		/// <remarks/>
		[XmlEnum("185")]
		Item185,

		/// <remarks/>
		[XmlEnum("189")]
		Item189,

		/// <remarks/>
		[XmlEnum("190")]
		Item190,

		/// <remarks/>
		[XmlEnum("191")]
		Item191,

		/// <remarks/>
		[XmlEnum("192")]
		Item192,

		/// <remarks/>
		[XmlEnum("210")]
		Item210,

		/// <remarks/>
		[XmlEnum("220")]
		Item220,

		/// <remarks/>
		[XmlEnum("230")]
		Item230,

		/// <remarks/>
		[XmlEnum("310")]
		Item310,

		/// <remarks/>
		[XmlEnum("311")]
		Item311,

		/// <remarks/>
		[XmlEnum("312")]
		Item312,

		/// <remarks/>
		[XmlEnum("313")]
		Item313,

		/// <remarks/>
		[XmlEnum("314")]
		Item314,

		/// <remarks/>
		[XmlEnum("315")]
		Item315,

		/// <remarks/>
		[XmlEnum("320")]
		Item320,

		/// <remarks/>
		[XmlEnum("330")]
		Item330,

		/// <remarks/>
		[XmlEnum("341")]
		Item341,

		/// <remarks/>
		[XmlEnum("342")]
		Item342,

		/// <remarks/>
		[XmlEnum("343")]
		Item343,

		/// <remarks/>
		[XmlEnum("360")]
		Item360,

		/// <remarks/>
		[XmlEnum("362")]
		Item362,

		/// <remarks/>
		[XmlEnum("363")]
		Item363,

		/// <remarks/>
		[XmlEnum("364")]
		Item364,

		/// <remarks/>
		[XmlEnum("365")]
		Item365,

		/// <remarks/>
		[XmlEnum("366")]
		Item366,

		/// <remarks/>
		[XmlEnum("367")]
		Item367,

		/// <remarks/>
		[XmlEnum("368")]
		Item368,

		/// <remarks/>
		[XmlEnum("369")]
		Item369,

		/// <remarks/>
		[XmlEnum("370")]
		Item370,

		/// <remarks/>
		[XmlEnum("371")]
		Item371,

		/// <remarks/>
		[XmlEnum("372")]
		Item372,

		/// <remarks/>
		[XmlEnum("373")]
		Item373,

		/// <remarks/>
		[XmlEnum("374")]
		Item374,

		/// <remarks/>
		[XmlEnum("375")]
		Item375,

		/// <remarks/>
		[XmlEnum("376")]
		Item376,

		/// <remarks/>
		[XmlEnum("377")]
		Item377,

		/// <remarks/>
		[XmlEnum("378")]
		Item378,

		/// <remarks/>
		[XmlEnum("379")]
		Item379,

		/// <remarks/>
		[XmlEnum("380")]
		Item380,

		/// <remarks/>
		[XmlEnum("381")]
		Item381,

		/// <remarks/>
		[XmlEnum("382")]
		Item382,

		/// <remarks/>
		[XmlEnum("383")]
		Item383,

		/// <remarks/>
		[XmlEnum("384")]
		Item384,

		/// <remarks/>
		[XmlEnum("385")]
		Item385,

		/// <remarks/>
		[XmlEnum("386")]
		Item386,

		/// <remarks/>
		[XmlEnum("387")]
		Item387,

		/// <remarks/>
		[XmlEnum("388")]
		Item388,

		/// <remarks/>
		[XmlEnum("389")]
		Item389,

		/// <remarks/>
		[XmlEnum("390")]
		Item390,

		/// <remarks/>
		[XmlEnum("391")]
		Item391,

		/// <remarks/>
		[XmlEnum("392")]
		Item392,

		/// <remarks/>
		[XmlEnum("393")]
		Item393,

		/// <remarks/>
		[XmlEnum("394")]
		Item394,

		/// <remarks/>
		[XmlEnum("395")]
		Item395,

		/// <remarks/>
		[XmlEnum("396")]
		Item396,

		/// <remarks/>
		[XmlEnum("397")]
		Item397,

		/// <remarks/>
		[XmlEnum("398")]
		Item398,

		/// <remarks/>
		[XmlEnum("399")]
		Item399,

		/// <remarks/>
		[XmlEnum("810")]
		Item810,

		/// <remarks/>
		[XmlEnum("811")]
		Item811,

		/// <remarks/>
		[XmlEnum("812")]
		Item812,

		/// <remarks/>
		[XmlEnum("813")]
		Item813,

		/// <remarks/>
		[XmlEnum("814")]
		Item814,

		/// <remarks/>
		[XmlEnum("815")]
		Item815,

		/// <remarks/>
		[XmlEnum("816")]
		Item816,

		/// <remarks/>
		[XmlEnum("817")]
		Item817,

		/// <remarks/>
		[XmlEnum("818")]
		Item818,

		/// <remarks/>
		[XmlEnum("821")]
		Item821,

		/// <remarks/>
		[XmlEnum("822")]
		Item822,

		/// <remarks/>
		[XmlEnum("823")]
		Item823,

		/// <remarks/>
		[XmlEnum("824")]
		Item824,

		/// <remarks/>
		[XmlEnum("825")]
		Item825,

		/// <remarks/>
		[XmlEnum("826")]
		Item826,

		/// <remarks/>
		[XmlEnum("827")]
		Item827,

		/// <remarks/>
		[XmlEnum("828")]
		Item828,

		/// <remarks/>
		[XmlEnum("829")]
		Item829,

		/// <remarks/>
		[XmlEnum("831")]
		Item831,

		/// <remarks/>
		[XmlEnum("832")]
		Item832,

		/// <remarks/>
		[XmlEnum("833")]
		Item833,

		/// <remarks/>
		[XmlEnum("834")]
		Item834,

		/// <remarks/>
		[XmlEnum("835")]
		Item835,

		/// <remarks/>
		[XmlEnum("836")]
		Item836,

		/// <remarks/>
		[XmlEnum("837")]
		Item837,

		/// <remarks/>
		[XmlEnum("838")]
		Item838,

		/// <remarks/>
		[XmlEnum("839")]
		Item839,

		/// <remarks/>
		[XmlEnum("840")]
		Item840,

		/// <remarks/>
		[XmlEnum("841")]
		Item841,

		/// <remarks/>
		[XmlEnum("842")]
		Item842,

		/// <remarks/>
		[XmlEnum("843")]
		Item843,

		/// <remarks/>
		[XmlEnum("844")]
		Item844,

		/// <remarks/>
		[XmlEnum("845")]
		Item845,

		/// <remarks/>
		[XmlEnum("846")]
		Item846,

		/// <remarks/>
		[XmlEnum("847")]
		Item847,

		/// <remarks/>
		[XmlEnum("848")]
		Item848,

		/// <remarks/>
		[XmlEnum("849")]
		Item849,

		/// <remarks/>
		[XmlEnum("850")]
		Item850,

		/// <remarks/>
		[XmlEnum("851")]
		Item851,

		/// <remarks/>
		[XmlEnum("1501")]
		Item1501,

		/// <remarks/>
		[XmlEnum("1502")]
		Item1502,

		/// <remarks/>
		[XmlEnum("1503")]
		Item1503,

		/// <remarks/>
		[XmlEnum("1504")]
		Item1504,

		/// <remarks/>
		[XmlEnum("1505")]
		Item1505,

		/// <remarks/>
		[XmlEnum("1506")]
		Item1506,

		/// <remarks/>
		[XmlEnum("1511")]
		Item1511,

		/// <remarks/>
		[XmlEnum("1512")]
		Item1512,

		/// <remarks/>
		[XmlEnum("1513")]
		Item1513,

		/// <remarks/>
		[XmlEnum("1514")]
		Item1514,

		/// <remarks/>
		[XmlEnum("1515")]
		Item1515,

		/// <remarks/>
		[XmlEnum("1516")]
		Item1516,

		/// <remarks/>
		[XmlEnum("1517")]
		Item1517,

		/// <remarks/>
		[XmlEnum("1518")]
		Item1518,

		/// <remarks/>
		[XmlEnum("1519")]
		Item1519,

		/// <remarks/>
		[XmlEnum("1521")]
		Item1521,

		/// <remarks/>
		[XmlEnum("1522")]
		Item1522,

		/// <remarks/>
		[XmlEnum("1523")]
		Item1523,

		/// <remarks/>
		[XmlEnum("1524")]
		Item1524,

		/// <remarks/>
		[XmlEnum("1525")]
		Item1525,

		/// <remarks/>
		[XmlEnum("1531")]
		Item1531,

		/// <remarks/>
		[XmlEnum("1532")]
		Item1532,

		/// <remarks/>
		[XmlEnum("1533")]
		Item1533,

		/// <remarks/>
		[XmlEnum("1534")]
		Item1534,

		/// <remarks/>
		[XmlEnum("1541")]
		Item1541,

		/// <remarks/>
		[XmlEnum("1542")]
		Item1542,

		/// <remarks/>
		[XmlEnum("1543")]
		Item1543,

		/// <remarks/>
		[XmlEnum("1551")]
		Item1551,

		/// <remarks/>
		[XmlEnum("1552")]
		Item1552,

		/// <remarks/>
		[XmlEnum("1553")]
		Item1553,

		/// <remarks/>
		[XmlEnum("1591")]
		Item1591,

		/// <remarks/>
		[XmlEnum("1592")]
		Item1592,

		/// <remarks/>
		[XmlEnum("1593")]
		Item1593,

		/// <remarks/>
		[XmlEnum("1594")]
		Item1594,

		/// <remarks/>
		[XmlEnum("1601")]
		Item1601,

		/// <remarks/>
		[XmlEnum("1602")]
		Item1602,

		/// <remarks/>
		[XmlEnum("1603")]
		Item1603,

		/// <remarks/>
		[XmlEnum("1604")]
		Item1604,

		/// <remarks/>
		[XmlEnum("1605")]
		Item1605,

		/// <remarks/>
		[XmlEnum("1606")]
		Item1606,

		/// <remarks/>
		[XmlEnum("1607")]
		Item1607,

		/// <remarks/>
		[XmlEnum("1711")]
		Item1711,

		/// <remarks/>
		[XmlEnum("1712")]
		Item1712,

		/// <remarks/>
		[XmlEnum("1721")]
		Item1721,

		/// <remarks/>
		[XmlEnum("1723")]
		Item1723,

		/// <remarks/>
		[XmlEnum("1724")]
		Item1724,

		/// <remarks/>
		[XmlEnum("1725")]
		Item1725,

		/// <remarks/>
		[XmlEnum("1726")]
		Item1726,

		/// <remarks/>
		[XmlEnum("1727")]
		Item1727,

		/// <remarks/>
		[XmlEnum("1728")]
		Item1728,

		/// <remarks/>
		[XmlEnum("1729")]
		Item1729,

		/// <remarks/>
		[XmlEnum("1751")]
		Item1751,

		/// <remarks/>
		[XmlEnum("1752")]
		Item1752,

		/// <remarks/>
		[XmlEnum("1753")]
		Item1753,

		/// <remarks/>
		[XmlEnum("1761")]
		Item1761,

		/// <remarks/>
		[XmlEnum("1762")]
		Item1762,

		/// <remarks/>
		[XmlEnum("1763")]
		Item1763,

		/// <remarks/>
		[XmlEnum("1764")]
		Item1764,

		/// <remarks/>
		[XmlEnum("1765")]
		Item1765,

		/// <remarks/>
		[XmlEnum("1766")]
		Item1766,

		/// <remarks/>
		[XmlEnum("1781")]
		Item1781,

		/// <remarks/>
		[XmlEnum("1782")]
		Item1782,

		/// <remarks/>
		[XmlEnum("2201")]
		Item2201,

		/// <remarks/>
		[XmlEnum("2202")]
		Item2202,

		/// <remarks/>
		[XmlEnum("2203")]
		Item2203,

		/// <remarks/>
		[XmlEnum("2301")]
		Item2301,

		/// <remarks/>
		[XmlEnum("2302")]
		Item2302,

		/// <remarks/>
		[XmlEnum("2303")]
		Item2303,

		/// <remarks/>
		[XmlEnum("2304")]
		Item2304,

		/// <remarks/>
		[XmlEnum("2305")]
		Item2305,

		/// <remarks/>
		[XmlEnum("3100")]
		Item3100,

		/// <remarks/>
		[XmlEnum("3101")]
		Item3101,

		/// <remarks/>
		[XmlEnum("3102")]
		Item3102,

		/// <remarks/>
		[XmlEnum("3103")]
		Item3103,

		/// <remarks/>
		[XmlEnum("3104")]
		Item3104,

		/// <remarks/>
		[XmlEnum("3105")]
		Item3105,

		/// <remarks/>
		[XmlEnum("3106")]
		Item3106,

		/// <remarks/>
		[XmlEnum("3107")]
		Item3107,

		/// <remarks/>
		[XmlEnum("3108")]
		Item3108,

		/// <remarks/>
		[XmlEnum("3109")]
		Item3109,

		/// <remarks/>
		[XmlEnum("3110")]
		Item3110,

		/// <remarks/>
		[XmlEnum("3111")]
		Item3111,

		/// <remarks/>
		[XmlEnum("3112")]
		Item3112,

		/// <remarks/>
		[XmlEnum("3113")]
		Item3113,

		/// <remarks/>
		[XmlEnum("3114")]
		Item3114,

		/// <remarks/>
		[XmlEnum("3115")]
		Item3115,

		/// <remarks/>
		[XmlEnum("3116")]
		Item3116,

		/// <remarks/>
		[XmlEnum("3117")]
		Item3117,

		/// <remarks/>
		[XmlEnum("3118")]
		Item3118,

		/// <remarks/>
		[XmlEnum("3119")]
		Item3119,

		/// <remarks/>
		[XmlEnum("3120")]
		Item3120,

		/// <remarks/>
		[XmlEnum("3121")]
		Item3121,

		/// <remarks/>
		[XmlEnum("3122")]
		Item3122,

		/// <remarks/>
		[XmlEnum("3123")]
		Item3123,

		/// <remarks/>
		[XmlEnum("3124")]
		Item3124,

		/// <remarks/>
		[XmlEnum("3125")]
		Item3125,

		/// <remarks/>
		[XmlEnum("3126")]
		Item3126,

		/// <remarks/>
		[XmlEnum("3127")]
		Item3127,

		/// <remarks/>
		[XmlEnum("3128")]
		Item3128,

		/// <remarks/>
		[XmlEnum("3129")]
		Item3129,

		/// <remarks/>
		[XmlEnum("3130")]
		Item3130,

		/// <remarks/>
		[XmlEnum("3131")]
		Item3131,

		/// <remarks/>
		[XmlEnum("3132")]
		Item3132,

		/// <remarks/>
		[XmlEnum("3133")]
		Item3133,

		/// <remarks/>
		[XmlEnum("3134")]
		Item3134,

		/// <remarks/>
		[XmlEnum("3135")]
		Item3135,

		/// <remarks/>
		[XmlEnum("3136")]
		Item3136,

		/// <remarks/>
		[XmlEnum("3137")]
		Item3137,

		/// <remarks/>
		[XmlEnum("3138")]
		Item3138,

		/// <remarks/>
		[XmlEnum("3201")]
		Item3201,

		/// <remarks/>
		[XmlEnum("3301")]
		Item3301,

		/// <remarks/>
		[XmlEnum("3302")]
		Item3302,

		/// <remarks/>
		[XmlEnum("3303")]
		Item3303,

		/// <remarks/>
		[XmlEnum("3304")]
		Item3304,
	}
}