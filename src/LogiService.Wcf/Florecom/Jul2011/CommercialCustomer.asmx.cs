using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Xml;
using System.Transactions;

namespace LogiService.Wcf.Florecom.Jul2011
{
    /// <summary>
    /// Florecom Webservice Implementation
    /// ==================================
    /// System:     Florecom Commercial Scenario's
    /// Actor:      Buyer
    /// Version:    Juli 2011
    /// 
    /// This example contains both stubs and demos for all commercial use cases that are relevant for buyers of floricultural products. 
    /// 
    /// Notes:
    /// -SOAP 1.2 is disabled using the web.config, because Florecom conversations rely on SOAP 1.1.
    /// -Please, do not alter the serialization attributes. This will guarantee the interoperability (WSDL compatibility) for buyers among different market places.
    /// -Please, do check the functional and technical documentation as publisehed on sdk.florecom.org.
    /// 
    /// Author:
    /// -For more information, see http://sdk.florecom.org or contact Cris Ilbrink (cris.ilbrink@florecom.nl)
    /// </summary>
    [System.Web.Services.WebServiceAttribute(Namespace = "http://webservice.florecom.org/CommercialBuyer", Description = "Florecom Web Service for the 'Commercial Use Cases' as used by the actor 'Buyer'. Release Jul 2011.", Name = "Florecom Commerical Web Service for the Buyer. Release Jul 2011.")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "CommercialBuyer", Namespace = "http://webservice.florecom.org/CommercialBuyer")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CommercialCustomer : ICommercialCustomer
    {
        #region Demo: Get Supply

        /// <summary>
        /// Return supply to a buyer. 
        /// 
        /// Note:
        ///     -This demo neglects the supply filter i.e. the request could contain a filter to specify what items to return.
        /// </summary>
        /// <param name="SupplyRequest"></param>
        /// <returns></returns>
        public SupplyResponseMessage GetSupply(SupplyRequestMessage SupplyRequest)
        {
            try
            {
                //Hydrate a supply response message object containing a single line item (i.e. a batch of the same article)
                SupplyResponseMessage oMsg = new SupplyResponseMessage();
                return oMsg;
            }
            catch (Exception err)
            {
                ThrowFlorecomError(err.Message, 10000, "[Xpath location of element that causes the error e.g. /GetSupply/Header/Password]");
                return null;
            }
        }
        #endregion

        #region Stub: Put Order
        /// <summary>
        /// This is a stub for a web method to place an order.
        /// </summary>
        /// <param name="OrderRequest"></param>
        /// <returns></returns>
        public OrderResponseMessage PutOrder(OrderRequestMessage PutOrderRequest)
        {
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    OrderResponseMessage oMsg = new OrderResponseMessage();
                    oMsg.Body = new OrderResponseMessageBody();
                    oMsg.Body.OrderResponse = new OrderResponseType();
                    //TO DO
                    return oMsg;
                }
            }
            catch (TransactionAbortedException err)
            {
                ThrowFlorecomError("Roll back!", 200, "/");
                return null;
            }
            catch (Exception err)
            {
                ThrowFlorecomError(err.Message, 10000, "[Xpath location of element that causes the error e.g. /PutOrder/Header/Password]");
                return null;
            }
        }
        #endregion

        #region Demo: Get Status
        /// <summary>
        /// Get status of (specific) supply line items. 
        /// 
        /// This demo retuns the physical location of a specific item at the market place (used for example to estimate the time of auction). 
        /// Antoher could example would be to retreive the availability (quantity) of a specific supply line item.
        /// </summary>
        /// <param name="StatusRequest"></param>
        /// <returns></returns>
        public StatusResponseMessage GetStatus(StatusRequestMessage StatusRequest)
        {
            try
            {
                StatusResponseMessage oResponse = new StatusResponseMessage();
                return oResponse;
            }
            catch (Exception err)
            {
                ThrowFlorecomError(err.Message, 10000, "[Xpath location of element that causes the error e.g. /GetStatus/Header/Password]");
                return null;
            }
        }
        #endregion

        #region Get Image
        /// <summary>
        /// Get the full binary image of a supply line item. The image is encoded in base64.
        /// 
        /// This is a stub web service i.e. not a full working demo.
        /// </summary>
        /// <param name="ImagesRequest"></param>
        /// <returns></returns>
        public ImagesResponseMessage GetImage(ImagesRequestMessage ImagesRequest)
        {
            try
            {
                ImagesResponseMessage msg = new ImagesResponseMessage();

                //For your convenience, here is the code to convert a local image to a Base64 encoded string
                //System.IO.StreamReader reader = new System.IO.StreamReader(@"c:\temp\image.jpg");
                //byte[] bytedata = System.Text.Encoding.Default.GetBytes(reader.ReadToEnd());
                //reader.Close();
                //msg.Body = new ImagesResponseMessageBody();
                //..etc..
                //msg.Body.ImagesResponseDetails.ImageLine[0].ImageBinary = System.Convert.ToBase64String(bytedata);

                return msg;
            }
            catch (Exception err)
            {
                //Return a SOAP exception according to the Florecom Communication Protocol
                ThrowFlorecomError(err.Message, 10000, "[Xpath location of element that causes the error e.g. /GetImage/Header/Password]");
                return null;
            }
        }
        #endregion

        #region Error Handling
        /// <summary>
        /// Errors should be returned using a SOAP:Fault (SOAP version 1.1). The Detail-element of the Soap:fault should contain the errors according to
        /// The ErrorList scheme is described in the document "Webservice Beschrijvingen Virtuele Marktplaats" that is publised on sdk.florecom.org.
        /// the Florecom Errorlist-scheme e.g.:
        /// <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xml="http://www.w3.org/XML/1998/namespace">
        //  <env:Body>
        //    <env:Fault>
        //      <env:faultcode>env:Sender</env:faultcode>
        //      <env:faultstring>Error detected by the webservice. Check the Detail-element in this SOAP:Fault response message for more error details.</env:faultstring>
        //      <env:detail>
        //        <ErrorList xmlns="urn:fec:messages:data:final:FlorecomStandardMessage:0_5">
        //          <Error>
        //            <ErrorLevel>message</ErrorLevel>
        //            <Severity>error</Severity>
        //            <ErrorCode>102</ErrorCode>
        //            <ErrorLocation>SupplyResponse/Body/SupplyResponseDetails/AgentParty</fsm:ErrorLocation>
        //            <DescriptionText>Unknown agent party.</DescriptionText>
        //          </Error>
        //        </ErrorList>
        //      </env:detail>
        //    </env:Fault>
        //  </env:Body>
        //</env:Envelope>
        /// </summary>
        /// <param name="ErrorMessage"></param>
        public void ThrowFlorecomError(string ErrorMessage, int ErrorCode, string ErrorLocation)
        {
            //Construct the Florecom ErrorList structure
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Xml.XmlNamespaceManager ns = new System.Xml.XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            ns.AddNamespace("ferab", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
            System.Xml.XmlNode FlorecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            FlorecomErrorNode.AppendChild(FlorecomErrorLevelNode);
            FlorecomErrorNode.AppendChild(FlorecomErrorSeverity);
            FlorecomErrorNode.AppendChild(FlorecomErrorErrorCode);
            FlorecomErrorNode.AppendChild(FlorecomErrorLocation);
            FlorecomErrorNode.AppendChild(FlorecomErrorDescriptionText);
            FlorecomErrorListNode.AppendChild(FlorecomErrorNode);
            //Add the custom values to the ErrorList strucure
            FlorecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
            FlorecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
            FlorecomErrorErrorCode.AppendChild(doc.CreateTextNode(ErrorCode.ToString()));
            FlorecomErrorLocation.AppendChild(doc.CreateTextNode(ErrorLocation));
            FlorecomErrorDescriptionText.AppendChild(doc.CreateTextNode(ErrorMessage));
            detailNode.AppendChild(FlorecomErrorListNode);
            //Return a Soap:Fault that incorporates the Florecom ErrorList scheme
            throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, detailNode);
        }
        #endregion

    }
}