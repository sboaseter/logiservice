﻿using System;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;

namespace LogiService.Wcf.Florecom.Jul2011
{
	public static class Helper
	{
		// <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xml="http://www.w3.org/XML/1998/namespace">
		//  <env:Body>
		//    <env:Fault>
		//      <env:faultcode>env:Sender</env:faultcode>
		//      <env:faultstring>Error detected by the webservice. Check the Detail-element in this SOAP:Fault response message for more error details.</env:faultstring>
		//      <env:detail>
		//        <ErrorList xmlns="urn:fec:messages:data:final:FlorecomStandardMessage:0_5">
		//          <Error>
		//            <ErrorLevel>message</ErrorLevel>
		//            <Severity>error</Severity>
		//            <ErrorCode>102</ErrorCode>
		//            <ErrorLocation>SupplyResponse/Body/SupplyResponseDetails/AgentParty</fsm:ErrorLocation>
		//            <DescriptionText>Unknown agent party.</DescriptionText>
		//          </Error>
		//        </ErrorList>
		//      </env:detail>
		//    </env:Fault>
		//  </env:Body>
		//</env:Envelope>
		/// <summary>
		/// Errors should be returned using a SOAP:Fault (SOAP version 1.1). The Detail-element of the Soap:fault should contain the errors according to
		/// The ErrorList scheme is described in the document "Webservice Beschrijvingen Virtuele Marktplaats" that is publised on sdk.florecom.org.
		/// the Florecom Errorlist-scheme e.g.:
		/// </summary>
		/// <param name="errorMessage"></param>
		/// <param name="errorCode"></param>
		/// <param name="errorLocation"></param>
		public static void ThrowFlorecomError(string errorMessage, int errorCode, string errorLocation)
		{
			//Construct the Florecom ErrorList structure
			var doc = new XmlDocument();
			var ns = new XmlNamespaceManager(doc.NameTable);
			ns.AddNamespace("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			ns.AddNamespace("ferab", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
			var florecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var florecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
			var florecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			var florecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
			florecomErrorNode.AppendChild(florecomErrorLevelNode);
			florecomErrorNode.AppendChild(florecomErrorSeverity);
			florecomErrorNode.AppendChild(florecomErrorErrorCode);
			florecomErrorNode.AppendChild(florecomErrorLocation);
			florecomErrorNode.AppendChild(florecomErrorDescriptionText);
			florecomErrorListNode.AppendChild(florecomErrorNode);
			//Add the custom values to the ErrorList strucure
			florecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
			florecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
			florecomErrorErrorCode.AppendChild(doc.CreateTextNode(errorCode.ToString()));
			florecomErrorLocation.AppendChild(doc.CreateTextNode(errorLocation));
			florecomErrorDescriptionText.AppendChild(doc.CreateTextNode(errorMessage));
			detailNode.AppendChild(florecomErrorListNode);
			//Return a Soap:Fault that incorporates the Florecom ErrorList scheme
			throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, detailNode);
		}
	}
}
