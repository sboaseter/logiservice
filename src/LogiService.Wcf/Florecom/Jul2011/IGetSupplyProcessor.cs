﻿using System;
using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Jul2011
{
	public interface IGetSupplyProcessor
	{
		SupplyResponseMessage Process(SupplyRequestMessage SupplyRequest, LoginPrincipal principal);
	}
}