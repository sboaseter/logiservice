﻿using System;
using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Jul2011
{
	public interface IServiceAuthenticator
	{
		LoginPrincipal Authenticate(IRequestHeader requestHeader, string requestHeaderPrefix);
	}
}