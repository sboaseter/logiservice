﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Common;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Common.Services;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using BA.Interface.Communicator;


namespace LogiService.Wcf.Florecom.Jul2011
{
	public class PutOrderProcessor : IPutOrderProcessor
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
        protected IShopPropertyProvider ShopPropertyProvider { get; set; }
		protected ICommonOrderProvider CommonOrderProvider { get; set; }
		protected IEventLogProvider EventLogProvider { get; set; }
		protected PrincipalService PrincipalService { get; set; }
		protected ISignalProvider SignalProvider { get; set; }
		protected IRepository<OfferDetailView> OfferDetails { get; set; }

		public PutOrderProcessor(
			IShopPropertyProvider shopPropertyProvider,
			ICommonOrderProvider commonOrderProvider,
			IEventLogProvider eventLogProvider,
			PrincipalService principalService,
			ISignalProvider signalProvider,
			IRepository<OfferDetailView> offerDetails)
		{
			ShopPropertyProvider = shopPropertyProvider;
			CommonOrderProvider = commonOrderProvider;
			EventLogProvider = eventLogProvider;
			PrincipalService = principalService;
			SignalProvider = signalProvider;
			OfferDetails = offerDetails;
		}

		public OrderResponseMessage Process(OrderRequestMessage PutOrderRequest, LoginPrincipal principal)
		{
            string remark = "";
			// Controleer of de gebruiker wel een Buyer is. Anders heeft dit weinig zin.
			if (!principal.IsBuyer)
				throw new FlorecomException("Only Buyers can access PutOrder.", "/PutOrderRequest/Header");

			var response = new OrderResponseMessage();

			if (PutOrderRequest.Body == null)
				throw new FlorecomException("No Body specified.", "/PutOrderRequest/Body");

			response.Body = new OrderResponseMessageBody();

			if (PutOrderRequest.Body.Order == null)
				throw new FlorecomException("No Order specified.", "/PutOrderRequest/Body/Order");

			response.Body.OrderResponse = new OrderResponseType();
            
			if (PutOrderRequest.Body.Order.OrderTradeLineItem == null || PutOrderRequest.Body.Order.OrderTradeLineItem.Length == 0)
				throw new FlorecomException("No Order Lines specified.", "/PutOrderRequest/Body/Order/OrderTradeLineItem");

            var firstOrderLine = PutOrderRequest.Body.Order.OrderTradeLineItem[0];
 
            if (firstOrderLine == null)
                throw new FlorecomException("No Order Lines specified.", "/PutOrderRequest/Body/Order/OrderTradeLineItem");

            Log.DebugFormat("MessageDateTime : [{0}]", PutOrderRequest.Header.MessageDateTime); 
            System.DateTime deliveryDate = System.DateTime.Now;
            Boolean deliveryDateSpec = false;
			
            // Bepaal de default delivery date.
            if (firstOrderLine.Delivery != null)
            {
                if (firstOrderLine.Delivery.LatestDeliveryDateTimeSpecified == true)
                {
                    deliveryDate = firstOrderLine.Delivery.LatestDeliveryDateTime < DateTime.Now ? OudeCode.Util.Helper.NextWorkday(DateTime.Now) : firstOrderLine.Delivery.LatestDeliveryDateTime;
                    deliveryDateSpec = true;
                    Log.Debug("deliveryDateSpec  is true en deliveryDate gecontroleerd op verleden deliverydate is : " + deliveryDate.ToString());
                }
            }

            //Extra check de deliverdate mag niet vandaag zijn als het na 7:00 uur is..
            if (DateTime.Now.Hour > 7 && deliveryDate.Date <= System.DateTime.Today.Date)
            {
                Log.Debug("deliveryDateSpec was vandaag en het is na 7 uur dus corrigeren deliverydate is nu : " + deliveryDate.Date.ToString() + " was kleiner dan " + System.DateTime.Today.Date.ToString());
                deliveryDate = OudeCode.Util.Helper.NextWorkday(DateTime.Now);
            }
            else
                Log.Debug("deliveryDateSpec was of niet vandaag of het was voor 7 uur dus geen correctie nodig : " + deliveryDate.Date.ToString() + " was niet kleiner dan " + System.DateTime.Today.Date.ToString());

            Log.DebugFormat("Deliverdate : [{0}]", deliveryDate);

            // Doe de initiële processing en checks op de Order Lines.
			var details = new List<PutOrderDetail>();

			for (var i = 0; i < PutOrderRequest.Body.Order.OrderTradeLineItem.Length; i++)
			{
				var prefix = String.Format("/PutOrderRequest/Body/Order/OrderTradeLineItem[{0}]/", i);
				details.Add(CreateDetailFromRequest(PutOrderRequest.Body.Order.OrderTradeLineItem[i], principal, prefix, deliveryDate, deliveryDateSpec));
			}

            //Nog een keer kijken hoe we de orderomschrijving willen bepalen..
            remark = "LogiService";
			var detailsWithActual = details.Where(l => l.UnitsActual > 0);

			// Loop through the unique Header Sequence and Delivery Date combinations.)
			foreach (var header in detailsWithActual.Select(d => new
			{
				d.OfferDetailView.HeaderSequence,
				d.DeliveryDate,
                d.DeliveryDateSpecified
			}).Distinct())
			{
				var head = header;
				int orderHeaderSequence;
				try
				{
                    Log.InfoFormat("OrderHeadsequence [{0}]", head.HeaderSequence.ToString());
                    Log.InfoFormat("Deliverydate [{0}]", deliveryDate);
                    Log.InfoFormat("personsequence [{0}]", principal.Person.Sequence);
                    Log.InfoFormat("DeliverydateSpecified [{0}]", deliveryDateSpec); 
                    orderHeaderSequence = CommonOrderProvider.CreateOrderHeaderJun2010(head.HeaderSequence.Value, deliveryDate, principal.Person.Sequence, ShopPropertyProvider.ShopId, remark, deliveryDateSpec, false);
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Error creating Order Header.", ex);
					throw new FlorecomException("Unexpected Error saving Order.");
				}

				Log.InfoFormat("Created Order Header [{0}].", orderHeaderSequence);
                
				// Loop through the lines for this header.
				foreach (var line in detailsWithActual.Where(l =>
					l.OfferDetailView.HeaderSequence == head.HeaderSequence &&
					l.DeliveryDate == head.DeliveryDate))
				{
					var referencedDocumentTypes = new List<ReferencedDocumentType>(line.Response.ReferencedDocument);
					var referencedDocumentType = new ReferencedDocumentType
					{
						IssuerAssignedID = new IDType { Value = orderHeaderSequence.ToString() },
						TypeCode = new DocumentCodeType { Value = DocumentNameCodeContentType.Item101 }
					};
					referencedDocumentTypes.Add(referencedDocumentType);

                    //Beginnen status op fout, als er iets misgaat staat dit goed.
                    //Aan het eind wijzigen we de status naar 73.
                    line.Response.Status = new StatusType[]
                    {
                        new StatusType 
                        {
                            ConditionCode = new StatusConditionCodeType[] 
                            {
                                new StatusConditionCodeType 
                                { 
                                    Value =  StatusConditionCodeContentType.Item72
                                }
                            }, 
                            DescriptionText  = new TextType[] 
                            {
                                new TextType 
                                { 
                                    Value = "Goods are not available for ordering.", 
                                    languageCode = "en" 
                                }
                            }
                        }
                    };

					try
					{
						Log.DebugFormat("Calling proc_createOrderDetailService");
						Log.DebugFormat("OFDT_SEQ = [{0}]", line.OfferDetailView.Sequence);
						Log.DebugFormat("ORHE_SEQ = [{0}]", orderHeaderSequence);
						Log.DebugFormat("NUM_OF_UNITS = [{0}]", line.UnitsActual);
                        Log.DebugFormat("CustCode = [{0}]", line.CustCode);
                        int ofhdSeq = -1;
                        int ofdtSeq = -1;

						// Aanmaken OrderDetail regel en even loggen van de nieuwe Sequence.
						var orderDetailSequence = CommonOrderProvider.CreateOrderDetailService(
							line.OfferDetailView.Sequence,
							orderHeaderSequence,
							line.UnitsActual,
                            line.CustCode,
                            line.OrderRefID,
							line.Remark,
                            string.Empty,
                            false,
                            ref ofdtSeq,
                            ref ofhdSeq);
                        
						referencedDocumentTypes.Add(new ReferencedDocumentType
						{
							IssuerAssignedID = new IDType { Value = orderDetailSequence.ToString() },
							TypeCode = new DocumentCodeType { Value = DocumentNameCodeContentType.Item102 }
						});

                        if (orderDetailSequence <= 0)
                        {
                            Log.ErrorFormat("Fout bij aanmaken OrderDetail met OfferDetailSequence = [{0}]  Header.Sequence = [{1}]  NumberOfUnits = [{2}]", line.OfferDetailView.Sequence, orderHeaderSequence, line.UnitsActual);
                            Log.Error("Fout bij het aanmaken of bevestigen van de order ");
                            throw new FlorecomException("Unexpected Error saving Order.");
                        }
				
						Log.InfoFormat("OrderDetail aangemaakt met Sequence = [{0}]", orderDetailSequence);

                        //Nu de order nog in LF plaatsen..
                        if (principal.IsInRole(LogiCAB.Common.Enums.Role.SHOP_LF))
                        {
                            try
                            {
                                Log.Info("Shop_LF role aanwezig. Order wordt geplaatst in LogiFlora.");
                                var orderLF = new CommWinsock();
                                orderLF.PlaceOrder(Convert.ToInt64(principal.Organisation.Sequence), Convert.ToInt64(orderDetailSequence), principal.Login.Name, "SHOP_LF", 0);
                                line.Response.Status[0].ConditionCode[0].Value = StatusConditionCodeContentType.Item73;
                                line.Response.Status[0].DescriptionText[0].Value = "Officially acknowledged";
                            }
                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Fout bij toewijzen van de OrderDetail met OfferDetailSequence = [{0}]  Header.Sequence = [{1}]  NumberOfUnits = [{2}] in LogiFlora.", line.OfferDetailView.Sequence, orderHeaderSequence, line.UnitsActual);
                                //Dit komt in principe niet voor maar als het gebeurt is dit waarschijnlijk een verschil in voorrraad tussen LF en Web.
                                throw new FlorecomException(string.Format("Insufficient Units available to order [{1}]. Available [{2}].", line.UnitsOrdered, line.UnitsActual));
                            }
                        }
                        else
                        {
                            Log.Info("Shop_LF role NIET aanwezig.");
                            line.Response.Status[0].ConditionCode[0].Value = StatusConditionCodeContentType.Item73;
                            line.Response.Status[0].DescriptionText[0].Value = "Officially acknowledged";
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Fout bij aanmaken OrderDetail met OfferDetailSequence = [{0}]  Header.Sequence = [{1}]  NumberOfUnits = [{2}]", line.OfferDetailView.Sequence, orderHeaderSequence, line.UnitsActual);
                        Log.Error("Fout bij het aanmaken of bevestigen van de order ", ex);
                        throw new FlorecomException("Unexpected Error saving Order.");
					}
                    line.Response.ReferencedDocument = referencedDocumentTypes.ToArray();
				}

				// Bevestig de Header.
				Log.DebugFormat("Calling proc_updOrderHeader");
				Log.DebugFormat("ORHE_SEQ = [{0}]", orderHeaderSequence);
				CommonOrderProvider.UpdateOrderHeader(orderHeaderSequence);
                SignalProvider.SignalOrderConfirmed(orderHeaderSequence);

				// Log het bevestigen als 'Order' moment naar de LogiCAB event log.
				EventLogProvider.LogiServiceOrder(principal.Login.Sequence, orderHeaderSequence);
			}

			// Geef het resultaat terug aan de besteller.
			response.Body.OrderResponse.OrderResponseTradeLineItem = details.Select(d => d.Response).ToArray();
			return response;
		}

		public class PutOrderDetail
		{
			public string Prefix { get; set; }
			public OrderTradeLineItemType Request { get; set; }
			public OrderTradeLineItemType Response { get; set; }
			public ICabPrincipal Grower { get; set; }
			public OfferDetailView OfferDetailView { get; set; }
			public DateTime DeliveryDate { get; set; }
            public Boolean DeliveryDateSpecified { get; set; }
			public decimal UnitsOrdered { get; set; }
            public string CustCode { get; set; }
            public string OrderRefID { get; set; }
			public decimal UnitsActual { get; set; }
			public string Remark { get; set; }
		}

		private PutOrderDetail CreateDetailFromRequest(OrderTradeLineItemType orderLine, ICabPrincipal buyer, string prefix, DateTime deliveryDate, Boolean deliveryDateSpec)
		{
			// Bouw een detail met een Stub Response.
            var detail = new PutOrderDetail
			{
				Prefix = prefix,
				Request = orderLine,
				Response = new OrderTradeLineItemType
				{
					ID = new IDType {schemeName = "VN"},
					DocumentType = new DocumentCodeType1{Value = DocumentNameCodeContentType1.Item231},
					LineDateTime = DateTime.Now
				},
				DeliveryDate = deliveryDate,
                DeliveryDateSpecified = deliveryDateSpec
			};

			// Check de BuyerParty (Buyer).
            // Buyerparty is niet verplicht..
			//if (orderLine.BuyerParty == null)
			//	throw new FlorecomException("No BuyerParty specified.", detail.Prefix + "BuyerParty");
			
            //if (orderLine.BuyerParty.PrimaryID == null)
			//	throw new FlorecomException("No BuyerParty PrimaryID specified.", detail.Prefix + "BuyerParty/PrimaryID");
			
            //if (String.IsNullOrEmpty(orderLine.BuyerParty.PrimaryID.Value))
				//throw new FlorecomException("No BuyerParty PrimaryID Value specified.", detail.Prefix + "BuyerParty/PrimaryID/Value");

			// Een User mag alleen voor zijn eigen Organisatie kopen.
			//if (!buyer.Organisation.EAN.Equals(orderLine.BuyerParty.PrimaryID.Value))
				//throw new FlorecomException("Invalid BuyerParty specified.", detail.Prefix + "BuyerParty");
            if (orderLine.BuyerParty != null)
                if (orderLine.BuyerParty.PrimaryID != null)
                    detail.Response.BuyerParty = detail.Request.BuyerParty;

            if (orderLine.ID == null)
                //detail.Response.Status = new StatusType { ConditionCode = 
                throw new FlorecomException("No orderline PrimaryID specified.", detail.Prefix + "orderline/PrimaryID");
            
            if (orderLine.ID.Value == null)
                throw new FlorecomException("No orderline PrimaryID specified.", detail.Prefix + "orderline/PrimaryID");
            
            detail.OrderRefID = orderLine.ID.Value;
			
			// Check de SupplierParty (Grower).
            //SupplierParty niet verplicht.
			if (orderLine.SupplierParty != null)
				//throw new FlorecomException("No SupplierParty specified.", detail.Prefix + "SupplierParty");
                if (orderLine.SupplierParty.PrimaryID != null)
                {
                    detail.Grower = PrincipalService.GetOnEAN(orderLine.SupplierParty.PrimaryID.Value);
                    detail.Response.SupplierParty = detail.Request.SupplierParty;
                }
         		    //throw new FlorecomException("No SupplierParty PrimaryID specified.", detail.Prefix + "SupplierParty/PrimaryID");
			    //if (String.IsNullOrEmpty(orderLine.SupplierParty.PrimaryID.Value))
				//throw new FlorecomException("No SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");
			// Maak een principal aan voor de Supplier, en check dat het een grower is.

            //if (detail.Grower == null)
				//throw new FlorecomException("Invalid SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");
			
            //if (!detail.Grower.IsGrower)
				//throw new FlorecomException("Invalid SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");
			
            // Check de ReferencedDocument (Offer Detail Sequence).
			if (orderLine.ReferencedDocument == null || orderLine.ReferencedDocument.Length == 0)
				throw new FlorecomException("No ReferencedDocument specified.", detail.Prefix + "ReferencedDocument");
			
            if (orderLine.ReferencedDocument[0].IssuerAssignedID == null)
				throw new FlorecomException("No ReferencedDocument IssuerAssignedID specified.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID");
			
            if (String.IsNullOrEmpty(orderLine.ReferencedDocument[0].IssuerAssignedID.Value))
				throw new FlorecomException("No ReferencedDocument IssuerAssignedID Value specified.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

			// Ga op zoek naar de Offer Detail.
			var offerDetailSequence = orderLine.ReferencedDocument[0].IssuerAssignedID.Value.ToDecimal();
			
            if (!offerDetailSequence.HasValue)
				throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");
			
            if (offerDetailSequence < 0)
				throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");
			
            detail.OfferDetailView = OfferDetails.FirstOrDefault(od => od.Sequence == offerDetailSequence);
			
            if (detail.OfferDetailView == null)
			{
				Log.WarnFormat("offerDetailSequence [{0}] not found.", offerDetailSequence);
				throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");
			}

            if (detail.OfferDetailView.ValidFrom > System.DateTime.Now.AddHours(1) || detail.OfferDetailView.ValidTo < System.DateTime.Now.AddMinutes(-1))
            {
                Log.WarnFormat("Offerte is niet (meer) geldig, geldig vanaf [{0}], tot [{1}], datum order [{2}].", detail.OfferDetailView.ValidFrom.ToString(), detail.OfferDetailView.ValidTo.ToString(), System.DateTime.Now.ToString());
                throw new FlorecomException(String.Format("Offerte is niet (meer) geldig, geldig vanaf {0}, tot {1}, datum order{2}.", detail.OfferDetailView.ValidFrom, detail.OfferDetailView.ValidTo, System.DateTime.Now));
            }

            if (detail.OfferDetailView.LatestOrderDateTime.HasValue)
            {
                if (detail.OfferDetailView.LatestOrderDateTime.Value < System.DateTime.Now.AddMinutes(-1))
                {
                    Log.WarnFormat("Offerte is niet (meer) geldig, geldig vanaf [{0}], tot [{1}], datum order [{2}].", detail.OfferDetailView.ValidFrom.ToString(), detail.OfferDetailView.LatestOrderDateTime.ToString(), System.DateTime.Now.ToString());
                    throw new FlorecomException(String.Format("Offerte is niet (meer) geldig, geldig vanaf {0}, tot {1}, datum order{2}.", detail.OfferDetailView.ValidFrom.ToString(), detail.OfferDetailView.LatestOrderDateTime.ToString(), System.DateTime.Now.ToString()));
                }
            }

            detail.Response.ReferencedDocument = orderLine.ReferencedDocument;
            detail.Response.ID = new IDType
            { 
                    schemeName = "VN",
                    schemeDataURI = buyer.Organisation.EAN,
                    schemeURI = buyer.Organisation.EAN,
                    Value = orderLine.ID.Value
            };
             
			// Bepaal de delivery date time voor deze regel.
            if (orderLine.Delivery != null)
            {
                if (orderLine.Delivery.LatestDeliveryDateTimeSpecified)
                {
                    detail.DeliveryDate = orderLine.Delivery.LatestDeliveryDateTime;
                    detail.DeliveryDateSpecified = true;
                }
            }

			// Check de Ordered Quantity.
			if (orderLine.Quantity == null)
				throw new FlorecomException("No Quantity specified.", detail.Prefix + "Quantity");
			
            if (orderLine.Quantity.unitCodeSpecified && orderLine.Quantity.unitCode != FEC_MeasurementUnitCommonCodeContentType.Item3)
				throw new FlorecomException("Invalid Quantity unitCode specified.", detail.Prefix + "Quantity/unitCode");
			
            if (orderLine.Quantity.Value < 1)
				throw new FlorecomException("Invalid Quantity Value specified.", detail.Prefix + "Quantity/Value");

            // Bepaal hoeveel we er daadwerkelijk gaan bestellen.
            detail.UnitsActual = detail.UnitsOrdered = orderLine.Quantity.Value;

            //Controle op aantal stelen alleen nodig als unit quantity code item1 is per stuk...
            //if ((System.Decimal.Remainder(detail.OfferDetailView.ItemsPerUnit, detail.UnitsOrdered ) != 0) || (detail.UnitsOrdered < detail.OfferDetailView.ItemsPerUnit))
            //  throw new FlorecomException("Invalid Quantity Value specified. Only complete units possible.", detail.Prefix + "Quantity/Value");
			
            if (detail.UnitsActual > detail.OfferDetailView.UnitsAvailable)
                throw new FlorecomException("Not enough available.", detail.Prefix + "Quantity/Value");
				//detail.UnitsActual = detail.OfferDetailView.UnitsAvailable;
//				Log.WarnFormat("[{0}] Insufficient Units available to order [{1}]. Ordering [{2}] instead.", detail.OfferDetailView.Sequence, detail.UnitsOrdered, detail.UnitsActual);
			
            detail.Response.Quantity = new QuantityType
			{
				unitCode = FEC_MeasurementUnitCommonCodeContentType.Item3,
				unitCodeSpecified = true,
				Value = detail.UnitsActual
			};

			// Haal een mogelijke remark op.
			if (orderLine.AdditionalInformationTradeNote != null && orderLine.AdditionalInformationTradeNote.Length > 0)
				if (orderLine.AdditionalInformationTradeNote[0].Content != null)
					if (!String.IsNullOrEmpty(orderLine.AdditionalInformationTradeNote[0].Content.Value))
						detail.Remark = orderLine.AdditionalInformationTradeNote[0].Content.Value;

            // Eventuele klant code
            if (orderLine.EndUserParty != null)
            {
                if (orderLine.EndUserParty.PrimaryID != null)
                {
                    if (!string.IsNullOrEmpty(orderLine.EndUserParty.PrimaryID.Value))
                    {
                        detail.CustCode = orderLine.EndUserParty.PrimaryID.Value;
                        Log.Debug("orderLine.EndUserParty.PrimaryID.Value = " + orderLine.EndUserParty.PrimaryID.Value);
                    }
                    else
                        Log.Debug("IsNullOrEmpty(orderLine.EndUserParty.PrimaryID.Value");
                }
                else
                    Log.Debug("orderLine.EnduserParty.PrimaryID is null");
            }
            else
                Log.Debug("orderLine.EnduserParty is null");

            // Valideer de opgehaalde buyer en grower met elkaar.
			if (detail.OfferDetailView.Buyer != buyer.Buyer)
				throw new FlorecomException("Invalid Offer Detail specified. Incorrect Buyer.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");
			
            //if (detail.OfferDetailView.Grower != detail.Grower.Grower)
				//throw new FlorecomException("Invalid Offer Detail specified. Incorrect Grower.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

			return detail;
		}
	}
}