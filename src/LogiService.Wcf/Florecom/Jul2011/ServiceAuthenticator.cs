﻿using System.Web;
using LogiCAB.Common;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Services;

namespace LogiService.Wcf.Florecom.Jul2011
{
	/// <summary>
	/// De ServiceAuthenticator heeft als doel het controleren van de login
	/// gegevens uit de header van het request, en op basis hiervan een
	/// LoginPrincipal te bouwen.
	/// De ServiceAuthenticator zelf controleert op de aanwezigheid van de
	/// LogiService Role. Overige Authorisatie ligt bij de gebruiker.
	/// Als op basis van de header geen login gevonden kan worden, of de
	/// LogiService Role niet aanwezig is, wordt er een null teruggegeven.
	/// </summary>
	public class ServiceAuthenticator : IServiceAuthenticator
	{
		protected PrincipalService PrincipalService { get; set; }
  
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public ServiceAuthenticator(PrincipalService principalService)
		{
			PrincipalService = principalService;
		}

		public LoginPrincipal Authenticate(IRequestHeader requestHeader, string requestHeaderPrefix)
		{
			// Valideer de binnenkomende header.
			if (requestHeader == null)
				throw new FlorecomException("RequestHeader is null.", requestHeaderPrefix);
			if (requestHeader.UserName == null)
				throw new FlorecomException("RequestHeader UserName is null.", requestHeaderPrefix + "/UserName");
			if (requestHeader.Password == null)
				throw new FlorecomException("RequestHeader Password is null.", requestHeaderPrefix + "/Password");
			if (string.IsNullOrEmpty(requestHeader.UserName.Value))
				throw new FlorecomException("RequestHeader UserName Value is null or empty.", requestHeaderPrefix + "/UserName/Value");
			if (string.IsNullOrEmpty(requestHeader.Password.Value))
				throw new FlorecomException("RequestHeader Password Value is null or empty.", requestHeaderPrefix + "/Password/Value");
			var givenUserName = requestHeader.UserName.Value;

			// Kijk of we de gegeven login kunnen vinden.
			var principal = PrincipalService.GetOnUsernamePassword(givenUserName, requestHeader.Password.Value);
			if (principal == null)
			{
				Log.WarnFormat("Unable to find Login with username [{0}] and the given password.", givenUserName);
				return null;
			}

			// Check op Validiteit.
			if (!principal.Identity.IsAuthenticated)
			{
				Log.WarnFormat("Principal Identity is not Authenticated for username [{0}]. (ORAD_STATUS)", givenUserName);
				return null;
			}

			// Check op LogiService Role.
			if (!principal.IsInRole(Role.LogiService))
			{
				Log.WarnFormat("Principal does not have LogiService Role for username [{0}].", givenUserName);
				return null;
			}

			// Valide Login.
			Log.DebugFormat("Valid Principal created for username [{0}].", givenUserName);
			HttpContext.Current.User = principal;
			return principal;
		}
	}
}