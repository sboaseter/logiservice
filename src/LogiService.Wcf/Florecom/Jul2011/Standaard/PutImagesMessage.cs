﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2011
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class PutImagesMessage
	{
		/// <remarks/>
		public PutImagesMessageHeader Header { get; set; }

		/// <remarks/>
		public PutImagesMessageBody Body { get; set; }
	}
}