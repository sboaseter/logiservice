﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2011
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[System.SerializableAttribute]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class PutImagesMessageBody
	{
		/// <remarks/>
		public ImagesType PutImagesRequestDetails { get; set; }
	}
}