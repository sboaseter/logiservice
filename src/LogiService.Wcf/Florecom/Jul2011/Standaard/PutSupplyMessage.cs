﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2011
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(Namespace = "urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")]
	public class PutSupplyMessage
	{
		/// <remarks/>
		public PutSupplyMessageHeader Header { get; set; }

		/// <remarks/>
		public PutSupplyMessageBody Body { get; set; }
	}
}