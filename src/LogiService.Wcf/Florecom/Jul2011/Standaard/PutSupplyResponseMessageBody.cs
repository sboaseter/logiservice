﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2011
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[System.SerializableAttribute]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")]
	public class PutSupplyResponseMessageBody
	{
		/// <remarks/>
		[XmlArrayItemAttribute("SupplyAcknowledge", IsNullable = false)]
		public SupplyAcknowledgementTypeSupplyAcknowledge[] PutSupplyResponseDetails { get; set; }
	}
}