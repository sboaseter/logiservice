using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Xml;
using System.Transactions;

namespace LogiService.Wcf.Florecom.Jul2012
{
    /// <summary>
    /// Florecom Webservice Implementation
    /// ==================================
    /// System:     Florecom Commercial Scenario's
    /// Actor:      Supplier
    /// Version:    Juli 2012
    /// 
    /// This example contains both stubs and demos for all commercial use cases that are relevant for suppliers of floricultural products. 
    /// 
    /// Notes:
    /// -SOAP 1.2 is disabled using the web.config, because Florecom conversations rely on SOAP 1.1.
    /// -Please, do not alter the serialization attributes. This will guarantee the interoperability (WSDL compatibility) for suppliers among different market places.
    /// -Please, do check the functional and technical documentation as publisehed on sdk.florecom.org.
    /// 
    /// Author:
    /// -For more information, see http://sdk.florecom.org or contact Cris Ilbrink (cris.ilbrink@florecom.nl)
    /// </summary>
    [System.Web.Services.WebServiceAttribute(Namespace = "http://logiofferte.nl/logiservice/Florecom/Jul2012/CommercialSupplier.asmx", Description = "Florecom Web Service for the 'Commercial Use Cases' as used by the actor 'Supplier'. Release Jul 2012.", Name = "Florecom Commerical Web Service for the Supplier. Release Jul 2012.")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "CommercialSupplier", Namespace = "http://logioffer.nl/logiservice/Florecom/Jul2012/CommercialSupplier.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CommercialSupplier : ICommercialSupplier
    {
        #region PutSupply
        /// <summary>
        /// Stub for providing supply of a supplier to this market place
        /// </summary>
        /// <param name="PutSupplyRequest">Supply request message</param>
        /// <returns>Suplpy response message</returns>
        PutSupplyResponseMessage ICommercialSupplier.PutSupply(PutSupplyMessage PutSupplyRequest)
        {
            try
            {
                PutSupplyResponseMessage msg = new PutSupplyResponseMessage();
                return msg;
            }
            catch (Exception err)
            {
                ThrowFlorecomError(err.Message, 10000, "[Xpath location of element that causes the error e.g. /GetSupply/Header/Password]");
                return null;
            }

        }
        #endregion

        #region Error Handling
        /// <summary>
        /// Errors should be returned using a SOAP:Fault (SOAP version 1.1). The Detail-element of the Soap:fault should contain the errors according to
        /// The ErrorList scheme is described in the document "Webservice Beschrijvingen Virtuele Marktplaats" that is publised on sdk.florecom.org.
        /// the Florecom Errorlist-scheme e.g.:
        /// <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xml="http://www.w3.org/XML/1998/namespace">
        //  <env:Body>
        //    <env:Fault>
        //      <env:faultcode>env:Sender</env:faultcode>
        //      <env:faultstring>Error detected by the webservice. Check the Detail-element in this SOAP:Fault response message for more error details.</env:faultstring>
        //      <env:detail>
        //        <ErrorList xmlns="urn:fec:messages:data:final:FlorecomStandardMessage:0_5">
        //          <Error>
        //            <ErrorLevel>message</ErrorLevel>
        //            <Severity>error</Severity>
        //            <ErrorCode>102</ErrorCode>
        //            <ErrorLocation>SupplyResponse/Body/SupplyResponseDetails/AgentParty</fsm:ErrorLocation>
        //            <DescriptionText>Unknown agent party.</DescriptionText>
        //          </Error>
        //        </ErrorList>
        //      </env:detail>
        //    </env:Fault>
        //  </env:Body>
        //</env:Envelope>
        /// </summary>
        /// <param name="ErrorMessage"></param>
        public void ThrowFlorecomError(string ErrorMessage, int ErrorCode, string ErrorLocation)
        {
            //Construct the Florecom ErrorList structure
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Xml.XmlNamespaceManager ns = new System.Xml.XmlNamespaceManager(doc.NameTable);
            ns.AddNamespace("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            ns.AddNamespace("ferab", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
            System.Xml.XmlNode FlorecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:florecom:xml:data:draft:ReusableAggregateBusinessInformationEntity:1");
            System.Xml.XmlNode FlorecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            System.Xml.XmlNode FlorecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:4");
            FlorecomErrorNode.AppendChild(FlorecomErrorLevelNode);
            FlorecomErrorNode.AppendChild(FlorecomErrorSeverity);
            FlorecomErrorNode.AppendChild(FlorecomErrorErrorCode);
            FlorecomErrorNode.AppendChild(FlorecomErrorLocation);
            FlorecomErrorNode.AppendChild(FlorecomErrorDescriptionText);
            FlorecomErrorListNode.AppendChild(FlorecomErrorNode);
            //Add the custom values to the ErrorList strucure
            FlorecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
            FlorecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
            FlorecomErrorErrorCode.AppendChild(doc.CreateTextNode(ErrorCode.ToString()));
            FlorecomErrorLocation.AppendChild(doc.CreateTextNode(ErrorLocation));
            FlorecomErrorDescriptionText.AppendChild(doc.CreateTextNode(ErrorMessage));
            detailNode.AppendChild(FlorecomErrorListNode);
            //Return a Soap:Fault that incorporates the Florecom ErrorList scheme
            throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, detailNode);
        }
        #endregion

    }
}