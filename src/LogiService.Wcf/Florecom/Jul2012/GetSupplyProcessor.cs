﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LogiCAB.Common;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.Florecom.Jul2012
{
	public class GetSupplyProcessor : IGetSupplyProcessor
	{
		protected IImageProvider ImageProvider { get; set; }
		protected IShopPropertyProvider ShopPropertyProvider { get; set; }
        protected IRepository<Handshake> Handshakes { get; set; }
        protected IRepository<SpecialDate> SpecDate { get; set; }
        protected IRepository<HandshakeOption> HandShakeOptions { get; set; }
        protected IRepository<Address> Addresses { get; set; }
        
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public GetSupplyProcessor(
			IImageProvider imageProvider,
			IShopPropertyProvider shopPropertyProvider,
            IRepository<Handshake> handshakes,
            IRepository<HandshakeOption> handshakeOptions,
            IRepository<Address> addresses,
            IRepository<SpecialDate> specDate)
		{
			ImageProvider = imageProvider;
			ShopPropertyProvider = shopPropertyProvider;
            Handshakes = handshakes;
            HandShakeOptions = handshakeOptions;
            Addresses = addresses;
            SpecDate = specDate;
    	}

		public SupplyResponseMessage Process(SupplyRequestMessage SupplyRequest, LoginPrincipal principal)
		{
			// Controleer of de gebruiker wel een Buyer is. Anders heeft dit weinig zin.
			if (!principal.IsBuyer)
				throw new FlorecomException("Only Buyers can access GetSupply.", "/SupplyRequest/Header");

            List<System.DateTime> listHoliDays;
            listHoliDays = (from spd in SpecDate
                            where spd.SpecialType.Contains("NO_DELIVERY")
                            select spd.SpecialDay).ToList();
                   
			// Bepaal of er een MutationDateTime gegeven wordt.
			var mutationDateTime = new DateTime(2000, 1, 1);
            var delMoments = new List<DelMoment>();
            Boolean mutationDateTimeSpec = false;
            DateTime? latestedDelTime = null;
            var settingSupplierOrg = new SettingData().GetSettingByID("SUPPLIERGLNVMP", null, principal.Organisation.Sequence, null);
            //Todo toevoegen log met check of vaker zonder mutatiedatum wordt gewerkt, binnen 1 uur mag niet 2 keer zonder mutatiedatum worden gewerkt.
            //new EventLogProvider(new SvcConnectionProvider()).LogiServiceOrder(principal.Login.Sequence, );
            var settingExtraProps = new SettingData().GetSettingByID("ShowExtraPropVMP", null, principal.Organisation.Sequence, null);
            
            if ((SupplyRequest.Body != null) && (SupplyRequest.Body.SupplyRequestDetails != null) && (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine != null)
                 && (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine.Length > 0))
            {
                if (SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine[0].MutationDateTimeSpecified)
                {
                    mutationDateTime = SupplyRequest.Body.SupplyRequestDetails.SupplyRequestLine[0].MutationDateTime;
                    mutationDateTimeSpec = true;
                }
            }

			Log.Debug ("Header verwerkt");
            Log.DebugFormat("Meegegeven mutatiedatum (2000-1-1 is geen datum): {0} mutatiedatetime specified: {1}", mutationDateTime, mutationDateTimeSpec);

			try
			{
				var supplyLines = new List<SupplyTradeLineItemType>();
				var offerDetailData = new OfferDetailData();
                int prevOfhdSeq = -1;
                Boolean delOptionsAdded = false;
                Int16 recCount = 0;
                Handshake handshake = null;
                HandshakeOption handshakeOption = null;
            
				foreach (var offerDetail in offerDetailData.GetOfferDetailsDS(
					principal.Buyer.Sequence,
					ShopPropertyProvider.ShopId,
					mutationDateTime))
				{
                    int imageSequence = 0;
                    string imageUrl;
                    string imageType = "P";
                    string supplierOrgGLN = "";
                    
                    if (!string.IsNullOrEmpty(offerDetail.PictureRef))
                        imageUrl = offerDetail.PictureRef;
                    else
                        imageUrl = "";

                    recCount++;
                    //Log.Debug ("Er zijn details kijken naar foto");
                   
                    if (offerDetail.CABPicture.HasValue)
                    {
                        //Gevuld met de pict_sequence
                        imageType = "P";
                        imageSequence = offerDetail.CABPicture.Value;
                    }

                    if (offerDetail.OfferPicture.HasValue)
                    {
                        //Gevuld met de pict_sequence
                        imageType = "P";
                        imageSequence = offerDetail.OfferPicture.Value;
                    }
                    else
                    {
                        if (offerDetail.GrowAssortmentPicture.HasValue)
                        {
                            //Gevuld met de pict_sequence
                            imageType = "P";
                            imageSequence = offerDetail.GrowAssortmentPicture.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(imageUrl))
					    imageUrl = HttpContext.Current.Request.ResolveFullUrl(ImageProvider.GetStaticUrlFor(imageType, imageSequence, null, null, null));

					// Bouw een lijst met Characteristics.
					//Log.Debug("Lijst met kenmerken opbouwen");

					var characteristicTypes = offerDetailData
						.PropertiesDSVBN(offerDetail.OfdtFkCABCode, offerDetail.GrowAssortmentSequence)
						.Rows.Cast<DataRow>()
						.Select(drP => CharacteristicTypeVBN(drP))
						.Where(chara => chara != null).ToList();
					// Voeg de imageUrl toe.
				    //Log.Debug("Kenmerken zijn opgebouwd nu foto toevoegen.");

                    if (offerDetail.PictureRef.Length > 1)
                    {
                        imageUrl = offerDetail.PictureRef; 
                    }

                    if (settingExtraProps != null)
                    {
                        characteristicTypes.Add(GetCharacteristic("GroupCode", offerDetail.VBNGroupCode, "1", "LogiFlora", "1", "LogiFlora")); //todo terug zetten Florecom test
                        characteristicTypes.Add(GetCharacteristic("VBNDesc", offerDetail.VBNDesc, "1", "LogiFlora", "1", "LogiFlora")); //todo terug zetten Florecom test
                        characteristicTypes.Add(GetCharacteristic("GroupDesc", offerDetail.VBNGroupDesc, "1", "LogiFlora", "1", "LogiFlora")); //todo terug zetten Florecom test
                        characteristicTypes.Add(GetCharacteristic("GroupType", offerDetail.VBNGroupType.ToString(), "1", "LogiFlora", "1", "LogiFlora")); //todo terug zetten Florecom test

                        if (offerDetail.CABDesc.Length > 0) //todo terug zetten florecom test.
                        {
                            characteristicTypes.Add(GetCharacteristic("CabDesc", offerDetail.CABDesc, "1", "LogiFlora", "1", "CAB omschrijving"));
                        }

                        if (offerDetail.OfferType != null)// todo terug zetten Florecom test
                        {
                            characteristicTypes.Add(GetCharacteristic("OfferType", offerDetail.OfferType, "1", "LogiFlora", "1", "Offer type"));
                        }
                    }
                    
					var priceOptions = new List<PriceType>
					{
						// Standaardprijs-regel
						GetPriceType(offerDetail.OfdtItemPrice)
					};

                    //Log.Debug("Prijslijst opgebouwd");

                    if (offerDetail.HasStaffelBool)
                    {
                        priceOptions.AddRange(
                            offerDetailData.StaffelDS(offerDetail.OfdtSequence)
                                .Rows.Cast<DataRow>()
                                .Select(row => GetPriceTypeStaffel(row)));
                    }

                    //Log.Debug("Staffels toegevoegd");
					// Haal de GLN en Naam op van de huidige marktplaats.
					var marketPlaceGLN = ShopPropertyProvider.Property("MarketPlaceGLN", "8713783439227");
					var marketPlaceName = ShopPropertyProvider.Property("MarketPlaceName", "LogiCAB");
					//Log.Debug("Marktplaats en naam");

					var details = GetBaseLine(
						offerDetail.OfdtSequence,
						//String.Concat(offerDetail.OfhdRemark, " ", offerDetail.OfdtRemark),
                        offerDetail.OfdtRemark,
						offerDetail.MutationDate ?? DateTime.Now,
						marketPlaceGLN,
						marketPlaceName);

                    //Log.Debug("Baseline gevuld");
                    details.SellerParty = GetSellerPartyType(offerDetail.OrgaEANCode);
                    //Log.Debug("Sellerparty");
                    details.Product = GetSuppliedProductIdentity(offerDetail.CabcCABCode, offerDetail.VBNDesc, offerDetail.VbncVbnCode, offerDetail.CABDesc, characteristicTypes.ToArray());
					//Log.Debug("Product gevuld");
                    details.TradingTerms.TradePeriod = GetSupplyPeriod(offerDetail.OfhdValidFrom, offerDetail.OfhdValidTo);
                    //Log.Debug("TradingTerms tradeperiod");
                    //Deze alleen invullen als het om een light aanroep gaat..
                    details.TradingTerms.Condition = GetCondition();
                    //Log.Debug("TradingTerms condition");

                    if (offerDetail.GrowerGLN != null)
                    {
                        if (offerDetail.GrowerGLN.Length > 1)
                            details.Product.ManufacturerParty = GetManufacturerPartyType(offerDetail.GrowerGLN);
                        else
                            details.Product.ManufacturerParty = GetManufacturerPartyType(offerDetail.OrgaEANCode);
                    }

                    details.ReferencedDocument = GetReferenceDocumentType(offerDetail.OrgaEANCode + imageSequence.ToString(), "IRN", "8713783439227", "8713783439227", imageUrl, "001");
                    //Log.Debug("Manufacturerparty");
                    supplierOrgGLN = offerDetail.OrgaEANCode;

                    //De Supplierparty kan via een setting worden gestuurd..
                    if (settingSupplierOrg != null)
                        if (settingSupplierOrg.SettingValue.Length > 1)
                            supplierOrgGLN = settingSupplierOrg.SettingValue;

                    details.SupplierParty = GetSupplierPartyType(supplierOrgGLN);
                    //Log.Debug("Supplierparty");
					details.Quantity = QuantityType(offerDetail.OfdtNumOfItems, FEC_MeasurementUnitCommonCodeContentType.Item3);
                    details.IncrementalOrderableQuantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item3);
					//Log.Debug("Quantity");
					details.Price = priceOptions.ToArray();
					//Log.Debug("Price");
					details.Packing = GetPackingOptions(offerDetail.CdcaCaskCode, offerDetail.BulkCdcaCaskCode, offerDetail.CacaNumOfUnits, offerDetail.CacaUnitsPerLayer, offerDetail.CacaLayersPerTrolley, offerDetail.NumOfItemsPerBunch);
					//Log.Debug("Verpakking");

                    //Bepaalde gegevens alleen ophalen als de grower verandert..
                    //Dus handshake en delivery moments..
                    if (prevOfhdSeq != offerDetail.OfhdSequence)
                    {
                        handshake = null;
                        handshakeOption = null;
                        delMoments.Clear();
                        prevOfhdSeq = offerDetail.OfhdSequence;
                    }

                    //Voor de delivery options hebben we eerst de handshake nodig om op basis van de handshakeoptions achter de afspraak te komen.
                    //Orga1 is kweker, orga2 is koper.., maar 1 keer per kweker ophalen..
                    if (handshake == null)
                        handshake = Handshakes.First(h => h.Orga1.Sequence == offerDetail.GrowOrgSeq && h.Orga2.Sequence == principal.Buyer.Organisation.Sequence);
                    
                    int? delHeader = -1;
                    DateTime valid_from_cor = offerDetail.OfhdValidFrom;

                    //Mocht valid_from in het verleden liggen dan corrigeren en beginnen vanaf vandaag..
                    if (valid_from_cor.Date < DateTime.Now.Date)
                        valid_from_cor = DateTime.Now;

                    try
                    {
                        //Alleen de eerste keer ophalen. Blijft gedurende de offerte hetzelfde.
                        if (handshake.Sequence != null && handshakeOption == null)
                        {
                            handshakeOption = HandShakeOptions.FirstOrDefault(ho => ho.Handshake.Sequence == handshake.Sequence);
                           
                            if (handshakeOption.Sequence != null)
                            {
                                delHeader = handshakeOption.DeliveryMomentHeader;
                            }
                        }

                        if (handshakeOption != null)
                        {
                            if (handshakeOption.Sequence != null)
                            {
                                
                                //Controleren of we al data hebben..
                                //We hoeven het maar 1 keer op te halen.
                                if (delMoments.Count == 0)
                                {
                                    var dtable = offerDetailData.GetPossibleDelMoments(delHeader, offerDetail.OfhdSequence, handshakeOption.Sequence);

                                    if (dtable != null && dtable.Rows.Count > 0)
                                    {
                                        var result = dtable.Rows.Cast<DataRow>();
                                        delMoments = result.Select(x => new DelMoment
                                        {
                                            MAX_DELIVER_TIME = x.Field<TimeSpan?>("MAX_DELIVER_TIME"),
                                            MAX_ORDER_TIME = x.Field<TimeSpan?>("MAX_ORDER_TIME"),
                                            DELIVERY_TYPE = x.Field<String>("DELIVERY_TYPE"),
                                            DMDT_WEEKDAY_NUMBER = x.Field<int>("DMDT_WEEKDAY_NUMBER"),
                                            GLN_LOCATION = x.Field<String>("GLN_LOCATION")
                                        }).ToList();
                                    }
                                }

                                details.Delivery = GetDeliveryOptions(valid_from_cor, offerDetail.OfhdValidTo, listHoliDays, delMoments);
                                delOptionsAdded = true;
                            }
                        }

                    }

                    catch(Exception ex)
                    {
                        delOptionsAdded = false;    
                    }

                    if (delOptionsAdded == false)
                    {
                        //Log.Info("Delivery options niet gevonden gebruik default..");
                        details.Delivery = GetDeliveryOptions(valid_from_cor, offerDetail.OfhdValidTo, listHoliDays, delMoments);
                    }
                    
                    details.Status = GetStatus(offerDetail.OfdtNumOfItems);
					supplyLines.Add(details);
				}

               var response = new SupplyResponseMessage
				{
					Body = new SupplyResponseMessageBody
					{
						SupplyResponseDetails = new SupplyType
						{
							SupplyTradeLineItem = supplyLines.ToArray()
						}
                        
					}
				};
                Log.DebugFormat("Totaal {0} verwerkt", recCount.ToString());
				return response;
			}

			catch (Exception ex)
			{
				Log.Error("Error in FillMessage", ex);
				throw new FlorecomException("Error creating Response.");
			}
		}

		private static SupplyTradeLineItemType GetBaseLine(
			decimal sequence,
			string remark,
			DateTime lineDateTime,
			string marketPlaceGLN,
			string marketPlaceName)
		{
            return new SupplyTradeLineItemType
            {
                ID = new IDType
                {
                    //GLN bedrijfscode van de marktplaats
                    schemeDataURI = marketPlaceGLN,
                    schemeURI = marketPlaceGLN,
                    //schemeID  =  "1",
                    schemeName = "AAG",
                    //schemeAgencyName = "FEC",
                    //Partij identificatie
                    Value = sequence.ToString()
                },
                
                TradingTerms = new TradingTermsType
                {
                    MarketPlace = new MarketPlaceType 
                    { 
                        ID = new IDType 
                        { 
                            schemeAgencyName = "FEC", 
                            schemeID = "1",   
                            Value = marketPlaceGLN 
                        } 
                    },
                    MarketFormCode = new MarketFormCodeType
                    {
                        //2 is standaard, 5 is catalogus, 1 is committed en 3 is buffer.
                        Value = MarketFormCodeContentType.Item002
                    },
                },

                LineDateTime = lineDateTime,

                //9 is algemeen aanbod 310 is selectief
                DocumentType = new DocumentCodeType1
                {
                    Value = DocumentNameCodeContentType1.Item9
                },

                // General or promotion information.
                AdditionalInformationTradeNote = new[]
				{
					new TradeNoteType
					{
                        SubjectCode = CodeType("AAI"),
						Content = TextType(remark.Trim())
					},
				},
            };
		}

		private static CodeType CodeType(string value)
		{
			return new CodeType { Value = value };
		}

		private static MarketPlaceType MarketPlaceTypeCAB(string gln, string name)
		{
			// TODO: Deze moeten waarschijnlijk Shop specifiek worden...
			// TODO: De SupplyResponseDetails heeft ook een Agent veld wat mischien interessant is om te vullen.
            return new MarketPlaceType
            {
                //GLN bedrijfscode van de marktplaats
                ID = new IDType
                {
                    Value = gln,
                    schemeID = "1",
                    schemeAgencyName = "AAG"
                },
                NameText = new Jul2012.TextType { Value = name }
            };
		}

        private static ConditionType [] GetCondition()
        {
            return new[] 
            {
                new ConditionType
                {
                    TypeCode = new [] 
                    {
                        new TradeConditionCodeType 
                        { 
                            Value = TradeConditionCodeContentType.Item301  //is minimum implementatie conform vmp light.
                        }
                    },
                
                    ValueMeasure = new []
                    {
                        new MeasureType1 
                        { 
                            Value = 1 
                        }
                    }
                }
            };
        }

		private static PeriodType GetSupplyPeriod(DateTime validFrom, DateTime validTo)
		{
			return new PeriodType
			{
				StartDateTime = validFrom,
				StartDateTimeSpecified = true,
				EndDateTime = validTo,
				EndDateTimeSpecified = true
			};
		}

		private static ProductType GetSuppliedProductIdentity(
			string cabCode,
			string vbnDesc,
			string vbnCode,
            string cabDesc,
			CharacteristicType[] characteristicTypes)
		{
			return new ProductType
			{
				IndustryAssignedID = new  IDType
                {
                    Value = vbnCode,
                    schemeAgencyName = "VBN",
                    schemeID = "1",
                },      
				//ManufacturerAssignedID = IDType("CAB", cabCode),
                SupplierAssignedID = IDType("CAB", cabCode), // todo terug zetten.. IDType("CAB", cabCode),
                //SupplierAssignedID = new IDType { Value = cabCode }, // todo deze gebruiken bij testen florecom
				DescriptionText = TextType(cabDesc), //TextType(vbnDesc),
                TypeCode = new TradeItemCodeType{ Value = TradeItemCodeContentType.Item57},
                ApplicableGoodsCharacteristics = characteristicTypes
			};
		}

		private static IDType IDType(string scheme, string value)
		{
			return new IDType
			{
				schemeName = scheme,
				Value = value
			};
		}

		private static IDType IDType(string schemeDataUri, string scheme, string value)
		{
			return new IDType
			{
				schemeDataURI = schemeDataUri,
				schemeName = scheme,
				Value = value
			};
		}

		private static TextType TextType(string value)
		{
			return new TextType {Value = value, languageCode = "NL"};
		}

		private static CharacteristicType CharacteristicTypeVBN(DataRow drP)
		{
			return GetCharacteristic(
				(string)drP["VBNPropCode"],
				(string)drP["VBNPropValue"],
				"8", "VBN",
				"9", "VBN");
		}

		private static CharacteristicType GetCharacteristic(
			string classCode,
			char valueCode,
			string classCodeListId,
			string classCodeListAgencyName,
			string valueCodeListId,
			string valueCodeListAgencyName)
		{
            if (valueCode == null)
                return null;

			// TypeCode was ClassCode en single.
			// ValueCode was array.
			var result = new CharacteristicType
			{
				TypeCode = new[] {
					new CodeType
					{
						listID = classCodeListId,
						listAgencyName = classCodeListAgencyName,
						Value = classCode.ToString()
					}
				},
				ValueCode = new CodeType
				{
					listID = valueCodeListId,
					listAgencyName = valueCodeListAgencyName,
					Value = valueCode.ToString()
				}
			};

			return result;
		}

        private static CharacteristicType GetCharacteristic(
            string classCode,
            string valueCode,
            string classCodeListId,
            string classCodeListAgencyName,
            string valueCodeListId,
            string valueCodeListAgencyName)
        {
            if (String.IsNullOrEmpty(valueCode))
                return null;

            // TypeCode was ClassCode en single.
            // ValueCode was array.
            var result = new CharacteristicType
            {
                TypeCode = new[] {
					new CodeType
					{
						listID = classCodeListId,
						listAgencyName = classCodeListAgencyName,
						Value = classCode
					}
				},
                ValueCode = new CodeType
                {
                    listID = valueCodeListId,
                    listAgencyName = valueCodeListAgencyName,
                    Value = valueCode
                }
            };

            return result;
        }
        
        private static SupplierPartyType GetSupplierPartyType(string supplierEAN)
		{
			return new SupplierPartyType
			{
				PrimaryID = new IDType
				{
					schemeID = "1",
					schemeAgencyName = "FEC",
					Value = String.IsNullOrEmpty(supplierEAN) ? "8700000000000" : supplierEAN
				}
			};
		}

        private static ReferencedDocumentType [] GetReferenceDocumentType(string idType, string name, string dataUri, string uri, string uriID, string lineID)
        {
            return new [] 
            { 
                new ReferencedDocumentType
                {
                   
                    IssuerAssignedID = new IDType
                    {
                       // schemeID =  idType.PadRight(26,'0'),
                        schemeName = name,
                        schemeDataURI = dataUri,
                        schemeURI = uri
                    },
                    URIID = new IDType { Value = uriID },
                    LineID = new IDType { Value = string.IsNullOrEmpty(lineID)? "001" : lineID }
                }
            };
        }

        private static ManufacturerPartyType GetManufacturerPartyType(string manufacturerEAN)
        {
            return new ManufacturerPartyType
            {
                PrimaryID = new IDType
                {
                    schemeID = "1",
                    schemeAgencyName = "FEC",
                    Value = String.IsNullOrEmpty(manufacturerEAN) ? "8700000000000" : manufacturerEAN
                }
            };
        }

		private static SellerPartyType GetSellerPartyType(string sellerEAN)
		{
			return new SellerPartyType
			{
				PrimaryID = new IDType
				{
					schemeID = "1",
					schemeAgencyName = "FEC",
					Value = String.IsNullOrEmpty(sellerEAN) ? "8700000000000" : sellerEAN
				}
			};
		}

		private static PriceType GetPriceType(decimal price)
		{
			return new PriceType
			{
				TypeCode = new PriceTypeCodeType 
                { 
                    Value = PriceTypeCodeContentType.AE 
                },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
                NetPriceIndicatorSpecified = true,
				BasisQuantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item1),
                MinimumQuantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item3)
			};
		}

		private static PriceType GetPriceTypeStaffel(DataRow drS)
		{
			var ofdsItemPrice = (decimal)drS["OFDS_ITEM_PRICE"];
			var ofdsFrom = (decimal)drS["OFDS_FROM"];
			var ofdsTo = (decimal)drS["OFDS_TO"];
			var ofdsParentType = (short)drS["OFDS_PARENT_TYPE"];
			return GetPriceTypeStaffel(ofdsItemPrice, ofdsFrom, ofdsTo, ofdsParentType);
		}

		private static PriceType GetPriceTypeStaffel(
			decimal price,
			decimal minQuantity,
			decimal maxQuantity,
			int itemType)
		{
			var m = GetM(itemType);
			return new PriceType
			{
				TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.PC },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
                NetPriceIndicatorSpecified = true,
				MinimumQuantity = QuantityType(minQuantity, m),
				MaximumQuantity = QuantityType(maxQuantity, m)
			};
		}

		private static QuantityType QuantityType(
			decimal value,
			FEC_MeasurementUnitCommonCodeContentType unit)
		{
			return new QuantityType
			{
				unitCode = unit,
				unitCodeSpecified = true,
				Value = value
			};
		}

        private static QuantityType IncrementalOrderableQuantity(
            decimal value,
            FEC_MeasurementUnitCommonCodeContentType unit)
        {
            return new QuantityType
            {
                unitCode = unit,
                unitCodeSpecified = true,
                Value = value
            };
        }

		private static AmountType EuroAmount(decimal price)
		{
			return new AmountType
			{
				currencyCode =  ISO3AlphaCurrencyCodeContentType.EUR,
				currencyCodeSpecified = true,
				Value = price
			};
		}

		private static FEC_MeasurementUnitCommonCodeContentType GetM(int itemType)
		{
			switch (itemType)
			{
				case 2:
					return FEC_MeasurementUnitCommonCodeContentType.Item4;
				case 3:
					return FEC_MeasurementUnitCommonCodeContentType.Item5;
			}
			return FEC_MeasurementUnitCommonCodeContentType.Item3;
		}

		private static PackingType[] GetPackingOptions(string caskCode, string bulkCaskCode, decimal NumOfItemsPerUnit, decimal unitsPerLayer, decimal layersPerTrolley, decimal numPerBunch)
		{
            //Niet volledig beladen, dan de minimale verpakking..
            if (unitsPerLayer <= 0 || layersPerTrolley <= 0)
            {
                return new[]
                {
                    new PackingType
					{
						Package = new PackageType
						{
							TypeCode = new[]
                            { 
                                new CodeType
								{
									listAgencyName = "VBN",
									listID = "901",
									Value = caskCode
                                }
							},
							Quantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item3),
						},
					
                        InnerPackageQuantity = QuantityType(NumOfItemsPerUnit, FEC_MeasurementUnitCommonCodeContentType.Item1)
                    }
                };
            }
            else
            {
                PackingType[] packTypeArr = new PackingType[1];
                PackingType packType;
                FEC_MeasurementUnitCommonCodeContentType unitType = new FEC_MeasurementUnitCommonCodeContentType();
                decimal numOfBunch = 0;
                decimal numOfUnits = 0;

                if (NumOfItemsPerUnit > 0 && numPerBunch > 0)
                {
                    numOfBunch = NumOfItemsPerUnit / numPerBunch;

                    if ((NumOfItemsPerUnit % numPerBunch) != 0)
                        numOfBunch = 0;
                }

                packType = new PackingType
                {
                    Package = new PackageType
                    {
                        TypeCode = new[]
                        {
                            new CodeType
                            {
                                listAgencyName = "FEC",
                                listID = "8",
                                Value = "2"
                            }
                        },
                        Quantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item5)
                    },
                    InnerPackageQuantity = QuantityType(layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4)
                };

                packTypeArr[0] = packType;
                packType = null;

                packType = new PackingType
                {
                    Package = new PackageType
                    {
                        TypeCode = new[]
                        {
                            new CodeType
    					    {
	    					    listAgencyName = "FEC",
		    				    listID = "8",
			    			    Value = "19"
                            }
					    },
                        Quantity = QuantityType(layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4),
                    },
                    InnerPackageQuantity = QuantityType(unitsPerLayer, FEC_MeasurementUnitCommonCodeContentType.Item3)
                };
                
                if (numOfBunch > 0)
                {
                    numOfUnits = numOfBunch;
                    unitType = FEC_MeasurementUnitCommonCodeContentType.Item2;
                }
                else
                {
                    numOfUnits = NumOfItemsPerUnit;
                    unitType = FEC_MeasurementUnitCommonCodeContentType.Item1;
                }

                packTypeArr[0].InnerPacking = new PackingType[1];
                packTypeArr[0].InnerPacking[0] = packType;
                packType = null;

                packType = new PackingType
                {
                    Package = new PackageType
                    {
                        TypeCode = new[]
                        { 
                            new CodeType
                            {
                                listAgencyName = "VBN",
                                listID = "901",
                                Value = caskCode
                            }
                        },
                        Quantity = QuantityType(unitsPerLayer * layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item3),
                    },
                    InnerPackageQuantity = QuantityType(numOfUnits, unitType)
                };

                packTypeArr[0].InnerPacking[0].InnerPacking = new PackingType[1];
                packTypeArr[0].InnerPacking[0].InnerPacking[0] = packType;
                packType = null;

                if (numOfBunch > 0)
                {
                    packType = new PackingType
                    {
                        Package = new PackageType
                        {
                            TypeCode = new[]
                            { 
                                new CodeType
                                {
                                    listAgencyName = "VBN",
                                    listID = "901",
                                    Value = "800"
                                }
                            },
                            Quantity = QuantityType(unitsPerLayer * layersPerTrolley * numOfBunch, FEC_MeasurementUnitCommonCodeContentType.Item2),
                        },
                        InnerPackageQuantity = QuantityType(numPerBunch, FEC_MeasurementUnitCommonCodeContentType.Item1)
                    };

                    packTypeArr[0].InnerPacking[0].InnerPacking[0].InnerPacking = new PackingType[1];
                    packTypeArr[0].InnerPacking[0].InnerPacking[0].InnerPacking[0] = packType;
                    packType = null;
                }

                return packTypeArr;
/*                
                return new[]
			    {
				    new PackingType
				    {
					    Package = new PackageType
					    {
						    TypeCode = new[]
                            {
                                new CodeType
						        {
							        listAgencyName = "FEC",
							        listID = "8",
							        Value = "2"
                                }
						    },
                            Quantity = QuantityType(1, FEC_MeasurementUnitCommonCodeContentType.Item5)
					    },
                    
					    InnerPackageQuantity = QuantityType(layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4),
					    InnerPacking = new[]
					    {
						    new PackingType
						    {
							    Package = new PackageType
							    {
								    TypeCode = new[]
                                    {
                                        new CodeType
    								    {
	    								    listAgencyName = "FEC",
		    							    listID = "8",
			    						    Value = "19"
                                        }
								    },
								    Quantity = QuantityType(layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item4),
							    },
							    InnerPackageQuantity = QuantityType(unitsPerLayer, FEC_MeasurementUnitCommonCodeContentType.Item3),
							    InnerPacking = new[]
							    {
								    new PackingType
								    {
									    Package = new PackageType
									    {
										    TypeCode = new[]
                                            { 
                                                new CodeType
										        {
											        listAgencyName = "VBN",
											        listID = "901",
											        Value = caskCode
                                                }
										    },
										    Quantity = QuantityType(unitsPerLayer * layersPerTrolley, FEC_MeasurementUnitCommonCodeContentType.Item3),
									    },
									    InnerPackageQuantity = QuantityType(NumOfItemsPerUnit, FEC_MeasurementUnitCommonCodeContentType.Item1)
								    }
							    }
						    }
					    }
				    }
			    };*/
            }
		}

        private static DeliveryType[] GetDeliveryOptions(DateTime valid_from, DateTime valid_to, List<DateTime> listHolidays, List<DelMoment> delMoments)
        {
            List<DeliveryType> deltype = new List<DeliveryType>();
            int dow = (int)valid_from.DayOfWeek;
            TimeSpan? maxOrderTime = new TimeSpan();
            TimeSpan? maxDelTime = new TimeSpan();
            maxOrderTime = null;
            maxDelTime = null;
            Boolean addDelMoment = true;
            var totDays = valid_to.Date.Subtract(valid_from.Date).TotalDays;
            string delTypeRs = "";
            string locationGLN = "";

            for (int index = (int)dow; index <= (int)dow + totDays; index++)
            {
                var delmomDays = delMoments.Where(x => x.DMDT_WEEKDAY_NUMBER == (index % 7));

                foreach (var delMom in delmomDays)
                {
                    maxOrderTime = delMom.MAX_ORDER_TIME;
                    maxDelTime = delMom.MAX_DELIVER_TIME;
                    delTypeRs = delMom.DELIVERY_TYPE;
                    locationGLN = delMom.GLN_LOCATION;
                    addDelMoment = !((index % 7) == (int)dow && valid_from.Date == System.DateTime.Now.Date && maxOrderTime < System.DateTime.Now.TimeOfDay);
                    addDelMoment = addDelMoment && listHolidays.FirstOrDefault(h => h.Date == valid_from.AddDays(index - (int)dow).Date) == System.DateTime.MinValue;
                    addDelMoment = addDelMoment && maxDelTime != null && maxOrderTime != null;
                    //Controle of de levertermijn voor vandaag nog mogelijk is.
                    //Tenminste als vandaag ook de start van de geldigheid is.
                    if (addDelMoment)
                    {
                        //EXW – EX WORKS, is klant haalt het op bij de leverancier, locatie gln is de locatie van de kweker.
                        //DDP – DELIVERED DUTY PAID, is afleveren op locatie die de klant wil, de leverancier is verantwoordelijk voor de
                        //correcte levering en betaalt ook de kosten die hier bijkomen!.
                        //Dus default EXW gebruiken..
                        DeliveryTermsCodeContentType delTermType = delTypeRs == "DDP" ? DeliveryTermsCodeContentType.DDP : DeliveryTermsCodeContentType.EXW;
                        DateTime maxOrderDateTime = valid_from.AddDays(index - (int)dow).Date + maxOrderTime.Value;
                        DateTime maxDelDateTime = valid_from.AddDays(index - (int)dow).Date + maxDelTime.Value;
                        deltype.Add(GetDelType(maxOrderDateTime, locationGLN, delTermType, maxDelDateTime));
                    }
                }
            }
            return deltype.ToArray();
        }

        private static DeliveryType GetDelType(DateTime latestOrder, String locationGLN, DeliveryTermsCodeContentType typeOfDel, DateTime? latestDelivery)
        {
           return new DeliveryType
			{
                DeliveryTerms = new TradeDeliveryTermsType
				{
                    //EXW – EX WORKS, is klant haalt het op bij de leverancier, locatie gln is de locatie van de kweker.
                    //DDP – DELIVERED DUTY PAID, is afleveren op locatie die de klant wil, de leverancier is verantwoordelijk voor de
                    //correcte levering en betaalt ook de kosten die hier bijkomen!.
                    DeliveryTypeCode =  new DeliveryTermsCodeType { Value = typeOfDel },
					RelevantTradeLocation = new TradeLocationType
					{
						ID = new IDType
						{
							schemeID = "4",
							schemeAgencyName = "FEC",
							Value = (String.IsNullOrEmpty(locationGLN) ? "8714231208754" : locationGLN)
						}
					}
				},
				LatestOrderDateTime = latestOrder, // todo florecom test
				LatestOrderDateTimeSpecified = true,
				LatestDeliveryDateTime = (latestDelivery.HasValue ? latestDelivery.Value: latestOrder), // todo florecom test
				LatestDeliveryDateTimeSpecified = (latestDelivery.HasValue ? true: false),// todo florecom test
                //LatestDeliveryDateTimeSpecified = false, //todo deze gebruiken bij florecom test.
			};
        }

        private static StatusType[] GetStatus(decimal numOfUnitsAvailable)
        {
            StatusConditionCodeContentType status;

            if (numOfUnitsAvailable > 0)
                status = StatusConditionCodeContentType.Item71;
            else
                status = StatusConditionCodeContentType.Item72;

            return new[]
            {
                new StatusType
                {
                    ConditionCode = new [] 
                    {
                        new StatusConditionCodeType { Value = status },
                    },
                
                   // Mag niet in minimum offer..
                   // DescriptionText = new []
                   // {
                   //     new TextType { Value = "available for ordering", languageCode = "EN" }
                   // }
                },
            };
        }
	}
}