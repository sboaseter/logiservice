﻿using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Jul2012
{
	public interface IPutOrderProcessor
	{
		OrderResponseMessage Process(OrderRequestMessage PutOrderRequest, LoginPrincipal principal);
	}
}