﻿using System;
using LogiCAB.Common;

namespace LogiService.Wcf.Florecom.Jul2012
{
	public interface IServiceAuthenticator
	{
		LoginPrincipal Authenticate(IRequestHeader requestHeader, string requestHeaderPrefix);
	}
}