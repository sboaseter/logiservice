﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Data;
using LogiCAB.Common;
using LogiCAB.Common.Domain;
using LogiCAB.Common.Model;
using LogiCAB.Common.Services;
using LogiFlora.Common;
using LogiFlora.Common.Db;
using BA.Interface.Communicator;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.Florecom.Jul2012
{
	public class PutOrderProcessor : IPutOrderProcessor
	{
        protected List<DelMoment> delMoments;
        static Timer timer;
        static Boolean timeUp;
        static int orderComplete;
        protected IShopPropertyProvider ShopPropertyProvider { get; set; }
		protected ICommonOrderProvider CommonOrderProvider { get; set; }
		protected IEventLogProvider EventLogProvider { get; set; }
		protected PrincipalService PrincipalService { get; set; }
		protected ISignalProvider SignalProvider { get; set; }
        protected IRepository<SpecialDate> SpecDate { get; set; }
		protected IRepository<OfferDetailView> OfferDetails { get; set; }
        protected IRepository<Handshake> Handshakes { get; set; }
        protected IRepository<HandshakeOption> HandShakeOptions { get; set; }

		public PutOrderProcessor(
			IShopPropertyProvider shopPropertyProvider,
			ICommonOrderProvider commonOrderProvider,
			IEventLogProvider eventLogProvider,
			PrincipalService principalService,
			ISignalProvider signalProvider,
			IRepository<OfferDetailView> offerDetails,
            IRepository<Handshake> handshakes,
            IRepository<HandshakeOption> handshakeOptions,
            IRepository<SpecialDate> specDate)
		{
			ShopPropertyProvider = shopPropertyProvider;
			CommonOrderProvider = commonOrderProvider;
			EventLogProvider = eventLogProvider;
			PrincipalService = principalService;
			SignalProvider = signalProvider;
			OfferDetails = offerDetails;
            Handshakes = handshakes;
            HandShakeOptions = handshakeOptions;
            SpecDate = specDate;
		}
  
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public OrderResponseMessage Process(OrderRequestMessage PutOrderRequest, LoginPrincipal principal)
		{
            Log.Info("Process in PutO");
            string remark = "";
            List<System.DateTime> listHoliDays;
            listHoliDays = (from spd in SpecDate
                            where spd.SpecialType.Contains("NO_DELIVERY")
                            select spd.SpecialDay).ToList();

			// Controleer of de gebruiker wel een Buyer is. Anders heeft dit weinig zin.
			if (!principal.IsBuyer)
				throw new FlorecomException("Only Buyers can access PutOrder.", "/PutOrderRequest/Header");

			var response = new OrderResponseMessage();

			if (PutOrderRequest.Body == null)
				throw new FlorecomException("No Body specified.", "/PutOrderRequest/Body");

			response.Body = new OrderResponseMessageBody();

			if (PutOrderRequest.Body.Order == null)
				throw new FlorecomException("No Order specified.", "/PutOrderRequest/Body/Order");

			response.Body.OrderResponse = new OrderResponseType();
            
			if (PutOrderRequest.Body.Order.OrderTradeLineItem == null || PutOrderRequest.Body.Order.OrderTradeLineItem.Length == 0)
				throw new FlorecomException("No Order Lines specified.", "/PutOrderRequest/Body/Order/OrderTradeLineItem");

            var firstOrderLine = PutOrderRequest.Body.Order.OrderTradeLineItem[0];
 
            if (firstOrderLine == null)
                throw new FlorecomException("No Order Lines specified.", "/PutOrderRequest/Body/Order/OrderTradeLineItem");

            Log.DebugFormat("MessageDateTime : {0}", PutOrderRequest.Header.MessageDateTime); 
            
            // Doe de initiële processing en checks op de Order Lines
			var details = new List<PutOrderDetail>();

			for (var i = 0; i < PutOrderRequest.Body.Order.OrderTradeLineItem.Length; i++)
			{
				var prefix = String.Format("/PutOrderRequest/Body/Order/OrderTradeLineItem {0}/", i);
				details.Add(CreateDetailFromRequest(PutOrderRequest.Body.Order.OrderTradeLineItem[i], principal, prefix, listHoliDays));
			}

            // Check handshake_options.
            bool updatedHeader = false;
            //Nog een keer kijken hoe we de orderomschrijving willen bepalen..
            remark = "LogiService";
			var detailsWithActual = details.Where(l => l.UnitsActual > 0);
            var lstHeadersAdded = new List<CreatedOrderInfo>();

			// Loop through the unique Header Sequence and Delivery Date combinations.)
			foreach (var header in detailsWithActual.Select(d => new
			{
				d.OfferDetailView.HeaderSequence,
				d.DeliveryDate,
                d.DeliveryDateSpecified,
                d.HandShakeOption.AutoConfirm
			}).Distinct())
			{
                var orderDetailData = new OrderDetailsData();
                //Delivery date bekijken en indien aanwezig controleren of het aflevermoment mogelijk is.
            //    orderDetailData.GetPossibleDelMoments(
                var head = header;
                var orderHeadersList = new List<int>();
				int orderHeaderSeq;

				try
				{
                    Log.InfoFormat("OfferHeadsequence {0}", head.HeaderSequence.ToString());
                    Log.InfoFormat("Deliverydate {0}", header.DeliveryDate);
                    Log.InfoFormat("personsequence {0}", principal.Person.Sequence);
                    Log.InfoFormat("DeliverydateSpecified {0}", header.DeliveryDateSpecified);
					orderHeaderSeq = CommonOrderProvider.CreateOrderHeaderJun2010(head.HeaderSequence.Value, header.DeliveryDate, principal.Person.Sequence, ShopPropertyProvider.ShopId, remark, header.DeliveryDateSpecified, header.AutoConfirm == 1);
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Error creating Order Header.", ex);
					throw new FlorecomException("Unexpected Error saving Order.");
				}

				Log.InfoFormat("Created Order Header {0}.", orderHeaderSeq);
                orderHeadersList.Add (orderHeaderSeq);

				// Loop through the lines for this header.
				foreach (var line in detailsWithActual.Where(l =>
					l.OfferDetailView.HeaderSequence == head.HeaderSequence &&
					l.DeliveryDate == head.DeliveryDate))
				{
			        var referencedDocumentTypes = new List<ReferencedDocumentType>();
					//Beginnen status op fout, als er iets misgaat staat dit goed.
                    //Aan het eind wijzigen we de status naar 73.
                    line.Response.Status = new StatusType[]
                    {
                        new StatusType 
                        {
                            ConditionCode = new StatusConditionCodeType[] 
                            {
                                new StatusConditionCodeType 
                                { 
                                    Value =  StatusConditionCodeContentType.Item72
                                }
                            }, 
                            DescriptionText  = new TextType[] 
                            {
                                new TextType 
                                { 
                                    Value = "Goods are not available for ordering.", 
                                    languageCode = "en" 
                                }
                            }
                        }
                    };

					try
					{
						Log.DebugFormat("Calling proc_createOrderDetailService");
						Log.DebugFormat("OFDT_SEQ = {0}", line.OfferDetailView.Sequence);
						Log.DebugFormat("ORHE_SEQ = {0}", orderHeaderSeq);
						Log.DebugFormat("NUM_OF_UNITS = {0}", line.UnitsActual);
                        Log.DebugFormat("CustCode = {0}", line.CustCode);
                        Log.DebugFormat("OrderID = {0}", line.OrderRefID);
                        line.OrderHeaderSeq = orderHeaderSeq;
                        int baseOfdtSeq = -1;
                        int baseOfhdSeq = -1;
                        
                        // Aanmaken OrderDetail regel en even loggen van de nieuwe Sequence.
						line.OrderDetailSeq = CommonOrderProvider.CreateOrderDetailService(
							line.OfferDetailView.Sequence,
							line.OrderHeaderSeq,
							line.UnitsActual,
                            line.CustCode,
                            line.OrderRefID,
							line.Remark,
                            line.DeliverRemark,
                            false,
                            ref baseOfdtSeq,
                            ref baseOfhdSeq
						);

                        //Nu hebben we de uieke orderdetail reference dus nog toevoegen..
                        if (line.OrderDetailSeq <= 0)
                        {
                            Log.ErrorFormat("Fout bij aanmaken OrderDetail met OfferDetailSequence = {0}  Header.Sequence = {1}  NumberOfUnits = {2}", line.OfferDetailView.Sequence, line.OrderHeaderSeq, line.UnitsActual);
                            Log.Error("Fout bij het aanmaken of bevestigen van de order ");
                            throw new FlorecomException("Unexpected Error saving Order.");
                        }

                        Log.InfoFormat("OrderDetail aangemaakt met Sequence = {0}", line.OrderDetailSeq);
                        line.Response.ID.Value = line.OrderDetailSeq.ToString();
                       
                        //De huidige order ook toevoegen aan de list orders dan kunnen we alle orders via
                        //dezelfde methode bevestigen.
                        CreatedOrderInfo baseLine;
                        baseLine = new CreatedOrderInfo();
                        FillBaseLine(principal, line, baseLine);
                        lstHeadersAdded.Add(baseLine);
                    
                        //Check of de offerte regel verwijst naar een andere offerte.
                        //Dan moet er ook een order geplaatst worden bij de originele offerte..
                        //Theoretisch kan deze offerte ook weer gebaseerd zijn op een andere offerte.
                        bool baseExists = baseOfdtSeq > 0;

                        if (baseExists)
                        {
                            //De offerte is gebaseerd op een basis offerte dus hier ook een bestelling plaatsen.
                            //Voor het geval er meerdere offertes gelinkt zijn bouwen we een lijst op.
                            while (baseExists)
                            {
                                baseLine = new CreatedOrderInfo();
                                FillBaseLine(principal, line, baseLine);
                                baseLine.OfferDetailView = OfferDetails.FirstOrDefault(od => od.Sequence == baseOfdtSeq);
                                //De kvk kan een custcode voor de comm hebben meegegeven..
                                //Voor de order naar de leverancier moeten we de custcode van de comm gebruiken!
                                //Als deze niet gevuld is dan de code van de organisatie.
                                baseLine.CustCode = principal.Organisation.Info.OrderName;

                                if (string.IsNullOrEmpty(baseLine.CustCode))
                                    baseLine.CustCode = principal.Organisation.Code;    

                                baseExists = createOrderDetailBase(baseLine);
                                lstHeadersAdded.Add (baseLine);
                            }
                        }

                        if (!string.IsNullOrEmpty(line.OwnerEANCode))
                        {
                            referencedDocumentTypes.Add(new ReferencedDocumentType
                            {
                                IssuerAssignedID = new IDType
                                {
                                    schemeURI = line.OwnerEANCode,
                                    schemeDataURI = line.OwnerEANCode,
                                    schemeName = "AAG",
                                    Value = line.OfferDetailView.Sequence.ToString()
                                }
                            });
                        }

                        if (!string.IsNullOrEmpty(line.BuyerEANCode))
                        {
                            referencedDocumentTypes.Add(new ReferencedDocumentType
                            {
                                IssuerAssignedID = new IDType
                                {
                                    schemeURI = line.BuyerEANCode,
                                    schemeDataURI = line.BuyerEANCode,
                                    schemeName = "ON",
                                    Value = line.OrderRefID
                                },
                            });
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(principal.Organisation.EAN))
                            {
                                referencedDocumentTypes.Add(new ReferencedDocumentType
                                {
                                    IssuerAssignedID = new IDType
                                    {
                                        schemeURI = principal.Organisation.EAN,
                                        schemeDataURI = principal.Organisation.EAN,
                                        schemeName = "ON",
                                        Value = line.OrderRefID
                                    },
                                });
                            }
                        }

                        //Nu de order nog in LF plaatsen..
                        if (principal.IsInRole(LogiCAB.Common.Enums.Role.SHOP_LF))
                        {
                            try
                            {
                                Log.Info("Shop_LF role aanwezig. Order wordt geplaatst in LogiFlora.");
                                timeUp = false;
                                orderComplete = 0;
                                StartTimer();
                                var orderLF = new CommWinsock();
                                orderLF.OrderComplete += new CommWinsock.ProcessOrderComplete(orderLF_OrderComplete);
                                orderLF.PlaceOrder(Convert.ToInt64(principal.Organisation.Sequence), Convert.ToInt64(line.OrderDetailSeq), principal.Login.Name, "SHOP_LF", 0);

                                while (orderComplete == 0 && timeUp == false)
                                {
                                }

                                orderLF.OrderComplete -= new CommWinsock.ProcessOrderComplete(orderLF_OrderComplete);

                                if (orderComplete == 0 || orderComplete == 2)
                                {
                                    Log.ErrorFormat("Het is niet gelukt om de order op OfferDetailSequence = {0}  Header.Sequence = {1}  NumberOfUnits = {2} op tijd te bestellen bij de bron.", line.OfferDetailView.Sequence, line.OrderHeaderSeq, line.UnitsActual);
                                    throw new FlorecomException("Het is niet gelukt om de order op tijd te bestellen bij de bron.");
                                }

                                line.Response.Status[0].ConditionCode[0].Value = StatusConditionCodeContentType.Item73;
                                line.Response.Status[0].DescriptionText[0].Value = "Officially acknowledged";
                            }
                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Fout bij toewijzen van de OrderDetail met OfferDetailSequence = {0}  Header.Sequence = {1}  NumberOfUnits = {2} in LogiFlora.", line.OfferDetailView.Sequence, line.OrderHeaderSeq, line.UnitsActual);
                                //Dit komt in principe niet voor maar als het gebeurt is dit waarschijnlijk een verschil in voorrraad tussen LF en Web.
                                throw new FlorecomException(string.Format("Insufficient Units available to order {0}. Available {1}.", line.UnitsOrdered, line.UnitsActual));
                            }
                        }
                        else
                        {
                            Log.Info("Shop_LF role NIET aanwezig.");
                            line.Response.Status[0].ConditionCode[0].Value = StatusConditionCodeContentType.Item73;
                            line.Response.Status[0].DescriptionText[0].Value = "Officially acknowledged";
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Fout bij aanmaken OrderDetail met OfferDetailSequence = {0}  Header.Sequence = {1}  NumberOfUnits = {2}", line.OfferDetailView.Sequence, line.OrderHeaderSeq, line.UnitsActual);
                        Log.Error("Fout bij het aanmaken of bevestigen van de order ", ex);
                        throw new FlorecomException("Unexpected Error saving Order.");
                    }

                    line.Response.ReferencedDocument = referencedDocumentTypes.ToArray();
                }

                foreach (CreatedOrderInfo createdHeader in lstHeadersAdded)
                {
                    string LogiCABUrl = ConfigurationManager.AppSettings["LogiCABUrl"];
                    //var detail = details.FirstOrDefault<d => d.>

                    if (createdHeader.AutoEKT == 1 || createdHeader.AutoKWB == 1)
                    {
                        if (createdHeader.AutoKWB == 1 && createdHeader.AutoConfirm == 1)
                        {
                            Log.Debug("AutoKWB optie aan in handshake.");

                            try
                            {
                                using (var client = new HttpClient())
                                {
                                    client.BaseAddress = new Uri(LogiCABUrl);
                                    var content = new FormUrlEncodedContent(new[] 
                                {
                                    new KeyValuePair<string, string>("",  createdHeader.OrderHeaderSeq.ToString())
                                });
                                    var result = client.PostAsync("api/OrderAPI/SendKWB", content).Result;
                                    string resultContent = result.Content.ReadAsStringAsync().Result;
                                }
                            }

                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Fout bij versturen van de url i.v.m. autoKWB url {0}  Header.Sequence = {1}", LogiCABUrl, createdHeader.OrderHeaderSeq);
                            }
                        }

                        if (createdHeader.AutoEKT == 1 && createdHeader.AutoConfirm == 1)
                        {
                            Log.Debug("AutoEKT optie aan in handshake.");

                            try
                            {
                                using (var client = new HttpClient())
                                {
                                    client.BaseAddress = new Uri(LogiCABUrl);
                                    var content = new FormUrlEncodedContent(new[] 
                                {
                                    new KeyValuePair<string, string>("", createdHeader.OrderHeaderSeq.ToString())
                                });
                                    var result = client.PostAsync("api/OrderAPI/SendEKT", content).Result;
                                    string resultContent = result.Content.ReadAsStringAsync().Result;
                                }
                            }

                            catch (Exception ex)
                            {
                                Log.ErrorFormat("Fout bij versturen van de url i.v.m. autoEKT url {0}  Header.Sequence = {1}", LogiCABUrl, createdHeader.OrderHeaderSeq);
                            }
                        }
                    }


                    Log.DebugFormat("Calling proc_updOrderHeader");
                    Log.DebugFormat("ORHE_SEQ = {0}", createdHeader.OrderHeaderSeq);


                    CommonOrderProvider.UpdateOrderHeader(createdHeader.OrderHeaderSeq, createdHeader.AutoConfirm == 1, false);
                    SignalProvider.SignalOrderConfirmed(createdHeader.OrderHeaderSeq);

                    // Log het bevestigen als 'Order' moment naar de LogiCAB event log.
                    EventLogProvider.LogiServiceOrder(principal.Login.Sequence, createdHeader.OrderHeaderSeq);
                }
            }

            // Geef het resultaat terug aan de besteller.
            response.Body.OrderResponse.OrderResponseTradeLineItem = details.Select(d => d.Response).ToArray();
            return response;
        }

        public class PutOrderDetail
        {
            public string Prefix { get; set; }
            public OrderTradeLineItemType Request { get; set; }
            public OrderTradeLineItemType Response { get; set; }
            public HandshakeOption HandShakeOption { get; set; }
            public ICabPrincipal Grower { get; set; }
            public OfferDetailView OfferDetailView { get; set; }
            public DateTime DeliveryDate { get; set; }
            public Boolean DeliveryDateSpecified { get; set; }
            public decimal UnitsOrdered { get; set; }
            public string CustCode { get; set; }
            public string OrderRefID { get; set; }
            public string OwnerEANCode { get; set; }
            public string BuyerEANCode { get; set; }
            public decimal UnitsActual { get; set; }
            public string Remark { get; set; }
            public string DeliverRemark { get; set; }
            public int OrderHeaderSeq { get; set; }
            public int OrderDetailSeq { get; set; }
        }

        public class CreatedOrderInfo
        {
            public OfferDetailView OfferDetailView { get; set; }
            public int PersSeq { get; set; }
            public DateTime DeliveryDate { get; set; }
            public Boolean DeliveryDateSpecified { get; set; }
            public short AutoConfirm { get; set; }
            public short AutoEKT { get; set; }
            public short AutoKWB { get; set; }
            public decimal UnitsOrdered { get; set; }
            public string CustCode { get; set; }
            public string OrderRefID { get; set; }
            public string OwnerEANCode { get; set; }
            public string BuyerEANCode { get; set; }
            public decimal UnitsActual { get; set; }
            public string Remark { get; set; }
            public string DeliverRemark { get; set; }
            public int OrderHeaderSeq { get; set; }
            public int OrderDetailSeq { get; set; }
        }

        private PutOrderDetail CreateDetailFromRequest(OrderTradeLineItemType orderLine, ICabPrincipal buyer, string prefix, List<DateTime> listHoliDays)
        {
            DateTime prevLatestDelDateTime = DateTime.MinValue;

            // Bouw een detail met een Stub Response.
            var detail = new PutOrderDetail
            {
                Prefix = prefix,
                Request = orderLine,
                Response = new OrderTradeLineItemType
                {
                    ID = new IDType { schemeURI = buyer.Organisation.EAN, schemeDataURI = buyer.Organisation.EAN, schemeName = "VN" },
                    DocumentType = new DocumentCodeType1 { Value = DocumentNameCodeContentType1.Item231 },
                    LineDateTime = DateTime.Now,
                    TradingTerms = new TradingTermsType
                    {
                        Condition = new[]
                        {
                            new ConditionType 
                            { 
                                TypeCode = new [] 
                                { 
                                    new TradeConditionCodeType 
                                    { 
                                        Value = TradeConditionCodeContentType.Item301 
                                    }
                                },
                                ValueMeasure = new [] { new MeasureType1 { Value = 001 } }
                            }
                        }
                    }
                }
            };

            // Check de BuyerParty (Buyer).
            // Buyerparty is niet verplicht..
            //if (orderLine.BuyerParty == null)
            //	throw new FlorecomException("No BuyerParty specified.", detail.Prefix + "BuyerParty");

            //if (orderLine.BuyerParty.PrimaryID == null)
            //	throw new FlorecomException("No BuyerParty PrimaryID specified.", detail.Prefix + "BuyerParty/PrimaryID");

            //if (String.IsNullOrEmpty(orderLine.BuyerParty.PrimaryID.Value))
            //throw new FlorecomException("No BuyerParty PrimaryID Value specified.", detail.Prefix + "BuyerParty/PrimaryID/Value");

            // Een User mag alleen voor zijn eigen Organisatie kopen.
            //if (!buyer.Organisation.EAN.Equals(orderLine.BuyerParty.PrimaryID.Value))
            //throw new FlorecomException("Invalid BuyerParty specified.", detail.Prefix + "BuyerParty");

            if (orderLine.BuyerParty != null)
                if (orderLine.BuyerParty.PrimaryID != null)
                    detail.Response.BuyerParty = detail.Request.BuyerParty;

            if (orderLine.ID == null)
                //detail.Response.Status = new StatusType { ConditionCode = 
                throw new FlorecomException("No orderline PrimaryID specified.", detail.Prefix + "orderline/PrimaryID");

            if (orderLine.ID.Value == null)
                throw new FlorecomException("No orderline PrimaryID specified.", detail.Prefix + "orderline/PrimaryID");

            detail.OrderRefID = orderLine.ID.Value;

            if (!string.IsNullOrEmpty(orderLine.ID.schemeURI))
                detail.BuyerEANCode = orderLine.ID.schemeURI;

            //if (String.IsNullOrEmpty(orderLine.SupplierParty.PrimaryID.Value))
            //throw new FlorecomException("No SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");
            // Maak een principal aan voor de Supplier, en check dat het een grower is.

            //if (detail.Grower == null)
            //throw new FlorecomException("Invalid SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");

            //if (!detail.Grower.IsGrower)
            //throw new FlorecomException("Invalid SupplierParty PrimaryID Value specified.", detail.Prefix + "SupplierParty/PrimaryID/Value");

            // Check de ReferencedDocument (Offer Detail Sequence).
            if (orderLine.ReferencedDocument == null || orderLine.ReferencedDocument.Length == 0)
                throw new FlorecomException("No ReferencedDocument specified.", detail.Prefix + "ReferencedDocument");

            if (orderLine.ReferencedDocument[0].IssuerAssignedID == null)
                throw new FlorecomException("No ReferencedDocument IssuerAssignedID specified.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID");

            if (String.IsNullOrEmpty(orderLine.ReferencedDocument[0].IssuerAssignedID.Value))
                throw new FlorecomException("No ReferencedDocument IssuerAssignedID Value specified.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

            decimal? offerDetailSequence = null;

            // Ga op zoek naar de Offer Detail.
            foreach (ReferencedDocumentType refDoc in orderLine.ReferencedDocument)
            {
                if (refDoc.IssuerAssignedID.schemeName == "AAG")
                {
                    offerDetailSequence = refDoc.IssuerAssignedID.Value.ToDecimal();
                    detail.OwnerEANCode = refDoc.IssuerAssignedID.schemeURI;
                }
            }

            if (!string.IsNullOrEmpty(detail.OwnerEANCode))
            {
                detail.Response.ID.schemeURI = detail.OwnerEANCode;
                detail.Response.ID.schemeDataURI = detail.OwnerEANCode;
            }

            if (!offerDetailSequence.HasValue)
                throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

            if (offerDetailSequence < 0)
                throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

            Log.DebugFormat("OfferteSequence in referenceDocument IssuerAssingedID {0} found", offerDetailSequence.Value);
            detail.OfferDetailView = OfferDetails.FirstOrDefault(od => od.Sequence == offerDetailSequence);

            if (detail.OfferDetailView == null)
            {
                Log.WarnFormat("offerDetailSequence {0} not found.", offerDetailSequence);
                throw new FlorecomException("ReferencedDocument IssuerAssignedID Value does not appear to be a valid sequence.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");
            }

            if (detail.OfferDetailView.ValidFrom > System.DateTime.Now || detail.OfferDetailView.ValidTo < System.DateTime.Now)
            {
                Log.WarnFormat(" Offerte is nog (meer) geldig, geldig vanaf {0}, tot {1}, datum order {2}, offerteID {3}.", detail.OfferDetailView.ValidFrom, detail.OfferDetailView.ValidTo, System.DateTime.Now, offerDetailSequence);
                throw new FlorecomException(String.Format("Offerte is niet (meer) geldig, geldig vanaf {0}, tot {1}, datum order{2}.", detail.OfferDetailView.ValidFrom, detail.OfferDetailView.ValidTo, System.DateTime.Now));
            }

            if (detail.OfferDetailView.LatestOrderDateTime.HasValue)
            {
                if (detail.OfferDetailView.LatestOrderDateTime.Value < System.DateTime.Now)
                {
                    Log.WarnFormat("Laatste besteldatum overschreden, geldig vanaf {0}, kopen tot {1}, datum order {2}, offerteID {3}.", detail.OfferDetailView.ValidFrom, detail.OfferDetailView.LatestOrderDateTime, System.DateTime.Now, offerDetailSequence);
                    throw new FlorecomException(String.Format("Laatste besteldatum overschreden, geldig vanaf {0}, kopen tot {1}, datum order {2}, offerteID {3}.", detail.OfferDetailView.ValidFrom, detail.OfferDetailView.LatestOrderDateTime, System.DateTime.Now, offerDetailSequence));
                }
            }

            if (orderLine.SupplierParty != null)
            {
                //throw new FlorecomException("No SupplierParty specified.", detail.Prefix + "SupplierParty");
                if (orderLine.SupplierParty.PrimaryID != null)
                {
                    detail.Response.SupplierParty = detail.Request.SupplierParty;
                }
            }

            detail.Grower = new OrgaPrincipal(detail.OfferDetailView.Grower.Organisation);
            //throw new FlorecomException("No SupplierParty PrimaryID specified.", detail.Prefix + "SupplierParty/PrimaryID");
            //Als we zelf de deliveryDates hebben verzonnen de tijd goed zetten..
            //if (!deliveryDateSpec && detail.OfferDetailView.LatestDeliveryDateTime.HasValue)
            //deliveryDate = deliveryDate.Date + detail.OfferDetailView.LatestDeliveryDateTime.Value.TimeOfDay;
            detail.Response.ReferencedDocument = orderLine.ReferencedDocument;
            //detail.Response.ID = new IDType
            //{ 
            //      schemeName = "VN",
            //    schemeDataURI = buyer.Organisation.EAN,
            //     schemeURI = buyer.Organisation.EAN,
            //     Value = orderLine.ID.Value
            //};

            // Check de Ordered Quantity.
            if (orderLine.Quantity == null)
                throw new FlorecomException("No Quantity specified.", detail.Prefix + "Quantity");

            if (orderLine.Quantity.unitCodeSpecified && orderLine.Quantity.unitCode != FEC_MeasurementUnitCommonCodeContentType.Item3)
                throw new FlorecomException("Invalid Quantity unitCode specified.", detail.Prefix + "Quantity/unitCode");

            if (orderLine.Quantity.Value < 1)
                throw new FlorecomException("Invalid Quantity Value specified.", detail.Prefix + "Quantity/Value");

            // Bepaal hoeveel we er daadwerkelijk gaan bestellen.
            detail.UnitsActual = detail.UnitsOrdered = orderLine.Quantity.Value;

            // Bestelmomenten er bij halen..
            // Bestelmomenten kunnen op offerte header zijn vastgelegd of anders op handshake optie.
            // Als er niets is vastgelegd voor deze relatie wordt later de template gebruikt. 
            // Hier staan de logiCAB standaard afspraken in.
            var handshake = Handshakes.FirstOrDefault(h => h.Orga1.Sequence == detail.OfferDetailView.Grower.Organisation.Sequence && h.Orga2.Sequence == detail.OfferDetailView.Buyer.Organisation.Sequence);

            if (handshake != null)
            {
                try
                {
                    detail.HandShakeOption = HandShakeOptions.FirstOrDefault(ho => ho.Handshake.Sequence == handshake.Sequence);
                }
                catch (Exception ex)
                {
                    //Wel loggen maar we willen wel dat de order gewoon wordt bevestigd.
                    Log.ErrorFormat("Error bij opzoeken van de instellingen autoKWB en of autoEKT.", ex);
                }
            }

            if (detail.UnitsActual > detail.OfferDetailView.UnitsAvailable)
            {
                Log.ErrorFormat("Not enough available, offerSeq in Message {0}, units Ordered {1}, offerteSeqfound {2}, available in offerview {3}.", offerDetailSequence, orderLine.Quantity.Value, detail.OfferDetailView.Sequence, detail.OfferDetailView.UnitsAvailable);
                throw new FlorecomException("Not enough available.", detail.Prefix + "Quantity/Value");
            }
            //detail.UnitsActual = detail.OfferDetailView.UnitsAvailable;
            //				Log.WarnFormat("{0} Insufficient Units available to order {1}. Ordering {2} instead.", detail.OfferDetailView.Sequence, detail.UnitsOrdered, detail.UnitsActual);

            // Bepaal de delivery date time voor deze regel.
            System.DateTime deliveryDate = System.DateTime.Now;
            detail.DeliverRemark = "";
            bool delFound = false;
            bool delSpecified = false;

            try
            {
                if (orderLine.Delivery != null && detail.HandShakeOption != null)
                {
                    if (orderLine.Delivery.LatestDeliveryDateTimeSpecified)
                    {
                        delSpecified = true;

                        if (prevLatestDelDateTime != orderLine.Delivery.LatestDeliveryDateTime)
                        {
                            if (delMoments != null)
                                delMoments.Clear();

                            prevLatestDelDateTime = orderLine.Delivery.LatestDeliveryDateTime;
                            detail.DeliveryDate = orderLine.Delivery.LatestDeliveryDateTime;
                            detail.DeliveryDateSpecified = true;
                            var orderDetailsData = new OrderDetailsData();
                            var dtable = orderDetailsData.GetPossibleDelMoments(detail.HandShakeOption.DeliveryMomentHeader, detail.OfferDetailView.HeaderSequence.Value, detail.HandShakeOption.Handshake.Sequence);
                            fillDelMoment(dtable);
                        }

                        if (delMoments != null || delMoments.Count > 0)
                        {
                            bool useDelMoment = false;
                            var delmomDays = delMoments.Where(x => x.DMDT_WEEKDAY_NUMBER == (int)orderLine.Delivery.LatestDeliveryDateTime.DayOfWeek);

                            foreach (var delMom in delmomDays)
                            {
                                useDelMoment = !(orderLine.Delivery.LatestDeliveryDateTime.Date == System.DateTime.Now.Date && delMom.MAX_ORDER_TIME < System.DateTime.Now.TimeOfDay);
                                useDelMoment = useDelMoment && listHoliDays.FirstOrDefault(h => h.Date == orderLine.Delivery.LatestDeliveryDateTime.Date) == System.DateTime.MinValue;
                                useDelMoment = useDelMoment && delMom.MAX_DELIVER_TIME != null && delMom.MAX_ORDER_TIME != null;

                                if (useDelMoment)
                                {

                                    if (delMom.DELIVERY_TYPE == orderLine.Delivery.DeliveryTerms.DeliveryTypeCode.Value.ToString() && TimeSpan.Compare(detail.DeliveryDate.TimeOfDay, delMom.MAX_DELIVER_TIME.Value) == 0)
                                    {
                                        if (listHoliDays.FirstOrDefault(h => h.Date == orderLine.Delivery.LatestDeliveryDateTime.Date) == System.DateTime.MinValue)
                                            Log.Debug("Deliverdate time en deliverytype is gespecifeerd en gevonden.");
                                        else
                                            Log.DebugFormat("Deliverdate time en deliverytype is gespecifeerd en gevonden, maar is een feestdag! Deliverytime {0}, deliveryType {1}.", orderLine.Delivery.LatestDeliveryDateTime, orderLine.Delivery.DeliveryTerms.DeliveryTypeCode);

                                        delFound = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                //Wel loggen maar we willen wel dat de order gewoon wordt bevestigd.
                Log.ErrorFormat("Error bij opzoeken van de deliverymoments en het vergelijken met de deliverymoments in de offerte.", ex);
                delFound = false;
            }

            if (delFound == false)
            {
                var validFrom = detail.OfferDetailView.ValidFrom;
                Boolean useDelMoment = false;

                //Mocht valid_from in het verleden liggen dan corrigeren en beginnen vanaf vandaag..
                if (validFrom.Date < DateTime.Now.Date)
                    validFrom = DateTime.Now;

                int dow = (int)validFrom.DayOfWeek;
                var totDays = detail.OfferDetailView.ValidTo.Date.Subtract(validFrom.Date).TotalDays;

                if ((delMoments == null || delMoments.Count == 0) && detail.HandShakeOption != null)
                {
                    var orderDetailsData = new OrderDetailsData();
                    var dtable = orderDetailsData.GetPossibleDelMoments(detail.HandShakeOption.DeliveryMomentHeader, detail.OfferDetailView.HeaderSequence.Value, detail.HandShakeOption.Handshake.Sequence);
                    fillDelMoment(dtable);
                }

                //Op zoek naar het eerst mogelijke levermoment na nu..
                if (delMoments != null && delMoments.Count > 0)
                {
                    for (int index = (int)dow; index <= (int)dow + totDays; index++)
                    {
                        var delmomDays = delMoments.Where(x => x.DMDT_WEEKDAY_NUMBER == (index % 7));

                        foreach (var delMom in delmomDays)
                        {
                            useDelMoment = !(index == (int)dow && validFrom.Date == System.DateTime.Now.Date && delMom.MAX_ORDER_TIME < System.DateTime.Now.TimeOfDay);
                            useDelMoment = useDelMoment && listHoliDays.FirstOrDefault(h => h.Date == validFrom.AddDays(index - (int)dow).Date) == System.DateTime.MinValue;
                            useDelMoment = useDelMoment && delMom.MAX_DELIVER_TIME != null && delMom.MAX_ORDER_TIME != null;
                            //Controle of de levertermijn voor vandaag nog mogelijk is.
                            //Tenminste als vandaag ook de start van de geldigheid is.
                            if (useDelMoment)
                            {
                                detail.DeliveryDate = validFrom.AddDays(index - (int)dow).Date + delMom.MAX_DELIVER_TIME.Value;
                                break;
                            }
                        }

                        if (useDelMoment)
                            break;
                    }
                }
                else
                    if (detail.HandShakeOption != null)
                        Log.ErrorFormat("Geen aflevermomenten gevonden voor kweker (growseq) {0} en handshakeseq {1}.", detail.Grower.Grower.Sequence, detail.HandShakeOption.Handshake.Sequence);
                    else
                        Log.ErrorFormat("Geen handshakeoptie gevonden voor kweker (growseq) {0} en buyerseq {1}.", detail.Grower.Grower.Sequence, detail.OfferDetailView.Buyer.Sequence);

                if (!delSpecified)
                    Log.Debug("DeliveryDateSpec is false, dus CAB gegenereerde datum wordt gebruikt.");

                if (delSpecified && !delFound)
                {
                    detail.DeliverRemark = string.Format("Deliverytime {0} en deliverytype {1}.", orderLine.Delivery.LatestDeliveryDateTime, orderLine.Delivery.DeliveryTerms.DeliveryTypeCode.Value);
                    Log.DebugFormat("DeliveryDateSpecified, maar op basis hiervan geen juiste gegevens gevonden!! Deliverytime {0}, deliveryType {1}.", orderLine.Delivery.LatestDeliveryDateTime, orderLine.Delivery.DeliveryTerms.DeliveryTypeCode);
                }
            }

            detail.Response.Quantity = new QuantityType
            {
                unitCode = FEC_MeasurementUnitCommonCodeContentType.Item3,
                unitCodeSpecified = true,
                Value = detail.UnitsActual
            };

            detail.Response.Price = new[] 
            {
                new PriceType
                {
                    TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.AE },
                    ChargeAmount = new AmountType 
                    {
                        currencyCode = ISO3AlphaCurrencyCodeContentType.EUR,
                        currencyCodeSpecified = true,
                        Value = detail.OfferDetailView.ItemPrice
                    },
                    
                    //ChargeAmount = new AmountType  EuroAmount(orderLine.Price[0].ChargeAmount.Value),
	    		    NetPriceIndicator = true,
                    NetPriceIndicatorSpecified = true,
			        BasisQuantity = new QuantityType { unitCode = FEC_MeasurementUnitCommonCodeContentType.Item1, Value = 1, unitCodeSpecified = true }
                }
            };

            // Haal een mogelijke remark op.
            if (orderLine.AdditionalInformationTradeNote != null && orderLine.AdditionalInformationTradeNote.Length > 0)
                if (orderLine.AdditionalInformationTradeNote[0].Content != null)
                    if (!String.IsNullOrEmpty(orderLine.AdditionalInformationTradeNote[0].Content.Value))
                        detail.Remark = orderLine.AdditionalInformationTradeNote[0].Content.Value;

            // Eventuele klant code
            if (orderLine.EndUserParty != null)
            {
                if (orderLine.EndUserParty.PrimaryID != null)
                {
                    if (!string.IsNullOrEmpty(orderLine.EndUserParty.PrimaryID.Value))
                    {
                        detail.CustCode = orderLine.EndUserParty.PrimaryID.Value;
                        Log.Debug("orderLine.EndUserParty.PrimaryID.Value = " + orderLine.EndUserParty.PrimaryID.Value);
                    }
                    else
                        Log.Debug("IsNullOrEmpty(orderLine.EndUserParty.PrimaryID.Value");
                }
                else
                    Log.Debug("orderLine.EnduserParty.PrimaryID is null");
            }
            else
                Log.Debug("orderLine.EnduserParty is null");

            // Valideer de opgehaalde buyer en grower met elkaar.
            if (detail.OfferDetailView.Buyer != buyer.Buyer)
                throw new FlorecomException("Invalid Offer Detail specified. Incorrect Buyer.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

            //if (detail.OfferDetailView.Grower != detail.Grower.Grower)
            //throw new FlorecomException("Invalid Offer Detail specified. Incorrect Grower.", detail.Prefix + "ReferencedDocument[0]/IssuerAssignedID/Value");

            return detail;
        }

        void orderLF_OrderComplete(object sender, EventArgs e, string retValue, long orderDet)
        {
            if (string.IsNullOrEmpty(retValue))
                orderComplete = 1;
            else
                orderComplete = 2;
        }

        public void StartTimer()
        {
            timer = new Timer(10000);
            timer.Elapsed += timer_Elapsed;
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeUp = true;
        }

        bool createOrderDetailBase(CreatedOrderInfo baseOrder)
        {

            var handshake = Handshakes.FirstOrDefault(h => h.Orga1.Sequence == baseOrder.OfferDetailView.Grower.Organisation.Sequence && h.Orga2.Sequence == baseOrder.OfferDetailView.Buyer.Organisation.Sequence);

            if (handshake != null)
            {
                try
                {
                    var handshakeOption = HandShakeOptions.FirstOrDefault(ho => ho.Handshake.Sequence == handshake.Sequence);

                    if (handshakeOption != null)
                    {
                        baseOrder.AutoConfirm = handshakeOption.AutoConfirm;
                        baseOrder.AutoEKT = handshakeOption.AutoEKT;
                        baseOrder.AutoKWB = handshakeOption.AutoKWB;
                    }
                }
                catch (Exception ex)
                {
                    //Wel loggen maar we willen wel dat de order gewoon wordt bevestigd.
                    Log.ErrorFormat("Error bij opzoeken van de instellingen autoKWB en of autoEKT.", ex);
                }
            }

            if (baseOrder.OfferDetailView.Sequence <= 0 || baseOrder.OfferDetailView.HeaderSequence.Value <= 0)
            {
                Log.DebugFormat("createOrderDetailBase called with offerDet {0} and offerHeader {1}. Both should be filled!", baseOrder.OfferDetailView.Sequence, baseOrder.OfferDetailView.HeaderSequence.Value);
                return false;
            }

            //Eerst een bastaande of nieuwe orderheader ophalen voor de  base order.
            try
            {
                Log.InfoFormat("OfferHeadsequence {0}", baseOrder.OfferDetailView.HeaderSequence);
                Log.InfoFormat("Deliverydate {0}", baseOrder.DeliveryDate);
                Log.InfoFormat("DeliverydateSpecified {0}", baseOrder.DeliveryDateSpecified);
                baseOrder.OrderHeaderSeq = CommonOrderProvider.CreateOrderHeaderJun2010(baseOrder.OfferDetailView.HeaderSequence.Value, baseOrder.DeliveryDate, baseOrder.PersSeq, ShopPropertyProvider.ShopId, "", baseOrder.DeliveryDateSpecified, baseOrder.AutoConfirm == 1);
            }

            catch (Exception ex)
            {
                Log.ErrorFormat("Error creating Order Header voor base order.", ex);
                throw new FlorecomException("Unexpected Error saving Order for base order.");
            }

            Log.InfoFormat("Created Order Header {0}.", baseOrder.OrderHeaderSeq);
            Log.DebugFormat("Calling proc_createOrderDetailService voor base order");
            Log.DebugFormat("OFDT_SEQ_BASE = {0}", baseOrder.OfferDetailView.Sequence);
            Log.DebugFormat("ORHE_SEQ = {0}", baseOrder.OrderHeaderSeq);
            Log.DebugFormat("NUM_OF_UNITS = {0}", baseOrder.UnitsActual);
            Log.DebugFormat("CustCode = {0}", baseOrder.CustCode);
            Log.DebugFormat("OrderID = {0}", baseOrder.OrderRefID);
            int ofdtSeq = -1;
            int ofhdSeq = -1;

            // Aanmaken OrderDetail regel en even loggen van de nieuwe Sequence.
            baseOrder.OrderDetailSeq = CommonOrderProvider.CreateOrderDetailService(
                baseOrder.OfferDetailView.Sequence,
                baseOrder.OrderHeaderSeq,
                baseOrder.UnitsActual,
                baseOrder.CustCode,
                baseOrder.OrderRefID,
                baseOrder.Remark,
                baseOrder.DeliverRemark,
                true, //Kvk order voorkomen dat de order nog een keer de aanwezige voorraad muteert.
                ref ofdtSeq,
                ref ofhdSeq);

            if (baseOrder.OrderDetailSeq <= 0)
            {
                Log.ErrorFormat("Fout bij aanmaken OrderDetail met (base) OfferDetailSequence = {0}  Header.Sequence = {1}  NumberOfUnits = {2}", baseOrder.OfferDetailView.Sequence, baseOrder.OrderHeaderSeq, baseOrder.UnitsActual);
                Log.Error("Fout bij het aanmaken of bevestigen van de order op de base offer ");
                throw new FlorecomException("Unexpected Error saving Order by supplier.");
            }

            Log.InfoFormat("OrderDetail aangemaakt op base offer met Sequence = {0}", baseOrder.OrderDetailSeq);
                  
            //Nog steeds een baseofferSeq dan is ook deze weer afkomstig van een andere offerte!!
            if (ofdtSeq > 0)
            {
                baseOrder.OfferDetailView.Sequence = ofdtSeq;
                baseOrder.OfferDetailView.HeaderSequence = ofhdSeq;
                baseOrder.OrderDetailSeq = -1;
                baseOrder.OrderHeaderSeq = -1;
                return true;
            }
            else
                return false;
        }

        private static void FillBaseLine(LoginPrincipal principal, PutOrderDetail line, CreatedOrderInfo baseLine)
        {
            baseLine.OfferDetailView = line.OfferDetailView;
            baseLine.PersSeq = principal.Person.Sequence;
            baseLine.DeliveryDate = line.DeliveryDate;
            baseLine.DeliveryDateSpecified = line.DeliveryDateSpecified;
            baseLine.UnitsOrdered = line.UnitsOrdered;
            baseLine.CustCode = line.CustCode;
            baseLine.OrderRefID = line.OrderRefID;
            baseLine.OwnerEANCode = line.OwnerEANCode;
            baseLine.BuyerEANCode = line.BuyerEANCode;
            baseLine.UnitsActual = line.UnitsActual;

            if (line.Remark != null)
                baseLine.Remark = line.Remark;
            else
                baseLine.Remark = "LogiService";

            if (line.DeliverRemark != null)
                baseLine.DeliverRemark = line.DeliverRemark;
            else
                baseLine.DeliverRemark = "";

            baseLine.OrderHeaderSeq = line.OrderHeaderSeq;
            baseLine.OrderDetailSeq = line.OrderDetailSeq;
            baseLine.AutoConfirm = line.HandShakeOption.AutoConfirm;
            baseLine.AutoEKT = line.HandShakeOption.AutoEKT;
            baseLine.AutoKWB = line.HandShakeOption.AutoKWB;
        }
        
        void fillDelMoment(DataTable dtable)
        {
            if (dtable != null)
            {
                if (dtable.Rows.Count > 0)
                {
                    var result = dtable.Rows.Cast<DataRow>();
                    delMoments = result.Select(x => new DelMoment
                    {
                        MAX_DELIVER_TIME = x.Field<TimeSpan?>("MAX_DELIVER_TIME"),
                        MAX_ORDER_TIME = x.Field<TimeSpan?>("MAX_ORDER_TIME"),
                        DELIVERY_TYPE = x.Field<String>("DELIVERY_TYPE"),
                        DMDT_WEEKDAY_NUMBER = x.Field<int>("DMDT_WEEKDAY_NUMBER"),
                        GLN_LOCATION = x.Field<String>("GLN_LOCATION")
                    }).ToList();
                }
            }
        }
	}
}