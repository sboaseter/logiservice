﻿using System.Web;
using LogiCAB.Common;
using System.Text;
using System;
using LogiCAB.Common.Enums;
using LogiCAB.Common.Services;

namespace LogiService.Wcf.Florecom.Jul2012
{
	/// <summary>
	/// De ServiceAuthenticator heeft als doel het controleren van de login
	/// gegevens uit de header van het request, en op basis hiervan een
	/// LoginPrincipal te bouwen.
	/// De ServiceAuthenticator zelf controleert op de aanwezigheid van de
	/// LogiService Role. Overige Authorisatie ligt bij de gebruiker.
	/// Als op basis van de header geen login gevonden kan worden, of de
	/// LogiService Role niet aanwezig is, wordt er een null teruggegeven.
    /// System.IdentityModel.Selectors  : UserName UserNamePasswordValidator Eigen implementatie van de security ivm inloggen met Base Authentication
	/// </summary>
    public class ServiceAuthenticator : IServiceAuthenticator
    {
        protected PrincipalService PrincipalService { get; set; }
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
        public ServiceAuthenticator(PrincipalService principalService)
        {
            PrincipalService = principalService;
        }

        //public override void Validate
        public LoginPrincipal Authenticate(IRequestHeader requestHeader, string requestHeaderPrefix)
        {
            string authStr = HttpContext.Current.ApplicationInstance.Request.Headers["Authorization"];
            string userName = "";
            string passWord = "";

            if (!string.IsNullOrEmpty(authStr))
            {
                Log.Debug("Ahtorization header found");
                
                if (authStr == null || authStr.Length == 0)
                {
                    return null;
                }

                authStr = authStr.Trim();

                if (authStr.IndexOf("Basic", 0) != 0)
                {
                    return null;
                }

                authStr = authStr.Trim();
                string encodedCredentials = authStr.Substring(6);
                byte[] decodedBytes = Convert.FromBase64String(encodedCredentials);
                string s = new ASCIIEncoding().GetString(decodedBytes);
                string[] userPass = s.Split(new char[] { ':' });
                userName = userPass[0];
                passWord = userPass[1];

                Log.DebugFormat("Username is {0} wachtwoord is {1}. ", userName, passWord);
                //if (!MyUserValidator.Validate(username, password))
                //{
                  //  DenyAccess();
                   // return null;
                //}
            }
            //else
            //{
                //app.Response.StatusCode = 401;
                //app.Response.End();
            //}


            // Valideer de binnenkomende header.
			if (requestHeader == null)
				throw new FlorecomException("RequestHeader is null.", requestHeaderPrefix);

            if (requestHeader.UserName == null)
            {
                if (string.IsNullOrEmpty(userName))
                    throw new FlorecomException("RequestHeader UserName is null.", requestHeaderPrefix + "/UserName");
            }
            else
            {
                if (string.IsNullOrEmpty(requestHeader.UserName.Value))
                {
                    if (string.IsNullOrEmpty(userName))
                        throw new FlorecomException("RequestHeader UserName Value is null or empty.", requestHeaderPrefix + "/UserName/Value");
                }
                else
                    userName = requestHeader.UserName.Value;
            }

            if (requestHeader.Password == null)
            {
                if (string.IsNullOrEmpty(passWord))
                    throw new FlorecomException("RequestHeader Password is null.", requestHeaderPrefix + "/Password");
            }
            else
            {
                if (string.IsNullOrEmpty(requestHeader.Password.Value))
                {
                    if (string.IsNullOrEmpty(passWord))
                        throw new FlorecomException("RequestHeader Password Value is null or empty.", requestHeaderPrefix + "/Password/Value");
                }
                else
                    passWord = requestHeader.Password.Value;
            }

			// Kijk of we de gegeven login kunnen vinden.
			var principal = PrincipalService.GetOnUsernamePassword(userName, passWord);

			if (principal == null)
			{
				Log.WarnFormat("Unable to find Login with username {0} and the given password.", userName);
				return null;
			}

			// Check op Validiteit.
			if (!principal.Identity.IsAuthenticated)
			{
				Log.WarnFormat("Principal Identity is not Authenticated for username {0}. (ORAD_STATUS)", userName);
				return null;
			}

			// Check op LogiService Role.
			if (!principal.IsInRole(Role.LogiService))
			{
				Log.WarnFormat("Principal does not have LogiService Role for username {0}.", userName);
				return null;
			}

			// Valide Login.
			Log.DebugFormat("Valid Principal created for username {0}.", userName);
			HttpContext.Current.User = principal;
			return principal;
		}

        private void DenyAccess()
        {

            HttpContext.Current.ApplicationInstance.Response.StatusCode = 401;
            HttpContext.Current.ApplicationInstance.Response.StatusDescription = "Access Denied";
            HttpContext.Current.ApplicationInstance.Response.Write("401 Access Denied");
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}