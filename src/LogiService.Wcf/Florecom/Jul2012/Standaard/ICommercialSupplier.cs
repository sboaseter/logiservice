﻿using System.CodeDom.Compiler;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2012
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[WebServiceBinding(Name = "CommercialSupplier", Namespace = "http://webservice.florecom.org/CommercialSupplier")]
	public interface ICommercialSupplier
	{

		/// <remarks/>
		[WebMethodAttribute]
		[SoapDocumentMethod("http://webservices.florecom.org/commercial/Supplier/PutSupply", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
		[return: XmlElement("PutSupplyResponse", Namespace = "urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")]
		PutSupplyResponseMessage PutSupply([XmlElementAttribute(Namespace = "urn:fec:florecom:xml:data:draft:SupplyStandardMessage:5")] PutSupplyMessage putSupplyRequest);

        ///// <remarks/>
        //[WebMethodAttribute]
        //[SoapDocumentMethodAttribute("http://webservices.florecom.org/commercial/Supplier/PutImage", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        //[return: XmlElementAttribute("PutImagesResponse", Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
        //PutImagesResponseMessage PutImage([XmlElementAttribute(Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")] PutImagesMessage putImagesRequest);
	}
}