﻿namespace LogiService.Wcf.Florecom.Jul2012
{
	/// <summary>
	/// PLN: Interface om de verschillende soorten Request Headers gelijk te
	/// kunnen behandelen.
	/// </summary>
	public interface IRequestHeader
	{
		TextType UserName { get; }
		TextType Password { get; }
	}
}