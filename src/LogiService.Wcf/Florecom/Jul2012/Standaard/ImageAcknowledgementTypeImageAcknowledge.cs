﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2012
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[Serializable]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class ImageAcknowledgementTypeImageAcknowledge
	{
		/// <remarks/>
		public ReferencedDocumentType ReferencedDocument { get; set; }

		/// <remarks/>
		public bool ImageResult { get; set; }
	}
}