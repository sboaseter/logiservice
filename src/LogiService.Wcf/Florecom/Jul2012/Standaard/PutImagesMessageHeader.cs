﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Xml.Serialization;

namespace LogiService.Wcf.Florecom.Jul2012
{
	/// <remarks/>
	[GeneratedCode("wsdl", "2.0.50727.3038")]
	[SerializableAttribute]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "urn:fec:florecom:xml:data:draft:ImagesStandardMessage:5")]
	public class PutImagesMessageHeader
	{
		/// <remarks/>
		public TextType UserName { get; set; }

		/// <remarks/>
		public TextType Password { get; set; }

		/// <remarks/>
		public IDType MessageID { get; set; }

		/// <remarks/>
		public DateTime MessageDateTime { get; set; }

		/// <remarks/>
		public decimal MessageSerial { get; set; }

		/// <remarks/>
		[XmlElementAttribute("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }
	}
}