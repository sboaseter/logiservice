﻿using System;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Integration.Web;
//using Common.Logging;
using log4net;
using LogiCAB.Common;
using LogiService.Wcf.OudeCode;
using log4net.Config;

namespace LogiService.Wcf
{
	public class Global : HttpApplication, IContainerProviderAccessor
	{
       // public BasicAuthHttpModule basicAuthMod;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );

		private static IContainerProvider _containerProvider;
        public static Uri currentUri;
		public IContainerProvider ContainerProvider
		{
			get { return _containerProvider; }
		}

		public static IContainer Container
		{
			get
			{
				return ((IContainerProviderAccessor)HttpContext.Current.ApplicationInstance)
					.ContainerProvider.RequestContainer;
			}
		}

		protected void Application_Start(object sender, EventArgs e)
		{
			InitializeLogging();
            //basicAuthMod = new BasicAuthHttpModule();
			Log.Info("Application Starting.");

			// Log wat identificerende informatie over de applicatie.
			Log.InfoFormat("MachineName: {0}", Environment.MachineName);
			Log.InfoFormat("AppPath    : {0}", HttpRuntime.AppDomainAppPath);
			Log.InfoFormat("Assembly   : {0}", Assembly.GetExecutingAssembly().FullName);

			// Initialize the Container.
			_containerProvider = ContainerConfig.CreateContainerProvider();
			var container = _containerProvider.ApplicationContainer;

			// Initialize the Paths.
			container.Resolve<PathTool>().InitializeApplicationPaths();
            //basicAuthMod.Init();
			Log.Info("Application Available.");
		}

		private static void InitializeLogging()
		{
			 //Application User
			//log4net.GlobalContext.Properties["ApplicationUser"] = new HttpContextUserProperty();

			 //Application Identifier = Hex(Hash(MachineName _ AppPath));
			//log4net.GlobalContext.Properties["ApplicationIdentifier"] = String.Concat(
				//Environment.MachineName, "_", HttpRuntime.AppDomainAppPath
			//).GetHashCode().ToString("X");
            XmlConfigurator.Configure();
		}

		protected void Session_Start(object sender, EventArgs e)
		{
            Log.InfoFormat("Request started from: {0}", HttpContext.Current.Request.UserHostAddress);
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
            Log.InfoFormat("Request started from: {0}", HttpContext.Current.Request.UserHostAddress);
//            HttpContext.Current.Request.
            LogiCAB.Common.ContainerConfig.currentUri = HttpContext.Current.Request.Url;
        }

        //protected void override 

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{
			// Log geen errors voor de ScriptResource.axd. Dat zijn waarschijnlijk IE8 bugs.
            Log.Error("Uncaught Exception: " + Request.Url, Server.GetLastError());
			if (Request.Url.LocalPath.ToLowerInvariant().EndsWith("scriptresource.axd"))
				return;

			
		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{
			Log.Info("Application Ended");
		}
	}
}