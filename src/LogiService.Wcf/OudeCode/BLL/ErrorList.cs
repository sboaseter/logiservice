﻿using System;
using System.Collections.Generic;
using Florecom.standards.xml.VMP_v0_1;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class ErrorList
	{
       
        public static ErrorListError[] AddError(string errormessage, Enums.ErrorLevel errorlevel)
		{
			var err = CreateError(errorlevel, errormessage);
			return new[]{ err };
		}
        
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ErrorList));
        #endregion
    
		private static ErrorListError CreateError(Enums.ErrorLevel errorlevel, string errormessage)
		{
			var err = new ErrorListError
			{
				ErrorLevel = new CodeType { Value = errorlevel.ToString() },
				ErrorCode = new CodeType { Value = errormessage },
				Severity = new CodeType{ Value = "error" }
			};

			if (errorlevel == Enums.ErrorLevel.System)
				Log.Error(errormessage);

			return err;
		}

		public static ErrorListError[] Authentication(string message)
		{
			return AddError(message, Enums.ErrorLevel.Authentication);
		}

		public static ErrorListError[] System(string message)
		{
			return AddError(message, Enums.ErrorLevel.System);
		}

		public static ErrorListError[] Content(string message, string[] validation)
		{
			var result = new List<ErrorListError>
			{
				CreateError(Enums.ErrorLevel.Content, message)
			};
			foreach (var value in validation)
				result.Add(CreateError(Enums.ErrorLevel.Content, value));
			return result.ToArray();
		}
	}
}