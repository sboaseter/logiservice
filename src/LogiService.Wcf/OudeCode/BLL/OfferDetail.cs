﻿using System;
using System.Collections.Generic;
using System.Web;
using Florecom.standards.xml.VMP_v0_1;
using System.Data;
using LogiCAB.Common;
using LogiCAB.Common.Domain;
using LogiFlora.Common;
using LogiService.Wcf.OudeCode.DAL;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class OfferDetail
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
        
        protected IImageProvider ImageProvider { get; set; }
		protected IShopPropertyProvider ShopPropertyProvider { get; set; }
		private OfferDetailData OfferDetailData { get; set; }
		private UserRow User { get; set; }

		public OfferDetail(UserRow user)
		{
			User = user;
			OfferDetailData = new OfferDetailData();
			ImageProvider = Global.Container.Resolve<IImageProvider>();
			ShopPropertyProvider = Global.Container.Resolve<IShopPropertyProvider>();
		}

		public bool FillMessageHeader(SupplyMessage sMsg)
		{
			if (User == null || User.BuyerSequence == 0)
			{
				sMsg.Body.ErrorList = ErrorList.AddError("Buyersequence not provided", Enums.ErrorLevel.Content);
				return false;
			}
			return true;
		}


		public bool FillMessage(SupplyMessage sMsg)
		{
			var supplyLines = new List<SupplyLine>();
			foreach (var offerDetail in OfferDetailData.GetOfferDetailsDS(User))
			{
                try
                {
                    int imageSequence = 0;
                    string imageUrl = "";
                    string imageType = "";
                    var manufacturerGLN = offerDetail.OrgaEANCode;

                    if (!string.IsNullOrEmpty(offerDetail.PictureRef))
                        imageUrl = offerDetail.PictureRef;

                    if (offerDetail.CABPicture.HasValue)
                    {
                        imageType = "C";
                        imageSequence = offerDetail.CABPicture.Value;
                    }

                    Log.Debug("Er zijn details kijken naar foto");

                    if (offerDetail.OfferPicture.HasValue)
                    {
                        imageType = "P";
                        imageSequence = offerDetail.OfferPicture.Value;
                    }
                    else
                    {
                        if (offerDetail.GrowAssortmentPicture.HasValue)
                        {
                            imageType = "G";
                            imageSequence = offerDetail.GrowAssortmentPicture.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(imageUrl))
                        imageUrl = HttpContext.Current.Request.ResolveFullUrl(ImageProvider.GetStaticUrlFor(imageType, imageSequence, null, null, null));

                    var priceOptions = new List<PriceType>
				    {
					    // Standaardprijs-regel
					    GetPriceType(offerDetail.OfdtItemPrice)
				    };
                    
                    if (offerDetail.HasStaffelBool)
                        foreach (DataRow drS in OfferDetailData.StaffelDS(offerDetail.OfdtSequence).Rows)
                        {
                            var ofdsItemPrice = (decimal)drS["OFDS_ITEM_PRICE"];
                            var ofdsFrom = (decimal)drS["OFDS_FROM"];
                            var ofdsTo = (decimal)drS["OFDS_TO"];
                            var ofdsParentType = (short)drS["OFDS_PARENT_TYPE"];
                            priceOptions.Add(GetPriceTypeStaffel(ofdsItemPrice, ofdsFrom, ofdsTo, ofdsParentType));
                        }

                    if (offerDetail.GrowerGLN != null)
                        if (offerDetail.GrowerGLN.Length > 1)
                            manufacturerGLN = offerDetail.GrowerGLN;
                    
                    supplyLines.Add(new SupplyLine
                    {
                        LineDetails = GetLineDetails(offerDetail.OfdtSequence, String.Concat(offerDetail.OfhdRemark, " ", offerDetail.OfdtRemark), offerDetail.OfdtFkCABCode, imageUrl, DateTime.Now),
                        SupplyPeriod = GetSupplyPeriod(offerDetail.OfhdValidFrom, offerDetail.OfhdValidTo),
                        SuppliedProductIdentity = GetSuppliedProductIdentity(offerDetail.CabcCABCode, offerDetail.CABDesc, offerDetail.VbncVbnCode, offerDetail.OfdtFkCABCode,manufacturerGLN),
                        SupplierParty = GetSupplierPartyType(offerDetail.OrgaEANCode),
                        AvailableQuantity = GetAvailableQuantity(offerDetail.OfdtNumOfItems, -1, -1),
                        PriceOptions = priceOptions.ToArray(),
                        PackingOptions = GetPackingOptions(offerDetail.CdcaCaskCode, offerDetail.BulkCdcaCaskCode, offerDetail.CacaNumOfUnits, offerDetail.CacaUnitsPerLayer, offerDetail.CacaLayersPerTrolley),
                        DeliveryOptions = GetDeliveryOptions(offerDetail.OfhdValidTo, offerDetail.LatestDeliveryDate)
                    });
                }
                catch (Exception ex)
                {
					Log.Error("Error in FillMessage", ex);
                    return false;
                }
            }

			sMsg.Body.Supply = new SupplyDetails
			{
				SupplyLine = supplyLines.ToArray()
			};
			return true;
		}

		private LineDetails GetLineDetails(int sequence, string remark, int cabSequence, string imageUrl, DateTime lineDateTime)
		{
			var charactaristics = new List<CharacteristicType>();
			foreach (DataRow drP in OfferDetailData.PropertiesDS(cabSequence).Rows)
			{
				var charactaristic = GetCharacteristic(
					drP["VBNPropCode"].ToString(),
					drP["VBNPropValue"].ToString(),
					"8", "VBN",
					"9", "VBN");
				if (charactaristic != null)
					charactaristics.Add(charactaristic);
			}
			charactaristics.Add(GetCharacteristic("refPict", imageUrl, "1", "LogiFlora", "1", "LogiFlora"));

			return new LineDetails
			{
				ID = new IDType
				{
					//GLN bedrijfscode van de marktplaats
					schemeDataURI = "8713783248188",
					schemeName = "AAG",
					//Partij identificatie
					Value = sequence.ToString()
				},

                Status = true,
				StatusSpecified = true,
				// Optioneel
				MarketplaceID = new MarketplaceIDType { Value = "LogiCAB" },
                // Optioneel (002 = Standard supply, price made by seller )
				MarketForm = new MarketFormType { Value = "002" },
                // <<<<< REMARK
				FreeText = new TextType { Value = remark.Trim() },
				Characteristics = charactaristics.ToArray(),
				LineDateTime = lineDateTime,
				LineDateTimeSpecified = true
			};
		}

		private static PeriodType GetSupplyPeriod(DateTime validFrom, DateTime validTo)
		{
			return new PeriodType
			{
				StartDateTime = validFrom,
				StartDateTimeSpecified = true,
				EndDateTime = validTo,
				EndDateTimeSpecified = true
			};
		}

		private ProductIdentityType GetSuppliedProductIdentity(
			string cabCode,
			string cabDesc,
			string vbnCode,
			int cabSequence,
            string manufacturerGLN)
		{
			var types = new List<CharacteristicType>();
			foreach (DataRow drP in OfferDetailData.PropertiesDS(cabSequence).Rows)
			{
				var chara = GetCharacteristic(
					(string)drP["VBNPropCode"],
					(string)drP["VBNPropValue"],
					"8", "VBN", "9", "VBN");
				if (chara != null)
					types.Add(chara);
			}

			return new ProductIdentityType
			{
				IndustryArticle = new ArticleType
				{
					ProductCode = new CodeType
					{
						listID = "1",
						listAgencyName = "VBN",
						Value = vbnCode
					},
					RegulationCharacteristic = types.ToArray(),
                    ManufacturerParty = new ManufacturerPartyType { PrimaryID = new IDType { schemeID = "1", schemeAgencyName = "FEC", Value = manufacturerGLN } }
				},
				ManufacturerAssignedID = new IDType { Value = cabCode },
				CustomerAssignedID = new IDType { Value = vbnCode },
				Description = new TextType { Value = cabDesc }
			};
		}

		private static CharacteristicType GetCharacteristic(
			string classCode,
			string valueCode,
			string classCodeListId,
			string classCodeListAgencyName,
			string valueCodeListId,
			string valueCodeListAgencyName)
		{
			if (valueCode.Length == 0)
				return null;

			var result = new CharacteristicType
			{
				ClassCode = new CodeType
				{
					listID = classCodeListId,
					listAgencyName = classCodeListAgencyName,
					Value = classCode
				},
				ValueCode = new[]
				{
					new CodeType
					{
						listID = valueCodeListId,
						listAgencyName = valueCodeListAgencyName,
						Value = valueCode
					}
				}
			};
			return result;
		}

		private static SupplierPartyType GetSupplierPartyType(string supplierEAN)
		{
			return new SupplierPartyType
			{
				PrimaryID = new IDType
				{
					schemeID = "1",
					schemeAgencyName = "FEC",
					Value = String.IsNullOrEmpty(supplierEAN) ? "8700000000000" : supplierEAN
				}
			};
		}

		private static QuantityType GetAvailableQuantity(decimal numOfUnits, decimal numOfLayers, decimal numOfContainers)
		{
			
            if (numOfContainers > 0)
			{
				return new QuantityType
				{
					unitCode = MeasurementUnitCommonCodeContentType.Item5,
					unitCodeSpecified = true,
					Value = numOfContainers
				};
			}
			
            if (numOfLayers > 0)
			{
				return new QuantityType
				{
					unitCode = MeasurementUnitCommonCodeContentType.Item4,
					unitCodeSpecified = true,
					Value = numOfLayers
				};
			}
			
            return new QuantityType
			{
				unitCode = MeasurementUnitCommonCodeContentType.Item3,
				unitCodeSpecified = true,
				Value = numOfUnits
			};
		}

		private static PriceType GetPriceType(decimal price)
		{
			return new PriceType
			{
				TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.PC },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
				OriginMarketplaceID = new MarketplaceIDType { Value = "LogiCAB" },
				BasisQuantity = QuantityType(1, MeasurementUnitCommonCodeContentType.Item1)
			};
		}

		private static PriceType GetPriceTypeStaffel(decimal price, decimal minQuantity, decimal maxQuantity, int itemType)
		{
			var m = GetM(itemType);
		
            return new PriceType
			{
				TypeCode = new PriceTypeCodeType { Value = PriceTypeCodeContentType.PC },
				ChargeAmount = EuroAmount(price),
				NetPriceIndicator = true,
				OriginMarketplaceID = new MarketplaceIDType { Value = "LogiCAB" },
				MinimumQuantity = QuantityType(minQuantity, m),
				MaximumQuantity = QuantityType(maxQuantity, m)
			};
		}

		private static QuantityType QuantityType(
			decimal value,
			MeasurementUnitCommonCodeContentType unit)
		{
			return new QuantityType
			{
				unitCode = unit,
				unitCodeSpecified = true,
				Value = value
			};
		}

		private static AmountType EuroAmount(decimal price)
		{
			return new AmountType
			{
				currencyCode = CurrencyCodeContentType.EUR,
				currencyCodeSpecified = true,
				Value = price
			};
		}

		private static MeasurementUnitCommonCodeContentType GetM(int itemType) {
			switch (itemType)
			{
				case 2:
					return MeasurementUnitCommonCodeContentType.Item4;
				case 3:
					return MeasurementUnitCommonCodeContentType.Item5;
			}
			return MeasurementUnitCommonCodeContentType.Item3;
		}

		private static PackingDetails[] GetPackingOptions(string caskCode, string bulkCaskCode, decimal perUnit, decimal perLayer, decimal perTrolley)
		{
			return new[]
			{
				new PackingDetails
				{
					Packing = new PackingType
					{
						TypeCode = new CodeType
						{
							listAgencyName = "VBN",
							listID = "901",
							Value = bulkCaskCode
						}
					},
					InnerPackingQuantity = QuantityType(perTrolley, MeasurementUnitCommonCodeContentType.Item4),
					InnerPacking = new[]
					{
						new PackingDetails
						{
							Packing = new PackingType
							{
								TypeCode = new CodeType
								{
									listAgencyName = "FEC",
									listID = "DE",
									Value = "18"
								},
								ObjectCountValue = 1,
								ObjectCountValueSpecified = true
							},
							InnerPackingQuantity = QuantityType(perLayer, MeasurementUnitCommonCodeContentType.Item3),
							InnerPacking = new[]
							{
								new PackingDetails
								{
									Packing = new PackingType
									{
										TypeCode = new CodeType
										{
											listAgencyName = "VBN",
											listID = "901",
											Value = caskCode
										},
										ObjectCountValue = perLayer,
										ObjectCountValueSpecified = true
									},
									InnerPackingQuantity = QuantityType(perUnit, MeasurementUnitCommonCodeContentType.Item1)
								}
							}
						}
					}
				}
			};
		}

        private static DeliveryOptionDetails[] GetDeliveryOptions(DateTime validTo, DateTime? latestDelivery)
		{
            Boolean deliveryDateTimeSpecified = false;
            DateTime latestDeliveryDateTime;

            if (!latestDelivery.HasValue)
            {
                deliveryDateTimeSpecified = false;
                latestDeliveryDateTime = DateTime.MinValue;
            }
            else
            {
                //We gebruiken alleen de tijd uit latestDelivery de datum komt van de validTo..
                TimeSpan diffTime = validTo - latestDelivery.Value;
                latestDeliveryDateTime = latestDelivery.Value.AddDays(diffTime.Days);

                if (diffTime.Hours > 0)
                    latestDeliveryDateTime = latestDeliveryDateTime.AddDays(1);
                
                deliveryDateTimeSpecified = true;
            }

            return new[]
			{
				new DeliveryOptionDetails
				{
					DeliveryTerms = new TradeDeliveryTermsType
					{
						RelevantTradeLocation = new TradeLocationType
						{
							ID = new IDType
							{
								schemeID = "4",
								schemeAgencyName = "FEC",
								Value = "5790001082307"
							}
						}
					},
					LatestOrderDateTime = validTo,
					LatestOrderDateTimeSpecified = true,
					LatestDeliveryDateTime = latestDeliveryDateTime,
					LatestDeliveryDateTimeSpecified = deliveryDateTimeSpecified,
				}
			};
		}

	}
}