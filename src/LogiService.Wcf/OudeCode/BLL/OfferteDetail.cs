using System;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class OfferteDetail
	{
		public decimal Sequence { get; set; }

		public OfferteHeader Header { get; set; }
		public string Remark { get; set; }
		public string CabCode { get; set; }
		public decimal NumberOfItems { get; set; }
		public decimal ItemPrice { get; set; }
		public string CdcaCaskCode { get; set; }
		public decimal CacaNumberOfUnits { get; set; }
		public decimal CacaUnitsPerLayer { get; set; }
		public decimal CacaLayersPerTrolley { get; set; }

		public string LastError { get; set; }

		public bool Save()
		{
			OfferHeader.CreateOfferDetailService(
				Header.Sequence,
				CabCode,
				NumberOfItems,
				ItemPrice,
				Remark,
				CdcaCaskCode,
				CacaNumberOfUnits,
				CacaUnitsPerLayer,
				CacaLayersPerTrolley);
			return true;
		}
	}
}