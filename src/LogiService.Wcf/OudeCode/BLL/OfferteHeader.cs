using System;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class OfferteHeader
	{
		public string LastError { get; set; }

		public decimal Sequence { get; set; }

		public UserRow User { get; set; }
		public OrganisationGrowerRow Grower { get; set; }
		public OrganisationBuyerRow Buyer { get; set; }
		public MainGroupRow MainGroup { get; set; }
		public string Remark { get; set; }
		public DateTime ValidFrom { get; set; }
		public DateTime ValidTo { get; set; }

		protected string OfhdNo { get; set; }

		private bool Validate()
		{
			bool valid;

			try
			{
				valid =
					Grower != null && Grower.GrowerSequence > 0 &&
					Buyer != null && Buyer.BuyerSequence > 0 &&
					User != null && User.PersonSequence > 0 &&
					MainGroup != null && MainGroup.MainGroupSequence > 0;
			}
			catch
			{
				valid = false;
			}

			return valid;

		}

		public bool Save()
		{
			if (!Validate())
			{
				LastError = "Invalide Offerte.";
				return false;
			}

			// Haal een Counter op voor de OFHD_NO.
			OfhdNo = OfferHeader.GetSystemCounter("OFHD", null, null, Grower.GrowerSequence, "", true);

			// En create de Offer Header.
			Sequence = OfferHeader.CreateOfferHeader(
				Grower.GrowerSequence,
				Buyer.BuyerSequence,
				MainGroup.MainGroupSequence,
				OfhdNo,
				ValidFrom,
				ValidTo,
				Remark);

			return true;
		}
	}
}