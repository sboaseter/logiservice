﻿using System;
using Florecom.standards.xml.VMP_v0_1;
using LogiCAB.Common.Domain;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.OudeCode.BLL
{
	/// <summary>
	/// Summary description for OrderDetails
	/// </summary>
	public class OrderDetail
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
        private ICommonOrderProvider CommonOrderProvider { get; set; }
		public OrderHeader Header { get; set; }
		public int OfferDetailSequence { get; set; }
		public decimal NumberOfUnits { get; set; }
		public decimal UnitsAvailable { get; set; }
        public string CustCode { get; set; }
        public string OrderRefID { get; set; }
		public int Sequence { get; private set; }
		public string LastError { get; set; }
		public OrderResponseLine ResponseLine { get; set; }

		public OrderDetail()
		{
			// Relatie via de Container.
			CommonOrderProvider = Global.Container.Resolve<ICommonOrderProvider>();
		}

		/// <summary>
		/// Hier lezen we de gegevens van de Order Regel in en vertalen dat
		/// naar de benodigde waarden voor LogiCAB. Hier gebeurt ook al een
		/// groot deel van de validatie.
		/// </summary>
		public bool Read(OrderLine line)
		{
			// Initialize hier meteen maar de Response line.
			ResponseLine = new OrderResponseLine();

			if (line.SupplierParty == null)
			{
				Log.Error(LastError = "SupplierParty is null.");
				return false;
			}
			
            if (line.SupplierParty.PrimaryID == null)
			{
				Log.Error(LastError = "SupplierParty PrimaryID is null.");
				return false;
			}
			
            if (String.IsNullOrEmpty(line.SupplierParty.PrimaryID.Value))
			{
				Log.Error(LastError = "SupplierParty PrimaryID Value is null or empty.");
				return false;
			}

			if (line.SupplierParty.PrimaryID.Value != Header.Grower.OrganisationEAN)
			{
				Log.Error(LastError = String.Format("Supplier EAN Mismatch! [{0}]", line.SupplierParty.PrimaryID.Value));
				return false;
			}

			if (line.LineDetails == null)
			{
				Log.Error(LastError = "LineDetails is null.");
				return false;
			}
			
            if (line.LineDetails.DocumentReference == null || line.LineDetails.DocumentReference.Length == 0)
			{
				Log.Error(LastError = "LineDetails has no DocumentReferences.");
				return false;
			}
			
            if (line.LineDetails.DocumentReference[0] == null)
			{
				Log.Error(LastError = "LineDetails First DocumentReference is null.");
				return false;
			}
			
            if (line.LineDetails.DocumentReference[0].IssuerAssignedID == null || String.IsNullOrEmpty(line.LineDetails.DocumentReference[0].IssuerAssignedID.Value))
			{
				Log.Error(LastError = "LineDetails First DocumentReference has no IssuerAssignedID.");
				return false;
			}

			int offerDetailSequence;
			
            if (!Int32.TryParse(line.LineDetails.DocumentReference[0].IssuerAssignedID.Value, out offerDetailSequence))
			{
				Log.Error(LastError = "LineDetails First DocumentReference IssuerAssignedID cannot be parsed.");
				return false;
			}
			
            OfferDetailSequence = offerDetailSequence;

			if (line.OrderedQuantity == null)
			{
				Log.Error(LastError = "OrderedQuantity is null.");
				return false;
			}
			
            if (line.OrderedQuantity.Value == 0)
			{
				Log.Error(LastError = "OrderedQuantity Value is zero.");
				return false;
			}

			// Maak de ResponseLine alvast aan.
			ResponseLine.LineDetails = new LineDetails();
			//Eerst vullen met een spatie dan gaan we die later vervangen voor de order..
			ResponseLine.LineDetails.ID = new IDType { Value = "" };
			ResponseLine.LineDetails.ID.schemeName = "VN"; //Response order = vn
			ResponseLine.OrderedQuantity = new QuantityType { Value = 0 };
            // Valideer of er nog stock beschikbaar is.
			UnitsAvailable = CommonOrderProvider.GetRealtimeStock(OfferDetailSequence);
			
            if (UnitsAvailable < 1)
			{
				Log.Error(LastError = "There are no Units available for ordering.");
				return false;
			}

			// Dit is al een Decimal, nog een keer parsen heeft weinig zin.
			NumberOfUnits = line.OrderedQuantity.Value;
			
            if (UnitsAvailable < NumberOfUnits)
			{
				Log.WarnFormat("Units Available is less than Units Ordered. Settings NumberOfUnits from {0} to {1}", NumberOfUnits, UnitsAvailable);
				NumberOfUnits = UnitsAvailable;
			}

            if (line.EndUserParty != null)
            {
                if (line.EndUserParty[0].PrimaryID != null)
                {
                    if (!string.IsNullOrEmpty(line.EndUserParty[0].PrimaryID.Value))
                    {
                        CustCode = line.EndUserParty[0].PrimaryID.Value;
                        Log.Debug("line.EndUserParty[0].PrimaryID.Value = " + line.EndUserParty[0].PrimaryID.Value);
                    }
                    else
                        Log.Debug("IsNullOrEmpty(line.EndUserParty[0].PrimaryID.Value");
                }
                else
                    Log.Debug("line.EnduserParty[0].PrimaryID is null");
            }
            else
                Log.Debug("line.EnduserParty is null");

			// Rapporteer terug hoeveel we er echt gaan bestellen.
			ResponseLine.OrderedQuantity.Value = NumberOfUnits;
			ResponseLine.LineDetails.StatusSpecified = true;
			ResponseLine.LineDetails.Status = NumberOfUnits > 0;

			// Sofar, so good.
			return true;
		}

		/// <summary>
		/// Hier controleren we of de geparsede Offer Detail regel wel dezelfde
		/// Grower en Buyer heeft als de Order Header waar we aan hangen. Deze
		/// validatie gebeurt ook bij het aanmaken van de Order Regels, dus die
		/// willen we vóór zijn zodat we geen lege Order Headers creëeren.
		/// </summary>
		public bool ValidateOfferDetail()
		{
			try
			{
				var validation = OrderDetailsData.ValidateOfferDetailBuyerGrower(
					OfferDetailSequence,
					Header.Grower.GrowerSequence,
					Header.Buyer.BuyerSequence
				);
				if (!validation)
				{
					Log.ErrorFormat("Validatie van Offer Detail gefaald met OfferDetailSequence = [{0}]  GrowerSequence = [{1}]  BuyerSequence = [{2}]", OfferDetailSequence, Header.Grower.GrowerSequence, Header.Buyer.BuyerSequence);
				}
				else
				{
					Log.DebugFormat("Validatie van Offer Detail geslaagd met OfferDetailSequence = [{0}]  GrowerSequence = [{1}]  BuyerSequence = [{2}]", OfferDetailSequence, Header.Grower.GrowerSequence, Header.Buyer.BuyerSequence);
				}
				return validation;
			}
			catch
			{
				Log.ErrorFormat("Validatie van Offer Detail gefaald met OfferDetailSequence = [{0}]  GrowerSequence = [{1}]  BuyerSequence = [{2}]", OfferDetailSequence, Header.Grower.GrowerSequence, Header.Buyer.BuyerSequence);
				throw;
			}
		}

		/// <summary>
		/// Hier slaan we de Order Detail regel op.
		/// </summary>
		public void Save()
		{
			try
			{
				Log.DebugFormat("Calling proc_createOrderDetailService");
				Log.DebugFormat("OFDT_SEQ = [{0}]", OfferDetailSequence);
 				Log.DebugFormat("ORHE_SEQ = [{0}]", Header.Sequence);
                Log.DebugFormat("CustCode = [{0}]", CustCode);
				Log.DebugFormat("NUM_OF_UNITS = [{0}]", NumberOfUnits);
                Log.DebugFormat("OrderRef = [{0}]", OrderRefID);

                int ofhdSeq = -1;
                int ofdtSeq = -1;

				// Aanmaken OrderDetail regel en even loggen van de nieuwe Sequence.
				Sequence = CommonOrderProvider.CreateOrderDetailService(
					OfferDetailSequence,
					Header.Sequence,
					NumberOfUnits,
                    CustCode,
                    OrderRefID,
					String.Empty,
                    string.Empty,
                    false,
                    ref ofdtSeq,
                    ref ofhdSeq);
				Log.InfoFormat("OrderDetail aangemaakt met Sequence = [{0}]", Sequence);
			}
			catch
			{
				Log.ErrorFormat("Fout bij aanmaken OrderDetail met OfferDetailSequence = [{0}]  Header.Sequence = [{1}]  NumberOfUnits = [{2}]", OfferDetailSequence, Header.Sequence, NumberOfUnits);
				throw;
			}
		}

	}
}