﻿using System;
using Florecom.standards.xml.VMP_v0_1;
using LogiCAB.Common.Domain;
using LogiService.Wcf.OudeCode.DAL;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class OrderHeader
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
        private ICommonOrderProvider CommonOrderProvider { get; set; }
		private ISignalProvider SignalProvider { get; set; }
		public OrganisationBuyerRow Buyer { get; set; }
		public OrganisationGrowerRow Grower { get; set; }
		public UserRow User { get; set; }
		public string OrderDescription { get; set; }
		public DateTime OrderDeliveryDate { get; set; }
        public Boolean OrderDeliverDateSpec { get; set; }
		public string Remark { get; set; }
		public int Sequence { get; private set; }
		public int OfferDetailSequence { get; set; }
		public string LastError { get; set; }

		public OrderHeader()
		{
			OrderDeliveryDate = DateTime.Now; // nextworkday for maingroup...
			// Relatie via de Container.
			CommonOrderProvider = Global.Container.Resolve<ICommonOrderProvider>();
			SignalProvider = Global.Container.Resolve<ISignalProvider>();
		}

		/// <summary>
		/// Hier slaan we de Order Header daadwerkelijk op in de database.
		/// </summary>
		public int Save()
		{
			try
			{
				Sequence = CommonOrderProvider.CreateOrderHeader(
                    OrderDescription, 
                    OrderDeliveryDate, 
                    OfferDetailSequence, 
                    Grower.GrowerSequence,
					Buyer.BuyerSequence,
					User.PersonSequence,
					Remark,
					User.ShopSequence,
                    OrderDeliverDateSpec
				);
				if (Sequence < 1)
					Log.Error(LastError = String.Format("Procedure returned invalid Sequence: {0}", Sequence));
				else
					Log.InfoFormat(
						"Created Order Header {0}, GrowerSequence {1}, BuyerSequence {2} , User {3}",
						Sequence,
						Grower.GrowerSequence,
						Buyer.BuyerSequence,
						User.PersonName);
				return Sequence;
			} catch (Exception ex)
			{
				Log.Error(ex);
				throw;
			}
		}

		/// <summary>
		/// Hier lezen we de gegevens van de Order Header in en vertalen dat
		/// naar de benodigde waarden voor LogiCAB. Hier gebeurt ook al een
		/// groot deel van de validatie.
		/// </summary>
		public bool Read(OrderRequest request, UserRow user)
		{
			try
			{
				var valid = true;
				int offerDetailSequence;

				if (request == null)
				{
					Log.Error(LastError = "OrderRequest is null.");
					return false;
				}
				
                if (request.Header == null)
				{
					Log.Error(LastError = "OrderRequest has no Header.");
					return false;
				}
				
                if (request.Header.MessageID == null)
				{
					Log.Error(LastError = "OrderRequest Header has no MessageID.");
					return false;
				}
				
                if (String.IsNullOrEmpty(request.Header.MessageID.Value))
				{
					Log.Error(LastError = "OrderRequest Header has no MessageID Value.");
					return false;
				}
				
                if (request.Body == null)
				{
					Log.Error(LastError = "OrderRequest has no Body.");
					return false;
				}
				
                if (request.Body.OrderRequestDetails == null)
				{
					Log.Error(LastError = "OrderRequest Body has no OrderRequestDetails.");
					return false;
				}
				
                if (request.Body.OrderRequestDetails.BuyerParty == null)
				{
					Log.Error(LastError = "OrderRequestDetails has no BuyerParty.");
					return false;
				}
				
                if (request.Body.OrderRequestDetails.BuyerParty.PrimaryID == null)
				{
					Log.Error(LastError = "BuyerParty has no PrimaryID.");
					return false;
				}
			
                if (String.IsNullOrEmpty(request.Body.OrderRequestDetails.BuyerParty.PrimaryID.Value))
				{
					Log.Error(LastError = "BuyerParty has no PrimaryID Value.");
					return false;
				}
			
                if (request.Body.OrderRequestDetails.OrderLine == null)
				{
					Log.Error(LastError = "OrderRequestDetails has no OrderLines.");
					return false;
				}
				if (request.Body.OrderRequestDetails.OrderLine.Length == 0)
				{
					Log.Error(LastError = "OrderRequestDetails has no OrderLines.");
					return false;
				}

				var firstOrderLine = request.Body.OrderRequestDetails.OrderLine[0];
				
                if (firstOrderLine == null)
				{
					Log.Error(LastError = "First Order Line is null?");
					return false;
				}
				
                if (firstOrderLine.LineDetails == null)
				{
					Log.Error(LastError = "First Order Line has no Line Details.");
					return false;
				}
				
                if (firstOrderLine.LineDetails.DocumentReference == null || firstOrderLine.LineDetails.DocumentReference.Length == 0)
				{
					Log.Error(LastError = "First Order Line has no Document Reference (OfferDetailSequence).");
					return false;
				}
				
                if (firstOrderLine.LineDetails.DocumentReference[0].IssuerAssignedID == null)
				{
					Log.Error(LastError = "First Order Line Document Reference has no IssuerAssignedID (OfferDetailSequence).");
					return false;
				}
				
                if (String.IsNullOrEmpty(firstOrderLine.LineDetails.DocumentReference[0].IssuerAssignedID.Value))
				{
					Log.Error(LastError = "First Order Line Document Reference has no IssuerAssignedID (OfferDetailSequence).");
					return false;
				}
				
                if (!Int32.TryParse(firstOrderLine.LineDetails.DocumentReference[0].IssuerAssignedID.Value, out offerDetailSequence))
				{
					Log.Error(LastError = "First Order Line Document Reference IssuerAssignedID is not a number (OfferDetailSequence).");
					return false;
				}
				
                if (firstOrderLine.SupplierParty == null)
				{
					Log.Error(LastError = "First Order Line has no SupplierParty (Grower).");
					return false;
				}

				var buyer = request.Body.OrderRequestDetails.BuyerParty;
                DateTime deliveryDate;

                if ((firstOrderLine.OrderedDelivery != null) && (firstOrderLine.OrderedDelivery.LatestDateTimeSpecified))
                    deliveryDate = firstOrderLine.OrderedDelivery.LatestDateTime;
                else
                {
                    deliveryDate = Helper.NextWorkday(DateTime.Now);
                    OrderDeliverDateSpec = false;
                }

				Log.DebugFormat("supplier.PrimaryID.Value = [{0}]", firstOrderLine.SupplierParty.PrimaryID.Value);
				Log.DebugFormat("buyer.PrimaryID.Value = [{0}]", buyer.PrimaryID.Value);
				Log.DebugFormat("offerDetailSequence = [{0}]", offerDetailSequence);
				OfferDetailSequence = offerDetailSequence;
				Grower = new OrganisationData().GetGrowerFromEAN(firstOrderLine.SupplierParty.PrimaryID.Value);
				Buyer = new OrganisationData().GetBuyerFromEAN(buyer.PrimaryID.Value);
				User = user;
				OrderDeliveryDate = deliveryDate;
				OrderDescription = "LogiService " + request.Header.MessageID.Value;
				Remark = String.Empty;

				if (Grower == null)
				{
					Log.Error("Grower is null!");
					valid = false;
				} else
				{
					Log.DebugFormat("Grower.GrowerSequence = [{0}]", Grower.GrowerSequence);
					Log.DebugFormat("Grower.BulkCaskFk = [{0}]", Grower.BulkCaskFk);
					if (Grower.GrowerSequence < 1)
					{
						Log.Error(LastError = String.Format("Grower.GrowerSequence [{0}] lijkt niet goed te zijn.", Grower.GrowerSequence));
						valid = false;
					}
				}

				if (Buyer == null)
				{
					Log.Error("Buyer is null!");
					valid = false;
				} else
				{
					Log.DebugFormat("Buyer.BuyerSequence = [{0}]", Buyer.BuyerSequence);
					if (Buyer.BuyerSequence < 1)
					{
						Log.Error(LastError = String.Format("Buyer.BuyerSequence [{0}] lijkt niet goed te zijn.", Buyer.BuyerSequence));
						valid = false;
					}
				}

				if (User == null)
				{
					Log.Error("User is null!");
					valid = false;
				} else
				{
					Log.DebugFormat("User.LoginSequence = [{0}]", User.LoginSequence);
					Log.DebugFormat("User.PersonSequence = [{0}]", User.PersonSequence);
					if (User.PersonSequence < 1)
					{
						Log.Error(LastError = String.Format("User.PersonSequence [{0}] lijkt niet goed te zijn.", User.PersonSequence));
						valid = false;
					}
				}

				if (OfferDetailSequence < 1)
				{
					Log.Error(LastError = String.Format("OfferDetailSequence [{0}] lijkt niet goed te zijn.", OfferDetailSequence));
					valid = false;
				}

				Log.DebugFormat("OrderDeliveryDate = [{0}]", OrderDeliveryDate);
				Log.DebugFormat("OrderDescription = [{0}]", OrderDescription);
				Log.DebugFormat("Remark = [{0}]", Remark);

				return valid;
			}
			catch (Exception ex)
			{
				Log.Error("Exception building orderHeader.", ex);
				throw;
			}
		}

		/// <summary>
		/// Hier bevestigen we de Order Header. Dit zet de Status op 'ORDERED'
		/// en herberekend de netto waarde.
		/// </summary>
		public void Bevestig()
		{
			Log.DebugFormat("Calling proc_updOrderHeader");
			Log.DebugFormat("ORHE_SEQ = [{0}]", Sequence);
			CommonOrderProvider.UpdateOrderHeader(Sequence);
			SignalProvider.SignalOrderConfirmed(Sequence);

			// Log het bevestigen als 'Order' moment naar de LogiCAB event log.
			new EventLogProvider(new SvcConnectionProvider()).LogiServiceOrder(User.LoginSequence, Sequence);
		}

	}
}