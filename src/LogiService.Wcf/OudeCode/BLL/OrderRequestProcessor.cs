﻿using System;
using System.Collections.Generic;
using Florecom.standards.xml.VMP_v0_1;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.OudeCode.BLL
{
	/// <summary>
	/// OrderRequest
	/// </summary>
	public class OrderRequestProcessor
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
        private readonly OrderRequest _orderRequest;
		private readonly UserRow _user;
		private readonly OrderResponseMessageBody _response;
		public string LastError { get; private set; }

		public OrderRequestProcessor(OrderRequest orderRequest, OrderResponseMessageBody responseMessage, UserRow user)
		{
			_orderRequest = orderRequest;
			_user = user;
			_response = responseMessage;
		}

		public bool ProcessOrderRequest()
		{
			if (_orderRequest == null)
			{
				LastError = "OrderRequest not set";
				return false;
			}

			if (_orderRequest.Body.OrderRequestDetails.OrderLine == null || _orderRequest.Body.OrderRequestDetails.OrderLine.Length == 0)
			{
				LastError = "No orderlines";
				return false;
			}

			var orderHeader = new OrderHeader();

			if (!orderHeader.Read(_orderRequest, _user))
			{
				LastError = "Order Invalid";
				return false;
			}

			var orderDetails = new List<OrderDetail>();
			var responseLines = new List<OrderResponseLine>();
			var orderLinesValid = true;
			try
			{
				// Check dat er Order Lines zijn is bovenaan al gebeurt...
				var orderIndex = 0;
				foreach (var line in _orderRequest.Body.OrderRequestDetails.OrderLine)
				{
					Log.DebugFormat("Processing OrderLine: {0}", orderIndex);

					// Geef de huidige Order Header mee aan de Detail regel.
					var detail = new OrderDetail { Header = orderHeader };

					// Lees de details van de OrderLine in en doe de initële validatie.
					if (!detail.Read(line))
						orderLinesValid = false;

					// Valideer de Buyer en Grower tegen de opgegeven Offer Detail regel.
					if (!detail.ValidateOfferDetail())
						orderLinesValid = false;

					// Bewaar dit detail en zijn response en hou bij of we nog valide zijn.
					orderDetails.Add(detail);
					responseLines.Add(detail.ResponseLine);
					orderIndex++;
				}
			} catch (Exception ex)
			{
				Log.Error("Exception building Order Details.", ex);
				throw;
			}

			// Sla de ResponseLines op in het Response object.
			_response.Order = new OrderResponseDetails {OrderResponseLine = responseLines.ToArray()};

			// Niet doorgaan als we foute regels hebben.
			if (!orderLinesValid)
			{
				Log.Error(LastError = "Invalid Order Lines found.");
				return false;
			}

			// Alle checks zijn doorlopen. Sla alles op in de database.
			try
			{
				// Creëer de Order.
				if (orderHeader.Save() <= 0)
				{
					LastError = "Could not create orderheader";
					return false;
				}

				// Maak de Order Regels aan.
				int index = 0;

				foreach (var orderDetail in orderDetails)
				{
					orderDetail.Save();
					if (orderDetail.Sequence > 0)
					{
						//Nu gaan we deze id vervangen door de order sequence die we nu eenmaal nu pas weten..
						responseLines[index].LineDetails.ID.Value = orderDetail.Sequence.ToString();
					}
					index++;
				}

				// En bevestig de Order.
				orderHeader.Bevestig();
				return true;
			}
			catch (Exception ex)
			{
				Log.Error("Exception bij het opslaan van de Order.", ex);
				throw;
			}
		}

	}
}