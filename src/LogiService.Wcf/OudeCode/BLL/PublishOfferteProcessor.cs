using System.Collections.Generic;
using Florecom.standards.xml.VMP_v0_1;
using Logiflora.Web.Offerte;
using LogiService.Wcf.OudeCode.DAL;

namespace LogiService.Wcf.OudeCode.BLL
{
	public class PublishOfferteProcessor
	{
		private readonly PublishOfferteRequest _request;
		private readonly UserRow _user;

		private OfferteDetails _offerteDetails;
		private SupplyLine _eersteRegel;

		public string LastError { get; private set; }

		public PublishOfferteProcessor(
			PublishOfferteRequest request,
			UserRow user)
		{
			_request = request;
			_user = user;
		}

		public string[] Validate()
		{
			var result = new List<string>();
			if (_request == null)
			{
				result.Add("PublishOfferteRequest not set.");
			} else
			{
				if (_request.Body == null)
				{
					result.Add("PublishOfferteRequest does not have a Body.");
				}
				else
				{
					if (_request.Body.OfferteDetails == null)
					{
						result.Add("OfferteDetails not specified.");
					}
					else
					{
						_offerteDetails = _request.Body.OfferteDetails;
						ValidateOfferteDetails(result);
					}
				}
			}
			if (_user == null)
			{
				result.Add("User not set.");
			} else
			{
				if (_user.PersonSequence == 0)
				{
					result.Add("User not set.");
				}
			}
			return result.ToArray();
		}

		private void ValidateOfferteDetails(List<string> result)
		{
			if (_offerteDetails.BuyerParty == null)
			{
				result.Add("BuyerParty not specified.");
			}
			else
			{
				_offerteDetails.BuyerParty.Validate(result, "BuyerParty");
			}
			if (_offerteDetails.SellerParty == null)
			{
				result.Add("SellerParty not specified.");
			}
			else
			{
				_offerteDetails.SellerParty.Validate(result, "SellerParty");
			}
			if (_offerteDetails.Remark == null)
			{
				result.Add("Remark not specified.");
			}
			else
			{
				_offerteDetails.Remark.Validate(result, "Remark");
			}
			if (_offerteDetails.SupplyLine == null)
			{
				result.Add("SupplyLine not specified.");
			}
			else
			{
				if (_offerteDetails.SupplyLine.Length == 0)
				{
					result.Add("SupplyLine not specified.");
				}
				else
				{
					_eersteRegel = _offerteDetails.SupplyLine[0];
					_eersteRegel.Validate(result, "SupplyLine[0]", null);
					for (var i = 1; i < _offerteDetails.SupplyLine.Length; i++)
					{
						_offerteDetails.SupplyLine[i].Validate(result, "SupplyLine[" + i + "]", _eersteRegel);
					}
				}
			}
		}

		public bool Process()
		{
			// Bouw een nieuwe header.
			var header = new OfferteHeader
			{
				Grower = _offerteDetails.SellerParty.Grower,
				Buyer = _offerteDetails.BuyerParty.Buyer,
				MainGroup = _eersteRegel.SuppliedProductIdentity.MainGroupRow,
				User = _user,
				Remark = _offerteDetails.Remark.Value,
				ValidFrom = _eersteRegel.SupplyPeriod.StartDateTime,
				ValidTo = _eersteRegel.SupplyPeriod.EndDateTime
			};

			// Sla de header op.
			if (!header.Save())
			{
				LastError = header.LastError;
				return false;
			}

			foreach (var line in _offerteDetails.SupplyLine)
			{
				// Cre�er een nieuwe detail regel.
				var detail = new OfferteDetail
				{
					Header = header,
					Remark =               line.LineDetails.FreeText.Value,
					CabCode =              line.SuppliedProductIdentity.ManufacturerAssignedID.Value,
					NumberOfItems =        line.AvailableQuantity.Value,
					ItemPrice =            line.PriceOptions[0].ChargeAmount.Value,
					CdcaCaskCode =         line.PackingOptions[0].InnerPacking[0].InnerPacking[0].Packing.TypeCode.Value,
					CacaNumberOfUnits =    line.PackingOptions[0].InnerPacking[0].InnerPacking[0].InnerPackingQuantity.Value,
					CacaUnitsPerLayer =    line.PackingOptions[0].InnerPacking[0].InnerPackingQuantity.Value,
					CacaLayersPerTrolley = line.PackingOptions[0].InnerPackingQuantity.Value
				};
				//  BulkCdcaCaskCode = line.PackingOptions[0].Packing.TypeCode.Value

				// Sla de detail regel op.
				if (!detail.Save())
				{
					LastError = detail.LastError;
					return false;
				}
			}

			return true;
		}
	}
}