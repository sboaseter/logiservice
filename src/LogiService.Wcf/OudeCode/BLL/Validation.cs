using System;
using System.Collections.Generic;
using Logiflora.Web.BLL;
using LogiService.Wcf.OudeCode.DAL;
using LogiService.Wcf.OudeCode.Util;

// Deze niet verplaatsen. Het zijn partial extensions van de Florecom classes!
namespace Florecom.standards.xml.VMP_v0_1
{
	public partial class SupplyLine
	{
		public void Validate(List<string> result, string prefix, SupplyLine validateAgainst)
		{
			if (SuppliedProductIdentity == null)
			{
				result.AddFormat("{0} SuppliedProductIdentity not specified.", prefix);
			} else
			{
				SuppliedProductIdentity.Validate(result, prefix + " SuppliedProductIdentity", validateAgainst == null ? null : validateAgainst.SuppliedProductIdentity);
			}
			if (SupplyPeriod == null)
			{
				result.AddFormat("{0} SupplyPeriod not specified.", prefix);
			}
			else
			{
				SupplyPeriod.Validate(result, prefix + " SupplyPeriod", validateAgainst == null ? null : validateAgainst.SupplyPeriod);
			}
			if (LineDetails == null)
			{
				result.AddFormat("{0} LineDetails not specified.", prefix);
			} else
			{
				LineDetails.Validate(result, prefix + " LineDetails", validateAgainst == null ? null : validateAgainst.LineDetails);
			}
			if (PackingOptions == null)
			{
				result.AddFormat("{0} PackingOptions not specified.", prefix);
			} else
			{
				if (PackingOptions.Length == 0)
				{
					result.AddFormat("{0} PackingOptions not specified.", prefix);
				}
				else if (PackingOptions.Length > 1)
				{
					result.AddFormat("{0} PackingOptions more than one row specified.", prefix);
				} else
				{
					PackingOptions[0].Validate(result, prefix + " PackingOptions[0]");
				}
			}
		}
	}

	public partial class LineDetails
	{
		public void Validate(List<string> result, string prefix, LineDetails validateAgainst)
		{
			if (FreeText == null)
			{
				FreeText = new TextType { Value = String.Empty };
			}
			else
			{
				FreeText.ValidateAllowEmpty(result, "FreeText");
			}
		}
	}

	/// OrderDetails.SupplyLine[].PackingOptions[]
	public partial class PackingDetails
	{
		public void Validate(List<string> result, string prefix)
		{
			if (Packing == null)
			{
				result.AddFormat("{0} Packing not specified.", prefix);
			} else
			{
				if (Packing.TypeCode == null)
				{
					result.AddFormat("{0} Packing TypeCode not specified.", prefix);
				} else
				{
					if (Packing.TypeCode.Validate(result, prefix + " Packing TypeCode", "VBN", "901"))
					{
						// BULK_CDCA_CASK_CODE
					}
				}
			}
			if (InnerPackingQuantity == null)
			{
				result.AddFormat("{0} InnerPackingQuantity not specified.", prefix);
			} else
			{
				InnerPackingQuantity.Validate(
					result, prefix + " InnerPackingQuantity",
					MeasurementUnitCommonCodeContentType.Item4
				);
			}
			if (InnerPacking == null)
			{
				result.AddFormat("{0} InnerPacking not specified.", prefix);
			} else
			{
				if (InnerPacking.Length == 0)
				{
					result.AddFormat("{0} InnerPacking not specified.", prefix);
				}
				else if (InnerPacking.Length > 1)
				{
					result.AddFormat("{0} InnerPacking more than one row specified.", prefix);
				}
			}
		}
	}

	public partial class QuantityType
	{
		public bool Validate(List<string> result, string prefix,
			MeasurementUnitCommonCodeContentType punitCode)
		{
			bool isValid = true;
			if (!unitCodeSpecified)
			{
				result.AddFormat("{0} unitCode not specified.", prefix);
				isValid = false;
			}
			else
			{
				if (!punitCode.Equals(unitCode))
				{
					result.AddFormat("{0} unitCode does not match '{1}'.", prefix, punitCode);
					isValid = false;
				}
			}
			if (Value == 0)
			{
				result.AddFormat("{0} Value is zero.", prefix);
				isValid = false;
			}
			return isValid;
		}
	}

	public partial class CodeType
	{
		public bool Validate(List<string> result, string prefix, string plistAgencyName, string plistID)
		{
			bool isValid = true;
			if (String.IsNullOrEmpty(listAgencyName))
			{
				result.AddFormat("{0} listAgencyName not specified.", prefix);
				isValid = false;
			} else
			{
				if (!plistAgencyName.Equals(listAgencyName))
				{
					result.AddFormat("{0} listAgencyName does not match '{1}'.", prefix, plistAgencyName);
					isValid = false;
				}
			}
			if (String.IsNullOrEmpty(listID))
			{
				result.AddFormat("{0} listID not specified.", prefix);
				isValid = false;
			}
			else
			{
				if (!plistID.Equals(listID))
				{
					result.AddFormat("{0} listID does not match '{1}'.", prefix, plistID);
					isValid = false;
				}
			}
			if (String.IsNullOrEmpty(Value))
			{
				result.AddFormat("{0} Value not specified.", prefix);
				isValid = false;
			}
			return isValid;
		}
	}

	public partial class ProductIdentityType
	{
		internal MainGroupRow MainGroupRow { get; set; }

		public void Validate(List<string> result, string prefix, ProductIdentityType validateAgainst)
		{
			if (ManufacturerAssignedID == null)
			{
				result.AddFormat("{0} ManufacturerAssignedID not specified.", prefix);
			}
			else
			{
				if (String.IsNullOrEmpty(ManufacturerAssignedID.Value))
				{
					result.AddFormat("{0} ManufacturerAssignedID Value not specified.", prefix);
				}
				// TODO: Toevoegen check op 'CAB' schema?
				if (String.IsNullOrEmpty(ManufacturerAssignedID.schemeID))
				{
					result.AddFormat("{0} ManufacturerAssignedID schemeID not specified.", prefix);
				} else
				{
					if (!"CAB".Equals(ManufacturerAssignedID.schemeID))
					{
						result.AddFormat("{0} ManufacturerAssignedID schemeID does not equal 'CAB'.", prefix);
					}
				}
				// TODO: Regex validatie voor CAB waarde?
				// TODO: MainGroup hier ophalen, entiteit verrijken en vergelijken met validateAgainst.
				if (MainGroupRow == null)
				{
					MainGroupRow = new MainGroupData().GetMainGroupFromCAB(
						ManufacturerAssignedID.Value
					);
				}
				if (MainGroupRow == null || MainGroupRow.MainGroupSequence == 0)
				{
					result.AddFormat("{0} Main Group not found in CAB database.", prefix);
				} else
				{
					if (validateAgainst != null && validateAgainst.MainGroupRow != null)
					{
						if (!validateAgainst.MainGroupRow.MainGroupSequence.Equals(MainGroupRow.MainGroupSequence))
						{
							result.AddFormat("{0} Main Group is inconsistent.", prefix);
						}
					}
				}
			}
		}
	}

	public partial class PeriodType
	{
		public void Validate(List<string> result, string prefix, PeriodType validateAgainst)
		{
			if (!StartDateTimeSpecified)
			{
				result.AddFormat("{0} StartDateTime not specified.", prefix);
			}
			else
			{
				if (StartDateTime < Config.MinimumDate)
				{
					result.AddFormat("{0} StartDateTime less than {1}.", prefix, Config.MinimumDateString);
				}
				if (StartDateTime > Config.MaximumDate)
				{
					result.AddFormat("{0} StartDateTime more than {1}.", prefix, Config.MaximumDateString);
				}
				if (validateAgainst != null)
				{
					if (!validateAgainst.StartDateTime.Equals(StartDateTime))
					{
						result.AddFormat("{0} StartDateTime inconsistent.", prefix);
					}
				}
			}
			if (!EndDateTimeSpecified)
			{
				result.AddFormat("{0} EndDateTime not specified.", prefix);
			}
			else
			{
				if (EndDateTime < Config.MinimumDate)
				{
					result.AddFormat("{0} EndDateTime less than {1}.", prefix, Config.MinimumDateString);
				}
				if (EndDateTime > Config.MaximumDate)
				{
					result.AddFormat("{0} EndDateTime more than {1}.", prefix, Config.MaximumDateString);
				}
				if (validateAgainst != null)
				{
					if (!validateAgainst.EndDateTime.Equals(EndDateTime))
					{
						result.AddFormat("{0} EndDateTime inconsistent.", prefix);
					}
				}
			}
		}
	}

	public partial class BuyerPartyType
	{
		public void Validate(List<string> result, string prefix)
		{
			if (PrimaryID == null)
			{
				result.AddFormat("{0} PrimaryID not specified.", prefix);
			}
			else
			{
				PrimaryID.ValidateEAN(result, prefix + " PrimaryID");
				Buyer = new OrganisationData().GetBuyerFromEAN(PrimaryID.Value);
				if (Buyer == null || Buyer.BuyerSequence == 0)
				{
					result.AddFormat("{0} Buyer Organisation not found.", prefix);
				}
			}
		}

		internal OrganisationBuyerRow Buyer { get; set; }
	}

	public partial class SellerPartyType
	{
		public void Validate(List<string> result, string prefix)
		{
			if (PrimaryID == null)
			{
				result.AddFormat("{0} PrimaryID not specified.", prefix);
			}
			else
			{
				PrimaryID.ValidateEAN(result, prefix + " PrimaryID");
				Grower = new OrganisationData().GetGrowerFromEAN(PrimaryID.Value);
				if (Grower == null || Grower.GrowerSequence == 0)
				{
					result.AddFormat("{0} Grower Organisation not found.", prefix);
				}
			}
		}

		internal OrganisationGrowerRow Grower { get; set; }
	}

	public partial class IDType
	{
		public void ValidateEAN(List<string> result, string prefix)
		{
			if (String.IsNullOrEmpty(Value))
			{
				result.AddFormat("{0} Value not specified.", prefix);
			}
			// TODO: Regex validatie voor EAN waarde?
			if (String.IsNullOrEmpty(schemeID))
			{
				result.AddFormat("{0} schemeID not specified.", prefix);
			}
			else
			{
				if (!"EAN".Equals(schemeID))
				{
					result.AddFormat("{0} schemeID does not equal 'EAN'.", prefix);
				}
			}
		}
	}

	public partial class TextType
	{
		public void Validate(List<string> result, string prefix)
		{
			if (String.IsNullOrEmpty(Value))
			{
				result.AddFormat("{0} Value not specified.", prefix);
			}
		}
		public void ValidateAllowEmpty(List<string> result, string prefix)
		{
			if (Value == null)
			{
				Value = String.Empty;
			}
		}
	}

}
