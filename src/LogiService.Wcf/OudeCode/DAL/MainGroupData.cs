﻿using System;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class MainGroupData
	{
		public MainGroupRow GetMainGroupFromCAB(string cabCode)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateQuery(@"
Select
	F.CDMG_DESC
,	F.CDMG_SEQUENCE
,	F.CDMG_NUM_OF_DAYS_OFFER
From
	V_VIEW_ARTICLES V
Inner Join
	F_CAB_CD_MAIN_GROUP_TYPE F
		On	( V.CDMG_SEQUENCE = F.CDMG_SEQUENCE )
Where
	( CABC_CAB_CODE = @CABCode )
;"))
			{
				cmd.AddInputParameter("@CABCode", Helper.FormatCABCode(cabCode));
				using (var dr = cmd.ExecuteReader())
				if (dr.Read())
				{
					return new MainGroupRow
					{
						Description = dr.Get<string>("CDMG_DESC"),
						MainGroupSequence = dr.Get<decimal>("CDMG_SEQUENCE"),
						NumberOfDaysOffer = dr.Get<int>("CDMG_NUM_OF_DAYS_OFFER")
					};
					// uitbreiden met group ?
				}
			}
			return null;
		}
	}
}