using System;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class MainGroupRow
	{
		public decimal MainGroupSequence { get; set; }
		public string Description { get; set; }
		public int NumberOfDaysOffer { get; set; }
	}
}