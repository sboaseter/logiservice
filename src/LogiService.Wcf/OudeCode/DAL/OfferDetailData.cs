using System;
using System.Collections.Generic;
using System.Data;
using LogiFlora.Common.Db;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class OfferDetailData
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion

		public List<OfferDetailRow> GetOfferDetailsDS(UserRow user)
		{
			return GetOfferDetailsDS(user.BuyerSequence, user.ShopSequence, null);
		}

		public List<OfferDetailRow> GetOfferDetailsDS(int buyerSequence, int shopSequence, DateTime? changesSince)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_getOfferDetailsService"))
			{
                cmd.CommandTimeout = 120;
				cmd.AddInputParameter("@BUYR_SEQ", buyerSequence);
				cmd.AddInputParameter("@SHOP_SEQ", shopSequence);
				cmd.AddInputParameter("@OfferType", 'X');
				cmd.AddInputParameter("@ChangesSince", changesSince);
				return OfferDetailRow.proc_getOfferDetailsService.MapList(cmd);
			}
		}

		public DataTable StaffelDS(int offerSequence)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_getOfferDetailsExtra"))
			{
				cmd.AddInputParameter("@OFDT_SEQ", offerSequence);
				return cmd.ExecuteTable();
			}
		}

		public DataTable PropertiesDS(int cabSequence)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_getDetailProperties"))
			{
				cmd.AddInputParameter("@CABC_SEQ", cabSequence);
				return cmd.ExecuteTable();
			}
		}
        public DataTable PropertiesDSVBN(int cabSequence, int grasSequence) {
            using (var conn = SvcConnectionProvider.GetConnection())
            using (var cmd = conn.CreateProcedure("proc_getDetailPropertiesVBN")) {
                cmd.AddInputParameter("@CABC_SEQ", cabSequence);
                cmd.AddInputParameter("@GRAS_SEQ", grasSequence);
                return cmd.ExecuteTable();
            }
        }

        public DataTable GetPossibleDelMoments(int? dmhdSeq, int ofhdSeq, int? hndoSeq)
        {
           return GetPossibleDelMoments(dmhdSeq, ofhdSeq, hndoSeq, null, null, null);
        }

        public DataTable GetPossibleDelMoments(int? dmhdSeq, int ofhdSeq, int? hndoSeq, int? dayOfTheWeek, TimeSpan? orderTime, TimeSpan? deliverTime)
        {
            try
            {
                using (var conn = SvcConnectionProvider.GetConnection())
                using (var cmd = conn.CreateProcedure("proc_GetPossibleDeliveryMoments"))
                {
                    cmd.AddInputParameter("@OFHD_SEQ", ofhdSeq);
                    cmd.AddInputParameter("@HNDO_SEQ", hndoSeq);
                    cmd.AddInputParameter("@DMHD_SEQ", dmhdSeq);
                    cmd.AddInputParameter("@Day_Number", dayOfTheWeek);
                    cmd.AddInputParameter("@Order_Time", orderTime);
                    cmd.AddInputParameter("@Deliver_Time", deliverTime);
                    cmd.ExecuteNonQuery();
                    return cmd.ExecuteTable();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
	}
}