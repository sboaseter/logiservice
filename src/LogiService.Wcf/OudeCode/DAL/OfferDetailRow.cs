﻿using System;
using LogiFlora.Common.Db;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class OfferDetailRow
	{
		public decimal OfdtItemPrice { get; set; }
		public int? CABPicture { get; set; }
		public int? OfferPicture { get; set; }
        public int? GrowAssortmentPicture { get; set; }
        public int GrowAssortmentSequence { get; set; }
        public string OfferType { get; set; }
		public int OfdtSequence { get; set; }
        public int GrowOrgSeq { get; set; }
		public int OfdtFkCABCode { get; set; }
        public int OfhdSequence { get; set; }
		public DateTime OfhdValidFrom { get; set; }
		public DateTime OfhdValidTo { get; set; }
		public string OrgaEANCode { get; set; }
		public string CabcCABCode { get; set; }
		public string OfhdRemark { get; set; }
		public string OfdtRemark { get; set; }
        public string CABDesc { get; set; }
        public string VBNGroupCode { get; set; }
        public string VBNGroupDesc { get; set; }
        public int VBNGroupType { get; set; }
        public string VBNDesc { get; set; }
		public string DescProps { get; set; }
		public string VbncVbnCode { get; set; }
		public decimal OfdtNumOfItems { get; set; }
		public string CdcaCaskCode { get; set; }
		public string BulkCdcaCaskCode { get; set; }
		public decimal CacaNumOfUnits { get; set; }
		public decimal CacaUnitsPerLayer { get; set; }
        public decimal NumOfItemsPerBunch { get; set; }
		public decimal CacaLayersPerTrolley { get; set; }
		public string PictureRef { get; set; }
		public string GrowerGLN { get; set; }
		public int HasStaffelInt { get; set; }
		public bool HasStaffelBool { get { return HasStaffelInt == 1; } }
		public DateTime? MutationDate { get; set; }
        public DateTime? LatestDeliveryDate { get; set; }

		/// <summary>
		/// De Field Mapping voor de proc_getOfferDetailsService procedure result set.
		/// </summary>
		public static readonly ReaderMapper<OfferDetailRow> proc_getOfferDetailsService =
			new ReaderMapper<OfferDetailRow>()
                .Field(x => x.CABPicture, "CABC_FK_PICTURE")
				.Field(x => x.OfferPicture, "OFDT_FK_PICTURE")
                .Field(x => x.GrowAssortmentPicture, "GRAS_FK_PICTURE")
				.Field(x => x.OfdtSequence, "OFDT_SEQUENCE")
				.Field(x => x.OfdtItemPrice, "OFDT_ITEM_PRICE")
				.Field(x => x.OfdtFkCABCode, "OFDT_FK_CAB_CODE")
                .Field(x => x.OfhdSequence, "OFHD_SEQUENCE")
				.Field(x => x.OfhdValidFrom, "OFHD_VALID_FROM")
				.Field(x => x.OfhdValidTo, "OFHD_VALID_TO")
				.Field(x => x.OrgaEANCode, "ORGA_EAN_CODE")
				.Field(x => x.CabcCABCode, "CABC_CAB_CODE")
                .Field(x => x.OfferType, "OFHD_OFFER_TYPE")
				.Field(x => x.OfhdRemark, "OFHD_REMARK")
				.Field(x => x.OfdtRemark, "OFDT_REMARK")
                .Field(x => x.CABDesc, "CAB_DESC")
                .Field(x => x.VBNDesc, "VBN_DESC")
				.Field(x => x.DescProps, "DESC_PROPS")
				.Field(x => x.VbncVbnCode, "VBNC_VBN_CODE")
				.Field(x => x.OfdtNumOfItems, "OFDT_NUM_OF_ITEMS")
				.Field(x => x.CdcaCaskCode, "CDCA_CASK_CODE")
				.Field(x => x.BulkCdcaCaskCode, "BULK_CDCA_CASK_CODE")
				.Field(x => x.CacaNumOfUnits, "CACA_NUM_OF_UNITS")
				.Field(x => x.CacaUnitsPerLayer, "CACA_UNITS_PER_LAYER")
				.Field(x => x.CacaLayersPerTrolley, "CACA_LAYERS_PER_TROLLEY")
				.Field(x => x.HasStaffelInt, "HasStaffel")
				.Field(x => x.MutationDate, "OFDT_MUTATION_DATE")
                .Field(x => x.LatestDeliveryDate, "ORGI_LATEST_DELIVERY_DATE_TIME")
                .Field(x => x.PictureRef, "OFDT_EXT_PICTURE_REF")
				.Field(x => x.GrowerGLN, "GREL_EAN_CODE")
                .Field(x => x.GrowOrgSeq, "GROW_SEQ")
                .Field(x => x.VBNGroupType, "CDVG_MAINGROUP")
                .Field(x => x.VBNGroupCode, "VBN_GROUP_CODE")
                .Field(x => x.VBNGroupDesc, "VBN_GROUP_DESC")
                .Field(x => x.GrowAssortmentSequence, "GRAS_SEQUENCE")
                .Field(x => x.NumOfItemsPerBunch, "NUM_ITEMS_BUNCH");
    }
}