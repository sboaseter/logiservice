using System;
using System.Data;
using LogiFlora.Common.Db;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class OfferHeader
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public static string GetSystemCounter(
			string counType,
			decimal? orgaSeq,
			decimal? buyrSeq,
			decimal? growSeq,
			string counFormat,
			bool updateYN)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_getSystemCounter"))
			{
				try
				{
					cmd.AddInputParameter("@COUN_TYPE", counType, 50);
					cmd.AddInputParameter("@ORGA_SEQ", orgaSeq, 18, 0);
					cmd.AddInputParameter("@BUYR_SEQ", buyrSeq, 18, 0);
					cmd.AddInputParameter("@GROW_SEQ", growSeq, 18, 0);
					cmd.AddInputParameter("@COUN_FORMAT", counFormat, 50);
					cmd.AddOutputParameter("@COUN_VALUE", DbType.String, 50);
					cmd.AddInputParameter("@UPDATE_YN", updateYN);

					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<string>("@COUN_VALUE");
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}

		public static decimal CreateOfferHeader(
			decimal? growSeq,
			decimal? buyrSeq,
			decimal? cdmgSeq,
			string ofhdNo,
			DateTime? ofhdValidFrom,
			DateTime? ofhdValidTo,
			string remark)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_createOfferHeader"))
			{
				try
				{
					cmd.AddInputParameter("@GROW_SEQ", growSeq, 18, 0);
					cmd.AddInputParameter("@BUYR_SEQ", buyrSeq, 18, 0);
					cmd.AddInputParameter("@CDMG_SEQ", cdmgSeq, 18, 0);
					cmd.AddInputParameter("@OFHD_NO", ofhdNo, 50);
					cmd.AddInputParameter("@OFHD_VALID_FROM", ofhdValidFrom);
					cmd.AddInputParameter("@OFHD_VALID_TO", ofhdValidTo);
					cmd.AddInputParameter("@REMARK", remark, 50);
					cmd.AddOutputParameter("@OFHD_SEQ", DbType.Decimal, 18, 0);

					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<Decimal>("@OFHD_SEQ", -1);
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}

		public static decimal CreateOfferDetailService(
			decimal? ofdtFkOfferHeader,
			string cabcCABCode,
			decimal? ofdtNumOfItems,
			decimal? ofdtItemPrice,
			string ofdtRemark,
			string cdcaCaskCode,
			decimal? cacaNumOfUnits,
			decimal? cacaUnitsPerLayer,
			decimal? cacaLayersPerTrolley)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_createOfferDetailService"))
			{
				try
				{
					cmd.AddInputParameter("@OFDT_FK_OFFER_HEADER", ofdtFkOfferHeader, 18, 0);
					cmd.AddInputParameter("@CABC_CAB_CODE", cabcCABCode, 255);
					cmd.AddInputParameter("@OFDT_NUM_OF_ITEMS", ofdtNumOfItems, 18, 0);
					cmd.AddInputParameter("@OFDT_ITEM_PRICE", ofdtItemPrice, 18, 3);
					cmd.AddInputParameter("@OFDT_REMARK", ofdtRemark, 255);
					cmd.AddInputParameter("@CDCA_CASK_CODE", cdcaCaskCode, 255);
					cmd.AddInputParameter("@CACA_NUM_OF_UNITS", cacaNumOfUnits, 18, 0);
					cmd.AddInputParameter("@CACA_UNITS_PER_LAYER", cacaUnitsPerLayer, 18, 0);
					cmd.AddInputParameter("@CACA_LAYERS_PER_TROLLEY", cacaLayersPerTrolley, 18, 0);
					cmd.AddOutputParameter("@OFDT_SEQ", DbType.Decimal, 18, 0);

					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<Decimal>("@OFDT_SEQ", -1);
				}
				catch (Exception ex)
				{
					Log.Error(ex);
					throw;
				}
			}
		}
	}
}