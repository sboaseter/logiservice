﻿using System;
using System.Data;
using LogiFlora.Common.Db;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class OrderDetailsData
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public static bool ValidateOfferDetailBuyerGrower(
			decimal? ofdtSeq,
			decimal? growSeq,
			decimal? buyrSeq)
		{
			try
			{
				using (var conn = SvcConnectionProvider.GetConnection())
				using (var cmd = conn.CreateProcedure("proc_ValidateOfferDetailBuyerGrower"))
				{
					cmd.AddInputParameter("@OFDT_SEQ", ofdtSeq, 18, 0);
					cmd.AddInputParameter("@GROW_SEQ", growSeq, 18, 0);
					cmd.AddInputParameter("@BUYR_SEQ", buyrSeq, 18, 0);
					cmd.AddOutputParameter("@Valid", DbType.Boolean);

					cmd.ExecuteNonQuery();
					return cmd.Parameters.Get<Boolean>("@Valid");
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex);
				throw;
			}
		}

        public DataTable GetPossibleDelMoments(int? dmhdSeq, int ofhdSeq, int hndoSeq)
        {
            return GetPossibleDelMoments(dmhdSeq, ofhdSeq, hndoSeq, -1, null, null, "");
        }

        public DataTable GetPossibleDelMoments(int? dmhdSeq, int ofhdSeq, int hndoSeq, int dayOfTheWeek, TimeSpan? orderTime, TimeSpan? deliverTime, String delType)
        {
            try
            {
                using (var conn = SvcConnectionProvider.GetConnection())
                using (var cmd = conn.CreateProcedure("proc_GetPossibleDeliveryMoments"))
                {
                    cmd.AddInputParameter("@OFHD_SEQ", ofhdSeq);
                    cmd.AddInputParameter("@HNDO_SEQ", hndoSeq);
                    cmd.AddInputParameter("@DMHD_SEQ", dmhdSeq);
                    cmd.AddInputParameter("@Day_Number", dayOfTheWeek);
                    cmd.AddInputParameter("@Order_Time", orderTime);
                    cmd.AddInputParameter("@Deliver_Type", delType);
                    cmd.AddInputParameter("@Deliver_Time", deliverTime);
                    cmd.ExecuteNonQuery();
                    return cmd.ExecuteTable();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
    }
}