﻿using System;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class OrganisationRow
	{
		public int OrganisationSequence { get; set; }
		public Enums.OrganisationType OrganisationType { get; set; }
		public string OrganisationName { get; set; }
		public string OrganisationEAN { get; set; }
	}

	public class OrganisationGrowerRow : OrganisationRow
	{
		public int BulkCaskFk { get; set; }
		// fust als losse class onderdeel maken van deze class
		public string BulkCask { get; set; }
		public int GrowerSequence { get; set; }
	}

	public class OrganisationBuyerRow : OrganisationRow
	{
		public int BuyerSequence { get; set; }
	}

	public class OrganisationData
	{
		public OrganisationGrowerRow GetGrowerFromEAN(string ean)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateQuery(@"
select * from F_CAB_ORGANISATION
  left outer join F_CAB_GROWER on ORGA_SEQUENCE = GROW_FK_ORGA 
  left outer join F_CAB_CD_CASK on GROW_FK_BULK_CASK = CDCA_SEQUENCE 
 where ( ORGA_EAN_CODE = @EANCode )
;"))
			{
				cmd.AddInputParameter("@EANCode", ean);
				using (var dr = cmd.ExecuteReader())
				if (dr.Read())
				{
					return new OrganisationGrowerRow
					{
						OrganisationSequence = dr.Get<int>("ORGA_SEQUENCE"),
						OrganisationName = dr.Get<string>("ORGA_NAME"),
						OrganisationType = Enums.OrganisationType.GROW,
						OrganisationEAN = dr.Get<string>("ORGA_EAN_CODE"),
						GrowerSequence = dr.Get<int>("GROW_SEQUENCE"),
						BulkCask = dr.Get<string>("CDCA_CASK_DESC"),
						BulkCaskFk = dr.Get<int>("GROW_FK_BULK_CASK")
					};
				}
			}
			return null;
		}

		public OrganisationBuyerRow GetBuyerFromEAN(string ean)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateQuery(@"
select * from F_CAB_ORGANISATION
  left outer join F_CAB_BUYER on ORGA_SEQUENCE = BUYR_FK_ORGA 
 where ( ORGA_EAN_CODE = @EANCode )
;"))
			{
				cmd.AddInputParameter("@EANCode", ean);
				using (var dr = cmd.ExecuteReader())
					if (dr.Read())
					{
						return new OrganisationBuyerRow
						{
							OrganisationSequence = dr.Get<int>("ORGA_SEQUENCE"),
							OrganisationName = dr.Get<string>("ORGA_NAME"),
							OrganisationType = Enums.OrganisationType.BUYR,
							OrganisationEAN = dr.Get<string>("ORGA_EAN_CODE"),
							BuyerSequence = dr.Get<int>("BUYR_SEQUENCE")
						};
					}
			}
			return null;
		}

	}
}