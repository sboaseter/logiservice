﻿using System;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class SettingRow
	{
        public int Sequence { get; set; }
        public int? ShopSeq { get; set; }
        public int? OrgaSeq { get; set; }
        public int? PersSeq { get; set; }
        public string SettingCodeID { get; set; }
        public string SettingDescription { get; set; }
        public string SettingValue { get; set; }
	}

	public class SettingData
	{
		public SettingRow GetSettingByID(string codeID, int? shopSeq, int? orgaSeq, int? persSeq)
		{
			using (var conn = SvcConnectionProvider.GetConnection())
			using (var cmd = conn.CreateProcedure("proc_getSettings"))
			{
                cmd.AddInputParameter("@SHOP_SEQ", shopSeq);
				cmd.AddInputParameter("@ORGA_SEQ", orgaSeq);
                cmd.AddInputParameter("@PERS_SEQ", persSeq);
                cmd.AddInputParameter("@CODE_ID", codeID);
				using (var dr = cmd.ExecuteReader())
                if (dr.Read())
				{
					return new SettingRow
					{
						Sequence = dr.Get<int>("SEQUENCE"),
						ShopSeq = dr.Get<int?>("SHOP_SEQ"),
						OrgaSeq = dr.Get<int?>("ORGA_SEQ"),
						PersSeq = dr.Get<int?>("PERS_SEQ"),
						SettingCodeID = dr.Get<string>("CODE_ID"),
                        SettingDescription = dr.Get<string>("DESCRIPTION"),
                        SettingValue = dr.Get<string>("VALUE")
					};
				}
			}
			return null;
		}

	}
}