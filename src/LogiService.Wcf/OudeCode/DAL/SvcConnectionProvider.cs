﻿using System;
using System.Data;
using System.Data.SqlClient;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class SvcConnectionProvider : ICabConnectionProvider
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public static IDbConnection GetConnection()
		{
			try
			{
				var con = new SqlConnection(Config.Conn);
				con.Open();
				return con;
			} catch (Exception ex)
			{
				Log.Error("Unable to open Logiflora Connection.", ex);
				throw;
			}
		}

		IDbConnection IConnectionProvider.GetConnection()
		{
			return GetConnection();
		}
	}
}