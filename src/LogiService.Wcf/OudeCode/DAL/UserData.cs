using System;
using System.Web;
using LogiCAB.Common;
using LogiFlora.Common.Db;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class UserData
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public UserRow GetUserBuyer(string user, string pass)
		{
			return GetUser(user, pass, Enums.OrganisationType.BUYR);
		}

		public UserRow GetUserGrower(string user, string pass)
		{
			return GetUser(user, pass, Enums.OrganisationType.GROW);
		}

		private static UserRow GetUser(
			string username,
			string password,
			Enums.OrganisationType organisationType)
		{
			var user = organisationType == Enums.OrganisationType.BUYR
				? QueryBuyer(username, password)
				: QueryGrower(username, password);

			if (user == null)
			{
				Log.WarnFormat("Failed login from {0}.", HttpContext.Current.Request["REMOTE_HOST"]);
				return null;
			}

			user.ShopSequence = Global.Container.Resolve<IShopPropertyProvider>().ShopId;
			Log.InfoFormat("Login by {1} from {0}.", HttpContext.Current.Request["REMOTE_HOST"], user.LoginSequence);
			return user;
		}

		private static UserRow QueryBuyer(string user, string pass)
		{
			using (var con = SvcConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	BUYR_SEQUENCE
,	ORGA_SEQUENCE
,	LOGI_SEQUENCE
,	PERS_SEQUENCE
,	PERS_NAME
,	ORGA_NAME
From
	V_LOGIN
Inner Join
	V_ORGA_ADMIN
		On	( ORAD_FK_ORGANISATION = ORGA_SEQUENCE )
Inner Join
	F_CAB_BUYER
		On	( BUYR_FK_ORGA = ORGA_SEQUENCE )
Where
	( LOGI_USER_NAME = @User )
And	( LOGI_PASS = @Pass )
;"))
			{
				cmd.AddInputParameter("@User", user);
				cmd.AddInputParameter("@Pass", pass);
				using (var dr = cmd.ExecuteReader())
					if (dr.Read())
						return new UserRow
						{
							BuyerSequence = dr.Get<int>("BUYR_SEQUENCE"),
							GrowerSequence = 0,
							OrganisationSequence = dr.Get<int>("ORGA_SEQUENCE"),
							LoginSequence = dr.Get<int>("LOGI_SEQUENCE"),
							PersonSequence = dr.Get<int>("PERS_SEQUENCE"),
							PersonName = dr.Get<string>("PERS_NAME"),
							OrganisationName = dr.Get<string>("ORGA_NAME"),
							OrganisationType = Enums.OrganisationType.BUYR
						};
			}
			return null;
		}

		private static UserRow QueryGrower(string user, string pass)
		{
			using (var con = SvcConnectionProvider.GetConnection())
			using (var cmd = con.CreateQuery(@"
Select
	GROW_SEQUENCE
,	ORGA_SEQUENCE
,	LOGI_SEQUENCE
,	PERS_SEQUENCE
,	PERS_NAME
,	ORGA_NAME
From
	V_LOGIN
Inner Join
	V_ORGA_ADMIN
		On	( ORAD_FK_ORGANISATION = ORGA_SEQUENCE )
Inner Join
	F_CAB_GROWER
		On	( GROW_FK_ORGA = ORGA_SEQUENCE )
Where
	( LOGI_USER_NAME = @User )
And	( LOGI_PASS = @Pass )
;"))
			{
				cmd.AddInputParameter("@User", user);
				cmd.AddInputParameter("@Pass", pass);
				using (var dr = cmd.ExecuteReader())
					if (dr.Read())
						return new UserRow
						{
							BuyerSequence = 0,
							GrowerSequence = dr.Get<int>("GROW_SEQUENCE"),
							OrganisationSequence = dr.Get<int>("ORGA_SEQUENCE"),
							LoginSequence = dr.Get<int>("LOGI_SEQUENCE"),
							PersonSequence = dr.Get<int>("PERS_SEQUENCE"),
							PersonName = dr.Get<string>("PERS_NAME"),
							OrganisationName = dr.Get<string>("ORGA_NAME"),
							OrganisationType = Enums.OrganisationType.GROW
						};
			}
			return null;
		}

	}
}