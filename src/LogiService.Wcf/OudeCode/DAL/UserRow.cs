﻿using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode.DAL
{
	public class UserRow
	{
		public int LoginSequence { get; set; }
		public int OrganisationSequence { get; set; }
		public int BuyerSequence { get; set; }
		public int GrowerSequence { get; set; }
		public int PersonSequence { get; set; }
		public string PersonName { get; set; }
		public string OrganisationName { get; set; }
		public Enums.OrganisationType OrganisationType { get; set; }
		public int ShopSequence { get; set; }
	}
}