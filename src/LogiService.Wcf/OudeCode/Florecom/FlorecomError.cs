﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for Error
/// </summary>
public static class FlorecomError
{

    
    public static void ThrowFlorecomError(string ErrorMessage, int ErrorCode, string ErrorLocation)
    {
        //Construct the Florecom ErrorList structure
        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        System.Xml.XmlNamespaceManager ns = new System.Xml.XmlNamespaceManager(doc.NameTable);
        ns.AddNamespace("fsm", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
        System.Xml.XmlNode FlorecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        System.Xml.XmlNode FlorecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
        FlorecomErrorNode.AppendChild(FlorecomErrorLevelNode);
        FlorecomErrorNode.AppendChild(FlorecomErrorSeverity);
        FlorecomErrorNode.AppendChild(FlorecomErrorErrorCode);
        FlorecomErrorNode.AppendChild(FlorecomErrorLocation);
        FlorecomErrorNode.AppendChild(FlorecomErrorDescriptionText);
        FlorecomErrorListNode.AppendChild(FlorecomErrorNode);
        //Add the custom values to the ErrorList strucure
        FlorecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
        FlorecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
        FlorecomErrorErrorCode.AppendChild(doc.CreateTextNode(ErrorCode.ToString()));
        FlorecomErrorLocation.AppendChild(doc.CreateTextNode(ErrorLocation));
        FlorecomErrorDescriptionText.AppendChild(doc.CreateTextNode(ErrorMessage));
        detailNode.AppendChild(FlorecomErrorListNode);
        //Return a Soap:Fault that incorporates the Florecom ErrorList scheme
        throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, HttpContext.Current.Request.Url.AbsoluteUri, detailNode);
    }



}




/// TODO: Implement FlorecomError-handler i.s.o. ErrorList()
/*
/// <summary>
/// Errors should be returned using a SOAP:Fault (SOAP version 1.1). The Detail-element of the Soap:fault should contain the errors according to
/// The ErrorList scheme is described in the document "Webservice Beschrijvingen Virtuele Marktplaats" that is publised on sdk.florecom.org.
/// the Florecom Errorlist-scheme e.g.:
/// <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xml="http://www.w3.org/XML/1998/namespace">
//  <env:Body>
//    <env:Fault>
//      <env:faultcode>env:Sender</env:faultcode>
//      <env:faultstring>Error detected by the webservice. Check the Detail-element in this SOAP:Fault response message for more error details.</env:faultstring>
//      <env:detail>
//        <ErrorList xmlns="urn:fec:messages:data:final:FlorecomStandardMessage:0_5">
//          <Error>
//            <ErrorLevel>message</ErrorLevel>
//            <Severity>error</Severity>
//            <ErrorCode>102</ErrorCode>
//            <ErrorLocation>SupplyResponse/Body/SupplyResponseDetails/AgentParty</fsm:ErrorLocation>
//            <DescriptionText>Unknown agent party.</DescriptionText>
//          </Error>
//        </ErrorList>
//      </env:detail>
//    </env:Fault>
//  </env:Body>
//</env:Envelope>
/// </summary>
/// <param name="ErrorMessage"></param>
public void ThrowFlorecomError(string ErrorMessage, int ErrorCode, string ErrorLocation)
{
    //Construct the Florecom ErrorList structure
    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
    System.Xml.XmlNamespaceManager ns = new System.Xml.XmlNamespaceManager(doc.NameTable);
    ns.AddNamespace("fsm", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode detailNode = doc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
    System.Xml.XmlNode FlorecomErrorListNode = doc.CreateNode(XmlNodeType.Element, "ErrorList", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorNode = doc.CreateNode(XmlNodeType.Element, "Error", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorLevelNode = doc.CreateNode(XmlNodeType.Element, "ErrorLevel", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorSeverity = doc.CreateNode(XmlNodeType.Element, "Severity", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorErrorCode = doc.CreateNode(XmlNodeType.Element, "ErrorCode", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorLocation = doc.CreateNode(XmlNodeType.Element, "ErrorLocation", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    System.Xml.XmlNode FlorecomErrorDescriptionText = doc.CreateNode(XmlNodeType.Element, "DescriptionText", "urn:fec:messages:data:final:FlorecomStandardMessage:0_5");
    FlorecomErrorNode.AppendChild(FlorecomErrorLevelNode);
    FlorecomErrorNode.AppendChild(FlorecomErrorSeverity);
    FlorecomErrorNode.AppendChild(FlorecomErrorErrorCode);
    FlorecomErrorNode.AppendChild(FlorecomErrorLocation);
    FlorecomErrorNode.AppendChild(FlorecomErrorDescriptionText);
    FlorecomErrorListNode.AppendChild(FlorecomErrorNode);
    //Add the custom values to the ErrorList strucure
    FlorecomErrorLevelNode.AppendChild(doc.CreateTextNode("message"));
    FlorecomErrorSeverity.AppendChild(doc.CreateTextNode("error"));
    FlorecomErrorErrorCode.AppendChild(doc.CreateTextNode(ErrorCode.ToString()));
    FlorecomErrorLocation.AppendChild(doc.CreateTextNode(ErrorLocation));
    FlorecomErrorDescriptionText.AppendChild(doc.CreateTextNode(ErrorMessage));
    detailNode.AppendChild(FlorecomErrorListNode);
    //Return a Soap:Fault that incorporates the Florecom ErrorList scheme
    throw new SoapException("Error. Check Detail-element for more info.", SoapException.ClientFaultCode, Context.Request.Url.AbsoluteUri, detailNode);
}

}*/
