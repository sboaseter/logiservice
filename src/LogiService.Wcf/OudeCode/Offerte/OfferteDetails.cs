using Florecom.standards.xml.VMP_v0_1;

namespace Logiflora.Web.Offerte
{
	/// <summary>
	/// Dit is een zo algemeen mogelijke class met alle mogelijke informatie over een 'Offerte'.
	/// Hij maakt gebruik van de SupplyLine die ook in de SupplyDetails zit.
	/// </summary>
	public class OfferteDetails
	{
		/// <summary>
		/// Een verzameling unieke identifier voor deze Offerte.
		/// </summary>
		[System.Xml.Serialization.XmlElementAttribute("OfferteID")]
		public IDType[] OfferteID { get; set; }

		public SellerPartyType SellerParty { get; set; }
		public BuyerPartyType BuyerParty { get; set; }

		public MarketplaceIDType MarketplaceID { get; set; }
		public MarketFormType MarketForm { get; set; }

		public TextType Remark { get; set; }

		[System.Xml.Serialization.XmlElementAttribute("SupplyLine")]
		public SupplyLine[] SupplyLine { get; set; }
	}
}