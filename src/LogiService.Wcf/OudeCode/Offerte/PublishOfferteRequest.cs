using System.Xml.Serialization;
using LogiService.Wcf.OudeCode.Util;

namespace Logiflora.Web.Offerte
{
	public class PublishOfferteRequest
	{

		[XmlNamespaceDeclarations]
		public XmlSerializerNamespaces namespaces;

		public PublishOfferteRequest()
		{
			namespaces = Helper.CompressionNamespaces();
		}

		public RequestHeader Header { get; set; }

		public PublishOfferteRequestBody Body { get; set; }
	}
}