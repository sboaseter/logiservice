namespace Logiflora.Web.Offerte
{
	public class PublishOfferteRequestBody
	{
		public OfferteDetails OfferteDetails { get; set; }
	}
}