using System.Xml.Serialization;

namespace Logiflora.Web.Offerte
{
	public class PublishOfferteResponse
	{
		#region Added by C.Ilbrink to use namespace aliases i.e. compress XML
		[XmlNamespaceDeclarations]
		public XmlSerializerNamespaces namespaces;

		/// <summary>
		/// Use namespace aliasses
		/// </summary>
		public PublishOfferteResponse()
		{
			namespaces = new XmlSerializerNamespaces();
			namespaces.Add("fsm", "urn:fec:messages:data:final:FlorecomStandardMessage:0_2");
			namespaces.Add("ram", "urn:un:unece:uncefact:data:draft:ReusableAggregateBusinessInformationEntity:2b");
			namespaces.Add("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:3b");
			namespaces.Add("qdt", "urn:un:unece:uncefact:data:draft:QualifiedDataType:1b");
		}
		#endregion

		public PublishOfferteResponseBody Body { get; set; }
	}
}