using Florecom.standards.xml.VMP_v0_1;

namespace Logiflora.Web.Offerte
{
	public class PublishOfferteResponseBody
	{
		public bool Success { get; set; }

		[System.Xml.Serialization.XmlArrayItemAttribute("Error", IsNullable = false)]
		public ErrorListError[] ErrorList { get; set; }
	}
}