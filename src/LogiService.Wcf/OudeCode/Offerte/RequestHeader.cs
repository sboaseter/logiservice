using Florecom.standards.xml.VMP_v0_1;

namespace Logiflora.Web.Offerte
{
	public class RequestHeader
	{
		public TextType UserName { get; set; }

		public TextType Password { get; set; }

		public IDType MessageID { get; set; }

		public System.DateTime MessageDateTime { get; set; }

		public decimal MessageSerial { get; set; }

		[System.Xml.Serialization.XmlElementAttribute("ReferencedDocument")]
		public ReferencedDocumentType[] ReferencedDocument { get; set; }
	}
}