﻿using System;
using System.IO;
using System.Web;
using LogiService.Wcf.OudeCode.Util;

namespace LogiService.Wcf.OudeCode
{
	/// <summary>
	/// Een simpele class om wat controles op paden uit te voeren bij het
	/// starten van de applicatie.
	/// </summary>
	public class PathTool
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public void InitializeApplicationPaths()
		{
			Config.CabImagesPath = MapPath(Config.CabImagesUrl);
			ValidateWriteablePath(Config.CabImagesPath, "CabImagesPath");
		}

		public string MapPath(string input)
		{
			return HttpContext.Current.Server.MapPath(input);
		}

		public bool ValidateWriteablePath(string path, string name)
		{
			// Check existance.
			if (!Directory.Exists(path))
			{
				Log.FatalFormat("{1} [{0}] does not exist!", path, name);
				return false;
			}

			// Check modify access.
			var tempFileName = "WriteTest-" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".txt";
			var tempFilePath = Path.Combine(path, tempFileName);
			try
			{
				File.WriteAllText(tempFilePath, name);
				File.Delete(tempFilePath);
			}
			catch (Exception ex)
			{
				Log.FatalFormat(String.Format("Unable to write to {1} [{0}].", path, name), ex);
				return false;
			}

			// Ok.
			Log.InfoFormat("Path {1} [{0}] looks OK.", path, name);
			return true;
		}

	}
}