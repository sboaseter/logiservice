﻿using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Florecom.standards.xml.VMP_v0_1;
using Logiflora.Web.Offerte;
using LogiService.Wcf.OudeCode.BLL;
using LogiService.Wcf.OudeCode.DAL;
using LogiService.Wcf.OudeCode.Util;
using Tracing.Asmx;

namespace Logiflora.Web.BLL
{
	[WebService(Namespace = "http://www.logioffer.nl/logiservice/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class VMP_Service
	{
		[WebMethodAttribute, TraceExtension]
		[SoapDocumentMethodAttribute("urn:fec:messages:data:final:FlorecomStandardMessage:0_2/Supply", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
		[return: XmlElementAttribute("SupplyResponse", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]

		public SupplyMessage GetSupply(
			[XmlElementAttribute("SupplyRequest", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]
			SupplyRequest request)
		{
			var response = new SupplyMessage {Body = new SupplyMessageBody()};
            //WK optie toevoegen voor Basic Authentication..
            // Determine the beginning index of the Base64-encoded string in the Authorization header by finding the first space.
            // Add 1 to the index so we can properly grab the substring.
            //var decodedAuth = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(request.Header.));
 
            // Split the credentials into the username and password portions on the colon character.
            //var splits = decodedAuth.Split(':');
            //var username = splits[0];
            //var password = splits[1];

			var user = new UserData().GetUserBuyer(request.Header.UserName.Value, request.Header.Password.Value);
			if (user == null)
			{
				response.Body.ErrorList = ErrorList.AddError("Name or password incorrect", Enums.ErrorLevel.Authentication);
				return response;
			}

			var offerdetails = new OfferDetail(user);
			if (offerdetails.FillMessageHeader(response))
				offerdetails.FillMessage(response); // Only continue if header was filled OK.

			return response; // Always return the message. It contains an error if previous has failed
		}

		[WebMethodAttribute, TraceExtension]
		[SoapDocumentMethod("urn:fec:messages:data:final:FlorecomStandardMessage:0_2/Order", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
		[return: XmlElementAttribute("OrderResponse", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]
		public OrderResponseMessage Order(
			[XmlElement("OrderRequest", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]
			OrderRequest request)
		{
			var response = new OrderResponseMessage { Body = new OrderResponseMessageBody() };

			var user = new UserData().GetUserBuyer(request.Header.UserName.Value, request.Header.Password.Value);
			if (user == null)
			{
				response.Body.ErrorList = ErrorList.AddError("Name or password incorrect", Enums.ErrorLevel.Authentication);
				return response;
			}

			// Process the order ...
			try
			{
				var orderRequest = new OrderRequestProcessor(request, response.Body, user);
				if (!orderRequest.ProcessOrderRequest())
				{
					response.Body.ErrorList = ErrorList.AddError("Could not process orderrequest: " + orderRequest.LastError, Enums.ErrorLevel.System);
				}
			}
			catch
			{
				response.Body.ErrorList = ErrorList.AddError("Could not process orderrequest.", Enums.ErrorLevel.System);
			}
			return response;
		}

		[WebMethod, TraceExtension]
		[SoapDocumentMethodAttribute("urn:fec:messages:data:final:FlorecomStandardMessage:0_2/PublishOfferte", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
		[return: XmlElementAttribute("PublishOfferteResponse", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]
		public PublishOfferteResponse PublishOfferte(
			[XmlElementAttribute("PublishOfferteRequest", Namespace = "urn:fec:messages:data:final:FlorecomStandardMessage:0_2")]
			PublishOfferteRequest request
		)
		{
			var response = new PublishOfferteResponse { Body = new PublishOfferteResponseBody { Success = false } };

			var user = new UserData().GetUserGrower(request.Header.UserName.Value, request.Header.Password.Value);
			if (user == null)
			{
				response.Body.ErrorList = ErrorList.AddError("Name or password incorrect", Enums.ErrorLevel.Authentication);
				return response;
			}

			var processor = new PublishOfferteProcessor(request, user);
			var validation = processor.Validate();
			if (validation.Length > 0)
			{
				response.Body.ErrorList = ErrorList.Content("Validation errors.", validation);
				return response;
			}
			if (!processor.Process())
			{
				response.Body.ErrorList = ErrorList.System("Could not Publish Offerte: " + processor.LastError);
				return response;
			}

			response.Body.Success = true;
			return response;
		}

	}
}