﻿using System;
using System.Configuration;
using System.Web;

namespace LogiService.Wcf.OudeCode.Util
{
	/// <summary>
	/// Retrieve various variables from the web.config-file
	/// </summary>
	public class Config
	{

        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		public static readonly DateTime MinimumDate = new DateTime(2000, 1, 1);
		public static readonly DateTime MaximumDate = new DateTime(2100, 1, 1);
		public static readonly string MinimumDateString = MinimumDate.ToString("yyyy-MM-dd");
		public static readonly string MaximumDateString = MaximumDate.ToString("yyyy-MM-dd");

		/// <summary>
		/// Returns 'env' from web.config
		/// Used to handle configuration-groups on different servers (local / live)
		/// </summary>
		public static string Environment
		{
			get { return ConfigurationManager.AppSettings["env"]; }
		}

		/// <summary>
		/// Returns connectionstring 'conn' from web.config
		/// </summary>
		public static string Conn
		{
			get { return GetConnectionString("conn"); }
		}

		/// <summary>
		/// web.config: cabImagesUrl
		/// </summary>
		public static string CabImagesUrl
		{
			get { return ConfigurationManager.AppSettings["cabImagesUrl"]; }
		}
		public static string CabImagesPath
		{
			get { return HttpContext.Current.Application["cabImagesPath"] as string; }
			set { HttpContext.Current.Application["cabImagesPath"] = value; }
		}

		private static string GetConnectionString(string name)
		{
			if (Environment.Length > 0)
				name += "." + Environment;

			var rv = "";
			try
			{
				rv = ConfigurationManager.ConnectionStrings[name].ConnectionString;
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}
			if (rv == null) rv = "";

			return rv;
		}
	}
}