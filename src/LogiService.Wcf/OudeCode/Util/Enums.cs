﻿using System;

namespace LogiService.Wcf.OudeCode.Util
{
	public static class Enums
	{
		public enum OrderDetailStatus // Use uppercase when inserting into database !
		{
			Req,             // Aanvraag
			Rejected,        // Geweigerd
			Cancel,          // Geannuleerd
			ChangedOrder,    // Gewijzigde bestelling
			ChangedByGrower, //	Gewijzigd door kweker
			Ordered,         // Besteld
			ChangedByBuyer,  // Gewijzigd door koper
			Confirmed        // Bevestigd
		}

		public enum OrderHeaderStatus // Use uppercase when inserting into database !
		{
			Req,             // Aanvraag
			Confirmed,       // Bevestigd
			Rejected,        // Geweigerd
			KwbSent,         // KWB verzonden
			EktKwbSent,      // EKT/KWB verzonden
			New,             // Nieuw opgeslagen order
			Cancel,          // Geannuleerd
			ChangedOrder,    // Gewijzigde bestelling
			ChangedByGrower, // Gewijzigd door leverancier
			Ordered,         // Besteld
			ChangedByBuyer,  // Gewijzigd door koper
			SavedByBuyer,    // Opgeslagen door koper
			SavedByGrower,   // Opgeslagen door leverancier
			EktSent,         // EKT verzonden
			BuyerRecall      // Teruggeroepen
		}

		public enum OrganisationType
		{
			BUYR,            // Buyer = Koper
			GROW             // Grower = Kweker
		}

		///<summary>
		/// 0: System
		/// 1: Authentication
		/// 2: Syntax
		/// 3: Content
		/// 4: Debug
		///</summary>
		public enum ErrorLevel
		{
			System,
			Authentication,
			Syntax,
			Content,
			Debug
		}
	}
}