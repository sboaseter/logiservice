﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogiService.Wcf.OudeCode.Util
{
	/// <summary>
	/// Some very useful and convenient methods
	/// </summary>
	public static class Helper
	{
		/// <summary>
		/// Returns the next workday (not considering holidays) based on the input-date
		/// </summary>
		/// <param name="currentDay">Day used as start for the calculation</param>
		/// <returns>Next workday (mon-fri)</returns>
		public static DateTime NextWorkday(DateTime currentDay)
		{
			var d = currentDay.AddDays(1);
			while (d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday)
				d = d.AddDays(1);
			return d;
		}

        /// <summary>
        /// Returns the next workday AND considering holidays based on the input-date
        /// </summary>
        /// <param name="currentDay">Day used as start for the calculation</param>
        /// <returns>Next workday (mon-fri) being not a holiday</returns>
        public static DateTime NextWorkday(DateTime currentDay, List<System.DateTime> holidays)
        {
            var nextDay = currentDay.AddDays(1);
            var validDay = false;

            while (!validDay)
            {
                validDay = true;

                while (nextDay.DayOfWeek == DayOfWeek.Saturday || nextDay.DayOfWeek == DayOfWeek.Sunday)
                    nextDay = nextDay.AddDays(1);

                foreach (DateTime holiday in holidays)
                {
                    if (holiday.Date == nextDay.Date)
                    {
                        nextDay = nextDay.AddDays(1);
                        validDay = false;
                        break;
                    }
                }
            }
            
            return nextDay;
        }

		public static XmlSerializerNamespaces CompressionNamespaces()
		{
			var compression = new XmlSerializerNamespaces();
			compression.Add("fsm", "urn:fec:messages:data:final:FlorecomStandardMessage:0_2");
			compression.Add("ram", "urn:un:unece:uncefact:data:draft:ReusableAggregateBusinessInformationEntity:2b");
			compression.Add("udt", "urn:un:unece:uncefact:data:standard:UnqualifiedDataType:3b");
			compression.Add("qdt", "urn:un:unece:uncefact:data:draft:QualifiedDataType:1b");
			return compression;
		}

		public static string FormatCABCode(string cabCode)
		{
			return cabCode.PadLeft(8, '0');
		}

		public static void AddFormat(this List<string> collection, string message, params object[] args)
		{
			collection.Add(String.Format(message, args));
		}
	}
}