﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="LogiService.Wcf.Services" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>LogiCAB Services</title>
	<style type="text/css">
		body 
		{
			font-family: Calibri, Tahoma, Arial, Sans-Serif;
		}
	</style>
</head>
<body>
	<form id="form1" runat="server">
	<div>
		<h2>Florecom Services</h2>
		<ul>
			<li>
				<a href="Default.asmx">Virtuele Marktplaats</a>
			</li>
		</ul>
		<br />
		<h2>Florecom Services July 2010</h2>
		<ul>
			<li>
				<a href="Florecom/Jul2010/CommercialCustomer.asmx">CommercialCustomer</a>
			</li>
			<li>
				<a href="Florecom/Jul2010/CommercialSupplier.asmx">CommercialSupplier</a>
			</li>
		</ul>
		<br />
		<h2>Florecom Services July 2011</h2>
		<ul>
			<li>
				<a href="Florecom/Jul2011/CommercialCustomer.asmx">CommercialCustomer</a>
			</li>
			<li>
				<a href="Florecom/Jul2011/CommercialSupplier.asmx">CommercialSupplier</a>
			</li>
		</ul>
		<br />
   		<h2>Florecom Services July 2012</h2>
		<ul>
			<li>
				<a href="Florecom/Jul2012/CommercialCustomer.asmx">CommercialCustomer</a>
			</li>
			<li>
				<a href="Florecom/Jul2012/CommercialSupplier.asmx">CommercialSupplier</a>
			</li>
		</ul>
		<br />
		<h2>LogiCAB Services</h2>
		<ul>
			<li>
				<a href="AuthenticationService.svc">Authentication Service</a>
			</li>
			<li>
				<a href="OrganisationService.svc">Organisation Service</a>
			</li>
			<li>
				<a href="PicturesService.svc">Pictures Service</a>
			</li>
		</ul>
		<br />

		<ul>
			<li>
				<a href="SampleServiceClient.zip">SampleServiceClient.zip</a>
			</li>
		</ul>
	</div>
	</form>
</body>
</html>
