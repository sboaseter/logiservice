﻿using System;

namespace LogiService.Wcf
{
	public partial class Services : System.Web.UI.Page
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected void Page_Load(object sender, EventArgs e)
		{
			Log.Debug("Ping.");
		}
	}
}
