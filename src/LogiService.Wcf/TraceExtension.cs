using System;
using System.IO;
using System.Web.Services.Protocols;

namespace Tracing.Asmx
{
	public class TraceExtension : SoapExtension
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		private Stream _external;
		private Stream _internal;
		private const int BufferSize = 1024*1024;

		public override Stream ChainStream(Stream stream)
		{
			_external = stream;
			if (Log.IsDebugEnabled)
			{
				_internal = new MemoryStream();
				return _internal;
			}
			return _external;
		}

		public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
		{
			return null;
		}

		public override object GetInitializer(Type webServiceType)
		{
			return null;
		}

		public override void Initialize(object initializer)
		{
		}

		public override void ProcessMessage(SoapMessage message)
		{
			if (!Log.IsDebugEnabled)
				return;

			switch (message.Stage)
			{
				case SoapMessageStage.AfterSerialize:
					Logger(_internal, _external, "OUT ");
					break;
				case SoapMessageStage.BeforeDeserialize:
					Logger(_external, _internal, "IN  ");
					break;
			}
		}

		private static void Logger(Stream input, Stream output, string direction)
		{
			// Eerst doorsturen naar originele stream.
			input.Position = 0;
			var buffer = new byte[BufferSize];
			var read = input.Read(buffer, 0, BufferSize);
			while (read != 0)
			{
				output.Write(buffer, 0, read);
				read = input.Read(buffer, 0, BufferSize);
			}
			output.Flush();
			input.Position = 0;

			// Dan loggen.
			Log.Debug(direction + new StreamReader(input).ReadToEnd());

			// En dan de memorystream 'clearen'.
			if (input is MemoryStream)
				input.Position = 0;
			if (output is MemoryStream)
				output.Position = 0;
		}
	}

	[AttributeUsage(AttributeTargets.Method)]
	public class TraceExtensionAttribute : SoapExtensionAttribute
	{
		private int _priority = 1;

		public override Type ExtensionType
		{
			get { return typeof(TraceExtension); }
		}

		public override int Priority
		{
			get { return _priority; }
			set { _priority = value; }
		}
	}
}