﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace LogiService.Test
{
	public class WcfMessageLogger
		: BehaviorExtensionElement
		, IClientMessageInspector
		, IDispatchMessageInspector 
		, IEndpointBehavior
	{
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
  
		protected string EndpointName { get; set; }

		public WcfMessageLogger()
		{
			EndpointName = String.Empty;
		}

		public WcfMessageLogger(string endpointName)
		{
			EndpointName = endpointName;
		}

		#region IClientMessageInspector
		public object BeforeSendRequest(ref Message request, IClientChannel channel)
		{
			Log.DebugFormat("Endpoint: {0}\n{1}", EndpointName, request.ToString());
			return null;
		}

		public void AfterReceiveReply(ref Message reply, object correlationState)
		{
			Log.DebugFormat("Endpoint: {0}\n{1}", EndpointName, reply.ToString());
		}
		#endregion IClientMessageInspector

		#region IDispatchMessageInspector
		public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
		{
			Log.DebugFormat("Endpoint: {0}\n{1}", EndpointName, request.ToString());
			return null;
		}

		public void BeforeSendReply(ref Message reply, object correlationState)
		{
			Log.DebugFormat("Endpoint: {0}\n{1}", EndpointName, reply.ToString());
		}
		#endregion IDispatchMessageInspector

		#region IEndpointBehavior
		public void Validate(ServiceEndpoint endpoint)
		{
			return;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
		{
			return;
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
		{
			Log.DebugFormat("Adding Dispatch MessageInspector for endpoint {0}.", endpoint.Name);
			endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new WcfMessageLogger(endpoint.Name));
		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
		{
			Log.DebugFormat("Adding Client MessageInspector for endpoint {0}.", endpoint.Name);
			clientRuntime.MessageInspectors.Add(new WcfMessageLogger(endpoint.Name));
		}
		#endregion IEndpointBehavior

		protected override object CreateBehavior()
		{
			return new WcfMessageLogger();
		}

		public override Type BehaviorType
		{
			get { return typeof(WcfMessageLogger); }
		}
	}
}
