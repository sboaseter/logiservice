using System;
using System.Security.Authentication;
using LogiServiceClient.AuthenticationService;

namespace LogiServiceClient
{
	///<summary>
	/// The AuthenticationManager manages Authentication for Requests.
	///</summary>
	public class AuthenticationManager
	{
		protected AuthenticationServiceContractClient Service { get; set; }
		protected string Username { get; set; }
		protected string Password { get; set; }
		protected string Ticket { get; set; }
		protected decimal RequestSequence { get; set; }

		/// <summary>
		/// Constructor taking an AuthenticationService, Username and Password.
		/// </summary>
		public AuthenticationManager(
			AuthenticationServiceContractClient authenticationService,
			string username,
			string password
		)
		{
			Service = authenticationService;
			Username = username;
			Password = password;
		}

		/// <summary>
		/// Authenticate the Username and Password and generate a Ticket.
		/// </summary>
		public void Authenticate()
		{
			// Fetch a new Seed value from the Authentication Service.
			var seed = Service.RequestSeed(new RequestSeedRequest()).Seed;

			// Double-hash the Password with the given Seed.
			var ch1 = Password.HashSha1();
			var ch2 = (ch1 + seed).HashSha1();

			// Create a Ticket Request.
			var ticketRequest = new RequestTicketRequest
			{
				Username = Username,
				Seed = seed,
				Hash = ch2
			};

			// Request the Ticket.
			var ticketResponse = Service.RequestTicket(ticketRequest);
			if (ticketResponse.ResultCode == -1001)
				throw new AuthenticationException();
			Ticket = ticketResponse.Ticket;
			RequestSequence = 0;
		}

		/// <summary>
		/// Authenticate the given AuthenticationService Request.
		/// </summary>
		public void Authenticate(AuthenticatedRequest request)
		{
			if (String.IsNullOrEmpty(Ticket))
				Authenticate();

			request.AuthenticationUsername = Username;
			request.AuthenticationRequestHash = GetNextRequestHash();
		}

		/// <summary>
		/// Authenticate the given OrganisationService Request.
		/// </summary>
		public void Authenticate(OrganisationService.AuthenticatedRequest request)
		{
			if (String.IsNullOrEmpty(Ticket))
				Authenticate();

			request.AuthenticationUsername = Username;
			request.AuthenticationRequestHash = GetNextRequestHash();
		}

        /// <summary>
        /// Authenticate the given PicturesService Request.
        /// </summary>
        public void Authenticate(PicturesService.AuthenticatedRequest request)
        {
            if (String.IsNullOrEmpty(Ticket))
                Authenticate();

            request.AuthenticationUsername = Username;
            request.AuthenticationRequestHash = GetNextRequestHash();
        }

		/// <summary>
		/// Returns a new RequestHash and updates the Request Sequence.
		/// </summary>
		public string GetNextRequestHash()
		{
			return (Ticket + RequestSequence++).HashSha1();
		}

		/// <summary>
		/// Clears the current Ticket.
		/// </summary>
		public void ClearTicket()
		{
			if (String.IsNullOrEmpty(Ticket))
				return;
			var request = new ClearTicketRequest();
			Authenticate(request);
			Service.ClearTicket(request);
			Ticket = null;
		}
	}
}