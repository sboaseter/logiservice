using System;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;

namespace LogiServiceClient
{
	///<summary>
	/// Some Extension methods.
	///</summary>
	public static class Extensions
	{
		/// <summary>
		/// Safely closes a service client connection.
		/// </summary>
		/// <param name="myServiceClient">The client connection to close.</param>
		public static void CloseConnection(this ICommunicationObject myServiceClient)
		{
			if (myServiceClient.State != CommunicationState.Opened)
			{
				return;
			}

			try
			{
				myServiceClient.Close();
			}
			catch (CommunicationException)
			{
				myServiceClient.Abort();
			}
			catch (TimeoutException)
			{
				myServiceClient.Abort();
			}
			catch (Exception)
			{
				myServiceClient.Abort();
				throw;
			}
		}

		///<summary>
		/// Produces the SHA1 Hash of a given string, using UTF8 and Base64 Encoding.
		///</summary>
		public static string HashSha1(this string input)
		{
			var inputBytes = Encoding.UTF8.GetBytes(input);
			var sha1 = new SHA1CryptoServiceProvider();
			var outputBytes = sha1.ComputeHash(inputBytes);
			var output = Convert.ToBase64String(outputBytes);
			return output;
		}
	}
}