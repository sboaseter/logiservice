﻿using System;
using LogiServiceClient.AuthenticationService;
using LogiServiceClient.OrganisationService;
using LogiServiceClient.PicturesService;

namespace LogiServiceClient
{
	///<summary>
	/// A Session used to communicate with the LogiCAB Services.
	/// Handles Authentication and disposal of resources.
	///</summary>
	public class Session : IDisposable
	{
		protected AuthenticationManager AuthenticationManager { get; set; }
		protected AuthenticationServiceContractClient AuthenticationServiceClient { get; set; }
		protected OrganisationServiceContractClient OrganisationServiceClient { get; set; }
        protected PicturesServiceContractClient PicturesServiceClient { get; set; }

		internal Session(
			string username,
			string password,
			AuthenticationServiceContractClient authenticationServiceClient,
			OrganisationServiceContractClient organisationServiceClient)
		{
			AuthenticationServiceClient = authenticationServiceClient;
			OrganisationServiceClient = organisationServiceClient;

			AuthenticationManager = new AuthenticationManager(
				authenticationServiceClient,
				username,
				password
			);
		}

        internal Session(
        string username,
        string password,
        AuthenticationServiceContractClient authenticationServiceClient,
        PicturesServiceContractClient picturesServiceClient)
        {
            AuthenticationServiceClient = authenticationServiceClient;
            PicturesServiceClient = picturesServiceClient;

            AuthenticationManager = new AuthenticationManager(
                authenticationServiceClient,
                username,
                password
            );
        }
        
        ///<summary>
		/// Gets a single Organisation based on the request values.
		///</summary>
		public OrganisationGetResponse OrganisationGet(OrganisationGetRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.OrganisationGet(request);
		}

		///<summary>
		/// Posts the given Organisation, updating the information store.
		///</summary>
		public OrganisationPostResponse OrganisationPost(OrganisationPostRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.OrganisationPost(request);
		}

        ///<summary>
        /// Gets a list of pictures to link based on the request values.
        ///</summary>
        public PicturesGetResponse PicturesGet(PicturesGetRequest request)
        {
            AuthenticationManager.Authenticate(request);
            return PicturesServiceClient.PicturesGet(request);
        }

        ///<summary>
        /// Posts the given Picture, updating the information store.
        ///</summary>
        public PicturesPostResponse PicturesPost(PicturesPostRequest request)
        {
            AuthenticationManager.Authenticate(request);
            return PicturesServiceClient.PicturesPost(request);
        }

		///<summary>
		/// Checks if the given Organisation has a link to the current shop,
		/// and creates it if not.
		///</summary>
		public ShopLinkResponse ShopLink(ShopLinkRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.ShopLink(request);
		}

		///<summary>
		/// Allows for the Adding or Removing of Shop Functions to/from an Organisation.
		///</summary>
		public FunctionResponse Function(FunctionRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.Function(request);
		}

		///<summary>
		/// Gets a single Login on its Sequence or Name.
		///</summary>
		public LoginGetResponse LoginGet(LoginGetRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.LoginGet(request);
		}

		///<summary>
		/// Lists the Logins of a given Person.
		///</summary>
		public LoginListResponse LoginList(LoginListRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.LoginList(request);
		}

		///<summary>
		/// Creates a new Login for a Person, or updates an existing one.
		/// Password is required for new Logins.
		/// If Password is given for existing ones, it is changed.
		/// If Password is null or empty for existing ones, it is not touched.
		///</summary>
		public LoginPostResponse LoginPost(LoginPostRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.LoginPost(request);
		}

		///<summary>
		/// Gets a single Person on its Sequence.
		///</summary>
		public PersonGetResponse PersonGet(PersonGetRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.PersonGet(request);
		}

		///<summary>
		/// Lists all Persons of an Organisation.
		///</summary>
		public PersonListResponse PersonList(PersonListRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.PersonList(request);
		}

		///<summary>
		/// Creates a new Person for an Organisation, or updates the
		/// information of an existing one.
		///</summary>
		public PersonPostResponse PersonPost(PersonPostRequest request)
		{
			AuthenticationManager.Authenticate(request);
			return OrganisationServiceClient.PersonPost(request);
		}

		public void Dispose()
		{
			AuthenticationManager.ClearTicket();
			AuthenticationServiceClient.CloseConnection();
			OrganisationServiceClient.CloseConnection();
            PicturesServiceClient.CloseConnection();
		}
	}
}