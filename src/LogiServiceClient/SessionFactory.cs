﻿using System;
using LogiServiceClient.AuthenticationService;
using LogiServiceClient.OrganisationService;

namespace LogiServiceClient
{
	///<summary>
	/// Handles the creation of Sessions.
	/// Make sure the Username and Password are set.
	///</summary>
	public class SessionFactory
	{
		///<summary>
		/// A valid Username for the LogiCAB Services.
		///</summary>
		public string Username { get; set; }

		///<summary>
		/// A valid Password for the LogiCAB Services.
		///</summary>
		public string Password { get; set; }

		///<summary>
		/// The name of the Endpoint for the AuthenticationService.
		/// Defaults to 'AuthenticationService'.
		///</summary>
		public string AuthenticationServiceEndpointName { get; set; }

		///<summary>
		/// The name of the Endpoint for the OrganisationService.
		/// Defaults to 'OrganisationService'.
		///</summary>
		public string OrganisationServiceEndpointName { get; set; }

		///<summary>
		/// Basic Constructor.
		///</summary>
		public SessionFactory()
		{
			AuthenticationServiceEndpointName = "AuthenticationService";
			OrganisationServiceEndpointName = "OrganisationService";
		}

		///<summary>
		/// Constructor with Username and Password.
		///</summary>
		public SessionFactory(string username, string password) : this()
		{
			Username = username;
			Password = password;
		}

		///<summary>
		/// Returns a Session to access the LogiCAB Services.
		///</summary>
		public Session GetSession()
		{
			return new Session(
				Username,
				Password,
				new AuthenticationServiceContractClient(AuthenticationServiceEndpointName),
				new OrganisationServiceContractClient(OrganisationServiceEndpointName)
			);
		}
	}
}