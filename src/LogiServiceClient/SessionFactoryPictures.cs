﻿using System;
using LogiServiceClient.AuthenticationService;
using LogiServiceClient.PicturesService;

namespace LogiServiceClient
{
    ///<summary>
    /// Handles the creation of Sessions.
    /// Make sure the Username and Password are set.
    ///</summary>
    public class SessionFactoryPictures
    {
        ///<summary>
        /// A valid Username for the LogiCAB Services.
        ///</summary>
        public string Username { get; set; }

        ///<summary>
        /// A valid Password for the LogiCAB Services.
        ///</summary>
        public string Password { get; set; }

        ///<summary>
        /// The name of the Endpoint for the AuthenticationService.
        /// Defaults to 'AuthenticationService'.
        ///</summary>
        public string AuthenticationServiceEndpointName { get; set; }

        ///<summary>
        /// The name of the Endpoint for the PicturesService.
        /// Defaults to 'PicturesService'.
        ///</summary>
        public string PicturesServiceEndpointName { get; set; }

        ///<summary>
        /// Basic Constructor.
        ///</summary>
        public SessionFactoryPictures()
        {
            AuthenticationServiceEndpointName = "AuthenticationService";
            PicturesServiceEndpointName = "PicturesService";
        }

        ///<summary>
        /// Constructor with Username and Password.
        ///</summary>
        public SessionFactoryPictures(string username, string password)
            : this()
        {
            Username = username;
            Password = password;
        }

        public SessionFactoryPictures(string username, string password, string authenticationService, string picturesService)
        {
            Username = username;
            Password = password;
            AuthenticationServiceEndpointName = authenticationService;
            PicturesServiceEndpointName = picturesService;
        }

        ///<summary>
        /// Returns a Session to access the LogiCAB Services.
        ///</summary>
        public Session GetSession()
        {
            return new Session(
                Username,
                Password,
                new AuthenticationServiceContractClient(AuthenticationServiceEndpointName),
                new PicturesServiceContractClient(PicturesServiceEndpointName)
            );
        }
    }
}